import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import groovy.lang.MissingPropertyException
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('/var/folders/_y/7sg47l1d5cvdsbwrt1hgp7zr0000gp/T/Katalon/Test Cases/TestCasesList/Vendor/TC_V029_Follow Up Activity can be eidited for Vendor successfully/20190522_115625/execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runTestCase('Test Cases/TestCasesList/Vendor/TC_V029_Follow Up Activity can be eidited for Vendor successfully', new TestCaseBinding('Test Cases/TestCasesList/Vendor/TC_V029_Follow Up Activity can be eidited for Vendor successfully',[:]), FailureHandling.STOP_ON_FAILURE , false)
    
