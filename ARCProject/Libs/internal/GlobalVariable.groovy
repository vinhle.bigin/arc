package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.main.TestCaseMain


/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object glb_SiteName
     
    /**
     * <p></p>
     */
    public static Object glb_URL
     
    /**
     * <p></p>
     */
    public static Object glb_username
     
    /**
     * <p></p>
     */
    public static Object glb_password
     
    /**
     * <p></p>
     */
    public static Object glb_adminuser
     
    /**
     * <p></p>
     */
    public static Object glb_adminpassword
     
    /**
     * <p></p>
     */
    public static Object glb_adminReferralUrl
     
    /**
     * <p></p>
     */
    public static Object glb_usermail
     
    /**
     * <p></p>
     */
    public static Object glb_gmailPass
     
    /**
     * <p></p>
     */
    public static Object glb_sshhostname
     
    /**
     * <p></p>
     */
    public static Object glb_sshlogin
     
    /**
     * <p></p>
     */
    public static Object glb_sshpassword
     
    /**
     * <p></p>
     */
    public static Object glb_DbBHost
     
    /**
     * <p></p>
     */
    public static Object glb_DbUser
     
    /**
     * <p></p>
     */
    public static Object glb_DbPassword
     
    /**
     * <p></p>
     */
    public static Object glb_DbPort
     
    /**
     * <p></p>
     */
    public static Object glb_TCStatus
     
    /**
     * <p></p>
     */
    public static Object glb_TCFailedMessage
     
    /**
     * <p></p>
     */
    public static Object glb_Serverhomefolder
     
    /**
     * <p></p>
     */
    public static Object root_user
     
    /**
     * <p></p>
     */
    public static Object root_pass
     
    /**
     * <p></p>
     */
    public static Object glb_dbName
     

    static {
        try {
            def selectedVariables = TestCaseMain.getGlobalVariables("default")
			selectedVariables += TestCaseMain.getGlobalVariables(RunConfiguration.getExecutionProfile())
            selectedVariables += RunConfiguration.getOverridingParameters()
    
            glb_SiteName = selectedVariables['glb_SiteName']
            glb_URL = selectedVariables['glb_URL']
            glb_username = selectedVariables['glb_username']
            glb_password = selectedVariables['glb_password']
            glb_adminuser = selectedVariables['glb_adminuser']
            glb_adminpassword = selectedVariables['glb_adminpassword']
            glb_adminReferralUrl = selectedVariables['glb_adminReferralUrl']
            glb_usermail = selectedVariables['glb_usermail']
            glb_gmailPass = selectedVariables['glb_gmailPass']
            glb_sshhostname = selectedVariables['glb_sshhostname']
            glb_sshlogin = selectedVariables['glb_sshlogin']
            glb_sshpassword = selectedVariables['glb_sshpassword']
            glb_DbBHost = selectedVariables['glb_DbBHost']
            glb_DbUser = selectedVariables['glb_DbUser']
            glb_DbPassword = selectedVariables['glb_DbPassword']
            glb_DbPort = selectedVariables['glb_DbPort']
            glb_TCStatus = selectedVariables['glb_TCStatus']
            glb_TCFailedMessage = selectedVariables['glb_TCFailedMessage']
            glb_Serverhomefolder = selectedVariables['glb_Serverhomefolder']
            root_user = selectedVariables['root_user']
            root_pass = selectedVariables['root_pass']
            glb_dbName = selectedVariables['glb_dbName']
            
        } catch (Exception e) {
            TestCaseMain.logGlobalVariableError(e)
        }
    }
}
