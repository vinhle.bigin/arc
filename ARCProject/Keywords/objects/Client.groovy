package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.Navigation
import internal.GlobalVariable
import pages_elements.Client_AddressTab
import pages_elements.Client_GenInfoPage
import pages_elements.ClientsPage
import pages_elements.LandingPage
import pages_elements.Messages

public class Client extends User {

	public static String Fullname
	public static String Status
	public static String CompanyName
	private List<PhoneInfo> PhoneList
	//private List<Mailinfo> MailList
	public List<AddressInfo> AddrInfo
	public List<ContactInfo> Contacts
	public String Description
	public Client() {
		super()
		Fullname = this.firstname+ " " + this.lastname
		Status = "Active"
		CompanyName = "Client_"+this.firstname

		PhoneList = new ArrayList()
		PhoneList.add(new PhoneInfo())

		Contacts = new ArrayList()
		//Contacts.add(new ContactInfo())

		AddrInfo = new ArrayList()
		AddrInfo.add(new AddressInfo())
		Description = "this is for testing."

	}

	//METHODS

	public void ReGenerateInfo()
	{
		User u_tmp = new User()
		CompanyName = "Client_"+u_tmp.firstname
		Status = "Inactive"
	}

	public void CreateClient_func(String submit = "submit")
	{

		if(ClientsPage.Comp_txt()==null)
			Common.HandleElementClick(ClientsPage.NewClient_btn())

		WebUI.delay(2)

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
			
		Common.WaitForElementDisplay(ClientsPage.State_ddl())

		ClientsPage.Comp_txt().sendKeys(this.CompanyName)

		Common.sendkeyTextToElement(ClientsPage.Descr_txt(), this.Description)

		Common.SelectDropdownItem(ClientsPage.State_ddl(), this.AddrInfo.get(0).State)

		Common.sendkeyTextToElement(ClientsPage.City_txt(), this.AddrInfo.get(0).City)

		if(submit == "submit")
		{
			//Common.HandleElementClick(null)

			ClientsPage.Submit_btn().click()

			//	Message.VerifyNotifyNotDisplay()
			Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When Create New Client")
		}

	}//end void

	public void EditInfo_func(String submit = "submit")
	{
		Client_GenInfoPage page = new Client_GenInfoPage()
		page.Prof_Edit_btn().click()
		WebUI.delay(1)

		Common.sendkeyTextToElement(page.CompName_txt(), this.CompanyName)
		
		if(submit == "submit")
		{
			page.Prof_Submit_btn().click()
			WebUI.delay(4)

			String msg = Messages.notification_msg().getText()

			String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
			println(msg)
			if(msg ==expected_msg)
			{
				GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Client-Profile Edit.Msg: '"+msg+"'.\n"
				Assert.fail(GlobalVariable.glb_TCFailedMessage)
			}
		}

		//STATUS

		Common.HandleElementClick(page.Stt_Edit_btn())
		
		Common.SelectDropdownItem(page.Status_ddl(), this.Status)

		
		if(submit == "submit" ||submit == "Submit" )
		{
			Common.HandleElementClick(page.Stt_Submit_btn())
			WebUI.delay(4)

			String msg = Messages.notification_msg().getText()

			String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
			println(msg)
			if(msg ==expected_msg)
			{
				GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Client-Status Edit.Msg: '"+msg+"'.\n"
				Assert.fail(GlobalVariable.glb_TCFailedMessage)
			}
		}


	}//end void



}
