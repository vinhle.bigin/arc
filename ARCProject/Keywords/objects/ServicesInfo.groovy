package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import internal.GlobalVariable
import pages_elements.LandingPage
import pages_elements.Transf_DomesticTab
import pages_elements.Transf_ServiceTab

public class ServicesInfo {

	public String Type
	public Boolean Status
	public List<ContactInfo> VendorCtactList




	public ServicesInfo() {
		Type = ""
		Status = false
		VendorCtactList = new ArrayList<>()
	}


	public void Edit_TranfService_func() {
	}

	public void Edit_Domestic_func() {
	}



	//Method - ADD VEndor Contact to List
	public void AddVendorContact_func(ContactInfo contact_tmp)
	{
		VendorCtactList.add(contact_tmp)
	}


}

//===============HOUSE HOLD GOOD
public class HHGService extends ServicesInfo {
	public String OriAddr
	public String DesAddr
	public String HHGPaymentType
	public String HHGMoveDate
	public String HHGDeliverDate
	public Boolean IsInvoceCreate
	public Boolean IsInvoiceClose
	public Boolean IsEscrowFeeReceive

	public HHGService() {
		super()
		Type = "Household goods"
		Status = true
		OriAddr = "AL, US"
		DesAddr = "UT, US"
		HHGPaymentType = "Credit"
		HHGMoveDate = "04/04/2019"
		HHGDeliverDate = "04/04/2019"
		IsInvoceCreate = true
		IsInvoiceClose = true
		IsEscrowFeeReceive = true

	}

	//@Override
	public void Edit_TranfService_func()
	{
		Transf_ServiceTab tab = new Transf_ServiceTab()

		if(tab.HHG_opt().isSelected()!=this.Status)
			tab.HHG_opt().click()
	}//end void

	//@Override
	public void Edit_Domestic_func(String button)
	{

		Common.HandleElementClick(Transf_DomesticTab.Domestic_tab())

		Common.SelectDropdownItem(Transf_DomesticTab.Menu_List(), this.Type)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(Transf_DomesticTab.Edit_btn())

		Transf_DomesticTab.FillHHGInfo_func(this,button)
	}
}//end class


//===============HOUSE PURCHASE
public class HPService extends ServicesInfo {
	public String SalePrice
	public String LoanAmount
	public String CommissionPercent
	public String CloseDate
	public String HouseHuntTripDate
	public String RebateAmount
	public String EscrowFeeAmount
	public String EscrowFeePercent
	public Boolean IsInvoiceCreate
	public Boolean IsInvoiceClose
	public Boolean IsEscrowFeeReceive
	public Boolean IsSignRefFeeAgree
	public Boolean IsTransfRent
	public Boolean IsRebateSent



	public HPService() {
		super()
		Type = "Home purchase"
		Status = true
		SalePrice = "\$20.00"
		LoanAmount = "\$1,000.00"
		CommissionPercent = "50.00%"

		CloseDate = "04/04/2019"
		HouseHuntTripDate = "04/04/2019"
		RebateAmount = "\$500"
		EscrowFeeAmount = "\$2,000.00"
		EscrowFeePercent = "20.00%"
		IsInvoiceCreate = true
		IsInvoiceClose = true
		IsEscrowFeeReceive = true
		IsSignRefFeeAgree = true
		IsTransfRent = true
		IsRebateSent= true

	}

	//	@Override
	public void Edit_TranfService_func()
	{
		Transf_ServiceTab tab = new Transf_ServiceTab()

		if(tab.HP_opt().isSelected()!=Status.toString())
			tab.HP_opt().click()
	}//end void

	//@Override
	public void Edit_Domestic_func(String button = "Submit")
	{
		Common.HandleElementClick(Transf_DomesticTab.Domestic_tab())

		Common.SelectDropdownItem(Transf_DomesticTab.Menu_List(), this.Type)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(Transf_DomesticTab.Edit_btn())

		Transf_DomesticTab.FillHPInfo_func(this,button)
	}//end void
}//end class


//===============HOME SALE
public class HSService extends ServicesInfo {
	public String SalePrice

	public String CommissionPercent
	public String CloseDate
	public String RebateAmount
	public String ListPrice
	public String ListStartDate
	public String ListEndDate
	public String BuyOutAcceptDate
	public String EscrowFeeAmount
	public String EscrowFeePercent

	public Boolean IsInvoiceCreate
	public Boolean IsInvoiceClose
	public Boolean IsEscrowFeeReceive
	public Boolean IsSignRefFeeAgree
	public Boolean IsRebaseSent
	public Boolean IsTookBuyOut



	public HSService() {
		super()
		Type = "Home sale"
		Status = true
		SalePrice = "\$20.00"

		CommissionPercent = "50.00%"

		CloseDate = "04/04/2019"

		RebateAmount = "\$500.00"
		ListPrice = "\$200.00"
		ListStartDate = "04/05/2019"
		ListEndDate = "04/05/2019"
		BuyOutAcceptDate = "04/06/2019"
		EscrowFeeAmount = "\$2,000.00"
		EscrowFeePercent = "20.00%"

		IsInvoiceCreate = true
		IsInvoiceClose = true
		IsEscrowFeeReceive = true
		IsSignRefFeeAgree = true
		IsRebaseSent = true
		IsTookBuyOut = true


	}
	//	@Override
	public void Edit_TranfService_func()
	{
		Transf_ServiceTab tab = new Transf_ServiceTab()

		if(tab.HS_opt().isSelected()!=Status.toString())
			tab.HS_opt().click()
	}//end void

	//@Override
	public void Edit_Domestic_func(String button = "Submit")
	{
		Common.HandleElementClick(Transf_DomesticTab.Domestic_tab())

		Common.SelectDropdownItem(Transf_DomesticTab.Menu_List(), this.Type)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(Transf_DomesticTab.Edit_btn())

		Transf_DomesticTab.FillHSInfo_func(this,button)
	}//end void
}//end class



//===============TQ SERVICE
public class TQService extends ServicesInfo {
	public String ServiceDesr
	public String Currency
	public String AccountPurpose
	public String NotiDate
	public String ServiceDate
	public String MoveInDate
	public String MoveOutDate
	public String DayAuth
	public String ApartmentType
	public String ApartmentFinish
	public String Baths
	public String Budget
	public String Pets
	public String SpecialInstruct
	public String WhoPay
	public String PaymentType
	public Boolean IsInvoiceCreate
	public Boolean IsInvoiceClose
	public Boolean IsEscrowFeeReceive
	public Boolean IsBillFirstDay

	public TQService() {
		super()
		Type = "Temporary quarter"
		Status = true
		ServiceDesr = "HHG Service Descr Testing"
		Currency = "VND - Viet Nam"
		AccountPurpose = "Reallocation"
		NotiDate = "04/04/2019"
		ServiceDate = "04/04/2019"
		MoveInDate = "04/04/2019"
		MoveOutDate = "05/04/2019"
		DayAuth = "2"
		ApartmentType = "Studio"
		ApartmentFinish = "Unfurnished"
		Baths = "2"
		Budget = "\$20,000.00"
		Pets = "1"
		SpecialInstruct = "Go straight on testing"
		WhoPay = "Assignee"
		PaymentType = "Credit"
		IsInvoiceCreate = true
		IsInvoiceClose = true
		IsEscrowFeeReceive = true
		IsBillFirstDay = true
	}

	//	@Override
	public void Edit_TranfService_func()
	{
		Transf_ServiceTab tab = new Transf_ServiceTab()

		if(tab.TQ_opt().isSelected()!=Status.toString())
			tab.TQ_opt().click()
	}//end void

	//@Override
	public void Edit_Domestic_func(String button = "Submit")
	{
		Common.HandleElementClick(Transf_DomesticTab.Domestic_tab())

		Common.SelectDropdownItem(Transf_DomesticTab.Menu_List(), this.Type)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(Transf_DomesticTab.Edit_btn())

		Transf_DomesticTab.FillTQInfo_func(this,button)
	}//end void
}//end class



//===========MORTGAGE
public class MortgService extends ServicesInfo {

	public String MortgageAmount
	public String IndexRate
	public String AmountAtClose
	public String DownPayment
	public String CloseDate
	public String LockInDate

	public MortgService() {
		super()
		Type = "Mortgage"
		Status = true
		MortgageAmount = "\$1,000.00"
		IndexRate = "50"
		AmountAtClose = "\$200.00"
		DownPayment = "\$100.00"
		CloseDate = "04/04/2019"
		LockInDate = "04/05/2019"


	}

	//	@Override
	public void Edit_TranfService_func()
	{
		Transf_ServiceTab tab = new Transf_ServiceTab()

		if(tab.Morgt_opt().isSelected()!=Status.toString())
			tab.Morgt_opt().click()
	}//end void

	//@Override
	public void Edit_Domestic_func(String button = "Submit")
	{
		Common.HandleElementClick(Transf_DomesticTab.Domestic_tab())

		Common.SelectDropdownItem(Transf_DomesticTab.Menu_List(), this.Type)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(Transf_DomesticTab.Edit_btn())

		Transf_DomesticTab.FillMortgageInfo_func(this,button)
	}//end void
}//end class
