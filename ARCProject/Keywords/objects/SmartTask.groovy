package objects

import org.junit.Assert
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import functions.Navigation
import internal.GlobalVariable
import pages_elements.ActivityTab
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.SmartTaskPage
import pages_elements.VendorDetail

public class SmartTask {
	public String Name
	public String Service
	public Boolean IsFieldchange_opt
	public Boolean IsEmailProcess_opt
	public Boolean IsActivityComplete_opt
	public Boolean IsFileCreate_opt
	public String TriggerType
	public String Condt_FieldName
	public String Condt_Operator
	public String Condt_Value
	public Action action
	public String Condt_EMailTemplName
	public String Condt_ActivityName

	public SmartTask() {

				
		Name = "SmartTask_"+DateTimeFuncs.getCurrentLocalTime()
		Service = "General Service"
		IsFieldchange_opt = false
		IsEmailProcess_opt = false
		IsActivityComplete_opt = false
		IsFileCreate_opt = false
		TriggerType = "" //Meet ANY of the following conditions
		Condt_FieldName = ""
		Condt_Operator = ""
		action = new Action()
		Condt_EMailTemplName = ""
		Condt_ActivityName = ""
	}

	public void SetFieldChangeCondt_func(String triggertype = "",String fieldname = "",String operator = "",String value_str="") {
		IsFieldchange_opt = true
		if(triggertype=="")
			TriggerType = "Meet ALL of the following conditions" //Meet ANY of the following conditions
		else
			TriggerType = triggertype //Meet ANY of the following conditions

		if(fieldname=="")
			Condt_FieldName = "Transferee - First Name"
		else
			Condt_FieldName = fieldname


		if(operator=="")
			Condt_Operator = "Has any change"
		else
			Condt_Operator = operator

		Condt_Value = value_str


	}

	public void SetEmailProcessCondt_func(String templatename) {
		IsEmailProcess_opt = true
		Condt_EMailTemplName = templatename
	}

	public void SetActivityCompleteCondt_func(String activityname) {
		IsActivityComplete_opt = true
		Condt_ActivityName = activityname
	}

	public void SetFileCreateCondt_func() {
		IsFileCreate_opt = true
	}
	
	
	
	public void Create_func(String button)
	{
		Navigation.GotoSmartTaskPage()
		
		SmartTaskPage.New_btn().click()
		
		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
		
		Common.WaitForElementDisplay(SmartTaskPage.Dtail_Name_txt())
		
		SmartTaskPage.FillDetailsInfo_func(this, button)
		
	}
}


//===CLASS ACTION

public class Action{
	public String Type
	public Activity Act_Activity
	public String Act_TemplateName
	public String Act_Field
	public String Act_ValueTo



	public Action()
	{
		Type = "" //Updating Field Action, Creating Activity Action
		Act_TemplateName = ""
		Act_Field = ""
		Act_ValueTo = ""
		Act_Activity = new Activity()

	}

	public void InitSendEmailAction_func(String templatename)
	{
		Type = "Sending Email Action"
		Act_TemplateName = templatename
	}

	
	
	public void InitUpdateFieldAction_func(String fieldname = "", String newvalue)
	{
		Type = "Updating Field Action"
		Act_Field = fieldname
		Act_ValueTo = newvalue
		
		if(fieldname=="")
			Act_Field = "Transferee - First Name"
		else
			Act_Field = fieldname
	
	
		Act_ValueTo = newvalue
	}

	
	
	public void InitCreateActivityAction_func(Activity activity_tmp)
	{
		Type = "Creating Activity Action"
		Act_Activity = activity_tmp

	}



}//
