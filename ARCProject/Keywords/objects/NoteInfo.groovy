package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import functions.Navigation
import internal.GlobalVariable
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.NoteTab

public class NoteInfo {

	private String Service

	private String Title

	private String Content

	private String Contact

	private String TransfFullName

	public NoteInfo() {

		Service = "General"

		Random r = new Random();
		int ret2 = r.nextInt(100)+1;
		Title = "Note-Title for Testing"+DateTimeFuncs.getCurrentLocalTime()

		Content = "Note-Content for Testing"+DateTimeFuncs.getCurrentLocalTime()
		Contact = ""
		TransfFullName=""
	}


	public void CreateNoteTransf_func() {


		//Go to Note Tab

		Navigation.GotoNoteTab_func()

		Common.WaitForElementDisplay(NoteTab.CreateNew_btn())

		//Click on New button
		Common.HandleElementClick(NoteTab.CreateNew_btn())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Fill info into modal
		NoteTab.FillInfoModalTransf_func(this)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Create Note.")

	}//end void


	public void CreateNoteClient_func() {


		//Go to Note Tab

		Navigation.GotoNoteTab_func()

		Common.WaitForElementDisplay(NoteTab.CreateNew_btn())

		//Click on New button
		Common.HandleElementClick(NoteTab.CreateNew_btn())

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Fill info into modal
		NoteTab.FillInfoModalClient_func(this)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Create Note.")

	}//end void



	public void CreateNoteVendor_func(){

		//Go to Note Tab

		Navigation.GotoNoteTab_func()

		//Common.WaitForElementDisplay(NoteTab.CreateNew_btn())

		//Click on New button
		Common.HandleElementClick(NoteTab.CreateNew_btn())

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Fill info into modal
		NoteTab.FillInfoModalVendor_func(this)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Create Note.")

		WebUI.delay(5)

	}//end void

	public void UpdateNoteTransf_func(String oldtitle,String button= "Submit")
	{
	
		Navigation.GotoNoteTab_func()

		println(oldtitle)

		NoteTab.SearchNote_func(oldtitle)


		//Click on hyperlink
		Common.HandleElementClick(NoteTab.noteTitle_lnk(oldtitle))
		
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Click on Edit on modal
		Common.WaitForElementDisplay(NoteTab.Edit_btn())

		Common.HandleElementClick(NoteTab.Edit_btn())

		NoteTab.FillInfoModalTransf_func(this,button)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Transf Note.")

		
	}
	
	
	public void UpdateNoteClient_func(String oldtitle,String button ="Submit")
	{
		Navigation.GotoNoteTab_func()
		
		println(oldtitle)

		NoteTab.SearchNote_func(oldtitle)


		//Click on hyperlink
		Common.HandleElementClick(NoteTab.noteTitle_lnk(oldtitle))
		
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Click on Edit on modal
		Common.WaitForElementDisplay(NoteTab.Edit_btn())

		Common.HandleElementClick(NoteTab.Edit_btn())

		NoteTab.FillInfoModalClient_func(this, button)
		//FillInfoModalClientfunc(this)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Client ote.")
	}

	public void UpdateNoteVendor_func(String oldtitle, NoteInfo newnote)
	{

		println(oldtitle)

		//Click on hyperlink
		Common.HandleElementClick(NoteTab.noteTitle_lnk(oldtitle))

		WebUI.delay(3)

		Common.HandleElementClick(NoteTab.Edit_btn())

		WebUI.delay(3)

		NoteTab.FillInfoModalVendor_func(newnote)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Note.")

	}

	public void Delete_func()
	{
		Navigation.GotoNoteTab_func()

		NoteTab.SearchNote_func(this.Title)

		WebUI.delay(2)

		NoteTab.Remove_icon(this.Title).click()

		WebUI.delay(5)

		Messages.Delete_btn().click()

		WebUI.delay(3)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Delete Note.")
	}

	public void DeleteNoteVendor_func()
	{
		Navigation.GotoNoteTab_func()

		NoteTab.SearchNote_func(this.Title)

		WebUI.delay(2)

		NoteTab.Remove_icon(this.Title).click()

		WebUI.delay(5)

		Messages.DeleteItem_btn().click()

		WebUI.delay(3)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Delete Note.")
	}

	public void clearAllDataInSearchbox(){
		NoteTab tab = new NoteTab()
		WebUI.delay(2)
		tab.Note_tab().click()
		String FileEnterDate = DateTimeFuncs.GetCurrentLocalDate_Str()

		Common.ActionSendkey(tab.SearchFromDate_txt(), FileEnterDate)
		WebUI.delay(1)
		tab.SearchFromDate_txt().sendKeys(Keys.TAB)

		Common.ActionSendkey(tab.SearchToDate_txt(), FileEnterDate)
		WebUI.delay(1)
		tab.SearchFromDate_txt().sendKeys(Keys.TAB)

		Common.sendkeyTextToElement(tab.SearchSubject_txt(), Title)
		Common.sendkeyTextToElement(tab.SearchContent_txt() , Content)
		Common.HandleElementClick(tab.Search_btn())
		WebUI.delay(3)
		Common.VerifyFieldTextEqual_func("No Results", Messages.NoResultMsg_lb(), Messages.noResutl_msg)
		Common.HandleElementClick(tab.Clear_btn())
	}

	public void verifyEmptyDatainSearchbox(){
		Common.VerifyFieldTextEqual_func("Search From Date", NoteTab.SearchFromDate_txt() , "")
		Common.VerifyFieldTextEqual_func("Search To Date", NoteTab.SearchToDate_txt() , "")
		Common.VerifyFieldTextEqual_func("Search Subject", NoteTab.SearchSubject_txt() , "")
		Common.VerifyFieldTextEqual_func("Search Content", NoteTab.SearchContent_txt() , "")
	}
}
