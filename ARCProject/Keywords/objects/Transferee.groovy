package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.concurrent.TimeUnit

import org.junit.Assert
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import pages_elements.HeadingMenu
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.NewTransfModal
import pages_elements.TransfGenInfoPage
import pages_elements.Transf_AddressTab
import pages_elements.Transf_DependTab
import pages_elements.Transf_OtherInfoTab
import pages_elements.Transf_ServiceTab
import pages_elements.TransfereePage

public class Transferee extends User{
	//General Information
	public String Salutation
	public String FullName
	public String TransfereeID
	public String Pronunciation
	public String Nickname
	public String Jobtitle
	public String Coordinator
	public String CoordinatorAssistant
	public String TeamLeader
	public String UrgentInstructions
	public String AddintionNote
	public Boolean IsExecutiveOfficer
	public List<PhoneInfo> PhoneList
	public List<Mailinfo> MailList
	public String ClientName
	public String ClientContactFullName

	//STATUS
	public String Status
	public String StatusChangeDate
	public Boolean IsOverRide
	public Boolean IsQualified
	public String FileEnterDate
	public Boolean IsReceivedSignedABAD
	public Boolean ReferedListSide
	public Boolean ReferedBuySide
	public Boolean ReferedToTitleComp

	//PARTNER INFO
	public String PartnerFname
	public String PartnerLname
	public String PartnerMid
	public String PartnerSSN

	//OTHERS' INFO
	public OtherInfo Other_info
	public AddressInfo OriResAddr_info
	public AddressInfo DesResAddr_info
	public AddressInfo OriOffAddr_info
	public AddressInfo DesOffAddr_info
	public List<DependentInfo> Dependent_list
	public BankInfo Bank_info
	public List<ServicesInfo> ServiceList
	public	HHGService HHG_service
	public	HPService HP_service
	public	HSService HS_service
	public TQService TQ_service
	public	MortgService MG_service


	public Transferee()
	{

		super()
		this.firstname ="Transf_" + this.firstname
		FullName = this.firstname + " " + this.lastname
		Salutation = "Mr."
		TransfereeID = ""
		Pronunciation = ""
		Nickname =""
		Jobtitle = ""
		Coordinator = "Daniel Yang"
		CoordinatorAssistant = ""
		TeamLeader = ""
		UrgentInstructions = ""
		AddintionNote = ""
		IsExecutiveOfficer = ""
		PhoneList = new ArrayList()
		PhoneList.add(new PhoneInfo())

		MailList = new ArrayList()
		Mailinfo newmail = new Mailinfo()

		newmail.From = this.Coordinator
		MailList.add(newmail)

		ClientName = "Alltel"
		ClientContactFullName = ""
		
		//STATUS
		Status = "Queued"
		StatusChangeDate
		IsOverRide = false
		IsQualified = false
		FileEnterDate = DateTimeFuncs.GetCurrentLocalDate_Str()
		IsReceivedSignedABAD = false
		ReferedListSide = false
		ReferedBuySide = false
		ReferedToTitleComp = false

		//PARTNER INFO
		PartnerFname = ""
		PartnerLname = ""
		PartnerMid = ""
		PartnerSSN = ""

		//OTHERS' INFO
		Other_info = new OtherInfo()
		OriResAddr_info = new OriResAddrInfo()
		DesResAddr_info = new DesResAddrInfo()
		OriOffAddr_info = new OriOffAddrInfo()
		DesOffAddr_info = new DesOffAddrInfo()
		Dependent_list = new ArrayList()
		Dependent_list.add(new DependentInfo())
		Bank_info = new BankInfo()

		HHG_service = new HHGService()
		HP_service = new HPService()
		HS_service = new HSService()
		TQ_service = new TQService()
		MG_service = new MortgService()

		ServiceList = new ArrayList()
		ServiceList.add(HHG_service)
		ServiceList.add(HP_service)

		ServiceList.add(HS_service)
		ServiceList.add(TQ_service)
		ServiceList.add(MG_service)

	}

	//METHOD

	public void ReGenenrateInfo()
	{
		this.firstname ="Updated" + this.firstname
		this.lastname = this.lastname + ""
		FullName = this.firstname + " " + this.lastname
		Salutation = "Dr."
		Pronunciation = "Director"
		Nickname ="tester"
		Jobtitle = "IT leader"
		Coordinator = "David Gibbons"
		CoordinatorAssistant = "Daniel Yang"
		TeamLeader = "David Gibbons"
		UrgentInstructions = "this is instruct testing"
		AddintionNote = "this is addnote testing"
		IsExecutiveOfficer = true
		PhoneList = new ArrayList()
		PhoneList.add(new PhoneInfo())

		MailList = new ArrayList()
		MailList.add(new Mailinfo())

		//STATUS
		Status = "Active"
		StatusChangeDate
		IsOverRide = true
		IsQualified = true
		IsReceivedSignedABAD = true
		ReferedListSide = ""
		ReferedBuySide = ""
		ReferedToTitleComp = ""

		//PARTNER INFO
		PartnerFname = "Partner_UFname"
		PartnerLname = "Partner_ULname"
		PartnerMid = ""
		PartnerSSN = "123"

		//OTHERS' INFO
		Other_info = new OtherInfo()
		OriResAddr_info = new OriResAddrInfo()
		DesResAddr_info = new DesResAddrInfo()
		OriOffAddr_info = new OriOffAddrInfo()
		DesOffAddr_info = new DesOffAddrInfo()

		Bank_info = new BankInfo()
		ServiceList.add(new TQService())

	}
	public void Create_func(Boolean submit = true)
	{
		Common.WaitForElementDisplay(TransfereePage.NewTransf_btn())

		Common.HandleElementClick(TransfereePage.NewTransf_btn())

		NewTransfModal.FillInfoModal_func(this,submit)
		
		Common.WaitForElementDisplay(TransfGenInfoPage.Generall_tab())


	}


	//EDIT GENERAL INFO
	public void EditGenInfo_func(){

		//TRANSF INFO

		TransfGenInfoPage page = new TransfGenInfoPage()

		//Common.HandleElementClick(page.Generall_tab())

		Common.HandleElementClick(page.TransInfoEdit_btn())


		Common.ScrollElementtoViewPort_func(page.Salulation_slect())

		
		Common.HandleElementClick(page.Salulation_slect())
		//WebUI.delay(2)
		Common.SelectDropdownItem(page.Salulation_slect(), this.Salutation)

		Common.sendkeyTextToElement(page.Fname_txt(), this.firstname)

		Common.sendkeyTextToElement(page.Lname_txt(), this.lastname)
		
		Common.SelectDropdownItem(page.Trf_CoordAss_ddl(), this.CoordinatorAssistant)
		
		if(page.Trf_IsExecOff_opt().getText()!=this.IsExecutiveOfficer.toString())
			page.Trf_IsExecOff_opt().click()
			
		Common.sendkeyTextToElement(page.Trf_Pronu_txt(), this.Pronunciation)

		Common.SelectDropdownItem(page.Trf_TeamLead_ddl(), this.TeamLeader)
	
		Common.sendkeyTextToElement(page.Trf_UrgentInstr_txt(), this.UrgentInstructions)
		
	
		
		page.TransInfoUpdate_btn().click()

		WebUI.delay(5)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull TRANSF-Info Update.Msg: '"+msg+"'.\n"
			//Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}


		//CLIENT INFO
		WebUI.delay(5)
		Common.HandleElementClick(page.ClientEdit_btn())


		//page.ClientContact_ddl().selectByVisibleText(this.Client_info.Contacts.get(0).FullName)

		Common.HandleElementClick(page.ClientUpdate_btn())

		WebUI.delay(5)

		msg = ""
		msg = Messages.notification_msg().getText()

		expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull STATUS - Info update.Msg: '"+msg+"'.\n"

			//	Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}


		//STATUS INFO
		WebUI.delay(5)

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(page.StatusEdit_btn())


		Common.HandleElementClick(page.StatusEdit_btn())

		String current_stt = Common.GetFieldText_func(page.Status_ddl())

		if(this.Status!=current_stt)
		{
			Common.ScrollElementtoViewPort_func(page.Status_ddl())

			Common.SelectDropdownItem(page.Status_ddl(), this.Status)
		}
		Common.HandleElementClick(page.Stt_Update_btn())

		WebUI.delay(2)

		msg = Messages.notification_msg().getText()

		expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull STATUS - Info update.Msg: '"+msg+"'.\n"
			//	Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

		//MAIL INFO
		
		//Common.SelectDropdownItem(page.PhoneType_ddl(0), this.PhoneList.get(0).Type)
		
		//Common.sendkeyTextToElement(page.Phone_txt(0), this.PhoneList.get(0).Numb)
		
		//Common.SelectDropdownItem(page.EmailType_ddl(0), this.MailList.get(0).Type)
		
		//Common.sendkeyTextToElement(page.EmailAddr_txt(0), this.MailList.get(0).EmailAddr)

	}//end void


	//EDIT SERVICE TAB
	public void EditService_func()
	{
		Transf_ServiceTab tab = new Transf_ServiceTab()

		Common.HandleElementClick(tab.Srv_tab())

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(tab.Edit_btn())

		Common.HandleElementClick(tab.Edit_btn())
		Common.ScrollElementtoViewPort_func(tab.HS_opt())
		int count = this.ServiceList.size()
		for(int i =0;i<count;i++)
		{
			this.ServiceList.get(i).Edit_TranfService_func()
		}//end for

		tab.Update_btn().click()

		WebUI.delay(2)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Service - Info update.Msg: '"+msg+"'.\n"
			//Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

	}//end void


	//EDIT ADDRESS TAB
	public void EditAddressInfo_func()
	{
		Transf_AddressTab tab = new Transf_AddressTab()

		Common.HandleElementClick(tab.Address_Tab())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		OriResAddr_info.Edit_TransfAddrInfo_func()

		DesResAddr_info.Edit_TransfAddrInfo_func()

		OriOffAddr_info.Edit_TransfAddrInfo_func()

		DesOffAddr_info.Edit_TransfAddrInfo_func()
	}



	//EDIT DEPENDET TAB
	public void CreateDepedent_func()
	{
		Transf_DependTab tab = new Transf_DependTab()

		Common.WaitForElementDisplay(tab.Dependent_tab())

		Common.HandleElementClick(tab.Dependent_tab())

		int count = this.Dependent_list.size()
		for(int i =0;i<count;i++)
		{
			this.Dependent_list.get(i).Edit_TransfDependent_func("")
		}//end for



	}

	public void UpdateDependent_func(String old_dependFname, DependentInfo new_dependent)
	{
		Transf_DependTab tab = new Transf_DependTab()

		Common.WaitForElementDisplay(tab.Dependent_tab())

		Common.HandleElementClick(tab.Dependent_tab())
		
		Common.WaitForElementDisplay(tab.Dependent_List())
		
		new_dependent.Edit_TransfDependent_func(old_dependFname,"Update")

	}



}
