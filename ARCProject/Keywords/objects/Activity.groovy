package objects

import org.junit.Assert
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import pages_elements.ActivityTab
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.VendorDetail

public class Activity {
	private String Type

	private String StartDate

	private String EndDate

	private String Service

	private String Title

	private String Content

	private Boolean Closed

	private String ScheduleWith

	private static String updateTitle

	private static String updateContent

	private static String reasonClosing

	private String Priority

	private String Status

	private String Days

	private String Date

	private String AssigneeType
	
	private String Assignee

	public Activity() {

		Type = "Task"

		AssigneeType = "Transf"
		StartDate = DateTimeFuncs.GetCurrentLocalDate_Str()

		Date dt = DateTimeFuncs.IncreaseDay(DateTimeFuncs.GetCurrentLocalDate_Date(),1)
		EndDate = DateTimeFuncs.ConvertDatetoString(dt)
		println("EndDate: "+EndDate)


		Service = "General"

		Random r = new Random();
		int ret2 = r.nextInt(100)+1;
		Title = "Activity-Title for Testing"+DateTimeFuncs.getCurrentLocalTime()

		Content = "Activity-Content for Testing"+DateTimeFuncs.getCurrentLocalTime()

		Closed = false

		updateTitle = "Updated_Activity-Title for Testing"+DateTimeFuncs.getCurrentLocalTime()
		updateContent = "Updated_Activity-Content for Testing"+DateTimeFuncs.getCurrentLocalTime()
		reasonClosing = "Reason for closing"

		Priority = "High"
		Status = "Active"

		ScheduleWith = "Coordinator"

		Date = "Trigger Date"

		Days = "1"
	}

	public void RegenerateInfo() {
		Title = "Updated_Activity-Title for Testing"+DateTimeFuncs.getCurrentLocalTime()

		Content = "Updated_Activity-Content for Testing"+DateTimeFuncs.getCurrentLocalTime()
	}

	public void CreateActivity_func(String recipient = "Transf",String button = "Submit") {

		ActivityTab.Activity_tab().click()

		Common.WaitForElementDisplay(ActivityTab.CreateNew_btn())

		Common.HandleElementClick(ActivityTab.CreateNew_btn())

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(ActivityTab.ActivityType_slect())


		if(recipient!="Transf") {

			Common.SelectDropdownItem(ActivityTab.ScheduledWith_slect(), this.ScheduleWith)
		}

		//Common.WaitForElementDisplay(ActivityTab.ServiceType_slect())

		Common.WaitForElementEnable(ActivityTab.ActivityType_slect())

		Common.WaitForElementEnable(ActivityTab.ServiceType_slect())

		ActivityTab.ActivityType_slect().click()

		Common.SelectDropdownItem(ActivityTab.ActivityType_slect(), this.Type)

		ActivityTab.Title_txt().sendKeys(Keys.TAB)

		if(recipient=="Transf")
		{
			ActivityTab.ServiceType_slect().click()
			Common.SelectDropdownItem(ActivityTab.ServiceType_slect(), this.Service)
		}


		ActivityTab.Title_txt().sendKeys(Keys.TAB)

		Common.ActionSendkey(ActivityTab.Start_txt(), this.StartDate)
		WebUI.delay(1)

		Common.ActionSendkey(ActivityTab.End_txt(), this.EndDate)
		WebUI.delay(1)

		ActivityTab.Title_txt().sendKeys(this.Title)

		//ActivityTab.Content_txt().sendKeys(this.Content)

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action = actions.sendKeys(ActivityTab.Title_txt(), Keys.TAB).sendKeys(Keys.TAB).sendKeys(this.Content).build()

		action.perform()

		WebUI.delay(2)

		if(button =="Submit")
			ActivityTab.Submit_btn().click()

		WebUI.delay(4)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Activity Creation.Msg: '"+msg+"'.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

	}//end void


	public void EditActivity_func(String old_title)
	{
		println(old_title)
		ActivityTab.ActivityName_lnk(old_title).click()

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(ActivityTab.Edit_btn())

		ActivityTab.Edit_btn().click()

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(ActivityTab.Title_txt())

		Common.sendkeyTextToElement(ActivityTab.Title_txt(), this.Title)

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action_key = actions.sendKeys(ActivityTab.Title_txt(),Keys.TAB).sendKeys(Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(this.Content).build()

		action_key.perform()

		WebUI.delay(2)

		if(Closed == true)
		{
			ActivityTab.Close_opt().click()
		}

		//ActivityTab.Update_btn().click()

		Common.HandleElementClick(ActivityTab.Update_btn())

		WebUI.delay(1)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Activity Update .Msg: '"+msg+"'.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}


		if(ActivityTab.ModalClose_btn()!=null)
		{
			Common.HandleElementClick(ActivityTab.ModalClose_btn())
		}


	}//end void



	public void CreateActivityVendor_func(String type, String priority) {
		WebUI.delay(4)
		ActivityTab.Activity_tab().click()

		Common.WaitForElementDisplay(ActivityTab.CreateNew_btn())

		Common.HandleElementClick(ActivityTab.CreateNew_btn())

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(ActivityTab.ActivityType_slect())

		WebUI.delay(4)

		ActivityTab.ActivityType_slect().click()

		Common.SelectDropdownItem(ActivityTab.ActivityType_slect(), type)

		ActivityTab.ActivityPriority_slect().click()

		Common.SelectDropdownItem(ActivityTab.ActivityPriority_slect(), priority)

		Common.selectRandomItemDropdown(ActivityTab.ScheduledWith_slect(), ActivityTab.ScheduledWith_listDrd())

		Common.selectRandomItemDropdown(ActivityTab.NotiRemider_slect() , ActivityTab.NotiRemider_listDrd())

		ActivityTab.Title_txt().sendKeys(Keys.TAB)

		ActivityTab.Title_txt().sendKeys(this.Title)

		//ActivityTab.Content_txt().sendKeys(this.Content)

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action = actions.sendKeys(ActivityTab.Title_txt(), Keys.TAB).sendKeys(Keys.TAB).sendKeys(this.Content).build()

		action.perform()

		WebUI.delay(2)

		ActivityTab.Submit_btn().click()

		WebUI.delay(4)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Activity Creation.Msg: '"+msg+"'.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

	}//end void

	public void EditActivityVendor_func(String oldtitle = "")
	{
		WebUI.delay(2)
		ActivityTab.ActivityNameOfVendor_lnk(oldtitle).click()

		WebUI.delay(3)
		Common.HandleElementClick(ActivityTab.EditActivityVendor_btn())

		WebUI.delay(3)
		Common.sendkeyTextToElement(ActivityTab.Title_txt(), this.Title)

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action = actions.sendKeys(ActivityTab.Title_txt(), Keys.TAB).sendKeys(Keys.TAB).sendKeys(this.Content).build()

		action.perform()

		WebUI.delay(2)

		if(Closed == true)
		{
			ActivityTab.CloseActivityVendor_opt().click()
			WebUI.delay(1)
			Common.selectRandomItemDropdown(ActivityTab.ResultActivity_slect(), ActivityTab.ResultActivity_listDrd())
			Common.sendkeyTextToElement(ActivityTab.ReasonClosing_txt(), reasonClosing)
		}

		Common.HandleElementClick(ActivityTab.Update_btn())

		WebUI.delay(1)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Activity Update .Msg: '"+msg+"'.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

	}//end void


	public void DeleteActivityVendor_func()
	{
		WebUI.delay(3)

		ActivityTab.Delete_icon().click()

		WebUI.delay(3)

		Messages.Delete_btn().click()

		WebUI.delay(3)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Delete Activity.")

	}//end void

	public void searchActivityDate()
	{
		WebUI.delay(3)

		Common.ActionSendkey(ActivityTab.SearchFromDate_txt(), this.StartDate)
		WebUI.delay(1)

		ActivityTab.SearchFromDate_txt().sendKeys(Keys.TAB)

		Common.ActionSendkey(ActivityTab.SearchToDate_txt(), this.EndDate)
		WebUI.delay(1)

		Common.HandleElementClick(ActivityTab.Search_btn())

		WebUI.delay(3)
	}

	public void searchType(String type)
	{
		WebUI.delay(3)
		Common.SelectDropdownItem(ActivityTab.SearchType_slect(), type)
		ActivityTab.Search_btn()
		WebUI.delay(3)
	}//end void

	public void searchPriority(String priority)
	{
		WebUI.delay(3)
		Common.SelectDropdownItem(ActivityTab.SearchPriority_slect(), priority)
		ActivityTab.Search_btn()
		WebUI.delay(3)
	}//end void

	public void searchStatus(String status)
	{
		WebUI.delay(3)
		Common.SelectDropdownItem(ActivityTab.SearchStatus_slect(), status)
		ActivityTab.Search_btn()
		WebUI.delay(3)
	}//end void

	public void clearAllDataInSearchbox(){
		ActivityTab tab = new ActivityTab()
		WebUI.delay(2)
		tab.Activity_tab().click()
		Common.ActionSendkey(tab.SearchFromDate_txt(), StartDate)
		WebUI.delay(1)
		tab.SearchFromDate_txt().sendKeys(Keys.TAB)
		Common.ActionSendkey(tab.SearchToDate_txt(), EndDate)
		WebUI.delay(1)
		Common.SelectDropdownItem(tab.SearchType_slect(), Type)
		Common.SelectDropdownItem(tab.SearchPriority_slect(), Priority)
		Common.SelectDropdownItem(tab.SearchStatus_slect(), Status)
		Common.HandleElementClick(tab.Search_btn())
		WebUI.delay(3)
		Common.VerifyFieldTextEqual_func("No Results", Messages.NoResultMsg_lb(), Messages.noResutl_msg)
		Common.HandleElementClick(tab.Clear_btn())
	}

	public void verifyEmptyDatainSearchbox(){
		Common.VerifyFieldTextEqual_func("Search From Date", ActivityTab.SearchFromDate_txt() , "")
		Common.VerifyFieldTextEqual_func("Search To Date", ActivityTab.SearchToDate_txt() , "")
		Common.VerifyFieldTextEqual_func("Search Type", Common.getFirstSelectedItem(ActivityTab.SearchType_slect()), "All")
		Common.VerifyFieldTextEqual_func("Search Detail", Common.getFirstSelectedItem(ActivityTab.SearchPriority_slect()), "All")
		Common.VerifyFieldTextEqual_func("Search User", Common.getFirstSelectedItem(ActivityTab.SearchStatus_slect()), "All")
	}

	public static void VerifyActivityVendorExistTable_func(String searchField = "", String searchName)
	{
		Boolean exist = false
		List<String> records = Common.GetTableRecordsPerPaging_func(ActivityTab.ActivityList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(searchName))
			{
				exist = true
				break
			}//end if
		}//end for each

		if(exist == false)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "["+searchName+"] NOT exist in table.\n"
		}//end if
		println ("Activity Vendor exists?: " + exist)
	}

	public void VerifyActivityVendorNotExistTable_func()
	{
		Boolean exist = false
		List<String> records = Common.GetTableRecordsPerPaging_func(ActivityTab.ActivityList_tbl())

		if(records.size()>0)
		{
			for(String index_str: records)
			{
				if(index_str.contains(Title))
				{
					exist = true
					break
				}//end if
			}//end foreach
		}


		if(exist==true)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Transferee- Activity["+Title+"] SHOULD NOT exist in table.\n"
		}//end if
	}

	public static void VerifyValidationOnActivityModal(){
		Vendor vendor = new Vendor()
		WebUI.delay(2)
		if(ActivityTab.ScheduledWith_slect().getText() == null)
		{
			Common.VerifyFieldTextEqual_func("Scheduled With", ActivityTab.newSheduledWithActivity_msg(), vendor.NewActivitySheduleWithMsg)
		}
		Common.VerifyFieldTextEqual_func("Title Activity", ActivityTab.newTitleActivity_msg(), vendor.NewActivityTitleMsg)
	}// end

	public static void openActivityModal(String activityType, String userType){
		WebUI.delay(2)
		if(activityType == "Task"){
			Common.HandleElementClick(ActivityTab.Task_btn())
		}else Common.HandleElementClick(ActivityTab.FollowUp_btn())
		if(userType == "Transferee"){
			Common.HandleElementClick(ActivityTab.Transf_opt())
		}else if(userType == "Client"){
			Common.HandleElementClick(ActivityTab.Client_opt())
		}else Common.HandleElementClick(ActivityTab.Vendor_opt())
		WebUI.delay(2)
	}// end

	public void CreateActivity(String type, String priority) {

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Common.WaitForElementDisplay(ActivityTab.ActivityType_slect())

		WebUI.delay(4)

		//ActivityTab.ActivityType_slect().click()

		//Common.SelectDropdownItem(ActivityTab.ActivityType_slect(), type)

		//ActivityTab.ActivityPriority_slect().click()

		//Common.SelectDropdownItem(ActivityTab.ActivityPriority_slect(), priority)

		//Common.selectRandomItemDropdown(ActivityTab.ScheduledWith_slect(), ActivityTab.ScheduledWith_listDrd())

		//Common.selectRandomItemDropdown(ActivityTab.NotiRemider_slect() , ActivityTab.NotiRemider_listDrd())

		ActivityTab.Title_txt().sendKeys(Keys.TAB)

		ActivityTab.Title_txt().sendKeys(this.Title)

		//ActivityTab.Content_txt().sendKeys(this.Content)

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action = actions.sendKeys(ActivityTab.Title_txt(), Keys.TAB).sendKeys(Keys.TAB).sendKeys(this.Content).build()

		action.perform()

		WebUI.delay(2)

		ActivityTab.Submit_btn().click()

		WebUI.delay(4)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Activity Creation.Msg: '"+msg+"'.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

	}//end void


}
