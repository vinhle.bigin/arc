package objects

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import pages_elements.VendorPage



public class ValidationMessage {

	String NewVendorNameMsg = "The vendor name field is required."
	String NewVendorCodeMsg = "The vendor code field is required."
	String NewVendorServiceMsg = "The service type field is required."

	public ValidationMessage() {
	}

	public void checkValidationMsgNewVendor() {
		Common.HandleElementClick(VendorPage.NewVendor_btn())
		WebUI.delay(2)
		Common.HandleElementClick(VendorPage.SubmitNewVendor_btn())
		WebUI.delay(2)

		Common.verifyTextEquals(VendorPage.NewVendorNameMsg(), NewVendorNameMsg)
		Common.verifyTextEquals(VendorPage.NewVendorCodeMsg(), NewVendorCodeMsg)
		Common.verifyTextEquals(VendorPage.NewVendorServiceMsg(), NewVendorServiceMsg)
	}//end void

}//end class
