package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import internal.GlobalVariable
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.Transf_DependTab

public class DependentInfo extends ContactInfo{


	public DependentInfo() {

		super()

		this.firstname = "Dependent_"+ this.firstname
		this.MailList.get(0).Type = "Personal Email"
	}



	public void Edit_TransfDependent_func(String depend_name ,String action_str = "Create") {
		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		Transf_DependTab tab = new Transf_DependTab()

		//this is for Create Action - Else will jump to Edit Action
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.New_btn())
		if(action_str =="Create")
			Common.HandleElementClick(tab.New_btn())

		else
		{
			Common.ScrollElementtoViewPort_func(tab.Dependent_List())
			
			tab.DependentName_lnk(depend_name).click()
			
			Common.HandleElementClick(tab.Modal_Edit_btn())
		}
			
			

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.Modal_State_txt())
		
		Common.sendkeyTextToElement(tab.Modal_Fname_txt(), this.firstname)

		Common.sendkeyTextToElement(tab.Modal_Lname_txt(), this.lastname)

		if(LandingPage.LoadingArc())
		Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)
		
		Common.WaitForElementDisplay(tab.Modal_State_txt())
		
		Common.SelectDropdownItem(tab.Modal_State_txt(), this.AddrInfo.State)

		Common.SelectDropdownItem(tab.Modal_EmailType_ddl(), this.MailList.get(0).Type)

		Common.sendkeyTextToElement(tab.Modal_Email_txt(), this.MailList.get(0).EmailAddr)

		tab.Modal_Submit_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When Create Dependent-Info")
		
		
	}//end void



}
