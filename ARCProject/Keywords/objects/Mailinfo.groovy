package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.util.Date

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.DateTimeFuncs
import functions.FileAccess
import internal.GlobalVariable
import objects.HHGService
import objects.Client

public class Mailinfo {

	public String Type
	public String EmailAddr

	//To send email
	public String ServiceType
	public String RecipientType
	public String TemplateName
	public List<String> To_list
	public List<String> CC_list
	public List<String> BCC_list
	public String From
	public String Subject
	public String Content
	public List<String> Attach_List

	//FOR RECEIVED MAIL
	public String activatelink
	public String resetlink
	public Date sentDate
	public FileAccess download_file



	public Mailinfo() {
		Random r = new Random();
		int ret2 = r.nextInt(100)+1;
		int ret = r.nextInt(100)+1;
		DateTimeFuncs dt = new DateTimeFuncs()
		String addr = 'test_mail'+ dt.getCurrentLocalTime()+(ret+ret2).toString()
		Type = "Work Email"
		EmailAddr = addr+"@bg.vn"
		ServiceType = ""
		RecipientType = ""

		TemplateName = ""
		To_list = new ArrayList<>()
		To_list.add(GlobalVariable.glb_usermail)

		CC_list = new ArrayList<>()
		BCC_list = new ArrayList<>()
		From = ""
		Subject = ""
		Content =""
		Attach_List = new ArrayList<>()
		
		//FOR RECEIVED EMAIL
		sentDate = dt.GetLocalDatetime()
	}//init


	//This method is used before providing info on New Email modal
	public void AssignTemplateToEmail_func(Template tpl,Boolean Iscontentoverride = true)
	{
		ServiceType = tpl.ServiceType
		RecipientType = tpl.Target
		TemplateName = tpl.Name

		if(tpl.EmailFrom != "Coordinator"&&tpl.EmailFrom != "Assistant")
			From = tpl.EmailFrom
		Subject = tpl.Subject

		if(Iscontentoverride==true)
			Content = tpl.Content
		Attach_List = new ArrayList<>()

		if(tpl.DocName!="")
			Attach_List.add(tpl.DocName)
	}


	//====This method is used to assign value into Template object befor verifying the Observation Tiny/Email Content
	//with Expected content that contains the Transfree General Info
	public void RenewContentTransGenInfo_func(Transferee transf_tmp)
	{
		String Content_str = ""
		Transferee transf = transf_tmp

		//Content_str += "Trans ID: \n"
		Content_str += "Trans FName: "+transf.firstname
		Content_str += "\nTrans LName: "+transf.lastname
		Content_str += "\nTrans FullName: "+transf.FullName
		//Content_str += "\nTrans JobTitle: \\n"
		Content_str += "\nTrans Coordinator: "+transf.Coordinator
		Content_str += "\nTrans Coor Assistant: "+transf.CoordinatorAssistant
		Content_str += "\nClient Name: "+transf.ClientName
		//Content_str += "Client Cotanct: \\n"
		Content_str += "\nTrans Status: "+transf.Status
		//Content_str += "Trans Status Change Date: \n"
		//Content_str += "Trans IsOveride: \n"
		//Content_str += "Trans IsQualified: \n"
		//Content_str += "Trans FileEnter CreateDate: \n"
		Content_str += "\nTrans IsReceiveSignABAD: "+transf.IsReceivedSignedABAD
		Content_str += "\nTrans IsReferListSide: "+transf.ReferedListSide
		Content_str += "\nTrans IsReferBuySide: "+transf.ReferedBuySide
		Content_str += "\nTrans IsReferToTitleComp: "+transf.ReferedToTitleComp
		Content_str += "\nTrans Mobile PhoneNumb: "+transf.PhoneList.get(0).Numb
		Content_str += "\nTrans WorkEmail: "+this.EmailAddr

		this.Content = Content_str
	}

	////====This method is used to assign value into Template object befor verifying the Observation Tiny/Email Content
	//with Expected content that contains the Transfree Other Info
	public void RenewContentTransOtherInfo_func(Transferee transf_tmp)
	{
		String Content_str = ""
		Transferee transf = transf_tmp

		//Content_str += "Trans ID: \n"
		Content_str += "MoveType: "+transf.Other_info.MoveType
		Content_str += "\nInItDate: "+transf.Other_info.InitDate
		Content_str += "\nEffectDate: "+transf.Other_info.EffectDate
		//Content_str += "\nTrans JobTitle: \\n"
		Content_str += "\nMoveInDate: "+transf.Other_info.InterMoveInDate
		Content_str += "\nMoveOutDate: "+transf.Other_info.InterMoveOutDate
		Content_str += "\nExpectMoveDate: "+transf.Other_info.ExpectedMoveByDate
		//Content_str += "Client Cotanct: \\n"
		Content_str += "\nMoveEnd: "+transf.Other_info.MoveEnd+"\n"


		this.Content = Content_str
	}


	public void RenewContentTransAddrInfo_func(Transferee transf_tmp)
	{
		String Content_str = ""
		Transferee transf = transf_tmp

		Content_str+= "OrgRes_Addr1: "+transf.OriResAddr_info.Addr1

		Content_str+= "\nOrgRes_Addr2: "+transf.OriResAddr_info.Addr2

		Content_str+= "\nOrgRes_City: "+transf.OriResAddr_info.City

		Content_str+= "\nOrgRes_State: "+transf.OriResAddr_info.State

		Content_str+= "\nOrgRes_Zip: "+transf.OriResAddr_info.Zip

		Content_str+= "\nOrgRes_Country: "+transf.OriResAddr_info.Country

		//Orginal Office Info
		Content_str+= "\nOrgOff_Addr1: "+transf.OriOffAddr_info.Addr1

		Content_str+= "\nOrgOff_Addr2: "+transf.OriOffAddr_info.Addr2

		Content_str+= "\nOrgOff_City: "+transf.OriOffAddr_info.City

		Content_str+= "\nOrgOff_State: "+transf.OriOffAddr_info.State

		Content_str+= "\nOrgOff_Zip: "+transf.OriOffAddr_info.Zip

		Content_str+= "\nOrgOff_Country: "+transf.OriOffAddr_info.Country

		//Destination Residient Info
		Content_str+= "\nDesRes_Addr1: "+transf.DesResAddr_info.Addr1

		Content_str+= "\nDesRes_Addr2: "+transf.DesResAddr_info.Addr2

		Content_str+= "\nDesRes_City: "+transf.DesResAddr_info.City

		Content_str+= "\nDesRes_State: "+transf.DesResAddr_info.State

		Content_str+= "\nDesRes_Zip: "+transf.DesResAddr_info.Zip

		Content_str+= "\nDesRes_Country: "+transf.DesResAddr_info.Country

		//Destionation Office Info
		Content_str+= "\nDesOff_Addr1: "+transf.DesOffAddr_info.Addr1

		Content_str+= "\nDesOff_Addr2: "+transf.DesOffAddr_info.Addr2

		Content_str+= "\nDesOff_City: "+transf.DesOffAddr_info.City

		Content_str+= "\nDesOff_State: "+transf.DesOffAddr_info.State

		Content_str+= "\nDesOff_Zip: "+transf.DesOffAddr_info.Zip

		Content_str+= "\nDesOff_Country: "+transf.DesOffAddr_info.Country


		this.Content = Content_str
	}// end void


	public void RenewContentTransHomeSaleInfo_func(HSService service)
	{
		HSService service_tmp = service
		String Content_str = ""
		Content_str+= "HS_TookBuyOut: "+ service_tmp.IsTookBuyOut
		Content_str+= "\nHS_ListPrice: " + service_tmp.ListPrice
		Content_str+= "\nHS_CommissionPercent: "+ service_tmp.CommissionPercent
		Content_str+= "\nHS_ListStartDate: " + service_tmp.ListStartDate
		Content_str+= "\nHS_ListEndDate: " + service_tmp.ListEndDate
		Content_str+= "\nHS_ClosingDate: " + service_tmp.CloseDate
		Content_str+= "\nHS_RebateAmount: " + service_tmp.RebateAmount
		Content_str+= "\nHS_DateBuyOutAccept: " + service_tmp.BuyOutAcceptDate
		Content_str+= "\nHS_EscrowFeePercent: "+ service_tmp.EscrowFeePercent
		Content_str+= "\nHS_EscowFeeAmount: " + service_tmp.EscrowFeeAmount
		Content_str+= "\nHS_Rebate: " + service_tmp.RebateAmount
		Content_str+= "\nHS_IsInvoiceCreate: " + service_tmp.IsInvoiceCreate
		Content_str+= "\nHS_InvoiceClosedOut: " + service_tmp.IsInvoiceClose
		Content_str+= "\nHS_ReffeeReceived: " + service_tmp.IsEscrowFeeReceive
		Content_str+= "\nHS_SignedRefFeeAgree: " + service_tmp.IsSignRefFeeAgree
		Content_str+= "\nHS_SelectReloDirectVendorName: " + service_tmp.VendorCtactList.get(0).VendorName
		Content_str+= "\nHS_SelectReloDirectVendorContactFirstName: "+ service_tmp.VendorCtactList.get(0).firstname
		Content_str+= "\nHS_SelectReloDirectVendorContactLastName: " + service_tmp.VendorCtactList.get(0).lastname
		Content_str+= "\nHS_SelectReloDirectVendorContactFullName: "+ service_tmp.VendorCtactList.get(0).FullName
		Content_str+= "\nHS_SelectReloDirectVendorContactPhone: " + service_tmp.VendorCtactList.get(0).PhoneList.get(0).Numb
		Content_str+= "\nHS_SelectReloDirectVendorContactEmail: " + service_tmp.VendorCtactList.get(0).MailList.get(0).EmailAddr
		Content_str+= "\nHS_SelectAgentVendorName: "
		Content_str+= "\nHS_SelectAgentVendorContactFirstName: "
		Content_str+= "\nHS_SelectAgentVendorContactLastName: "
		Content_str+= "\nHS_SelectAgentVendorContactLFullName: "
		Content_str+= "\nHS_SelectAgentVendorContactPhone: "
		Content_str+= "\nHS_SelectAgentVendorContactEmail: "
		Content_str+= "\nHS_SelectTitleClosingVendorName: "
		Content_str+= "\nHS_SelectTitleClosingVendorContactFirstName: "
		Content_str+= "\nHS_SelectTitleClosingVendorLastName: "
		Content_str+= "\nHS_SelectTitleClosingVendorContactPhone: "
		Content_str+= "\nHS_SelectTitleClosingVendorContactFullName: "
		Content_str+= "\nHS_SelectTitleClosingVendorContactEmail: "


		this.Content = Content_str
	}//end void

	public void RenewContentTransHomePurchaseInfo_func(HPService service_tmp)
	{
		String Content_str = ""
		Content_str+= "HP_IsSignRefFeeAgree: "+ service_tmp.IsSignRefFeeAgree
		Content_str+= "HP_SalePrice: "+ service_tmp.SalePrice
		Content_str+= "HP_LoanAmount: "+ service_tmp.LoanAmount
		Content_str+= "HP_Commission: "+ service_tmp.CommissionPercent
		Content_str+= "HP_RebateAmount: "+ service_tmp.RebateAmount
		Content_str+= "HP_CloseDate: "+ service_tmp.CloseDate
		Content_str+= "HP_HuntTripDate: "+ service_tmp.HouseHuntTripDate
		Content_str+= "HP_EscrowFeeAmount: "+ service_tmp.EscrowFeeAmount
		Content_str+= "HP_EscrowfePercent: "+ service_tmp.EscrowFeePercent
		Content_str+= "HP_IsRebateSent: "
		Content_str+= "HP_IsInvoiceCloseOut: " + service_tmp.IsInvoiceClose
		Content_str+= "HP_IsEscrowFeeReceive: "+ service_tmp.IsEscrowFeeReceive
		Content_str+= "HP_IsInvoiceCreate: "+ service_tmp.IsInvoiceCreate
		Content_str+= "HP_IsTransfereeRent: "+ service_tmp.IsTransfRent
		Content_str+= "HP_ReloVendorContactFirstName: "+ service_tmp.VendorCtactList.get(0).firstname
		Content_str+= "HP_ReloVendorContactMid: "
		Content_str+= "HP_ReloVendorContactLastName: "+ service_tmp.VendorCtactList.get(0).lastname
		Content_str+= "HP_ReloVendorContactFullName: "+ service_tmp.VendorCtactList.get(0).FullName
		Content_str+= "HP_ReloVendorContactPhone: "+ service_tmp.VendorCtactList.get(0).PhoneList.get(0).Numb
		Content_str+= "HP_AgentVendorName: "+ service_tmp.VendorCtactList.get(0).VendorName
		Content_str+= "HP_AgentVendorContactFirstName: "+ service_tmp.VendorCtactList.get(0).firstname
		Content_str+= "HP_AgentVendorContactMidName: "
		Content_str+= "HP_AgentVendorContactLastName: "
		Content_str+= "HP_AgentVendorContactFullName: "
		Content_str+= "HP_AgentVendorContactPhone: "
		Content_str+= "HP_AgentVendorContactEmail: "
		Content_str+= "HP_closing_vendor_name: "
		Content_str+= "HP_closing_vendor_contact_first_name: "+ service_tmp.VendorCtactList.get(0).firstname
		Content_str+= "HP_closing_vendor_contact_mid_name: "
		Content_str+= "HP_closing_vendor_contact_last_name: "+ service_tmp.VendorCtactList.get(0).lastname
		Content_str+= "HP_closing_vendor_contact_full_name: "+ service_tmp.VendorCtactList.get(0).FullName
		Content_str+= "HP_closing_vendor_contact_phone: "+ service_tmp.VendorCtactList.get(0).PhoneList.get(0).Numb
		Content_str+= "HP_closing_vendor_contact_email: "+ service_tmp.VendorCtactList.get(0).MailList.get(0).EmailAddr


		this.Content = Content_str
	}//end void

	public void RenewContentTransHHGoodInfo_func(HHGService service_tmp)
	{

		String Content_str = ""
		Content_str+= "HHG_destination_address1: " + service_tmp.DesAddr
		Content_str+= "\nHHG_destination_address2: "
		Content_str+= "\nHHG_house_hunting_trip_date: "
		Content_str+= "\nHHG_destination_city: "
		Content_str+= "\nHHG_destination_zipcode: "
		Content_str+= "\nHHG_destination_state: "
		Content_str+= "\nHHG_hhg_move_date: " + service_tmp.HHGMoveDate
		Content_str+= "\nHHG_hhg_delivery_date: "+ service_tmp.HHGDeliverDate
		Content_str+= "\nHHG_origin_address1: " + service_tmp.OriAddr
		Content_str+= "\nHHG_origin_address2: "
		//Content_str+= "\nHHG_origin_city: "
		//Content_str+= "\nHHG_origin_state: "
		Content_str+= "\nHHG_origin_zipcode: "
		Content_str+= "\nHHG_origin_country: "
		Content_str+= "\nHHG_destination_country: "
		Content_str+= "\nHHG_payment_type: "  + service_tmp.HHGPaymentType
		Content_str+= "\nHHG_selected_vendor_name: " + service_tmp.VendorCtactList.get(0).VendorName
		Content_str+= "\nHHG_selected_vendor_contact_first_name: " + service_tmp.VendorCtactList.get(0).firstname
		Content_str+= "\nHHG_selected_vendor_contact_mid_name: "
		Content_str+= "\nHHG_selected_vendor_contact_last_name: " + service_tmp.VendorCtactList.get(0).lastname
		Content_str+= "\nHHG_selected_vendor_contact_full_name: " + service_tmp.VendorCtactList.get(0).FullName
		Content_str+= "\nHHG_selected_vendor_contact_phone: " + service_tmp.VendorCtactList.get(0).PhoneList.get(0).Numb
		Content_str+= "\nHHG_selected_vendor_contact_email: " + service_tmp.VendorCtactList.get(0).MailList.get(0).EmailAddr


		this.Content = Content_str
	}//end void

	public void RenewContentTransMortgageInfo_func(MortgService service_tmp)
	{
		String Content_str = ""
		Content_str+= "MG_mortgage_amount: " + service_tmp.MortgageAmount
		Content_str+= "\nMG_index_rate: " + service_tmp.IndexRate
		Content_str+= "\nMG_amt_needed_at_closing: "
		Content_str+= "\nMG_down_payment: "
		Content_str+= "\nMG_closing_date: "
		Content_str+= "\nMG_selected_vendor_name: "
		Content_str+= "\nMG_selected_vendor_contact_first_name: " + service_tmp.VendorCtactList.get(0).firstname
		Content_str+= "\nMG_selected_vendor_contact_mid_name: "
		Content_str+= "\nMG_selected_vendor_contact_last_name: " + service_tmp.VendorCtactList.get(0).lastname
		Content_str+= "\nMG_selected_vendor_contact_full_name: "+ service_tmp.VendorCtactList.get(0).FullName
		Content_str+= "\nMG_selected_vendor_contact_phone: " + service_tmp.VendorCtactList.get(0).PhoneList.get(0).Numb
		Content_str+= "\nMG_selected_vendor_contact_email: " + service_tmp.VendorCtactList.get(0).MailList.get(0).EmailAddr


		this.Content = Content_str
	}//end void

	public void RenewContentTransTempQuarterInfo_func(TQService service_tmp)
	{
		String Content_str = ""
		Content_str+= "TQ_move_out_date: "+ service_tmp.MoveOutDate
		Content_str+= "\nTQ_move_in_date: " + service_tmp.MoveInDate
		Content_str+= "\nTQ_pet: "  + service_tmp.Pets
		Content_str+= "\nTQ_apartment_type: "  + service_tmp.ApartmentType
		Content_str+= "\nTQ_baths: "  + service_tmp.Baths
		Content_str+= "\nTQ_budget: "  + service_tmp.Budget
		Content_str+= "\nTQ_payment_type: "   + service_tmp.PaymentType
		Content_str+= "\nTQ_escrow_fee_received: " + service_tmp.IsEscrowFeeReceive
		Content_str+= "\nTQ_invoice_created: " + service_tmp.IsInvoiceCreate
		Content_str+= "\nTQ_invoice_closed_out: "  + service_tmp.IsInvoiceClose
		Content_str+= "\nTQ_selected_vendor_name: "  + service_tmp.VendorCtactList.get(0).VendorName
		Content_str+= "\nTQ_selected_vendor_contact_first_name: " + service_tmp.VendorCtactList.get(0).firstname
		Content_str+= "\nTQ_selected_vendor_contact_mid_name: "
		Content_str+= "\nTQ_selected_vendor_contact_last_name: " + service_tmp.VendorCtactList.get(0).lastname
		Content_str+= "\nTQ_selected_vendor_contact_full_name: " + service_tmp.VendorCtactList.get(0).FullName
		Content_str+= "\nTQ_selected_vendor_contact_phone: " + service_tmp.VendorCtactList.get(0).PhoneList.get(0).Numb
		Content_str+= "\nTQ_selected_vendor_contact_email: "  + service_tmp.VendorCtactList.get(0).MailList.get(0).EmailAddr


		this.Content = Content_str
	}//end void



	//RENEW THE CONTENT WITH CLIENTINFO
	public void RenewContent_ClientInfo_func(Client client_info)
	{
		String Content_str = ""
		Content_str+= "company_name: " +client_info.CompanyName
		//Content_str+= "\ncontact_salutation: "+client_info.
		Content_str+= "\ncontact_first_name: " +client_info.Contacts.get(0).firstname
		//	Content_str+= "\ncontact_mid_name: " +client_info.
		Content_str+= "\ncontact_last_name: " +client_info.Contacts.get(0).lastname
		Content_str+= "\ncontact_full_name: " +client_info.Contacts.get(0).FullName
		Content_str+= "\ncontact_phone_number: " +client_info.Contacts.get(0).PhoneList.get(0).Numb
		Content_str+= "\ncontact_email: " +client_info.Contacts.get(0).MailList.get(0).EmailAddr
		Content_str+= "\ncontact_address1: "+client_info.Contacts.get(0).AddrInfo.Addr1
		//	Content_str+= "\ncontact_address2: "
		Content_str+= "\ncontact_city: "+client_info.Contacts.get(0).AddrInfo.City
		Content_str+= "\ncontact_state: "+client_info.Contacts.get(0).AddrInfo.State
		Content_str+= "\ncontact_zip: " +client_info.Contacts.get(0).AddrInfo.Zip
		Content_str+= "\ncontact_country: "+client_info.Contacts.get(0).AddrInfo.Country

		this.Content = Content_str
	}//end void



	//RENEW THE CONTENT WITH VENDORINFO
	public void RenewContent_VendorInfo_func(Vendor vendor_info)
	{
		String Content_str = ""
		Content_str+= "vendor_name: " +vendor_info.vendorName
		Content_str+= "\nvendorcontact_first_name: "
		Content_str+= "\nvendorcontact_last_name: "
		Content_str+= "\nvendorcontact_full_name: "
		Content_str+= "\nvendorcontact_phone_number: "
		Content_str+= "\nvendorcontact_email: "
		Content_str+= "\nvendorcontact_address1: "
		Content_str+= "\nvendorcontact_city: "
		Content_str+= "\nvendorcontact_state: "
		Content_str+= "\nvendorcontact_zip: "
		Content_str+= "\nvendorcontact_country: "


		this.Content = Content_str
	}//end void
	
	
	//RENEW THE CONTENT WITH CUSTOMFIELD
	public void RenewContent_CustomField_func(CustomField customfield)
	{
		String Content_str = ""
		Content_str+= customfield.FieldName+": " +customfield.ValueList.get(0)
		this.Content = Content_str
	}//end void




	//METHOD FOR RECEIVED EMAIL
	public void GetActivateLink()
	{
		//<a href="https://agoyu-dev.bigin.top/auth/activate/6/5BlMOqMItoOw5008NfgQkt0Pb4Jiqrf0" style="color: #ffffff; text-decoration: none;">Active your email
		String temp_1 = GlobalVariable.glb_URL+ "/auth/activate"
		String temp_2 = "Active your email"

		int index_1 = this.Content.indexOf(temp_1)
		int index_2 = this.Content.indexOf(temp_2)

		String temp_str =  this.Content.substring(index_1, index_2)


		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")
		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")

		//OR can use this regex temp_str = temp_str.replaceAll(" style.+\$","")
		int index3 =  temp_str.indexOf(" style")
		temp_str =  temp_str.substring(0, index3)
		this.activatelink = temp_str

	}


	public void GetResetPassLink()
	{
		//<a href="https://agoyu-dev.bigin.top/auth/activate/6/5BlMOqMItoOw5008NfgQkt0Pb4Jiqrf0" style="color: #ffffff; text-decoration: none;">Active your email
		String temp_1 = GlobalVariable.glb_URL+ "/auth/reset"
		String temp_2 = "Reset Your Password"

		int index_1 = this.Content.indexOf(temp_1)
		int index_2 = this.Content.indexOf(temp_2)

		String temp_str =  this.Content.substring(index_1, index_2)


		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")
		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")

		//OR can use this regex temp_str = temp_str.replaceAll(" style.+\$","")
		int index3 =  temp_str.indexOf(" style")
		temp_str =  temp_str.substring(0, index3)
		this.activatelink = temp_str

	}

	public void VerifyActivateEmail()
	{

		String heading_content = "Agoyu Confirmation"

		String body_content = "In order to finish your registration you need to confirm your email address. It's easy - just click the Confirm Your Email link below."

		String active_lnk = GlobalVariable.glb_URL+ "/auth/activate"

		String active_lbl ="Active your email"


		if(!this.Content.contains(heading_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Confirmation text.[Observed:"+this.Content +"-Expected:"+heading_content+"]."
		}

		if(!this.Content.contains(body_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"]."
		}

		if(!this.Content.contains(active_lnk))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Activate link.[Observed:"+this.Content +"-Expected:"+active_lnk+"]."
		}


		if(!this.Content.contains(active_lbl))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Activate text.[Observed:"+this.Content +"-Expected:"+active_lbl+"]."
		}

	}

	public void VerifyWelcomeEmail()
	{

		String heading_content = "Congratulations!"

		String body_content = "You’re now part of a community that connects regular people like you and me, with the world’s most recognized moving companies across the world. Find a moving estimate that suits your budget. And discover techniques to save money during the process."


		if(!this.Content.contains(heading_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage = "Incorrect Welcome text.[Observed:"+this.Content +"-Expected:"+heading_content+"]."
		}

		if(!this.Content.contains(body_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage = "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"]."
		}


	}


	public void VerifyResetAccountEmail()
	{
		String heading_content = "Agoyu Confirmation"

		String body_content = "We've received a request to reset your password. You can reset your password by using the link below. If you didn't request this, you can safely ignore this email."

		String reset_lnk = GlobalVariable.glb_URL+ "/auth/reset"


		String reset_lbl ="Reset your password"


		if(!this.Content.contains(heading_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Confirmation text.[Observed:"+this.Content +"-Expected:"+heading_content+"]."
		}

		if(!this.Content.contains(body_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"]."
		}

		if(!this.Content.contains(reset_lnk))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Reset link.[Observed:"+this.Content +"-Expected:"+reset_lnk+"]."
		}


		if(!this.Content.contains(reset_lbl))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Reset text.[Observed:"+this.Content +"-Expected:"+reset_lbl+"]."
		}
	}//end void


	public void VerifyContentEmail(Mailinfo compare_mail)
	{
		if(this.Content!=compare_mail.Content)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Email Content.[Observed:"+this.Content +"-Expected:"+compare_mail.Content+"]."

		}
	}
	
	public void VerifyAttachmentContent_func(String expect_content)
	{
		if(this.download_file.Content!=expect_content)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Attachment Content.[Observed:"+this.download_file.Content +"-Expected:"+expect_content+"]."

		}
	}
	


}
