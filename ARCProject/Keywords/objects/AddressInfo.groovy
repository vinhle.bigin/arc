package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import pages_elements.Client_AddressTab
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.Transf_AddressTab



public class AddressInfo {

	public String ClientLocation
	public String Addr1
	public String Addr2
	public String City
	public String State
	public String Zip
	public String Country
	public Boolean IsAtri_Billing
	public Boolean IsAtri_Shipping
	public Boolean IsAtri_Mailing
	public Boolean IsAtri_Primary
	public Boolean IsSendMailPayment
	public Boolean IsSendMailTaxDoc
	public Boolean IsSendMailServices
	public String Description
	public String Code

	public AddressInfo() {
		DateTimeFuncs dt = new DateTimeFuncs()
		Code = "AddrCode_"+ dt.getCurrentLocalTime()
		Description = "Descr for testing"
		ClientLocation = ""
		Addr1 = "20 Wall Street"
		Addr2 = "50 High Road"
		City = "New York"
		State  = "Alabama"
		Country  = "United States of America"
		Zip = "12345"
		IsAtri_Billing = false
		IsAtri_Shipping = false
		IsAtri_Mailing = false
		IsAtri_Primary = false
		IsSendMailPayment = false
		IsSendMailTaxDoc = false
		IsSendMailServices = false
	}

	public void Edit_TransfAddrInfo_func() {
	}//end void

	//CREATE NEW CLIENT ADDRESS
	public void CreateClientAddress_func(String button = "Submit")
	{
		Client_AddressTab addrtab = new Client_AddressTab()

		Common.HandleElementClick(addrtab.Address_Tab())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		addrtab.New_btn().click()

		//Common.ScrollElementtoViewPort_func(null)
		addrtab.FillModalInfo_func(this, button)

	}

	public void UpdateClientAddress_func(String old_code,String button = "Submit")
	{
		Client_AddressTab addrtab = new Client_AddressTab()
		addrtab.Address_Tab().click()
		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		addrtab.AddrEdit_icon(old_code).click()
		
		WebUI.delay(3)
		
		Common.HandleElementClick(addrtab.ModalEdit_btn())
		
		//Common.ScrollElementtoViewPort_func(null)
		addrtab.FillModalInfo_func(this, button)

	}

	public void RemoveClientAddress_func(String button = "Submit")
	{
		Client_AddressTab addrtab = new Client_AddressTab()
		addrtab.Address_Tab().click()
		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		addrtab.AddrRemove_icon(this.Code).click()

		WebUI.delay(1)

		Messages.Delete_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When removing the Client Address.")




	}




}//end class

public class OriResAddrInfo extends AddressInfo {

	public OriResAddrInfo() {
		super()
		ClientLocation = ""
		Addr1 = "OriRes 20 Wall Street"
		Addr2 = "50 High Road"
		City = "California"
		State  = "Alabama"
		Country  = "United States of America"
		Zip = "28388"
	}

	@Override
	public void Edit_TransfAddrInfo_func()
	{
		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."

		Transf_AddressTab tab = new Transf_AddressTab()
		WebUI.delay(3)
		Common.WaitForElementDisplay(tab.OrgRes_Edit_btn())

		Common.HandleElementClick(tab.OrgRes_Edit_btn())

		Common.ScrollElementtoViewPort_func(tab.OrgRes_Addrs1())
		Common.sendkeyTextToElement(tab.OrgRes_Addrs1(), this.Addr1)

		Common.sendkeyTextToElement(tab.OrgRes_Addrs2(), this.Addr2)

		Common.sendkeyTextToElement(tab.OrgRes_City(), this.City)

		Common.sendkeyTextToElement(tab.OrgRes_Zip(), this.Zip)

		Common.SelectDropdownItem(tab.OrgRes_State(),  this.State)

		Common.SelectDropdownItem(tab.OrgRes_Country(), this.Country)

		//this.Country = "Viet Nam"
		//Common.sendkeyTextToElement()

		tab.OrgRes_Update_btn().click()

		WebUI.delay(5)

		Messages.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull OrgRes - Info update")
	}//end void
}


public class DesResAddrInfo extends AddressInfo {

	public DesResAddrInfo() {
		super()
		Addr1 = "DesRes 20 Wall Street"
		Addr2 = "50 High Road"
		City = "California"
		State  = "Alaska"
		Country  = "United States of America"
		Zip = "83828"
	}

	@Override
	public void Edit_TransfAddrInfo_func()
	{
		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."

		Transf_AddressTab tab = new Transf_AddressTab()

		Common.WaitForElementDisplay(tab.DesRes_Edit_btn())

		Common.HandleElementClick(tab.DesRes_Edit_btn())

		Common.ScrollElementtoViewPort_func(tab.DesRes_Addrs1())
		Common.sendkeyTextToElement(tab.DesRes_Addrs1(), this.Addr1)

		Common.sendkeyTextToElement(tab.DesRes_Addrs2(), this.Addr2)

		Common.sendkeyTextToElement(tab.DesRes_City(), this.City)

		Common.sendkeyTextToElement(tab.DesRes_Zip(), this.Zip)

		Common.SelectDropdownItem(tab.DesRes_State(),  this.State)

		Common.SelectDropdownItem(tab.DesRes_Country(), this.Country)

		//this.Country = "Viet Nam"
		//Common.sendkeyTextToElement()

		tab.DesRes_Update_btn().click()

		WebUI.delay(5)

		Messages.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull DesRes - Info update")
	}//end void
}

public class OriOffAddrInfo extends AddressInfo {
	public OriOffAddrInfo() {
		super()
		Addr1 = "DesRes 20 Wall Street"
		Addr2 = "50 High Road"
		City = "Chicago"
		State  = "Nevada"
		Country  = "United States of America"
		Zip = "68382"
	}

	@Override
	public void Edit_TransfAddrInfo_func()
	{
		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."

		Transf_AddressTab tab = new Transf_AddressTab()

		Common.WaitForElementDisplay(tab.OrgOff_Edit_btn())

		Common.HandleElementClick(tab.OrgOff_Edit_btn())

		Common.ScrollElementtoViewPort_func(tab.OrgOff_Addrs1())
		Common.sendkeyTextToElement(tab.OrgOff_Addrs1(), this.Addr1)

		Common.sendkeyTextToElement(tab.OrgOff_Addrs2(), this.Addr2)

		Common.sendkeyTextToElement(tab.OrgOff_City(), this.City)

		Common.sendkeyTextToElement(tab.OrgOff_Zip(), this.Zip)

		Common.SelectDropdownItem(tab.OrgOff_State(),  this.State)

		Common.SelectDropdownItem(tab.OrgOff_Country(), this.Country)

		//this.Country = "Viet Nam"
		//Common.sendkeyTextToElement()

		tab.OrgOff_Update_btn().click()

		WebUI.delay(5)

		Messages.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull OrgOff - Info update")
	}//end void
}

public class DesOffAddrInfo extends AddressInfo {

	public DesOffAddrInfo(){
		super()
		Addr1 = "DesRes 20 Wall Street"
		Addr2 = "50 High Road"
		City = "Denver"
		State  = "Utah"
		Country  = "United States of America"
		Zip = "93828"
	}

	@Override
	public void Edit_TransfAddrInfo_func()
	{
		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."

		Transf_AddressTab tab = new Transf_AddressTab()

		Common.WaitForElementDisplay(tab.DesOff_Edit_btn())

		Common.HandleElementClick(tab.DesOff_Edit_btn())

		Common.ScrollElementtoViewPort_func(tab.DesOff_Addrs1())

		Common.sendkeyTextToElement(tab.DesOff_Addrs1(), this.Addr1)

		Common.sendkeyTextToElement(tab.DesOff_Addrs2(), this.Addr2)

		Common.sendkeyTextToElement(tab.DesOff_City(), this.City)

		Common.sendkeyTextToElement(tab.DesOff_Zip(), this.Zip)

		Common.SelectDropdownItem(tab.DesOff_State(),  this.State)

		Common.SelectDropdownItem(tab.DesOff_Country(), this.Country)

		//this.Country = "Viet Nam"
		//Common.sendkeyTextToElement()

		tab.DesOff_Update_btn().click()

		WebUI.delay(5)

		Messages.VerifyNotifyMsgNotDisplay_func(expected_msg,"Unsuccessfull DesOff - Info update")
	}//end void
}
