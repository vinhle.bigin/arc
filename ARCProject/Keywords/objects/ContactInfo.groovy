package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import pages_elements.ContactTab
import pages_elements.LandingPage
import functions.Common
import internal.GlobalVariable
import pages_elements.Messages

public class ContactInfo extends User {

	public String FullName
	public AddressInfo AddrInfo
	public List<PhoneInfo> PhoneList
	public List<Mailinfo> MailList
	public IsDefault = true//false
	public ServiceType
	public String VendorName
	public String ClientName
	public ContactInfo()
	{
		super()

		FullName = this.firstname+ " " + this.lastname

		PhoneList = new ArrayList()
		PhoneList.add(new PhoneInfo())

		AddrInfo = new AddressInfo()

		MailList = new ArrayList()

		Mailinfo mail_1 = new Mailinfo()
		mail_1.EmailAddr = this.firstname +"@email." + this.lastname

		MailList.add(mail_1)

		//HHG
		ServiceType = "Van Lines"

		//HP-HS
		//ServiceType = "Realtor Broker"//Title

		//TQ
		//ServiceType = "Interim Housing"//Title

		//MORTGAGE
		//ServiceType = "Lender"//Title

		VendorName = ""
		ClientName = ""


	}

	public void CreateContact_func(String button = "Submit")
	{
		ContactTab tab = new ContactTab()

		Common.HandleElementClick(tab.Contact_Tab())
		
		Common.HandleElementClick(tab.New_btn())

		tab.FillModalInfo(this, button)
		
		WebUI.delay(3)
		

	}//end void



	public void UpdateContact_func(String old_fullname_str,String button = "submit")
	{
		ContactTab tab = new ContactTab()

		tab.Contact_Tab().click()

		Common.HandleElementClick(ContactTab.FullName_lnk(old_fullname_str))
		
		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
		
		Common.WaitForElementDisplay(ContactTab.ModalEdit_btn())
		
		Common.HandleElementClick(ContactTab.ModalEdit_btn())
		
	
		tab.FillModalInfo(this, button)

	}//end void


	//METHOD: REMOVE THE CLIENTCONTACT
	public void RemoveContact_func(String button = "Submit")
	{
		ContactTab tab = new ContactTab()

		tab.Contact_Tab().click()

		Common.HandleElementClick(tab.Remove_icon(this.FullName))

		WebUI.delay(1)

		Messages.Delete_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When removing the Client Contact")



	}//end void



}
