package objects

import org.junit.Assert
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.interactions.Actions

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.sun.java.swing.plaf.motif.MotifInternalFrameTitlePane.Title

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import pages_elements.ActivityTab
import pages_elements.HistoryTab
import pages_elements.LandingPage
import pages_elements.Messages

public class History {

	private String StartDate

	private String EndDate

	private String Type

	private String Detail

	private String User


	public History() {

		StartDate = DateTimeFuncs.GetCurrentLocalDate_Str()

		Date dt = DateTimeFuncs.IncreaseDay(DateTimeFuncs.GetCurrentLocalDate_Date(),1)
		EndDate = DateTimeFuncs.ConvertDatetoString(dt)
		println("EndDate: "+EndDate)

		Type = "Change Log"

		Detail = "Updated Ownership"

		User = HistoryTab.UserName_lb().getText()
	}


	public void searchHistoryDate() {
		WebUI.delay(3)

		Common.ActionSendkey(HistoryTab.SearchFromDate_txt(), StartDate)
		WebUI.delay(1)

		HistoryTab.SearchFromDate_txt().sendKeys(Keys.TAB)

		Common.ActionSendkey(HistoryTab.SearchToDate_txt(), EndDate)
		WebUI.delay(1)

		Common.HandleElementClick(HistoryTab.Search_btn())

		WebUI.delay(3)
	}

	public void searchType(String type) {
		WebUI.delay(3)
		Common.SelectDropdownItem(HistoryTab.SearchType_slect(), type)
		ActivityTab.Search_btn()
		WebUI.delay(3)
	}//end void

	public void searchHistory(String searchField, WebElement element, String subject_str)
	{
		Common.sendkeyTextToElement(element, subject_str)

		Common.HandleElementClick(HistoryTab.Search_btn())

		WebUI.delay(3)
	}

	public void clearAllDataInSearchbox(){
		HistoryTab tab = new HistoryTab()
		WebUI.delay(2)
		tab.History_tab().click()
		Common.ActionSendkey(tab.SearchFromDate_txt(), StartDate)
		WebUI.delay(1)
		tab.SearchFromDate_txt().sendKeys(Keys.TAB)
		Common.ActionSendkey(tab.SearchToDate_txt(), EndDate)
		WebUI.delay(1)
		Common.SelectDropdownItem(tab.SearchType_slect(), Type)
		Common.sendkeyTextToElement(tab.SearchDetail_txt(), Detail)
		Common.sendkeyTextToElement(tab.SearchUser_txt() , User)
		Common.HandleElementClick(tab.Search_btn())
		WebUI.delay(3)
		Common.VerifyFieldTextEqual_func("No Results", Messages.NoResultMsg_lb(), Messages.noResutl_msg)
		Common.HandleElementClick(tab.Clear_btn())
	}

	public void verifyEmptyDatainSearchbox(){
		Common.VerifyFieldTextEqual_func("Search From Date", HistoryTab.SearchFromDate_txt() , "")
		Common.VerifyFieldTextEqual_func("Search To Date", HistoryTab.SearchToDate_txt() , "")
		Common.VerifyFieldTextEqual_func("Search Type", Common.getFirstSelectedItem(HistoryTab.SearchType_slect()), "All")
		Common.VerifyFieldTextEqual_func("Search Detail", HistoryTab.SearchDetail_txt() , "")
		Common.VerifyFieldTextEqual_func("Search User", HistoryTab.SearchUser_txt() , "")
	}

	public static void VerifyHistoryVendorExistTable_func(String searchField = "", String searchName)
	{
		Boolean exist = false
		List<String> records = Common.GetTableRecordsPerPaging_func(HistoryTab.HistoryList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(searchName))
			{
				exist = true
				break
			}//end if
		}//end for each

		if(exist == false)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "["+searchName+"] NOT exist in table.\n"
		}//end if
		println ("History Vendor exists?: " + exist)
	}

}
