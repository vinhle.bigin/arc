package objects

import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.Navigation
import internal.GlobalVariable
import pages_elements.Messages
import pages_elements.VendorDetail
import pages_elements.VendorPage
public class Vendor {
	
	

	public Vendor() {
		
		SearchOpt_IsZip = false
		
		SearchOpt_IsUSA = true
		
		SearchOpt_IsInter = false
		
		SearchOpt_ZipList = new ArrayList<>()
		
		SearchOpt_IsPrefered = true
		
		
		
		
	}

	// VENDOR PAGE
	int number = Common.randomNumber()
	String vendorName = "QAVendor" + number
	
	String vendorCode = String.valueOf(number)
	
	String selectService = ""
	String webSite = "https://google.com"

	// VENDOR DETAIL
	String why = "Good services"
	String selectedStar= "active"
	String client = "emClient"

	//CONTACT
	String firstContactName = "ContactVendor"
	String lastContactName = String.valueOf(number)
	String fullContactName = firstContactName + " " + lastContactName
	String address1 = "2024 Chestnut Street"
	String city = "Philadelphia"
	String state = "Pennsylvania"
	String zipCode = "19103"
	String email = "em.tran@bigin.vn"
	String typeEmail = "Work Email"

	// Validation
	String NewVendorNameMsg = "The vendor name field is required."
	String NewVendorCodeMsg = "The vendor code field is required."
	String NewVendorServiceMsg = "The service type field is required."

	String NewFirstNameContactMsg = "The first name field is required."
	String NewLastNameContactCodeMsg = "The last name field is required."
	String NewStateContactMsg = "Please Enter State If Country Is USA"

	String NewActivityTitleMsg = "The title field is required."
	String NewActivitySheduleWithMsg = "The scheduled with field is required."

	String NewNoteContactMsg = "The contact id field is required."
	String NewNoteSubjectMsg = "The subject field is required."
	String NewNoteContentMsg = "The content field is required."

	String NewDocumentFileMsg = "Document file is required"

	String noResutlMsg = "No matching records"
	
	Boolean SearchOpt_IsZip
	
	Boolean SearchOpt_IsUSA
	Boolean SearchOpt_IsInter
	Boolean SearchOpt_IsPrefered
	List<String> SearchOpt_ZipList
	
	
	
	


	public void searchVendor(String name, WebElement nameElement){
		Common.WaitForElementDisplay(nameElement)
		Common.sendkeyTextToElement(nameElement, name)
		Common.HandleElementClick(VendorPage.Search_btn())
		WebUI.delay(3)
	}//end

	public String service(){
		return selectService =  Common.selectRandomItemDropdown(VendorPage.SelectServiceNewVendor(), VendorPage.NewVendorService_listDrd())
	}

	public void createNewVendor(){
		Common.HandleElementClick(VendorPage.NewVendor_btn())
		WebUI.delay(2)
		Common.sendkeyTextToElement(VendorPage.NewVendorName(), vendorName)
		WebUI.delay(1)
		Common.selectRandomItemDropdown(VendorPage.SelectServiceNewVendor(), VendorPage.NewVendorService_listDrd())
		Common.sendkeyTextToElement(VendorPage.NewVendorCode(), vendorCode)
		Common.sendkeyTextToElement(VendorPage.NewVendorWebsite_txt(), webSite)
		WebUI.delay(2)

		Common.HandleElementClick(VendorPage.SubmitNewVendor_btn())
		WebUI.delay(3)

		if (VendorPage.ExistVendor_lb().isDisplayed() == true){
			Common.HandleElementClick(VendorPage.CreateNewVendor_btn())
		}
		WebUI.delay(5)
	}

	public void editGeneralInfoVendor(){
		Vendor vendor_tmp = this
		//Common.HandleElementClick(VendorDetail.vendorEdit_btn())
		VendorDetail.vendorEdit_btn().click()
		WebUI.delay(3)
		Common.sendkeyTextToElement(VendorDetail.vendorName_txt(), vendorName)
		Common.sendkeyTextToElement(VendorDetail.vendorCode_num(), vendorCode)
		Common.selectRandomItemDropdown(VendorDetail.vendorService_drd(), VendorDetail.vendorService_listDrd())
		Common.selectRandomItemDropdown(VendorDetail.vendorOwnership_drd(), VendorDetail.vendorOwnership_listDrd())
		Common.sendkeyTextToElement(VendorDetail.vendorWebSite_txt(), webSite)
		Common.HandleElementClick(VendorDetail.updateVendorInfo_btn())
		WebUI.delay(5)
	}

	public void verifyViewModeVendor(){
		Common.VerifyFieldTextEqual_func("Company Name", VendorPage.CompanyNameTitle(), vendorName)
		Common.VerifyFieldValueEqual_func("Vendor Name", VendorDetail.vendorName_txt(), vendorName)
		Common.VerifyFieldValueEqual_func("Vendor Code", VendorDetail.vendorCode_num(), vendorCode)
		Common.VerifyFieldValueEqual_func("Website", VendorDetail.vendorWebSite_txt(), webSite)
	}

	public void editRatingVendor(){
		Common.HandleElementClick(VendorDetail.editVendorRating_btn())
		WebUI.delay(3)
		Common.sendkeyTextToElement(VendorDetail.whyVendorRating_txt(), why)
		Common.HandleElementClick(VendorDetail.fiveVendorRating_icon())
		Common.HandleElementClick(VendorDetail.updateVendorRating_btn())
		WebUI.delay(3)
	}

	public void veiryRatingVendor(){
		Messages.VerifyNotificationMessageDisplay_func(Messages.updateVendor_msg)
		//Common.VerifyFieldValueEqual_func("Rating Vendor", VendorDetail.fiveVendorRating_icon() , selectedStar)
		Common.VerifyFieldTextEqual_func("Why", VendorDetail.whyVendorRating_view(), why)
	}

	public void editStatusVendor(String toggle = "", boolean status){
		Common.HandleElementClick(VendorDetail.editVendorStatus_btn())
		if(VendorDetail.active_toggle().isSelected() != status){
			Common.HandleElementClick(VendorDetail.active_toggle())
		}
		if (VendorDetail.available_toggle().isSelected() != status){
			Common.HandleElementClick(VendorDetail.available_toggle())
		}
		if (VendorDetail.international_toggle().isSelected() != status){
			Common.HandleElementClick(VendorDetail.international_toggle())
		}
		Common.HandleElementClick(VendorDetail.updateVendorStatus_btn())
	}

	public void verifyStatusVendor(String toggle, boolean status){
		boolean Active = VendorDetail.active_toggle().isSelected()
		if(Active != status){
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Toggle active is not " + toggle +  ".\n"
		}
		boolean Available = VendorDetail.available_toggle().isSelected()
		if(Available != status){
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Toggle available is not " +  toggle +  ".\n"
		}
		boolean International = VendorDetail.international_toggle().isSelected()
		if(International != status){
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Toggle international is not " +  toggle + ".\n"
		}
		println("status " + Active + Available + International)
	}


	public void editSearchOptionVendor(){
		Common.HandleElementClick(VendorDetail.searchOption_tap())
		WebUI.delay(3)
		Common.HandleElementClick(VendorDetail.editSearchOption_btn())
		Common.HandleElementClick(VendorDetail.preferredVendor_chk())
		Common.HandleElementClick(VendorDetail.useZipCodeVendor_chk())
		Common.WaitForElementEnable(VendorDetail.inputUseZipCodeVendor_chk())
		Common.sendkeyTextToElement(VendorDetail.inputUseZipCodeVendor_chk(), zipCode)
		Common.HandleElementClick(VendorDetail.excludedClients_multi())
		Common.SelectDropdownItem(VendorDetail.excludedClients_multi(), client)
		Common.HandleElementClick (VendorDetail.updateSearchOptionVendor_btn())
		WebUI.delay(3)
		Messages.VerifyNotificationMessageDisplay_func(Messages.updateVendor_msg)
	}

	public void verifyNewContactVendor(){

		Messages.VerifyNotificationMessageDisplay_func(Messages.newContactToast_msg)

		Common.VerifyFieldTextEqual_func("Full Name Contact", VendorDetail.fullNameContact(fullContactName), fullContactName)
	}

	public static void VerifyVendorExistTable_func(String searchField = "", String searchName)
	{
		Boolean exist = false
		List<String> records = Common.GetTableRecordsPerPaging_func(VendorPage.VendorList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(searchName))
			{
				exist = true
				break
			}//end if
		}//end for each

		if(exist == false)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "["+searchName+"] NOT exist in table.\n"
		}//end if
		println ("Vendor exists?: " + exist)
	}

	public static void VerifyVendorNOTExistTable_func(String searchField = "", String searchName)
	{
		Boolean exist = false
		List<String> records = Common.GetTableRecordsPerPaging_func(VendorDetail.contactList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(searchName))
			{
				exist = true
				break
			}//end if
		}//end for each

		if(exist == true)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "["+searchName+"] Should NOT exist in table.\n"
		}//end if
		println ("Vendor exists?: " + exist)
	}

	public void editContactVendor(){
		Common.HandleElementClick(VendorDetail.fullNameContact(fullContactName))
		WebUI.delay(2)
		Common.HandleElementClick(VendorDetail.editContact_btn())
		WebUI.delay(2)
		Common.sendkeyTextToElement(VendorDetail.firstNameContact_txt(), firstContactName)
		Common.sendkeyTextToElement(VendorDetail.lastNameContact_txt(), lastContactName)
		Common.SelectDropdownItem(VendorDetail.typeEmailContact_drp(), typeEmail)
		Common.sendkeyTextToElement(VendorDetail.emailContact_txt(), email)
		Common.HandleElementClick(VendorDetail.updateContact_btn())
	}

	public void verifyEditContactVendor(){
		Messages.VerifyNotificationMessageDisplay_func(Messages.updateContact_msg)
		Common.VerifyFieldValueEqual_func("First Name Vendor", VendorDetail.firstNameContact_txt(), firstContactName)
		Common.VerifyFieldValueEqual_func("Last Name Vendor", VendorDetail.lastNameContact_txt(), lastContactName)
		Common.VerifyFieldValueEqual_func("Email Vendor", VendorDetail.emailContact_txt(), email)
		Common.VerifyFieldTextEqual_func("Type Email", Common.getFirstSelectedItem(VendorDetail.typeEmailContact_drp()), typeEmail)
	} // end

	public void searchItemOnVendorPage(String searchType = "", WebElement dropdown, String searchItem){
		Common.SelectDropdownItem(dropdown, searchItem)
		Common.HandleElementClick(VendorPage.Search_btn())
	} //end

	public void searchOnlyMainVendor(String fullContactName){
		Common.sendkeyTextToElement(VendorPage.SearchContact_txt(), fullContactName )
		Common.HandleElementClick(VendorPage.OnlyMainContact())
		Common.HandleElementClick(VendorPage.Search_btn())
		WebUI.delay(5)
	}

	public void setDefaultContact(){
		if(VendorDetail.setDefaultContact_lnk().isDisplayed())
		{
			Common.HandleElementClick(VendorDetail.setDefaultContact_lnk())
		}
		WebUI.delay(5)
	}

	public void deleteContact(){
		if(VendorDetail.deleteContact_ic() .isDisplayed())
		{
			Common.HandleElementClick(VendorDetail.deleteContact_ic())
		}
		WebUI.delay(2)
		Messages.Delete_btn().click()
		WebUI.delay(3)
		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Delete Contact.")
		WebUI.delay(5)
	}

	public void clearAllDataInSearchbox(){
		Common.sendkeyTextToElement(VendorPage.SearchName_txt(), vendorName)
		Common.sendkeyTextToElement(VendorPage.SearchContact_txt() , fullContactName)
		Common.sendkeyTextToElement(VendorPage.SearchZipcode_num(), vendorCode)
		Common.sendkeyTextToElement(VendorPage.SearchAddress1_txt(), address1)
		Common.sendkeyTextToElement(VendorPage.SearchCity_txt(), city)
		Common.HandleElementClick(VendorPage.Search_btn())
		WebUI.delay(3)
		Common.VerifyFieldTextEqual_func("No Results", VendorPage.NoResultMsg_lb(), noResutlMsg)
		Common.HandleElementClick(VendorPage.Clear_btn())
	}

	public void verifyEmptyDatainSearchbox(){
		Common.VerifyFieldTextEqual_func("Search name", VendorPage.SearchName_txt(), "")
		Common.VerifyFieldTextEqual_func("Search zipcode", VendorPage.SearchZipcode_num(), "")
		Common.VerifyFieldTextEqual_func("Search contact", VendorPage.SearchContact_txt(), "")
		Common.VerifyFieldTextEqual_func("Search address1", VendorPage.SearchAddress1_txt(), "")
		Common.VerifyFieldTextEqual_func("Search city", VendorPage.SearchCity_txt(), "")
	}

	public void openVendorDetail(){
		this.searchVendor(vendorName,VendorPage.SearchName_txt())
		Navigation.gotoFirstVendorDetailsPage()
		WebUI.delay(5)
	}

	public void checkValidationMsgNewVendor(){
		Common.HandleElementClick(VendorPage.NewVendor_btn())
		WebUI.delay(2)
		Common.HandleElementClick(VendorPage.SubmitNewVendor_btn())
		WebUI.delay(2)

		Common.VerifyFieldTextEqual_func("New Vendor Name", VendorPage.NewVendorNameMsg(), NewVendorNameMsg)
		Common.VerifyFieldTextEqual_func("New Vendor Code", VendorPage.NewVendorCodeMsg(), NewVendorCodeMsg)
		Common.VerifyFieldTextEqual_func("New Vendor Service", VendorPage.NewVendorServiceMsg(), NewVendorServiceMsg)
	}

	public void checkValidationMsgNewContactVendor(){
		Common.HandleElementClick(VendorDetail.newContacts_btn())
		WebUI.delay(2)
		Common.HandleElementClick(VendorDetail.submitNewContacts_btn())
		WebUI.delay(2)
		Common.VerifyFieldTextEqual_func("New First Name Contact", VendorDetail.newFirstNameContact_msg(), NewFirstNameContactMsg)
		Common.VerifyFieldTextEqual_func("New Last Name Contact", VendorDetail.newLastNameContact_msg(), NewLastNameContactCodeMsg)
		Common.VerifyFieldTextEqual_func("New State Contact", VendorDetail.newStateContact_msg(), NewStateContactMsg)
	}

	public void checkValidationNewNoteModal(){
		Common.HandleElementClick(VendorDetail.note_tab())
		WebUI.delay(2)
		Common.HandleElementClick(VendorDetail.newNote_btn())
		WebUI.delay(2)
		Common.HandleElementClick(VendorDetail.submitNewNote_btn())
		WebUI.delay(2)
		Common.VerifyFieldTextEqual_func("Contact Note", VendorDetail.newContactNote_msg(), NewNoteContactMsg)
		Common.VerifyFieldTextEqual_func("Subject Note", VendorDetail.newSubjectNote_msg(), NewNoteSubjectMsg)
		Common.VerifyFieldTextEqual_func("Content Note", VendorDetail.newContentNote_msg(), NewNoteContentMsg)
	}

	public void checkValidationMsgNewDocument(){
		Common.HandleElementClick(VendorDetail.document_tab())
		WebUI.delay(2)
		Common.HandleElementClick(VendorDetail.newDocument_btn())
		WebUI.delay(2)
		Common.HandleElementClick(VendorDetail.submitNewDocument_btn())
		WebUI.delay(2)
		Common.VerifyFieldTextEqual_func("Title Document", VendorDetail.newFileDoucment_msg(), NewDocumentFileMsg)
	}


	//////===========================METHOD TO FILL VALUE INFO NEW MODAL DETAILS

	public void FillModalInfo_func(String button = "Submit"){

		Common.sendkeyTextToElement(VendorPage.NewVendorName(), vendorName)
		WebUI.delay(1)
		Common.selectRandomItemDropdown(VendorPage.SelectServiceNewVendor(), VendorPage.NewVendorService_listDrd())
		Common.sendkeyTextToElement(VendorPage.NewVendorCode(), vendorCode)
		Common.sendkeyTextToElement(VendorPage.NewVendorWebsite_txt(), webSite)
		WebUI.delay(2)

		if(button == "Submit")
			Common.HandleElementClick(VendorPage.SubmitNewVendor_btn())
		WebUI.delay(3)

		if (VendorPage.ExistVendor_lb().isDisplayed() == true){
			Common.HandleElementClick(VendorPage.CreateNewVendor_btn())
		}
		WebUI.delay(5)
	}

}
