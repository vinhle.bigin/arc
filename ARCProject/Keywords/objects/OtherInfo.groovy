package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.Transf_OtherInfoTab

public class OtherInfo {

	//MOVING INFO
	public String PropertyType
	public String MoveReason
	public String MoveType
	public String FileType
	public String SaleType
	public String ProgramType

	//Dates
	public String InitDate
	public String EffectDate
	public String InterMoveInDate
	public String InterMoveOutDate
	public String FullConvers
	public String ExpectedMoveByDate
	public String MoveEnd
	public String JobStart

	//EMPLOYEE INFO
	public String EmployeeNumb
	public String VendorNumb
	public String FileNumb
	public String GeneralLedger

	//Currency and Countries
	public String PrefCurrency
	public String HomeCurrency
	public String HostCurrency
	public String PriCountry
	public String SecCountry
	public String HomeCountry
	public String HostCountry
	public String OldCountry
	public String NewCountry

	//Transferee Other info
	public String SSN
	public String BirthDate
	public String PoBox
	public String Nation
	public OtherInfo() {


		PropertyType = "Rental"
		MoveReason = "Group"
		MoveType = "Mandated"
		FileType = "International"
		SaleType = "Inventory"
		ProgramType = "Lump Sum"

		DateTimeFuncs.currentDate = DateTimeFuncs.GetCurrentLocalDate_Date()

		DateTimeFuncs.GetCurrentLocalDate_Date()
		InitDate = DateTimeFuncs.GetCurrentLocalDate_Str()

		Date dt_tmp = DateTimeFuncs.IncreaseDay(DateTimeFuncs.currentDate, 1)

		EffectDate = DateTimeFuncs.ConvertDatetoString(dt_tmp)

		dt_tmp = DateTimeFuncs.IncreaseDay(DateTimeFuncs.currentDate, 2)

		InterMoveInDate = DateTimeFuncs.ConvertDatetoString(dt_tmp)

		dt_tmp = DateTimeFuncs.IncreaseDay(DateTimeFuncs.currentDate, 3)
		InterMoveOutDate = DateTimeFuncs.ConvertDatetoString(dt_tmp)

		//dt_tmp = DateTimeFuncs.IncreaseDay(DateTimeFuncs.currentDate, 3)
		FullConvers = DateTimeFuncs.ConvertDatetoString(dt_tmp)

		dt_tmp = DateTimeFuncs.IncreaseDay(DateTimeFuncs.currentDate, 4)
		ExpectedMoveByDate = DateTimeFuncs.ConvertDatetoString(dt_tmp)

		dt_tmp = DateTimeFuncs.IncreaseDay(DateTimeFuncs.currentDate, 5)
		MoveEnd = DateTimeFuncs.ConvertDatetoString(dt_tmp)

		dt_tmp = DateTimeFuncs.IncreaseDay(DateTimeFuncs.currentDate, 6)
		JobStart = DateTimeFuncs.ConvertDatetoString(dt_tmp)

		//EMPLOYEE INFO
		String t_tmp = DateTimeFuncs.getCurrentLocalTime()
		EmployeeNumb = "EM_"+ t_tmp
		VendorNumb = "VN_" + t_tmp
		FileNumb = "FN_" + t_tmp
		GeneralLedger = "GL For testing."

		//Currency and Countries
		PrefCurrency = "USD - United States of America"
		HomeCurrency = "BRL - Brazil"
		HostCurrency = "ALL - Albania"
		PriCountry = "Algeria"
		SecCountry = "Viet Nam"
		HomeCountry = "Aruba"
		HostCountry = "Bolivia"
		OldCountry = "Belize"
		NewCountry = "Sweden"

		//Transferee Other info
		SSN = t_tmp
		BirthDate = "07/15/1987"
		PoBox = "PO_001"
		Nation = "United States"


	}

	public void Edit_TransfOtherInfo_func()
	{

		String expected_msg = "Transferee has been updated successfully."

		Transf_OtherInfoTab tab = new Transf_OtherInfoTab()

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.HandleElementClick(tab.Other_tab())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.Move_Edit_btn())

		Common.HandleElementClick(tab.Move_Edit_btn())
		
		Common.ScrollElementtoViewPort_func(tab.PropertyType_ddl())

		Common.SelectDropdownItem(tab.PropertyType_ddl(), this.PropertyType)

		Common.SelectDropdownItem(tab.MoveReason_ddl(),  this.MoveReason)

		Common.SelectDropdownItem(tab.FileType_ddl(),  this.FileType)

		Common.SelectDropdownItem(tab.MoveType_ddl(),  this.MoveType)

		Common.SelectDropdownItem(tab.SaleType_ddl(),  this.SaleType)

		Common.SelectDropdownItem(tab.ProgramType_ddl(),  this.ProgramType)

		tab.Move_Update_btn().click()

		Messages.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update")

		//================DATES INFO

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.Date_Edit_btn())

		Common.HandleElementClick(tab.Date_Edit_btn())
		WebUI.delay(2)
		Common.ScrollElementtoViewPort_func(tab.InitDate_txt())
		Common.ActionSendkey(tab.InitDate_txt(),  this.InitDate)

		Common.ActionSendkey(tab.EffectDate_txt(),  this.EffectDate)

		//Common.HandleElementClick(tab.InterMoveInDate_txt())
		Common.ActionSendkey(tab.InterMoveInDate_txt(),  this.InterMoveInDate)

		//Common.HandleElementClick(tab.InterMoveOutDate_txt())
		Common.ActionSendkey(tab.InterMoveOutDate_txt(),  this.InterMoveOutDate)

		//Common.HandleElementClick(tab.FullConvrsDate_txt())
		Common.ActionSendkey(tab.FullConvrsDate_txt(),  this.FullConvers)

		//Common.HandleElementClick(tab.ExpectDate_txt())
		Common.ActionSendkey(tab.ExpectDate_txt(),  this.ExpectedMoveByDate)

		//Common.HandleElementClick(tab.MoveEndDate_txt())
		Common.ActionSendkey(tab.MoveEndDate_txt(),  this.MoveEnd)

		//Common.HandleElementClick(tab.JobStart_txt())
		Common.ActionSendkey(tab.JobStart_txt(),  this.JobStart)


		tab.Date_Update_btn().click()

		Messages.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update")


		//==================EMPLOYEE INFO
		Common.HandleElementClick(tab.Empl_Edit_btn())

		
		Common.ScrollElementtoViewPort_func(tab.EmplNumb_txt())
		Common.sendkeyTextToElement(tab.EmplNumb_txt(),  this.EmployeeNumb)

		
		//Common.sendkeyTextToElement(tab.VendorNumb_txt(),  this.VendorNumb) --Obsoleted

		//Common.sendkeyTextToElement(tab.FileNumb_txt(),  this.FileNumb)

		//Common.sendkeyTextToElement(tab.GeneralLedger_txt(),  this.GeneralLedger)

		Common.HandleElementClick(tab.Empl_Update_btn())

		Messages.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update")


		//===========Currency and Countries
		Common.HandleElementClick(tab.Curcy_Edit())

		Common.ScrollElementtoViewPort_func(tab.PrefCurrency_dll())
		Common.SelectDropdownItem(tab.PrefCurrency_dll(),  this.PrefCurrency)
		
		Common.ScrollElementtoViewPort_func(tab.HomeCurrency_dll())
		Common.SelectDropdownItem(tab.HomeCurrency_dll(), this.HomeCurrency)
		
		Common.ScrollElementtoViewPort_func(tab.PrefCurrency_dll())
		Common.SelectDropdownItem(tab.HostCurrency_dll(), this.HostCurrency)

		Common.ScrollElementtoViewPort_func(tab.PriCountry_dll())
		Common.SelectDropdownItem(tab.PriCountry_dll(), this.PriCountry)
		
		Common.ScrollElementtoViewPort_func(tab.SecCountry_dll())
		Common.SelectDropdownItem(tab.SecCountry_dll(), this.SecCountry)
		
		Common.ScrollElementtoViewPort_func(tab.HomeCountry_dll())
		Common.SelectDropdownItem(tab.HomeCountry_dll(), this.HomeCountry)

		Common.ScrollElementtoViewPort_func(tab.HostCountry_dll())
		Common.SelectDropdownItem(tab.HostCountry_dll(), this.HostCountry)
		
		Common.ScrollElementtoViewPort_func(tab.OldCountry_dll())
		Common.SelectDropdownItem(tab.OldCountry_dll(), this.OldCountry)
		
		Common.ScrollElementtoViewPort_func(tab.NewCountry_dll())
		Common.SelectDropdownItem(tab.NewCountry_dll(), this.NewCountry)

		Common.HandleElementClick(tab.Curcy_Update_btn())

		Messages.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update")

		//======================Transferee Other Info
		Common.HandleElementClick(tab.Other_Edit())
		Common.ScrollElementtoViewPort_func(tab.SSN_txt())
		Common.sendkeyTextToElement(tab.SSN_txt(), this.SSN)
		Common.sendkeyTextToElement(tab.POBox_txt(), this.PoBox)
		Common.ActionSendkey(tab.BirthDate_txt(), this.BirthDate)
		Common.SelectDropdownItem(tab.Nation_txt(), this.Nation)

		Common.HandleElementClick(tab.Other_Update_btn())

		Messages.VerifyNotificationMessageDisplay_func (expected_msg,"Unsuccessfull Service - Info update")


	}//end void
}
