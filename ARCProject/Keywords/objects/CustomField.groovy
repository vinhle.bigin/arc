package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action as Action

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.ActivityTab
import pages_elements.Cus_CustomFieldTab

public class CustomField {
	public String FieldType

	public String FieldName

	public String ControlType

	public Boolean IsActive

	public Boolean IsRequire

	public List<String> OptionList

	public List<String> ValueList

	public String FieldID
	
	public CustomGroup custom_group




	public CustomField(String fieldtype_str = "Textbox") {
		DateTimeFuncs dt = new DateTimeFuncs()
		FieldName = "Field_"+dt.getCurrentLocalTime()

		FieldType = "Transferee"
		ControlType = fieldtype_str // Checkbox, Date Picker, Dropdown, Multiselect, Numeric Field, Radio Button, TextArea, Textbox
		IsActive = true
		IsRequire = false
		OptionList = new ArrayList<>()
		String pre_fix = ""
		ValueList = new ArrayList<>()
		String value_str = "this is for testing"
		ValueList.add(value_str)

		FieldID = ""
		custom_group = null

		switch(ControlType)
		{
			case "Dropdown":
				pre_fix = "ddl_"

				break

			case "Multiselect":
				pre_fix = "mddl_"
				break

			case "Radio Button":
				pre_fix = "radi_"
				break

		}

		dt = new DateTimeFuncs()

		String time_str = dt.getCurrentLocalTime()

		for(int i =0;i<2;i++)
		{
			String tmp_str = pre_fix+time_str+"_"+i
			OptionList.add(tmp_str)

		}//end for

	}//end void


	public void Create_func(String button= "Submit")
	{
		Cus_CustomFieldTab tab = new Cus_CustomFieldTab()
		tab.ManageCustField_tab().click()
		tab.New_btn().click()

		tab.FillModalInfo_func(this, "Create")

		if(button=="Submit")
		{
			tab.Dtail_Submit_btn().click()

			WebUI.delay(1)

			Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "Create new Custom Field")
		}
		else if(button=="Cancel")
		{
			tab.Dtail_Cancel_btn().click()
		}

	}//end void

	public void Update_func(String oldfieldname , String button= "Submit")
	{
		Cus_CustomFieldTab tab = new Cus_CustomFieldTab()

		tab.FieldName_lnk(oldfieldname).click()

		tab.FillModalInfo_func(this, "Update")

		if(button=="Submit")
		{
			tab.Dtail_Submit_btn().click()

			WebUI.delay(1)

			Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "Update Custom Field")
		}
		else if(button=="Cancel")
		{
			tab.Dtail_Cancel_btn().click()
		}

	}//end void


	//ASSIGN GROUP TO CUSTOM FIELD
	public void AssignGroup_func(CustomGroup group)
	{
		this.FieldType = group.FieldType
		
		this.custom_group = group
	}


	//Remove the Custom Field

	public void Remove_func()
	{
		Cus_CustomFieldTab.SearchCustomField_func(this.FieldName)
		Cus_CustomFieldTab.Remove_icon(this.FieldName).click()
		Messages.Delete_btn().click()
		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When Remove Custom Field.\n")

	}

	public void GetFieldID_func()
	{
		Cus_CustomFieldTab.SearchCustomField_func(this.FieldName)

		String vlue_str = Cus_CustomFieldTab.FieldName_lnk(this.FieldName).getAttribute("onclick")
		
		vlue_str = vlue_str.toString().replace("vueApp.\$router.push({name:'edit', params: { id:", "")
		
		vlue_str = vlue_str.toString().replace(" } })", "")
		
		this.FieldID = vlue_str
	}

}
