package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action as Action

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.ActivityTab
import pages_elements.Cus_ManageGroupTab

public class CustomGroup {
	private String FieldType

	private String Tab

	private String Name


	public CustomGroup() {

		FieldType = "Transferee" //Client, Vendor
		DateTimeFuncs dt = new DateTimeFuncs()
		Name = "Group_"+dt.getCurrentLocalTime()
		Tab = "Custom Fields"

	}


	public void CreateGroup_func() {
		Cus_ManageGroupTab tab = new Cus_ManageGroupTab()

		Common.WaitForElementDisplay(tab.ManageGroup_tab())
		
		tab.ManageGroup_tab().click()

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.GroupList_tbl())

		tab.NewGroup_btn().click()

		tab.FillModalInfo_func(this)

		tab.ModalCreate_btn().click()

		WebUI.delay(2)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "create new custom group")
	}

	public void UpdateGroup_func(String oldgroupname_str) {
		Cus_ManageGroupTab tab = new Cus_ManageGroupTab()

		tab.ManageGroup_tab().click()

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.GroupList_tbl())

		tab.Edit_icon(oldgroupname_str).click()

		tab.FillModalInfo_func(this,"Update")

		tab.ModalUpdate_btn().click()

		WebUI.delay(2)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "update new custom group")
	}



	public void RemoveGroup_func() {

		Cus_ManageGroupTab tab = new Cus_ManageGroupTab()

		tab.Remove_icon(this.Name)

		Messages.ConfirmYes_btn().click()

		WebUI.delay(2)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "remove custom group")
	}
}
