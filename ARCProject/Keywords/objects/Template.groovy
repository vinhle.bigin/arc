package objects

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import functions.Navigation
import internal.GlobalVariable
import pages_elements.LandingPage
import pages_elements.Messages
import pages_elements.TemplatePage
import objects.Transferee
import objects.Client

public class Template {

	public String Name

	public String Type
	public String Target
	public String ServiceType
	public String EmailFrom
	public String Description
	public String Subject
	public String Content
	public String DocName
	public Boolean IsActive
	public String CodeField_Content_str
	public String DocProcessType



	public Template() {
		Type = "Email"
		Target = "Transferee"
		ServiceType = "General"
		EmailFrom = "Coordinator"//"Assistant"
		DateTimeFuncs dt = new DateTimeFuncs()
		Name = Type+ "-Template_" + dt.getCurrentLocalTime()
		Subject = "QA_Testing"+ Name
		Content = "This is for testing."
		IsActive=true

		DocName = ""//"Email_Template_Doc.docx"
		DocProcessType = "DOCX" //"PDF"




	}

	//METHOD

	public void Remove_func(String button = "Submit")
	{
		TemplatePage page_tmp = new TemplatePage()

		page_tmp.Search_func(this)

		Boolean exist  = Common.IsItemExistTable(page_tmp.Templ_List_tbl(), this.Name)

		if(exist ==true)
		{
			page_tmp.Remove_icon(this.Name).click
			if(Messages.ConfirmYes_btn()!=null&&button =="Submit")
			{
				Messages.ConfirmYes_btn().click()

				Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg)
			}//end if
			else
				Messages.ConfirmNo_btn().click()
		}//end if exist
		else
		{
			//GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Not Found template to remove.\n"

			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}//end else exist
	}//end void



	public void Create_func(String button = "Submit")
	{
		TemplatePage page_tmp = new TemplatePage()

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)


		Common.WaitForElementDisplay(page_tmp.New_btn())

		Common.HandleElementClick(page_tmp.New_btn())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(page_tmp.Edit_ServiceType_ddl())

		page_tmp.FillInfo_func("Create",this,button)


	}//end void


	public Template Update_func(Template newTemplate,String button = "Submit")
	{
		Template tpl_tmp = newTemplate

		TemplatePage page_tmp = new TemplatePage()

		page_tmp.Search_func(this)

		Navigation.GotoTemplateDetailsPage_func(this.Name)

		page_tmp.FillInfo_func("Update",tpl_tmp,button)

		return tpl_tmp
	}//end void


	//==this method to handle filling the code field into Template Content text field
	public void AddFieldCode_TransfGenInfo_func()
	{
		String Content_str = ""

		//Content_str += "Trans ID: \${transferee=id}\n"
		Content_str += "Trans FName: \${transferee=first_name}\n"
		Content_str += "Trans LName: \${transferee=last_name}\n"
		Content_str += "Trans FullName: \${transferee=full_name}\n"
		//Content_str += "Trans JobTitle: \\n"
		Content_str += "Trans Coordinator: \${coordinator=fullname}\n"
		Content_str += "Trans Coor Assistant: \${coordinator_assistant=fullname}\n"
		Content_str += "Client Name: \${transferee=client}\n"
		//Content_str += "Client Cotanct: \\n"
		Content_str += "Trans Status:\${transferee=status}\n"
		//Content_str += "Trans Status Change Date: \n"
		//Content_str += "Trans IsOveride: \n"
		//Content_str += "Trans IsQualified: \n"
		//Content_str += "Trans FileEnter CreateDate: \n"
		Content_str += "Trans IsReceiveSignABAD: \${transferee=is_received_signed_abad}\n"
		Content_str += "Trans IsReferListSide: \${transferee=is_referred_list_side}\n"
		Content_str += "Trans IsReferBuySide: \${transferee=is_referred_buy_side}\n"
		Content_str += "Trans IsReferToTitleComp: \${transferee=is_referred_to_title_company}\n"
		Content_str += "Trans Mobile PhoneNumb: \${transferee=mobile_phone}\n"
		Content_str += "Trans WorkEmail: \${transferee=work_email}"

		this.Content = Content_str


	}


	//Add Fieldcode of Transferee Other Info into Content
	public void AddFieldCode_TransfOtherInfo_func()
	{
		String Content_str = ""

		Content_str+="MoveType: \${transferee=move_type}"

		Content_str+="\nInItDate: \${transferee=initiation_date}"

		Content_str+="\nEffectDate: \${transferee=effective_date}"

		Content_str+="\nMoveInDate: \${transferee=interim_move_in_date}"

		Content_str+="\nMoveOutDate: \${transferee=interim_move_out_date}"

		Content_str+="\nExpectMoveDate: \${transferee=expected_move_date}"

		Content_str+="\nMoveEnd: \${transferee=move_end_date}\n"


		this.Content = Content_str


	}


	//Add Fieldcode of Transferee Other Info into Content
	public void AddFieldCode_TransfAddrInfo_func()
	{
		String Content_str = ""

		Content_str+= "OrgRes_Addr1: \${transferee=origin_residence_address1}"

		Content_str+= "\nOrgRes_Addr2: \${transferee=origin_residence_address2}"

		Content_str+= "\nOrgRes_City: \${transferee=origin_residence_city}"

		Content_str+= "\nOrgRes_State: \${transferee=origin_residence_state}"

		Content_str+= "\nOrgRes_Zip: \${transferee=origin_residence_zipcode}"

		Content_str+= "\nOrgRes_Country: \${transferee=origin_residence_country}"

		//Orginal Office Info
		Content_str+= "\nOrgOff_Addr1: \${transferee=origin_office_address1}"

		Content_str+= "\nOrgOff_Addr2: \${transferee=origin_office_address2}"

		Content_str+= "\nOrgOff_City: \${transferee=origin_office_city}"

		Content_str+= "\nOrgOff_State: \${transferee=origin_office_state}"

		Content_str+= "\nOrgOff_Zip: \${transferee=origin_office_zipcode}"

		Content_str+= "\nOrgOff_Country: \${transferee=origin_office_country}"

		//Destination Residient Info
		Content_str+= "\nDesRes_Addr1: \${transferee=destination_office_address1}"

		Content_str+= "\nDesRes_Addr2: \${transferee=destination_office_address2}"

		Content_str+= "\nDesRes_City: \${transferee=destination_office_city}"

		Content_str+= "\nDesRes_State: \${transferee=destination_office_state}"

		Content_str+= "\nDesRes_Zip: \${transferee=destination_office_zipcode}"

		Content_str+= "\nDesRes_Country: \${transferee=destination_office_country}"

		//Destionation Office Info
		Content_str+= "\nDesOff_Addr1: \${transferee=destination_residence_address1}"

		Content_str+= "\nDesOff_Addr2: \${transferee=destination_residence_address2}"

		Content_str+= "\nDesOff_City: \${transferee=destination_residence_city}"

		Content_str+= "\nDesOff_State: \${transferee=destination_residence_state}"

		Content_str+= "\nDesOff_Zip: \${transferee=destination_residence_zipcode}"

		Content_str+= "\nDesOff_Country: \${transferee=destination_residence_country}"

		this.Content = Content_str


	}//end void


	public void AddFieldCode_TransfHomeSaleInfo_func()
	{
		String Content_str = ""

		Content_str+= "HS_TookBuyOut: \${service_home_sale=took_buyout}"
		Content_str+= "\nHS_ListPrice: \${service_home_sale=list_price}"
		Content_str+= "\nHS_CommissionPercent: \${service_home_sale=commission_percent}"
		Content_str+= "\nHS_ListStartDate: \${service_home_sale=listing_start_date}"
		Content_str+= "\nHS_ListEndDate: \${service_home_sale=listing_end_date}"
		Content_str+= "\nHS_ClosingDate: \${service_home_sale=closing_date}"
		Content_str+= "\nHS_RebateAmount: \${service_home_sale=rebate_amount}"
		Content_str+= "\nHS_DateBuyOutAccept: \${service_home_sale=date_buy_out_accepted}"
		Content_str+= "\nHS_EscrowFeePercent: \${service_home_sale=escrow_fee_percent}"
		Content_str+= "\nHS_EscowFeeAmount: \${service_home_sale=escow_fee_ammount}"
		Content_str+= "\nHS_Rebate: \${service_home_sale=rebate}"
		Content_str+= "\nHS_IsInvoiceCreate: \${service_home_sale=is_invoice_created}"
		Content_str+= "\nHS_InvoiceClosedOut: \${service_home_sale=is_invoice_closed_out}"
		Content_str+= "\nHS_ReffeeReceived: \${service_home_sale=referral_fee_received}"
		Content_str+= "\nHS_SignedRefFeeAgree: \${service_home_sale=signed_referral_fee_agreeement}"
		Content_str+= "\nHS_SelectReloDirectVendorName: \${service_home_sale=selected_relo_director_vendor_name}"
		Content_str+= "\nHS_SelectReloDirectVendorContactFirstName: \${service_home_sale=selected_relo_director_vendor_contact_first_name}"
		Content_str+= "\nHS_SelectReloDirectVendorContactLastName: \${service_home_sale=selected_relo_director_vendor_contact_last_name}"
		Content_str+= "\nHS_SelectReloDirectVendorContactFullName: \${service_home_sale=selected_relo_director_vendor_contact_full_name}"
		Content_str+= "\nHS_SelectReloDirectVendorContactPhone: \${service_home_sale=selected_relo_director_vendor_contact_phone}"
		Content_str+= "\nHS_SelectReloDirectVendorContactEmail: \${service_home_sale=selected_relo_director_vendor_contact_email}"
		Content_str+= "\nHS_SelectAgentVendorName: \${service_home_sale=selected_agent_vendor_name}"
		Content_str+= "\nHS_SelectAgentVendorContactFirstName: \${service_home_sale=selected_agent_vendor_contact_first_name}"
		Content_str+= "\nHS_SelectAgentVendorContactLastName: \${service_home_sale=selected_agent_vendor_contact_last_name}"
		Content_str+= "\nHS_SelectAgentVendorContactLFullName: \${service_home_sale=selected_agent_vendor_contact_full_name}"
		Content_str+= "\nHS_SelectAgentVendorContactPhone: \${service_home_sale=selected_agent_vendor_contact_phone}"
		Content_str+= "\nHS_SelectAgentVendorContactEmail: \${service_home_sale=selected_agent_vendor_contact_email}"
		Content_str+= "\nHS_SelectTitleClosingVendorName: \${service_home_sale=selected_title/closing_vendor_name}"
		Content_str+= "\nHS_SelectTitleClosingVendorContactFirstName: \${service_home_sale=selected_title/closing_vendor_contact_first_name}"
		Content_str+= "\nHS_SelectTitleClosingVendorLastName: \${service_home_sale=selected_title/closing_vendor_contact_last_name}"
		Content_str+= "\nHS_SelectTitleClosingVendorContactPhone: \${service_home_sale=selected_title/closing_vendor_contact_phone}"
		Content_str+= "\nHS_SelectTitleClosingVendorContactFullName: \${service_home_sale=selected_title/closing_vendor_contact_full_name}"
		Content_str+= "\nHS_SelectTitleClosingVendorContactEmail: \${service_home_sale=selected_title/closing_vendor_contact_email}"

		this.Content = Content_str
	}//end void


	public void AddFieldCode_TransfHomePurchaseInfo_func()
	{
		String Content_str = ""
		Content_str+= "HP_IsSignRefFeeAgree: \${service_home_purchase=is_signed_referral_fee_agreement}"
		Content_str+= "\nHP_SalePrice: \${service_home_purchase=sale_price}"
		Content_str+= "\nHP_LoanAmount: \${service_home_purchase=loan_amount}"
		Content_str+= "\nHP_Commission: \${service_home_purchase=commission}"
		Content_str+= "\nHP_RebateAmount: \${service_home_purchase=rebate_amount}"
		Content_str+= "\nHP_CloseDate: \${service_home_purchase=closing_date}"
		Content_str+= "\nHP_HuntTripDate: \${service_home_purchase=house_hunting_trip_date}"
		Content_str+= "\nHP_EscrowFeeAmount: \${service_home_purchase=escrow_fee_amount}"
		Content_str+= "\nHP_EscrowfePercent: \${service_home_purchase=escrow_fee_percent}"
		Content_str+= "\nHP_IsRebateSent: \${service_home_purchase=is_rebate_sent}"
		Content_str+= "\nHP_IsInvoiceCloseOut: \${service_home_purchase=is_invoice_closed_out}"
		Content_str+= "\nHP_IsEscrowFeeReceive: \${service_home_purchase=is_escrow_fee_received}"
		Content_str+= "\nHP_IsInvoiceCreate: \${service_home_purchase=is_invoice_created}"
		Content_str+= "\nHP_IsTransfereeRent: \${service_home_purchase=is_transferee_renting}"
		Content_str+= "\nHP_ReloVendorContactFirstName: \${service_home_purchase=selected_relo_director_vendor_contact_first_name}"
		Content_str+= "\nHP_ReloVendorContactMid: \${service_home_purchase=selected_relo_director_vendor_contact_mid_name}"
		Content_str+= "\nHP_ReloVendorContactLastName: \${service_home_purchase=selected_relo_director_vendor_contact_last_name}"
		Content_str+= "\nHP_ReloVendorContactFullName: \${service_home_purchase=selected_relo_director_vendor_contact_full_name}"
		Content_str+= "\nHP_ReloVendorContactPhone: \${service_home_purchase=selected_relo_director_vendor_contact_phone}"
		Content_str+= "\nHP_AgentVendorName: \${service_home_purchase=selected_agent_vendor_name}"
		Content_str+= "\nHP_AgentVendorContactFirstName: \${service_home_purchase=selected_agent_vendor_contact_first_name}"
		Content_str+= "\nHP_AgentVendorContactMidName: \${service_home_purchase=selected_agent_vendor_contact_mid_name}"
		Content_str+= "\nHP_AgentVendorContactLastName: \${service_home_purchase=selected_agent_vendor_contact_last_name}"
		Content_str+= "\nHP_AgentVendorContactFullName: \${service_home_purchase=selected_agent_vendor_contact_full_name}"
		Content_str+= "\nHP_AgentVendorContactPhone: \${service_home_purchase=selected_agent_vendor_contact_phone}"
		Content_str+= "\nHP_AgentVendorContactEmail: \${service_home_purchase=selected_agent_vendor_contact_email}"
		Content_str+= "\nHP_closing_vendor_name: \${service_home_purchase=selected_title/closing_vendor_name}"
		Content_str+= "\nHP_closing_vendor_contact_first_name: \${service_home_purchase=selected_title/closing_vendor_contact_first_name}"
		Content_str+= "\nHP_closing_vendor_contact_mid_name: \${service_home_purchase=selected_title/closing_vendor_contact_mid_name}"
		Content_str+= "\nHP_closing_vendor_contact_last_name: \${service_home_purchase=selected_title/closing_vendor_contact_last_name}"
		Content_str+= "\nHP_closing_vendor_contact_full_name: \${service_home_purchase=selected_title/closing_vendor_contact_full_name}"
		Content_str+= "\nHP_closing_vendor_contact_phone: \${service_home_purchase=selected_title/closing_vendor_contact_phone}"
		Content_str+= "\nHP_closing_vendor_contact_email: \${service_home_purchase=selected_title/closing_vendor_contact_email}"


		this.Content = Content_str
	}//end void


	public void AddFieldCode_TransfHHGoodInfo_func()
	{
		String Content_str = ""
		Content_str+= "HHG_destination_address1: \${service_hhg=destination_address1}"
		Content_str+= "\nHHG_destination_address2: \${service_hhg=destination_address2}"
		Content_str+= "\nHHG_house_hunting_trip_date: \${service_home_purchase=house_hunting_trip_date}"
		Content_str+= "\nHHG_destination_city: \${service_hhg=destination_city}"
		Content_str+= "\nHHG_destination_zipcode: \${service_hhg=destination_zipcode}"
		Content_str+= "\nHHG_destination_state: \${service_hhg=destination_state}"
		Content_str+= "\nHHG_hhg_move_date: \${service_hhg=hhg_move_date}"
		Content_str+= "\nHHG_hhg_delivery_date: \${service_hhg=hhg_delivery_date}"
		Content_str+= "\nHHG_origin_address1: \${service_hhg=origin_address1}"
		Content_str+= "\nHHG_origin_address2: \${service_hhg=origin_address2}"
		Content_str+= "\nHHG_origin_city: \${service_hhg=origin_city}"
		Content_str+= "\nHHG_origin_state: \${service_hhg=origin_state}"
		Content_str+= "\nHHG_origin_zipcode: \${service_hhg=origin_zipcode}"
		Content_str+= "\nHHG_origin_country: \${service_hhg=origin_country}"
		Content_str+= "\nHHG_destination_country: \${service_hhg=destination_country}"
		Content_str+= "\nHHG_payment_type: \${service_hhg=payment_type}"
		Content_str+= "\nHHG_selected_vendor_name: \${service_hhg=selected_vendor_name}"
		Content_str+= "\nHHG_selected_vendor_contact_first_name: \${service_hhg=selected_vendor_contact_first_name}"
		Content_str+= "\nHHG_selected_vendor_contact_mid_name: \${service_hhg=selected_vendor_contact_mid_name}"
		Content_str+= "\nHHG_selected_vendor_contact_last_name: \${service_hhg=selected_vendor_contact_last_name}"
		Content_str+= "\nHHG_selected_vendor_contact_full_name: \${service_hhg=selected_vendor_contact_full_name}"
		Content_str+= "\nHHG_selected_vendor_contact_phone: \${service_hhg=selected_vendor_contact_phone}"
		Content_str+= "\nHHG_selected_vendor_contact_email: \${service_hhg=selected_vendor_contact_email}"



		this.Content = Content_str
	}//end void


	public void AddFieldCode_TransfMortgageInfo_func()
	{
		String Content_str = ""
		Content_str+= "MG_mortgage_amount: \${service_mortgage=mortgage_amount}"
		Content_str+= "\nMG_index_rate: \${service_mortgage=index_rate}"
		Content_str+= "\nMG_amt_needed_at_closing: \${service_mortgage=amt_needed_at_closing}"
		Content_str+= "\nMG_down_payment: \${service_mortgage=down_payment}"
		Content_str+= "\nMG_closing_date: \${service_mortgage=closing_date}"
		Content_str+= "\nMG_selected_vendor_name: \${service_mortgage=selected_vendor_name}"
		Content_str+= "\nMG_selected_vendor_contact_first_name: \${service_mortgage=selected_vendor_contact_first_name}"
		Content_str+= "\nMG_selected_vendor_contact_mid_name: \${service_mortgage=selected_vendor_contact_mid_name}"
		Content_str+= "\nMG_selected_vendor_contact_last_name: \${service_mortgage=selected_vendor_contact_last_name}"
		Content_str+= "\nMG_selected_vendor_contact_full_name: \${service_mortgage=selected_vendor_contact_full_name}"
		Content_str+= "\nMG_selected_vendor_contact_phone: \${service_mortgage=selected_vendor_contact_phone}"
		Content_str+= "\nMG_selected_vendor_contact_email: \${service_mortgage=selected_vendor_contact_email}"


		this.Content = Content_str
	}//end void


	public void AddFieldCode_TransfTempQuarterInfo_func()
	{
		String Content_str = ""
		Content_str+= "TQ_move_out_date: \${service_temporary_quarter=move_out_date}"
		Content_str+= "\nTQ_move_in_date: \${service_temporary_quarter=move_in_date}"
		Content_str+= "\nTQ_pet: \${service_temporary_quarter=pet}"
		Content_str+= "\nTQ_apartment_type: \${service_temporary_quarter=apartment_type}"
		Content_str+= "\nTQ_baths: \${service_temporary_quarter=baths}"
		Content_str+= "\nTQ_budget: \${service_temporary_quarter=budget}"
		Content_str+= "\nTQ_payment_type: \${service_temporary_quarter=payment_type}"
		Content_str+= "\nTQ_escrow_fee_received: \${service_temporary_quarter=escrow_fee_received}"
		Content_str+= "\nTQ_invoice_created: \${service_temporary_quarter=invoice_created}"
		Content_str+= "\nTQ_invoice_closed_out: \${service_temporary_quarter=invoice_closed_out}"
		Content_str+= "\nTQ_selected_vendor_name: \${service_temporary_quarter=selected_vendor_name}"
		Content_str+= "\nTQ_selected_vendor_contact_first_name: \${service_temporary_quarter=selected_vendor_contact_first_name}"
		Content_str+= "\nTQ_selected_vendor_contact_mid_name: \${service_temporary_quarter=selected_vendor_contact_mid_name}"
		Content_str+= "\nTQ_selected_vendor_contact_last_name: \${service_temporary_quarter=selected_vendor_contact_last_name}"
		Content_str+= "\nTQ_selected_vendor_contact_full_name: \${service_temporary_quarter=selected_vendor_contact_full_name}"
		Content_str+= "\nTQ_selected_vendor_contact_phone: \${service_temporary_quarter=selected_vendor_contact_phone}"
		Content_str+= "\nTQ_selected_vendor_contact_email: \${service_temporary_quarter=selected_vendor_contact_email}"


		this.Content = Content_str
	}//end void


	//ADD FIELD CODE OF CLIENT INFO
	public void AddFieldCode_ClientInfo_func()
	{
		String Content_str = ""
		Content_str+= "company_name: \${client=company_name}"
		//Content_str+= "\ncontact_salutation: \${client=contact_salutation}"
		Content_str+= "\ncontact_first_name: \${client=contact_first_name}"
		//Content_str+= "\ncontact_mid_name: \${client=contact_mid_name}"
		Content_str+= "\ncontact_last_name: \${client=contact_last_name}"
		Content_str+= "\ncontact_full_name: \${client=contact_full_name}"
		Content_str+= "\ncontact_phone_number: \${client=contact_phone_number}"
		Content_str+= "\ncontact_email: \${client=contact_email}"
		Content_str+= "\ncontact_address1: \${client=contact_address1}"
		//Content_str+= "\ncontact_address2: \${client=contact_address2}"
		Content_str+= "\ncontact_city: \${client=contact_city}"
		Content_str+= "\ncontact_state: \${client=contact_state}"
		Content_str+= "\ncontact_zip: \${client=contact_zip}"
		Content_str+= "\ncontact_country: \${client=contact_country}"


		this.Content = Content_str
	}//end void


	//ADD FIELD CODE OF CLIENT INFO
	public void AddFieldCode_VendorInfo_func()
	{
		String Content_str = ""
		Content_str+= "vendor_name: \${vendor=vendor_name}"
		Content_str+= "\nvendorcontact_first_name: \${vendor=contact_first_name}"
		Content_str+= "\nvendorcontact_last_name: \${vendor=contact_last_name}"
		Content_str+= "\nvendorcontact_full_name: \${vendor=contact_full_name}"
		Content_str+= "\nvendorcontact_phone_number: \${vendor=contact_phone_number}"
		Content_str+= "\nvendorcontact_email: \${vendor=contact_email}"
		Content_str+= "\nvendorcontact_address1: \${vendor=contact_address1}"
		Content_str+= "\nvendorcontact_city: \${vendor=contact_city}"
		Content_str+= "\nvendorcontact_state: \${vendor=contact_state}"
		Content_str+= "\nvendorcontact_zip: \${vendor=contact_zip}"
		Content_str+= "\nvendorcontact_country: \${vendor=contact_country}"

		this.Content = Content_str
	}//end void


	//ADD FIELD CODE OF CUSTOM FIELD
	public void AddFieldCode_CustomField_func(CustomField customfield)
	{
		String Content_str = ""
		Content_str+= customfield.FieldName+": \${ctf_"+customfield.FieldID+"_"+customfield.FieldName+"}"


		this.Content = Content_str
	}//end void


}//end class




//SubClass
public class AddressTemplate extends Template{

	public AddressTemplate()
	{
		super()
		Type = "Address Label"
		DateTimeFuncs dt = new DateTimeFuncs()
		Name = Type+ "-Template_" + dt.getCurrentLocalTime()
		Subject = "QA_Testing"+ Name
	}
}//end class

public class LetterTemplate extends Template{

	public LetterTemplate()
	{
		super()
		Type = "Letter"
		DateTimeFuncs dt = new DateTimeFuncs()
		Name = Type+ "-Template_" + dt.getCurrentLocalTime()
		Subject = "QA_Testing"+ Name
	}




}//end class

