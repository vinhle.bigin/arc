package objects

import org.junit.Assert

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.Navigation
import internal.GlobalVariable
import pages_elements.DocumentTab
import pages_elements.LandingPage
import pages_elements.Messages


public class DocumentInfo {
	private String Name
	private String ServiceType
	private Boolean IsClientView
	private Boolean IsTransView
	private String file_name
	private String description

	public DocumentInfo(){

		Name = ""
		ServiceType = "General"
		IsClientView = false
		IsTransView = false
		file_name = "testingdoc.docx"
		description = "Update description"
	}

	public void TransfCreate_func() {
		DocumentTab tab = new DocumentTab()

		Navigation.GotoDocumentTab_func()

		tab.CreateNew_btn().click()

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.Attach_lnk())

		WebUI.delay(5)
		//Upload file
		Common.UploadFile_func(tab.Attach_lnk(), file_name)

		WebUI.delay(5)

		//this.Name = tab.Modal_DocName_txt().getAttribute("value")

		tab.Submit_btn().click()
		WebUI.delay(5)
		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")


	}

	public void TransfEdit_func()//This function no needs to return a DocumentInfo object
	{

		DocumentTab tab = new DocumentTab()

		Navigation.GotoDocumentTab_func()

		tab.Search_func(file_name)

		tab.Edit_icon(file_name).click()

		WebUI.delay(2)

		tab.Modal_Edit_btn().click()

		WebUI.delay(2)

		Common.SelectDropdownItem(tab.Modal_ServiceType_slect(), ServiceType)

		tab.Submit_btn().click

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")

	}//end void

	public void TransfDelete_func()
	{
		DocumentTab tab = new DocumentTab()

		Navigation.GotoDocumentTab_func()

		tab.Search_func(file_name)

		tab.Remove_icon(file_name).click()

		WebUI.delay(2)

		Messages.Delete_btn().click()

		WebUI.delay(3)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")

		Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)
	}

	public void CreateNewDocumentVendor_func()
	{
		DocumentTab tab = new DocumentTab()
		WebUI.delay(2)
		tab.DocumentVendor_tab().click()

		tab.NewDocumentVendor_btn().click()

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.Attach_lnk())

		WebUI.delay(3)
		//Upload file
		Common.UploadFile_func(tab.Attach_lnk(), file_name)

		WebUI.delay(3)

		//this.Name = file_name

		tab.Submit_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.DocList_tbl())

		WebUI.delay(3)

	}

	public void EditDocumentVendor_func()
	{

		DocumentTab tab = new DocumentTab()
		WebUI.delay(2)

		tab.DocumentVendor_tab().click()

		tab.Search_func(file_name)

		WebUI.delay(5)

		//tab.Edit_icon(Name).click()
		tab.Edit_ic().click()

		WebUI.delay(2)

		tab.Modal_Edit_btn().click()

		WebUI.delay(2)

		Common.sendkeyTextToElement(DocumentTab.UpdateDescriptionDoc_txt(), description)

		tab.Submit_btn().click()

		WebUI.delay(2)

		tab.Close_icon().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")

		WebUI.delay(2)


	}//end void

	public void DeleteDocumentVendor_func()
	{
		DocumentTab tab = new DocumentTab()

		tab.DocumentVendor_tab().click()

		tab.Search_func(file_name)

		//tab.Remove_icon(Name).click()
		tab.Remove_ic().click()

		WebUI.delay(2)

		Messages.DeleteItem_btn().click()

		WebUI.delay(3)

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")

		//Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)
	}

	public void clearAllDataInSearchbox(){
		DocumentTab tab = new DocumentTab()
		WebUI.delay(2)
		tab.DocumentVendor_tab().click()
		Common.sendkeyTextToElement(tab.SearchSubject_txt(), file_name)
		Common.sendkeyTextToElement(tab.SearchDescription_txt() , description)
		Common.HandleElementClick(tab.Search_btn())
		WebUI.delay(3)
		Common.VerifyFieldTextEqual_func("No Results", Messages.NoResultMsg_lb(), Messages.noResutl_msg)
		Common.HandleElementClick(tab.Clear_btn())
	}

	public void verifyEmptyDatainSearchbox(){
		Common.VerifyFieldTextEqual_func("Search name", DocumentTab.SearchSubject_txt() , "")
		Common.VerifyFieldTextEqual_func("Search description", DocumentTab.SearchDescription_txt() , "")
	}

	public void CreateClientDoc_func() {
		DocumentTab tab = new DocumentTab()

		Navigation.GotoDocumentTab_func()

		tab.CreateNew_btn().click()

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(tab.Attach_lnk())

		WebUI.delay(5)
		//Upload file
		Common.UploadFile_func(tab.Attach_lnk(), file_name)

		WebUI.delay(5)

		//this.Name = tab.Modal_DocName_txt().getAttribute("value")

		tab.Submit_btn().click()
		WebUI.delay(5)
		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")


	}

	public void EditClientDoc_func()//This function no needs to return a DocumentInfo object
	{

		DocumentTab tab = new DocumentTab()

		Navigation.GotoDocumentTab_func()

		tab.Search_func(file_name)

		tab.Edit_icon(file_name).click()

		WebUI.delay(2)

		tab.Modal_Edit_btn().click()

		WebUI.delay(2)

		tab.Submit_btn().click

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"when Edit Document.")


	}//end void


}//end class

