package objects
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import functions.DateTimeFuncs
import java.sql.ResultSet
import functions.DBCONNECT
import functions.Convert

class User {
	public int generation
	public String Id
	public String userId
	public String firstname
	public String midname
	public String lastname
	public String username
	//public String email
	//public String password
	//public String phone
	public String role
	public String birthday
	public String passpord
	//public String address1
	//public String address2
	//public String city
	//public String state
	//public String country
	//public String postalcode
	//public String referral_lnk
	DateTimeFuncs dt = new DateTimeFuncs()



	public User(){
		Random r = new Random();
		int ret2 = r.nextInt(100)+1;
		int ret = r.nextInt(100)+1;
		userId = ''
		username = 'testuser'+ dt.getCurrentLocalTime()+(ret+ret2).toString()
		firstname = username
		lastname = 'vn'
		//password = 'Password1'
		//phone = '1234567891'
		role = 'User'
		//email = username+'@bg.vn'
		birthday = '2000-12-04'
		passpord = '358738'
		//address1 = '20 Wall Street'
		//address2 = '50 High Road'
		//city = 'HCM'
		//state = 'Guam'
		//postalcode = '3832'
		//referral_lnk = GlobalVariable.glb_URL.toString().replace("login", "")+"register/"+username
		generation = 1
		Id =0
		//country = "United States of America"
	}

	//METHOD
	public void ChangeUserInfo()
	{
		firstname = username+"updated"
		lastname = 'vnupdated'
		//password = 'Password2'
		role = 'User'
		//email = username+'updated'+'@bg.vn'
		birthday = '2001-12-04'
		passpord = '351507'
		//address1 = '20 Wall Street updated'
		//address2 = '50 High Road updated'
		//city = 'HCM updated'
		//state = 'testing_state_updated'
		//postalcode = '1507'
	}


	public Boolean IsUserExisted(){

		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSession()

		dbconnection.OpenDBConnection()

		ResultSet rs = dbconnection.stmt.executeQuery("SELECT * FROM eden_users where username = '"+username+"'");

		return rs.next()
	}
	public void GetUserInfoFromDB() {
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSession()

		dbconnection.OpenDBConnection()


		ResultSet rs = dbconnection.stmt.executeQuery("SELECT * FROM eden_users where username = '"+username+"'");

		while (rs.next()) {
			this.Id = rs.getString("id")
			this.userId = rs.getString("uid")
		//	this.email = rs.getString("email")
			this.passpord = rs.getString("passport")
			//this.address1 = rs.getString("address")
		//	this.address2 = rs.getString("address2")
		//	this.city = rs.getString("city")
		//	this.state = rs.getString("state")
			//this.count = rs.getString("country")
		//	this.postalcode = rs.getString("postal_code")
			this.firstname = rs.getString("first_name")
			this.lastname = rs.getString("last_name")
		}
		rs.close()
		dbconnection.CloseAllConnect()
	}

	public void UpdateActiveSttFromDB(String active_stt) {
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSession()

		dbconnection.OpenDBConnection()

		int i = dbconnection.stmt.executeUpdate("Update eden_users set active = "+active_stt+" where username = '"+username+"'")
		if(i==0)
		{
			KeywordUtil.markFailedAndStop("Username '"+username+"' found to update status.")
		}

		dbconnection.CloseAllConnect()
	}

	/*
	 public void GetPackageDatesFromDB()
	 {
	 String result =""
	 WebUI.comment('CONNECT DB')
	 DBCONNECT dbconnection = new DBCONNECT()
	 dbconnection.OpenSSHSession()
	 dbconnection.OpenDBConnection()
	 ResultSet rs = dbconnection.stmt.executeQuery("select * from eden_user_packages up join eden_users u on up.user_id = u.id AND u.username = '"+username+"' AND package_id = "+this.order.Package_numb);
	 while (rs.next()) {
	 this.order.Open_date = rs.getString("buy_date")
	 this.order.Release_date = rs.getString("release_date")
	 }
	 rs.close()
	 dbconnection.CloseAllConnect()
	 }
	 */
}