package functions

import org.junit.Assert
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.firefox.GeckoDriverService

import com.kms.katalon.core.webui.driver.DriverFactory


import internal.GlobalVariable
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor

import java.io.File
import java.io.BufferedReader
import java.io.FileReader
import java.net.URL

import org.apache.commons.codec.binary.Base64InputStream
import org.apache.commons.io.FileUtils
import org.apache.poi.xwpf.usermodel.XWPFDocument
import org.apache.poi.xwpf.usermodel.XWPFParagraph
import org.apache.poi.xwpf.usermodel.XWPFRun

import java.nio.ByteBuffer
import java.nio.channels.Channel
import java.nio.channels.FileChannel

public class FileAccess {

	public String file_path
	public String Content


	public FileAccess() {

		String container = File.separator+ "Data Files"+File.separator

		String workingDirectory = System.getProperty("user.dir")+container

		println(workingDirectory)
		file_path = workingDirectory
		Content = ""

		/*
		 try {
		 //1: Creat and connect the data stream
		 File f = new File(file_path);
		 FileReader fr = new FileReader(f);
		 //#2: Read Data
		 BufferedReader br = new BufferedReader(fr);
		 String line;
		 while ((line = br.readLine()) != null){
		 System.out.println(line);
		 Content+=line
		 }
		 //#3: Close the stream
		 fr.close();
		 br.close();
		 } catch (Exception ex) {
		 System.out.println("Read File Error: "+ex);
		 }
		 */
	}

	public void ReadWordFile_func(String file_name) {
		try {
			//1: Creat and connect the data stream
			File f = new File(file_path+file_name);
			FileReader fr = new FileReader(f);

			FileInputStream fis = new FileInputStream(file_path+file_name);

			XWPFDocument document = new XWPFDocument(fis);

			List<XWPFParagraph> paragraphs = document.getParagraphs();

			for (XWPFParagraph para : paragraphs) {
				String content_str = para.getText()
				Content+=content_str


			}
			fis.close();

		} catch (Exception e) {
			GlobalVariable.glb_TCFailedMessage+=e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}




	}

	public void CreateWordFile_func(String file_name,String content_str)
	{

		//Blank Document

		String[] lines = content_str.split("\n")

		int count = lines.size()

		XWPFDocument document = new XWPFDocument();
		//Write the Document in file system
		FileOutputStream out = new FileOutputStream(new File(file_path+file_name));
		XWPFParagraph paragraph = document.createParagraph();

		//create Paragraph
		for(int i = 0;i<count;i++)
		{
			XWPFRun run = paragraph.createRun();
			run.setText(lines[i]);
			run.addBreak()

		}

		document.write(out);
		//Close document
		out.close();
		System.out.println(file_name + " written successfully");

	}

	/*
	 //METHOD: SAVE FILE FROM URL GET FROM ELEMENT HYPERLINK / DOWNLOAD BUTTON
	 public void SaveFileFromURL_func(String filename, String url)
	 {
	 File new_file = new File(this.file_path+filename)
	 URL new_url = new URL(url)
	 System.setProperty("http.agent", "Chrome");
	 HttpURLConnection http_connect = (HttpURLConnection) new_url.openConnection()
	 http_connect.connect()
	 InputStream input_stream = (InputStream) new_url.openStream()
	 //new_url.get
	 println(new_url.getContent())
	 Base64InputStream data_buff = new Base64InputStream(input_stream)	
	 DataOutputStream output = new DataOutputStream(	new BufferedOutputStream(new FileOutputStream(new_file)));
	 byte[] buffer = new byte[1024];
	 int bytesRead;
	 while ((bytesRead = data_buff.read(buffer)) != -1) {
	 println(bytesRead)
	 output.write(buffer, 0, bytesRead);
	 }//end while
	 output.close()
	 http_connect.disconnect()
	 }//end void
	 public void SavefileTxt(String filename, String url)
	 {
	 File new_file = new File(this.file_path+filename)
	 URL new_url = new URL(url)
	 System.setProperty("http.agent", "Chrome");
	 HttpURLConnection http_connect = (HttpURLConnection) new_url.openConnection()
	 http_connect.connect()
	 InputStream input_stream = (InputStream) new_url.openStream()
	 //FileUtils.Copy(input_stream, new_file)
	 FileUtils.copyURLToFile(new_url, new_file)
	 http_connect.disconnect()
	 WebDriver webDriver = DriverFactory.getWebDriver()
	 webDriver.get("about:config")
	 String name  = "browser.download.useDownloadDir"
	 String value = "false"
	 //webDriver.execute_script()
	 ((JavascriptExecutor) webDriver).executeScript("""var prefs = Components.classes["@mozilla.org/preferences-service;1"].getService(Components.interfaces.nsIPrefBranch);prefs.setBoolPref(arguments[0], arguments[1]);""", name, value);
	 Common.HandleElementClick(null)
	 }
	 */
	public void VerifyFileContent_func(String filename,String expect_content)
	{
		this.ReadWordFile_func(filename)

		if(this.Content !=expect_content)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Content[Observed:"+this.Content+"Expect:"+expect_content+"].\n"
		}
	}



}
