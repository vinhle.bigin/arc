package functions

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import com.kms.katalon.core.util.KeywordUtil
import WebUiBuiltInKeywords as WebUI

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.lang.String
import java.lang.Boolean as Boolen
import java.util.Date
import java.lang.Math as Math
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.TimeZone as TimeZone
import java.sql.ResultSet
import functions.DBCONNECT

import org.junit.After
import org.postgresql.util.PSQLException
import com.jcraft.jsch.ChannelExec
import com.jcraft.jsch.Channel
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.Session.GlobalRequestReply
import java.text.ParseException;


public class DateTimeFuncs {

	public static Date currentDate

	public DateTimeFuncs() {
		currentDate = null
	}


	public Date GetLocalDatetime() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss")
		//DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
		
		LocalDateTime now = LocalDateTime.now()
		String output = now.format(dtf)
		
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		
		Date d_temp = formatter.parse(output)
		
		return d_temp

		}



	public static String getCurrentLocalTime() {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss")
		LocalDateTime now = LocalDateTime.now()
		String return_str = now.hour.toString() +now.minute.toString()+now.second.toString()
		return return_str
	}

	public static Date GetCurrentLocalDate_Date() {
		//timedatectl set-time '2018-05-25 11:10:40'
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy")
		LocalDateTime now = LocalDateTime.now()
		String output = dtf.format(now)
		Date d_temp = this.ConvertStringtoDate(output)
		return d_temp
	}

	public static String GetCurrentLocalDate_Str() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy")
		LocalDateTime now = LocalDateTime.now()
		String output = dtf.format(now)
		return output
	}

	public static void GetServerDateTime() {

		String result =""
		DBCONNECT dbconnect = new DBCONNECT()
		dbconnect.OpenSSHSessionWithKey()

		int index=0
		String output_str = ''
		String servertime =''
		ChannelExec channel= (ChannelExec)dbconnect.session.openChannel("exec");
		InputStream in_temp = channel.getInputStream();
		channel.setCommand("date")
		channel.connect();

		BufferedReader reader = new BufferedReader(new InputStreamReader(in_temp));
		String line;
		while ((line = reader.readLine()) != null) {
			//System.out.println(++index + " : " + line);
			result = line
		}//while

		channel.disconnect()
		dbconnect.session.disconnect()


		String format_str = "E MMM dd HH:mm:ss z yyyy"
		//String format_str = "YYYY MMM dd HH:mm:ss"
		SimpleDateFormat formatter =new SimpleDateFormat(format_str)
		//formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
		Date date =formatter.parse(result)

		this.currentDate = date
	}


	public void ChangeServerTime(String time) {
		//datetime += " 11:00:00"
		DBCONNECT dbconnect = new DBCONNECT()
		dbconnect.OpenSSHSessionWithKey()

		int index=0
		ChannelExec channel= (ChannelExec)dbconnect.session.openChannel("exec");
		InputStream in_temp = channel.getInputStream();
		channel.setCommand("timedatectl set-ntp 0")
		channel.connect();
		channel.disconnect()

		channel= (ChannelExec)dbconnect.session.openChannel("exec");
		in_temp = channel.getInputStream();
		channel.setCommand("timedatectl set-time '"+time+"'")
		channel.connect();
		channel.disconnect()

		dbconnect.session.disconnect()

	}

	public void IncreaseServerDay(Date orgindate, int add_day) {

		Date temp_date = orgindate
		temp_date.setDate(temp_date.getDate()+add_day)

		println(temp_date)
		//2018-06-18 8:00:00

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
		String strDate = formatter.format(temp_date);
		println(strDate)
		this.ChangeServerTime(strDate)

	}

	public static Date IncreaseDay(Date orgindate, int add_day) {

		Date temp_date = orgindate
		temp_date.setDate(temp_date.getDate()+add_day)
		println(temp_date)
		return temp_date
	}


	public static void IncreaseServerDay(int add_day) {

		this.GetServerDateTime()
		this.currentDate.setHours(this.currentDate.getDate()+add_day)

		//println(temp_date)
		//2018-06-18 8:00:00

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
		String strDate = formatter.format(this.currentDate);
		//println(strDate)
		this.ChangeServerTime(strDate)

	}
	

	public void IncreaseServerHour(int add_hour) {

		this.GetServerDateTime()
		this.currentDate.setHours(this.currentDate.getHours()+add_hour)

		println(this.currentDate)
		//2018-06-18 8:00:00

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
		String strDate = formatter.format(this.currentDate);
		println("UTC date 12format: "+ strDate)
		this.currentDate = formatter.parse(strDate)

		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
		strDate = formatter.format(this.currentDate);
		println("UTC date 24format: "+ strDate)
		this.ChangeServerTime(strDate)
	}

	public String FormatDate(Date date, String format_str = "MM/dd/yyyy HH:mm") {


		Date temp_date = date

		println(temp_date)
		//2018-06-18 8:00:00

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
		String strDate = formatter.format(temp_date);
		println("UTC date 12format: "+ strDate)
		temp_date = formatter.parse(strDate)
		//format_string = "yyyy-MM-dd HH:mm:ss"
		formatter = new SimpleDateFormat(format_str);
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"))
		strDate = formatter.format(temp_date);
		println("UTC date 24format: "+ strDate)
		return strDate
	}


	public String FormatDateString(String date_str, String format_str = "MM/dd/yyyy") {

		SimpleDateFormat formatter = new SimpleDateFormat(format_str);
		String strDate = formatter.format(date_str);
		println("UTC date 24format: "+ strDate)
		return strDate
	}


	public void RollBackServerTime() {
		DBCONNECT dbconnect = new DBCONNECT()
		dbconnect.OpenSSHSessionWithKey()
		String output_str = ''
		String servertime =''
		ChannelExec channel= (ChannelExec)dbconnect.session.openChannel("exec");
		InputStream in_temp = channel.getInputStream();
		channel.setCommand("timedatectl set-ntp 1")
		channel.connect();
		channel.disconnect()
		dbconnect.session.disconnect()
	}



	public static String ConvertDatetoString(Date temp_date){

		//SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String strDate = formatter.format(temp_date);
		println(strDate)
		return strDate
	}

	public static Date ConvertStringtoDate(String temp_date){

		//SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		
		Date strDate = formatter.parse(temp_date);
		return strDate
	}
}
