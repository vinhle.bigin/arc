package functions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import static org.junit.Assert.assertFalse as AssertFalse
import org.junit.Assert as Assert

import internal.GlobalVariable
import net.bytebuddy.implementation.bytecode.Throw
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import java.lang.String as String
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.util.concurrent.TimeoutException
import java.lang.Boolean as Boolen
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.ExpectedCondition
import java.lang.NullPointerException
import pages_elements.Messages
import functions.DBCONNECT
import functions.DateTimeFuncs
import java.sql.ResultSet
import javax.mail.*
import javax.mail.Folder
import javax.mail.Message
import javax.mail.Store
import com.sun.mail.pop3.POP3Store
import javax.mail.Session as Session
import javax.mail.MessagingException
import javax.mail.NoSuchProviderException
import javax.mail.Part
import javax.mail.Part.*
import javax.mail.Multipart
import objects.Mailinfo
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.List
import functions.FileAccess

public class EmailConnect {

	public List<Mailinfo> Maillist
	public String host
	public String mailStoreType
	public String username
	public String password
	public String port
	public String pop3_type
	public FileAccess downloadfile


	public EmailConnect(String mail_type) {

		Maillist = new ArrayList<>()
		if(mail_type=="Outlook") {
			host = "mail.bigin.vn";// change accordingly
			mailStoreType = "pop3";
			username = "vinh.le@bigin.vn";// change accordingly
			password = "QNqhPZ93";// change accordingly
			pop3_type = "pop3"
		}

		else {
			host = "pop.gmail.com";// change accordingly
			mailStoreType = "pop3";
			username = "bigintesting@gmail.com";// change accordingly
			password = "Password@1234";// change accordingly
			port = '995'
			pop3_type = "pop3s"

		}
		
		downloadfile= new FileAccess()

		
	}


	public String OptimizeEmailContent(String input_str) {
		//input_str = input_str.replace("\n\n","\n")

		int i = input_str.indexOf("<html xmlns:")

		String a
		if(i>0)
			a = input_str.substring(0,i)
		else
			a = input_str
		a = a.replaceAll("</a>", "")
		a = a.replaceAll("</td>", "")
		a = a.replaceAll("</tr>", "")
		a = a.replaceAll("</tbody>", "")
		a = a.replaceAll("</table>", "")
		a = a.replaceAll("</title>", "")
		a = a.replaceAll("</style>", "")
		a = a.replaceAll("</head>", "")
		a = a.replaceAll("</body>", "")
		a = a.replaceAll("</html>", "")
		a = a.replaceAll("</div>", "")
		a = a.replaceAll("</i>", "")
		a = a.replaceAll("</p>", "")
		a = a.replaceAll("</h2>", "")
		a = a.replaceAll("</strong>", "")
		a = a.replaceAll("<i>", "")
		a = a.replaceAll("<strong>", "")
		a = a.replaceAll("<td.+", "")
		a = a.replaceAll("<td.+?>", "")
		a = a.replaceAll("<tr.+", "")
		a = a.replaceAll("<tr.+?>", "")
		a = a.replaceAll("<tbody.+", "")
		a = a.replaceAll("<table.+>", "")

		a = a.replaceAll("<style.+", "")
		a = a.replaceAll("<title.+","")
		a = a.replaceAll("<head.+", "")
		a = a.replaceAll("<div.+", "")
		//a = a.replaceAll("<p.+>", "")
		a = a.replaceAll("<p>", "")
		a = a.replaceAll("<br.+", "")
		a = a.replaceAll("<h2.+>", "")
		a = a.replaceAll("&nbsp;", "")

		a = a.replaceAll("<body.+", "")
		a = a.replaceAll("<html.+", "")
		a = a.replaceAll("<link.+>", "")


		a = a.replaceAll(".+word-break.+}", "")
		a = a.replaceAll("word-break.+}", "")
		a = a.replace("<!-- Start Nav -->", "")
		a = a.replaceAll("body.+", "")
		a = a.replaceAll(".+html.+", "")
		a = a.replaceAll(".+?}", "")
		a = a.replaceAll("<base.+", "")
		a = a.replaceAll("td[class].+", "")
		a = a.replaceAll("<img src=\"https://u10082166.+", "")
		a = a.replaceAll("<meta.+", "")
		a = a.replaceAll(" {2,50}", "")

		a = a.replaceAll("}\\s", "")
		a = a.replaceAll("\\s}", "")
		a = a.replaceAll("\n\\s+", "")



		return a

	}

	public void FetchEmail_func() {
		try {
			// create properties field
			Properties properties = new Properties();

			properties.put("mail.pop3.host", this.host);
			properties.put("mail.pop3.port", this.port);
			properties.put("mail.pop3.starttls.enable", "true");
			Session emailSession = Session.getDefaultInstance(properties);

			//create the POP3 store object and connect with the pop server
			POP3Store store = (POP3Store) emailSession.getStore(pop3_type);

			store.connect(host, username, password);


			// create the folder object and open it
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// retrieve the messages from the folder in an array and print it
			Message[] messages = emailFolder.getMessages();
		
			for (int i = 0; i < messages.length; i++) {
				Mailinfo tmpmail = new Mailinfo()

				Message message = messages[i]
				tmpmail.sentDate = message.getSentDate()

				List<String> from_tmp = GetAddressFrom(message)
				tmpmail.From =  from_tmp.get(0)

				tmpmail.To_list =   GetAddressTo(message)

				tmpmail.Subject =   GetSubject(message)

				tmpmail.Content = writePart(message);

				tmpmail.Content = OptimizeEmailContent(tmpmail.Content).trim()

				tmpmail.download_file = this.downloadfile
				//println(tmpmail.Content)
				Maillist.add(tmpmail)
			}//end for
			println("Message length:"+messages.length)


		} catch (NoSuchProviderException e) {

			e.printStackTrace();

			GlobalVariable.glb_TCFailedMessage+= e.message
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		} catch (MessagingException e) {
			e.printStackTrace();
			GlobalVariable.glb_TCFailedMessage+= e.message
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		} catch (IOException e) {
			e.printStackTrace();
			GlobalVariable.glb_TCFailedMessage+= e.message
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		} catch (Exception e) {
			e.printStackTrace();
			GlobalVariable.glb_TCFailedMessage+= e.message
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

		//println(Maillist.get(0).Content)
	}

	public String writePart(Part p) throws Exception {

		String temp_content = ""

		//check if the content is plain text
		if (p.isMimeType("text/plain")) {

			temp_content += (String) p.getContent();
		}

		//check if the content has attachment
		else if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			int count = mp.getCount();
			println("MultiPart count:"+count)
			for (int i = 0; i < count; i++)
				temp_content +=writePart(mp.getBodyPart(i));
		}
		//check if the content is a nested message
		else if (p.isMimeType("message/rfc822")) {

			temp_content +=writePart((Part) p.getContent());
		}
		//check if the content is an inline image
		else if (p.isMimeType("image/jpeg")) {

			Object o = p.getContent();

			InputStream x = (InputStream) o;
			// Construct the required byte array
			int j
			byte[] bArray
			while ((j = (int) ((InputStream) x).available()) > 0) {
				bArray = new byte[x.available()];
				int result = (int) (((InputStream) x).read(bArray));
				if (result == -1)
					j = 0;
				break;

			}
			FileOutputStream f2 = new FileOutputStream("/tmp/image.jpg");
			f2.write(bArray);
		}
		else if (p.getContentType().contains("image/")) {


			//===download attachment
			System.out.println("content type" + p.getContentType());
			String file_name = "image" + new Date().getTime() + ".jpg"

			this.DownloadAttachment_func(p,file_name)
		}
		else {
			println("Start to get content of Attachment:")
			Object o = p.getContent();
			println("Attachment File Name:"+p.fileName)
			println("File Type:"+p.getContentType())
			println("File Content:"+p.getContent())
			if (o instanceof String) {

				temp_content +=(String) o;
			}
			else if (o instanceof InputStream) {
				//==================
				InputStream is = (InputStream) o
				int c;

				String file_name  = "Expected_" + p.getFileName()
				this.DownloadAttachment_func(p,downloadfile.file_path +file_name)
				FileAccess file_tmp = new FileAccess()
				file_tmp.ReadWordFile_func(file_name)
				this.downloadfile = file_tmp
			}
			else {
				temp_content += o.toString();
			}
		}

		//println(temp_content)
		return temp_content
	}



	/*
	 * This method would print FROM,TO and SUBJECT of the message
	 */
	public static List<String> GetAddressFrom(Message m) throws Exception {
		List<String> temp_addr = new ArrayList<>()
		Address[] a;

		// FROM
		if ((a = m.getFrom()) != null) {
			for (int j = 0; j < a.length; j++)
				temp_addr.add(a[j].toString())
		}
		return temp_addr

	}


	public static List<String> GetAddressTo(Message m) throws Exception {

		List<String> temp_addr = new ArrayList<>()

		Address[] a;

		// TO
		if ((a = m.getRecipients(Message.RecipientType.TO)) != null) {
			for (int j = 0; j < a.length; j++)
				temp_addr.add(a[j].toString())
		}

		return temp_addr


	}

	public static String GetSubject(Message m) throws Exception {

		// SUBJECT
		String tmp_subj = ""
		if (m.getSubject() != null)
			tmp_subj = m.getSubject()

		return tmp_subj

	}

	public void VerifyEmailExist(Mailinfo email, String emailtype = "Activate")
	{
		Mailinfo email_var = email

		int count = this.Maillist.size()

		if(count==0)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Mail box is empty."
			return
		}

		Mailinfo resultmail = this.FindExistEmail(email)

		if(resultmail==null)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "No "+emailtype+" email sent from Agoyu System."
		}

		else
		{
			//println(mtemp.Content)

			if(emailtype == "Activate")
				resultmail.VerifyActivateEmail()

			else if(emailtype == "Welcome")
				resultmail.VerifyWelcomeEmail()

			else if(emailtype == "Reset")
				resultmail.VerifyResetAccountEmail()
		}

	}



	public Boolean IsMailExist(Mailinfo email)
	{
		Mailinfo email_var = email


		Boolean status = false
		int count = this.Maillist.size()

		if(count==0)
		{
			return status
		}
		Math math = new Math()
		for(int i =0;i<count;i++)
		{
			Mailinfo mtemp = this.Maillist.get (i)
			if(mtemp.From.contains(email_var.From)
			&& mtemp.Subject.contains(email_var.Subject)
			&&(email_var.sentDate.month==mtemp.sentDate.month)
			&&(email_var.sentDate.date==mtemp.sentDate.date)
			&&(email_var.sentDate.hours==mtemp.sentDate.hours)
			&&(math.abs(email_var.sentDate.minutes-mtemp.sentDate.minutes)<8)
			)
			{
				status = true
				break
			}

		}//end for

		return status
	}


	public Mailinfo FindExistEmail(Mailinfo email)
	{
		Mailinfo email_var = email
		Mailinfo email_index = null
		int count = this.Maillist.size()

		if(count==0)
		{
			return email_index
		}

		Math math = new Math()
		for(int i =count-1;i>=0;i--)
		{
			Mailinfo mtemp = this.Maillist.get (i)
			println(email_var.From+"-"+mtemp.From)
			println(email_var.Subject+"-"+mtemp.Subject)
			println(email_var.sentDate.month+"-"+mtemp.sentDate.month)
			println(email_var.sentDate.date+"-"+mtemp.sentDate.date)
			println(email_var.sentDate.hours+"-"+mtemp.sentDate.hours)
			println(email_var.sentDate.minutes+"-"+mtemp.sentDate.minutes)

			if(mtemp.From.contains(email_var.From)
			&& mtemp.Subject.contains(email_var.Subject)
			&&(email_var.sentDate.month==mtemp.sentDate.month)
			&&(email_var.sentDate.date==mtemp.sentDate.date)
			&&(email_var.sentDate.hours==mtemp.sentDate.hours)
			&&(math.abs(email_var.sentDate.minutes-mtemp.sentDate.minutes)<8)
			)
			{

				email_index = mtemp
				break
			}

		}//end for



		return email_index
	}


	//==METHOD: DOWNLOAD ATTACHMENT
	public void DownloadAttachment_func(Part p,String file_name)
	{
		File f = new File(file_name);
		DataOutputStream output = new DataOutputStream(	new BufferedOutputStream(new FileOutputStream(f)));
		com.sun.mail.util.BASE64DecoderStream test = (com.sun.mail.util.BASE64DecoderStream) p.getContent();
		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = test.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);

		}
		output.close()
	}

}