
package functions
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.Assert

import com.jcraft.jsch.ChannelExec
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import pages_elements.Messages
import pages_elements.VendorPage



public class Common {

	public static void VerifyWeightFieldText(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {
		expected_str = Convert.FormatString(expected_str)
		expected_str+=" lbs"

		this.VerifyFieldTextEqual_func(fieldname_str, element_temp,expected_str, note_str)
	}

	public static void VerifyFieldValidation_func(String fieldname_str, WebElement element_temp,String input_str, String Expected_msg) {
		if (Expected_msg != '') {
			if (element_temp == null || element_temp.getText()=="") {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += "[Input: "+input_str+"]Validation message does not displays."
			}

			else {
				this.VerifyFieldTextEqual_func(fieldname_str, element_temp, Expected_msg)
			}
		}
		else {
			if (element_temp != null && element_temp.getText()!="") {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += "[Input: "+input_str+"]"+fieldname_str+ "Validation message should not display."
			}
		}
	}//end void

	public static void VerifyFieldDisplayed_func(String fieldname_str,WebElement element,String note = "")
	{

		if(element==null)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "The "+fieldname_str+" Should Displayed "+ note+"."
		}
	
		/*	
	else if(!element.isDisplayed())
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "The "+fieldname_str+" Should Displayed "+ note+"."
		}
		*/
	}//end void

	public static void VerifyFieldNotDisplayed_func(String fieldname_str,WebElement element,String note = "")
	{

		if(element!=null && element.isDisplayed())
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "The "+fieldname_str+" Should NOT Displayed "+ note+"."
		}
	}//end void



	//TEXT VERIFI METHOD - COMPARE FIELD TEXT() WITH EXPECTED VALUE
	public static void VerifyFieldTextEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "")
	{

		String observed = GetFieldText_func(element_temp)

		println("Field Text: "+ observed)

		if(observed!=expected_str)
		{
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+" Text should EQUAL.[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+ "\n"
		}
	}//end void

	public static void VerifyFieldTextNotEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "")
	{
		String observed = GetFieldText_func(element_temp)

		println(observed)

		if(observed==expected_str)
		{
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+" Text should NOT EQUAL.[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+ "\n"
		}
	}//end void



	//VALUE VERIFY METHOD - COMPARE VALUE ATTRIBUTE OF ELEMENT TO EXPECTED VALUE
	public static void VerifyFieldValueEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {
		String observed = element_temp.getAttribute("value")

		println("Value is: " + observed)

		if(observed!=expected_str) {
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+" Value should EQUAL.[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+"\n"
		}
	}//end void


	//VERIFY LIST - ELEMENT IS DISPLAY
	public static void verifyListElementIsDisplayed(List<WebElement> elements){
		//boolean isDisplayed = false
		int size = VendorPage.RowsTable().size()
		println(size)
		for(int i = 0; i < size ;i++)
		{
			WebElement element = elements.get(i)
			println(element.isDisplayed())
			if(element.isDisplayed() == false){
				GlobalVariable.glb_TCStatus = false
				GlobalVariable.glb_TCFailedMessage += "Item is displayed as unexpected ."
				break
			}
		}
	}//end void



	//VALUE VERIFY METHOD - COMPARE NOT EQUAL OF ELEMENT VALUE AND EXPECTED VALUE
	public static void VerifyFieldValueNotEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {
		String observed = element_temp.getAttribute("value")

		println(observed)

		if(observed!=expected_str) {
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+".[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+"."
		}
	}//end void



	public static void MacUploadFile_func(String file_path) {

		String exec_str =""

		exec_str += "tell application \"Firefox\"\n"
		exec_str += "activate \"FireFox\"\n"
		exec_str +="end tell\n"
		exec_str +="tell application \"System Events\"\n"
		exec_str +="keystroke \"G\" using {command down, shift down}\n"
		exec_str +="keystroke \""+file_path+"\"\n"
		//exec_str +="keystroke \"/Users/bigin1/Documents/Agoyu-Automation/AgoyuProj/Data Files/logo4M.jpg\"\n"

		exec_str +="keystroke return\n"
		exec_str +="delay 2\n"
		exec_str +="keystroke return\n"
		exec_str +="delay 2\n"
		exec_str +="end tell\n"

		println(exec_str)

		String []Run_str = new String[3]

		Run_str[0] = "osascript"
		Run_str[1] = "-e"
		Run_str[2] = exec_str

		println ("Start to upload image in MAC")

		Runtime rtime = Runtime.getRuntime();
		Process prcess = rtime.exec(Run_str)

		println ("Completed to upload image in MAC")

	}//end void

	public static void UploadFile_func(WebElement upload_btn, String file_name) {
		WebElement element_tmp = upload_btn

		String file = file_name

		String container = File.separator+ "Data Files"+File.separator

		String workingDirectory = System.getProperty("user.dir")+container+file

		element_tmp.click()
		Thread.sleep(2000)
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard()

		StringSelection string_slect = new StringSelection( workingDirectory )

		clipboard.setContents(string_slect, string_slect)
		WebDriver driver = DriverFactory.getWebDriver()

		if(this.GetOS_func().toString().contains("mac")) {

			MacUploadFile_func(workingDirectory)

			/*
			 println ("Start to upload image in MAC")
			 Runtime.getRuntime().exec("osascript "+"/Users/bigin1/Documents/macupload_valid.scpt")
			 Thread.sleep(2000)
			 println ("Completed to upload image in MAC")
			 */
		}
		else{
			Robot rbot = new Robot()
			rbot.keyPress(KeyEvent.VK_CONTROL)
			rbot.keyPress(KeyEvent.VK_V)
			Thread.sleep(1000)
			rbot.keyRelease(KeyEvent.VK_V);
			rbot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000)

			rbot.keyPress(KeyEvent.VK_ENTER);
			rbot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000)
		}


		//KeyStroke keystroke = KeyStroke.getKeyStroke(KeyEvent.VK_V)


	}//end void

	public static List<String> GetTableRecordsPerPaging_func(WebElement TableElement) {
		List<WebElement> rows_table = null;
		WebElement table = TableElement
		'To locate rows of table it will Capture all the rows available in the table'

		WebUI.delay(2)

		rows_table = table.findElements(By.tagName('tr'))

		int rows_count = rows_table.size()

		println(rows_table.size())
		if(rows_count==0) {
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Package Report: No record found."
			return
		}

		//for per one row
		List<String > list = new ArrayList<>()

		for (int row = 0; row < rows_count; row++) {
			'To locate columns(cells) of that specific row'
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

			int column_size = Columns_row.size()

			String cur_record =""
			for(int column = 0; column <column_size;column++)
			{
				String text = Columns_row.get(column).getText()

				cur_record+=Columns_row.get(column).getText()

				if(column<column_size-1)
					cur_record+="|"


			}//end for column

			list.add(cur_record)

		}//end for get row record

		//==add records to total list array
		return list
	}//end void

	public static void FilterTableRecord_func(WebElement Filter_txt,WebElement submit_btn,String input_str){
		WebElement textfield = Filter_txt
		WebElement submit = submit_btn

		String input_temp = input_str

		textfield.sendKeys(input_temp)

		if(submit!=null)
		{
			submit.click()
		}
		WebUI.delay(2)
	}//end void

	public static void waitForLoad()
	{

		WebDriver driver = DriverFactory.getWebDriver()
		ExpectedCondition<Boolean> pageLoadCondition = new
				ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver driver_temp) {
						return ((JavascriptExecutor)driver_temp).executeScript("return document.readyState").equals("complete");
					}
				};
		WebDriverWait wait = new WebDriverWait(driver, 15)
		wait.until(pageLoadCondition)
	}//end void

	public static void WaitForElementDisplay(WebElement element) {

		if(element==null)
		{
			WebDriver driver = DriverFactory.getWebDriver()
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

			for(int i =0;i<10;i++)
			{
				try {

					if(element.isDisplayed())
					{
						println("element is displayed.")
						break
					}

					else
						WebUI.delay(2)
				}
				catch (NullPointerException t) {
					WebUI.delay(2)
				}
			}
		}

	}//end void

	public static void ClearServercache() {
		Messages msg = new Messages()

		if(msg.Servercache_msg()!=null)
		{

			String pathfolder = "cd ../"+GlobalVariable.glb_Serverhomefolder
			//====================================================
			WebUI.comment('Run BE JOB')
			DBCONNECT dbconnection = new DBCONNECT()
			dbconnection.OpenSSHSessionWithKey()
			int index=0
			ChannelExec channel= (ChannelExec)dbconnection.session.openChannel("exec");
			InputStream in_temp = channel.getInputStream();
			channel.setCommand(pathfolder)
			channel.connect();
			channel.disconnect()

			channel= (ChannelExec)dbconnection.session.openChannel("exec");
			in_temp = channel.getInputStream();
			channel.setCommand("er")
			channel.connect();
			channel.disconnect()

			dbconnection.session.disconnect()
			WebUI.delay(60)
			WebUI.refresh()

		}//end if
	}//end void


	//=====TO HANDLE CLICK ACTION IN SOME SPECIAL CASE ELEMENT CANNOT BE CLICK NORMALY THROUGH THE SELENIUM METHOD
	public static void HandleElementClick(WebElement element)
	{
		try{
			element.click()
		}
		catch(Exception e)
		{
			WebDriver driver = DriverFactory.getWebDriver()

			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
		}


	}//end void


	public static void ActionSendkey(WebElement element,String input_str)
	{
		WebDriver webDriver = DriverFactory.getWebDriver();

		try{
			Actions actions = new Actions(webDriver);
			element.click()
			WebUI.delay(1)
			Action action
			//for(int i =0;i<10;i++)
			//{
			//	action = actions.doubleClick(element).sendKeys(Keys.BACK_SPACE).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).build();
			action = actions.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.DELETE).build();
			action.perform()


			//}

			action = actions.sendKeys(element,input_str).build();
			action.perform()
		}
		catch(Exception e)
		{

		}


	}//end void

	public TestData GetFileData(String filename)
	{
		TestData  data = TestDataFactory.findTestData("Data Files/"+filename)
		return data
	}//end void

	public static Boolean IsTextFieldEditable_func(WebElement e,String value_str) {
		WebDriver webDriver = DriverFactory.getWebDriver();

		//	((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", e);

		Actions actions = new Actions(webDriver);

		this.ActionSendkey(e,value_str)

		if(e.getAttribute("value").contains(value_str)) {
			return true
		}
		return false
	}//end void

	public static void VerifyFieldEditable_func(String fieldname, WebElement e,String value_str,String note="")
	{
		Boolean status = this.IsTextFieldEditable_func(e,value_str)

		if(!status)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += fieldname+" SHOULD EDITABLE "+note+"."
		}

	}//end void

	public static void VerifyFieldNotEditable_func(String fieldname, WebElement e,String value_str,String note="")
	{
		Boolean status = this.IsTextFieldEditable_func(e,value_str)

		if(status)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += fieldname+" SHOULD NOT EDITABLE "+note+"."
		}

	}//end void

	public static String GetOS_func()
	{
		String os = System.getProperty("os.name").toLowerCase()
		println(os)
		return os
	}//end void

	public static void SelectItemULElement_func(WebElement ul_element,String item_text)
	{
		List<WebElement> item_list = null;
		WebElement dropdown = ul_element
		'To locate rows of table it will Capture all the rows available in the table'

		WebUI.delay(2)

		item_list = dropdown.findElements(By.tagName('li'))

		int rows_count = item_list.size()

		//println(item_list.size())
		if(rows_count==0) {

			GlobalVariable.glb_TCFailedMessage += "No item in the dropdown.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
			return
		}

		//for per one item
		Boolean exist = false

		for(int index = 0; index <rows_count;index++)
		{
			WebElement option = item_list.get(index)

			String text = option.getText()
			println("Item ="+text)
			if(text.contains(item_text))
			{
				exist = true
				Common.HandleElementClick(option)
				//option.click()
				WebUI.delay(3)
				break

			}

		}//end for

		if(!exist)
		{
			GlobalVariable.glb_TCFailedMessage += "Unfound item["+item_text+"] in the dropdown.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

	}//end void

	public static void WaitForElementNotDisplay(String element_xpath)
	{
		WebDriver driver = DriverFactory.getWebDriver()

		WebDriverWait wait = new WebDriverWait(driver,10)

		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(element_xpath)))


	}//end void

	public static void WaitForElementEnable(WebElement element)
	{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(webDriver, 5);
		if(element!=null)
			for(int i = 0;i<20;i++)
		{
			if(element.isEnabled())
			{
				break
			}
			WebUI.delay(3)
		}


		//wait.until(ExpectedConditions.not(ExpectedConditions.))
	}//end void

	public static int randomNumber(){
		Random random = new Random()
		int number = random.nextInt(99999)
		return number
	}//end void

	public static WebElement getFirstSelectedItem(WebElement element) {
		Select select = new Select(element);
		return select.getFirstSelectedOption();
	}

	public static void sendkeyTextToElement(WebElement element, String value) {

		try{
			element.clear();
			element.sendKeys(value);
		}

		catch(Exception e)
		{
			this.ScrollElementtoViewPort_func(element)

			element.clear();
			element.sendKeys(value);
		}

	}//end void

	public static void sendkeyNumberToElement(WebElement element, int number) {
		element.clear();
		element.sendKeys(String.valueOf(number));
	}//end void

	public static void getText(WebElement element){
		String text = element.getText()
		println(text)
	}//end void


	//SELECT ITEM FROM DROPDOWN ELEMENTS
	public static void SelectDropdownItem(WebElement element,String item_text)
	{
		String tag_name = element.getTagName()
		println(tag_name)
		if(tag_name == "ul")
			this.SelectItemULElement_func(element, item_text)
		else
		{

			//Common.HandleElementClick(element)

			Common.HandleElementClick(element)

			//WebUI.delay(2)
			Select slect_e = new Select(element)
			slect_e.selectByVisibleText(item_text)
			//slect_e.selectByValue("526")
		}

	}//end void

	public static void selectRandomItemDropdown( WebElement dropdown, List<WebElement> itemsInDropdown){
		dropdown.click();

		WebUI.delay(1)

		int size = itemsInDropdown.size();
		println(size)

		int randomNumber = ThreadLocalRandom.current().nextInt(0, size);
		println(randomNumber)

		itemsInDropdown.get(randomNumber).click();
	}//end void

	public static int countRowInTable(List<WebElement> rowsTable) {
		return rowsTable.size()
		println(rowsTable.size())
	}//end void

	public static VerifyItemExistDropdown(WebElement element,String item_text)
	{
		//for per one item
		Boolean exist = this.IsItemExisDropdown(element,item_text)

		println("IsExist = "+exist)
		if(exist==false)
		{
			GlobalVariable.glb_TCStatus =false
			GlobalVariable.glb_TCFailedMessage += "Unfound item["+item_text+"] in the dropdown.\n"
		}

	}//end void

	public static VerifyItemNotExistDropdown(WebElement element,String item_text)
	{
		//for per one item
		Boolean exist = this.IsItemExisDropdown(element,item_text)


		if(exist==true)
		{
			GlobalVariable.glb_TCStatus =false
			GlobalVariable.glb_TCFailedMessage += "Item["+item_text+"] SHOULD NOT in the dropdown.\n"
		}

	}//end void


	//GET ELEMENT FROM TABLE
	public static WebElement GetElementInTable(String searchvlue, WebElement table, String element_xpath)
	{
		try{

			List<WebElement> rows_table = table.findElements(By.tagName('tr'))

			int rows_count = rows_table.size()

			for (int row = 0; row < rows_count; row++) {

				String text_str = rows_table.get(row).getText()
				println(text_str)
				if(text_str.contains(searchvlue))
				{
					WebElement e_temp = rows_table.get(row).findElement(By.xpath(element_xpath))
					return e_temp
				}//end if

			}//end for

		}//try
		catch(NoSuchElementException e) {
			return null
		}
	}//end void




	//========VERIFY ITEM EXIST IN TABLE
	public static Boolean IsItemExistTable(WebElement table,String item_name)
	{
		Boolean exist = false

		List<String> result = Common.GetTableRecordsPerPaging_func(table)

		for(int i = 0;i<result.size();i++)
		{
			if(result.get(i).contains(item_name))
			{
				exist = true
				break
			}
		}//end for


		return exist

	}//end void

	//=============CHECK ITEM IS EXIST IN DROPDOWN
	public static Boolean IsItemExisDropdown(WebElement dropdown_e,String item_name)
	{
		Boolean exist = false
		WebElement dropdown = dropdown_e

		List<WebElement> item_list = null;


		WebUI.delay(2)
		if(dropdown.getTagName()=="ul")
		{
			item_list = dropdown.findElements(By.tagName('li'))

		}//end if tagname = ul

		else if(dropdown.getTagName()=="select")
		{
			item_list = dropdown.findElements(By.tagName('option'))
		}

		///==================VERIFY SCRIPTS

		int rows_count = item_list.size()

		println(item_list.size())
		println(item_name)

		if(rows_count!=0) {

			for(int index = 0; index <rows_count;index++)
			{
				WebElement option = item_list.get(index)

				String text = option.getText()
				println("Item ="+text)
				if(text==item_name)
				{
					exist = true
					break

				}//end if

			}//end for

		}//end if

		return exist
	}


	public static String GetFieldText_func(WebElement element_tmp)
	{
		String observed = element_tmp.getText()

		Boolean Isblank = false
		if(element_tmp.getTagName()=="select")
		{
			Select e_tmp = new Select(element_tmp)

			int size_tmp = e_tmp.getAllSelectedOptions().size()

			if(size_tmp!=0)
				observed = e_tmp.getAllSelectedOptions().get(0).getText()
			else
			{
				Isblank = true
			}

		}
		else if(element_tmp.getAttribute("type")=="checkbox"||element_tmp.getAttribute("type")=="radio")
		{
			observed = element_tmp.isSelected().toString()
		}

		if(Isblank==true)
			observed = element_tmp.findElement(By.xpath("./../span")).getText()

		return observed
	}//end void

	public static void ScrollElementtoViewPort_func(WebElement element)
	{
		WebDriver driver = DriverFactory.getWebDriver()

		int center_y = driver.manage().window().position.getY()

		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,"+center_y+")","");

		center_y = ((Number)((JavascriptExecutor) driver).executeScript("return window.innerHeight","")).intValue()

		center_y= center_y/2+150

		int x = element.location.getX()
		Math math_c = new Math()
		int y =  element.location.getY()-center_y

		((JavascriptExecutor) driver).executeScript("window.scrollTo(0,"+y+")","");

		//element.click()
	}

	public static void VerifyRecordExistTable_func(WebElement table, String expect_str,String note = "")
	{
		Boolean exist = false
		List<String> list_str = Common.GetTableRecordsPerPaging_func(table)

		for(int i = 0;i <list_str.size();i++)
			if(list_str.get(i).contains(expect_str))
		{
			exist = true
			break
		}//end if

		if(exist==false)
		{
			GlobalVariable.glb_TCStatus= false
			GlobalVariable.glb_TCFailedMessage+="Expect record not exist in table.[Observed: list_str - Expected: "+expect_str+"] "+note+".]\n"
		}
	}//end void


	//VERIFY RECORD ITEM NOT DISPLAY IN A TABLE
	public static void VerifyRecordNotExistTable_func(WebElement table, String expect_str,String note = "")
	{
		Boolean exist = false
		List<String> list_str = Common.GetTableRecordsPerPaging_func(table)

		for(int i = 0;i <list_str.size();i++)
			if(list_str.get(i).contains(expect_str))
		{
			exist = true
			break
		}//end if

		if(exist==true)
		{
			GlobalVariable.glb_TCStatus= false
			GlobalVariable.glb_TCFailedMessage+="Expect record Should NOT exist in table.[Observed: "+list_str+" - Expected: "+expect_str+"] "+note+".]\n"
		}
	}//end void

	//VERIFY TINY CONTENT TEXT EQUAL
	public static void VerifyTinyContentEqual_func(String area, WebElement tinyElement, String expect_str)
	{
		String Content_str = tinyElement.getText()

		WebDriver webDriver = DriverFactory.getWebDriver()

		webDriver = Navigation.SwitchIframe_func()

		if(Content_str!=expect_str)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += area+"- Content. "+"Incorrect:[Observed: "+Content_str+" - Expected: "+expect_str+"].\n"
		}
	}
	
	public static void ChangeFFDownloadSetting_func()
	{
		WebDriver webDriver = null
		try{
			webDriver = DriverFactory.getWebDriver()
		}
		
		catch(Exception e)
		{
			WebUI.openBrowser(GlobalVariable.glb_URL)
			webDriver = DriverFactory.getWebDriver()
		}
		
		
		
		webDriver.get("about:config")
		
		((JavascriptExecutor) webDriver).executeScript("document.getElementsByTagName('button')[0].click();");
		((JavascriptExecutor) webDriver).executeScript("var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);prefs.setBoolPref('browser.download.useDownloadDir', false);");
				
		
	}
	
	public static void DownloadFile_func(WebElement element_tmp, String file_name)
	{
		FileAccess file_access = new FileAccess() 
		element_tmp.click()
		Thread.sleep(3000)
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard()

		StringSelection string_slect = new StringSelection(file_access.file_path+file_name)

		clipboard.setContents(string_slect, string_slect)
		WebDriver driver = DriverFactory.getWebDriver()
		
		if(this.GetOS_func().toString().contains("mac")) {
			
		MacUploadFile_func(file_access.file_path+file_name)
			
		/*
		 println ("Start to upload image in MAC")
		 Runtime.getRuntime().exec("osascript "+"/Users/bigin1/Documents/macupload_valid.scpt")
		 Thread.sleep(2000)
		 println ("Completed to upload image in MAC")
		 */
		}
		else{
			Robot rbot = new Robot()
			
			//SELECT OPTION SAVE FILE
			rbot.keyPress(KeyEvent.VK_ALT)
			rbot.keyPress(KeyEvent.VK_S)
			Thread.sleep(2000)
			
			rbot.keyRelease(KeyEvent.VK_S);
			rbot.keyRelease(KeyEvent.VK_ALT);
			
			Thread.sleep(2000)
			rbot.keyPress(KeyEvent.VK_ENTER);
			rbot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000)
			
				
			rbot.keyPress(KeyEvent.VK_CONTROL)
			rbot.keyPress(KeyEvent.VK_V)
			Thread.sleep(1000)
			rbot.keyRelease(KeyEvent.VK_V);
			rbot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000)

			rbot.keyPress(KeyEvent.VK_ENTER);
			rbot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000)
		}
		
		
	}//end void
	
	
}
