package functions

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.Session.GlobalRequestReply

import java.sql.Connection;
import java.util.logging.Logger
import java.util.logging.Level
import java.sql.Statement
import internal.GlobalVariable as GlobalVariable
import java.sql.*
import org.postgresql.util.PSQLException
import com.jcraft.jsch.ChannelExec
import com.jcraft.jsch.Channel
import java.lang.Integer
import java.net.ConnectException
import java.io.File


public class DBCONNECT {

	/**
	 * Java Program to connect to remote database through SSH using port forwarding
	 * @throws SQLException
	 */

	String sshhostname
	String sshlogin
	String sshpassword
	int SshPort// remote SSH host port number
	Session session

	int Localport
	int DbRemotePort // remote port number of your database
	String DbUser// database loging username
	String DbPassword// database login password
	String DbBHost
	String driverName
	Statement stmt
	Connection conn
	String connection_string
	String Dbname
	String privatekey

	public DBCONNECT()
	{
		sshhostname = GlobalVariable.glb_sshhostname
		sshlogin = GlobalVariable.glb_sshlogin
		sshpassword = GlobalVariable.glb_sshpassword
		SshPort = 22; // remote SSH host port number
		DbRemotePort = GlobalVariable.glb_DbPort
		DbBHost =GlobalVariable.glb_DbBHost
		DbUser =GlobalVariable.glb_DbUser // database loging username
		DbPassword = GlobalVariable.glb_DbPassword // database login password
		Localport = GlobalVariable.glb_DbPort
		Dbname = GlobalVariable.glb_dbName
		privatekey ="ssh_bigin_top"


		/*
		 if(GlobalVariable.glb_SiteName=='Bec'||GlobalVariable.glb_CoinName=='TTC')
		 {
		 driverName="com.mysql.jdbc.Driver";
		 connection_string = "jdbc:mysql://localhost:"+Localport+'/'+Dbname
		 }
		 */

		driverName="org.postgresql.Driver";
		connection_string = "jdbc:postgresql://localhost:"+Localport+'/'+Dbname

		conn = null;

	}


	public void OpenSSHSessionWithKey()
	{

		try{

			println ("Connecting..")
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch ssh = new JSch();

			println ("Add private key")

			String container = File.separator+ "Data Files"+File.separator

			String path = System.getProperty("user.dir")+container+privatekey
			println(path)
			ssh.addIdentity(path)
			session = ssh.getSession(sshlogin, sshhostname, SshPort);
			println ("Sessiongot")
			session.setConfig(config);
			//session.setPassword(sshpassword);
			session.connect();
			println ("Connected")

		}
		catch(Exception e)
		{

			println (e.getMessage())
		}
	}



	public void OpenSSHSession()
	{

		try{
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch ssh = new JSch();
			session = ssh.getSession(sshlogin, sshhostname, SshPort);
			session.setConfig(config);
			session.setPassword(sshpassword);
			session.connect();
		}
		catch(ConnectException e)
		{
			throw e
		}
	}



	public void OpenDBConnection()
	{
		println("Forwarding...with "+DbBHost)
		session.setPortForwardingL(Localport, DbBHost, DbRemotePort);
		println("Forwarded")
		Class.forName(driverName);

		try{

			println("ConnectingDB...")
			conn = DriverManager.getConnection(connection_string, DbUser, DbPassword);
			println("ConnectedDB Completed")
		}//end try
		catch (PSQLException e) {
			e.printStackTrace();
		}

		stmt = conn.createStatement();

	}//end method



	public void CloseAllConnect()
	{
		stmt.close()
		conn.close()
		session.disconnect();
	}


}
