package functions

import org.junit.Assert
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable
import pages_elements.ActivityTab
import pages_elements.ClientsPage
import pages_elements.DocumentTab
import pages_elements.HistoryTab
import pages_elements.LandingPage
import pages_elements.LeftMenu
import pages_elements.LoginPage
import pages_elements.NoteTab
import pages_elements.SignUpModal
import pages_elements.SmartTaskPage
import pages_elements.TemplatePage
import pages_elements.TransfereePage
import pages_elements.Usersetting_menu
import pages_elements.VendorPage

public class Navigation {

	private static String TinyIframe_txt_xpath = "//div[@class = 'tox-edit-area']/iframe"

	public Navigation() {
	}

	public static WebElement TinyIframe_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TinyIframe_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebDriver SwitchIframe_func(WebElement element=null) {
		WebDriver webDriver = DriverFactory.getWebDriver()

		if(element!=null)
			webDriver.switchTo().frame(element)
		else
			webDriver.switchTo().parentFrame()

		return webDriver//DriverFactory.changeWebDriver(webDriver)
	}


	public static void GotoUserProfile_func(){
		Usersetting_menu.UserSetting_dropdown().click()
		WebUI.delay(1)
		Usersetting_menu.Profile_mnuitem().click()
		WebUI.delay(5)
	}

	public static void OpenSite_func() {
		WebUI.openBrowser(GlobalVariable.glb_URL)

		LoginPage.Acceptalert()

		WebUI.delay(5)
	}

	public static void OpenSignUpModal(String user_type = "Personal") {
		LandingPage currentpage = new LandingPage()

		LoginPage loginpage = new LoginPage()
		WebUI.delay(5)
		currentpage.Login_lnk().click()
		WebUI.delay(2)
		loginpage.Signup_link().click()

		SignUpModal modal = new SignUpModal()

		WebUI.delay(5)

		if(user_type=="Personal") {
			modal.Personal_opt().click()
		}
		else if(user_type=="Business") {
			modal.Business_opt().click()
		}

		else {
			modal.Mover_opt().click()
		}
		WebUI.delay(5)
	}

	public static void GotoForgotPass() {
		this.OpenSite_func()

		LandingPage.Login_lnk().click()

		LoginPage.ForgotPass_lnk().click()
		WebUI.delay(5)
	}

	public static void GotoTransfereePage() {
		try{
			if(TransfereePage.TransfList_tbl()==null) {
				this.CloseBrowserTab(1)
				LeftMenu.Transferee_mnuitem().click()
				WebUI.delay(1)

				if(LandingPage.LoadingArc()!=null)
					Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

				if(LandingPage.Loading_icon()!=null)
					Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

				Common.WaitForElementDisplay(TransfereePage.NewTransf_btn())
			}
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO TRANSFEREE PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoVendorPage() {

		try{
			if(VendorPage.VendorList_tbl()==null) {
				this.CloseBrowserTab(1)
				LeftMenu.Vendor_mnuitem().click()
				WebUI.delay(5)

				if(LandingPage.LoadingArc()!=null)
					Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

				if(LandingPage.Loading_icon()!=null)
					Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

				Common.WaitForElementDisplay(VendorPage.VendorList_tbl())
			}
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO VENDOR PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoClientPage() {

		try{
			if(ClientsPage.ClientList_tbl()==null) {
				this.CloseBrowserTab(1)
				LeftMenu.Client_mnuitem().click()
				WebUI.delay(5)

				if(LandingPage.LoadingArc()!=null)
					Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

				if(LandingPage.Loading_icon()!=null)
					Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

				Common.WaitForElementDisplay(ClientsPage.ClientList_tbl())
			}
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT PAGE due to:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoRolePage() {
		try{
			this.CloseBrowserTab(1)
			LeftMenu.Role_mnuitem().click()
			WebUI.delay(5)
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO ROLE PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoUserPage() {
		try{
			this.CloseBrowserTab(1)
			LeftMenu.User_mnuitem().click()
			WebUI.delay(5)
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO USER PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoTaskListPage() {
		try{
			this.CloseBrowserTab(1)
			LeftMenu.TaskList_mnuitem().click()
			WebUI.delay(5)
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO TASKLIST PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoSmartTaskPage() {

		try{
			
			if(SmartTaskPage.SmartTaskList_tbl()==null)
			{
			LeftMenu.Admin_mnu().click()
			WebUI.delay(1)

			Common.HandleElementClick(LeftMenu.SmartTask_mnuitem())

			WebUI.delay(5)

			if(LandingPage.LoadingArc()!=null)
				Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

			if(LandingPage.Loading_icon()!=null)
				Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
			}
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO SMARTTASK PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoTemplatePage() {

		try{

			if(TemplatePage.Templ_List_tbl()==null) {
				
				Common.HandleElementClick(LeftMenu.Admin_mnu())
			
				WebUI.delay(1)
				LeftMenu.Template_mnuitem().click()

				if(LandingPage.Loading_icon()!=null)
					Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

				Common.WaitForElementDisplay(TemplatePage.Search_btn())
			}//end if


		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO TEMPLATE PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoCustomFieldPage() {
		try{
			LeftMenu.Admin_mnu().click()
			WebUI.delay(1)
			LeftMenu.Custom_mnuitem().click()
			WebUI.delay(5)
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO CUSTOMFIELD PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoTransDetailsPage(String fullname_str) {

		try{

			Common.WaitForElementDisplay(TransfereePage.TransfList_tbl())

			TransfereePage.TransfName_link(fullname_str).click()
			WebUI.delay(5)
			this.GoToBrowserTab(1)
			//webDriver.close();
			//webDriver.switchTo().window(tabs2.get(0));


		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO TRANFEREE DETAILS PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GotoClientDetailsPage(String fullname_str) {
		try{
			ClientsPage.ClientName_link(fullname_str).click()
			WebUI.delay(5)
			this.GoToBrowserTab(1)
			//webDriver.close();
			//webDriver.switchTo().window(tabs2.get(0));


		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO CLIENT DETAILS PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void GoToBrowserTab(int index=0) {
		WebDriver webDriver = DriverFactory.getWebDriver()
		ArrayList<String> tabs2 = new ArrayList<String> (webDriver.getWindowHandles());
		println(tabs2)
		webDriver.switchTo().window(tabs2.get(index));
		Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)
	}

	public static void CloseBrowserTab(int index = 1) {
		WebDriver webDriver = DriverFactory.getWebDriver()
		ArrayList<String> tabs2 = new ArrayList<String> (webDriver.getWindowHandles());
		//		println(tabs2)
		//		webDriver.switchTo().window(tabs2.get(index));
		if(tabs2.size>1)
		{
			webDriver.close();
			webDriver.switchTo().window(tabs2.get(0));
		}

		Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)
	}

	public static void gotoVendorDetailsPage(String vendorName) {
		try{
			VendorPage.vendorName_link(vendorName).click()
			WebUI.delay(5)
			WebDriver webDriver = DriverFactory.getWebDriver()
			ArrayList<String> tabs2 = new ArrayList<String> (webDriver.getWindowHandles());
			println(tabs2)
			webDriver.switchTo().window(tabs2.get(1));
			//webDriver.close();
			//webDriver.switchTo().window(tabs2.get(0));
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO VENDOR DETAILS PAGE:"+e.message+".\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}

	public static void gotoFirstVendorDetailsPage() {
		try{
			VendorPage.firstVendorName_link().click()
			WebUI.delay(5)
			WebDriver webDriver = DriverFactory.getWebDriver()
			ArrayList<String> tabs2 = new ArrayList<String> (webDriver.getWindowHandles());
			println(tabs2)
			webDriver.switchTo().window(tabs2.get(1));
			//webDriver.close();
			//webDriver.switchTo().window(tabs2.get(0));
		}
		catch(Exception e) {
			GlobalVariable.glb_TCFailedMessage +="CANNOT NAVIGATE TO VENDOR DETAILS PAGE:"+e.message+".\n"
			//Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
	}//end void


	public static void GotoActivityTab_func()
	{
		Common.WaitForElementDisplay(ActivityTab.activity_tab())

		if(ActivityTab.newActivity_btn() == null)
			ActivityTab.activity_tab().click()

		WebUI.delay(3)

		//Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
	}//end void

	public static void GotoNoteTab_func()
	{
		Common.WaitForElementDisplay(NoteTab.Note_tab())

		if(NoteTab.CreateNew_btn()==null)
			NoteTab.Note_tab().click()

		WebUI.delay(3)

		//Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
	}//end void

	public static void GotoHistoryTab_func()
	{
		Common.WaitForElementDisplay(HistoryTab.History_tab())

		HistoryTab.History_tab().click()

		WebUI.delay(3)

		//Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
	}//end void

	public static void GotoDocumentTab_func()
	{
		Common.WaitForElementDisplay(DocumentTab.Doc_Tab())

		if(DocumentTab.CreateNew_btn()==null)

			Common.HandleElementClick(DocumentTab.Doc_Tab())

		WebUI.delay(3)

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

	}//end void

	public static void GotoTemplateDetailsPage_func(String tpl_name)
	{
		TemplatePage.TemplName_link(tpl_name).click()

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(TemplatePage.Edit_ServiceType_ddl())
	}
}
