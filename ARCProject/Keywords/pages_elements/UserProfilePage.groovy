package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action

public class UserProfilePage {
	private static WebDriver webDriver;
	private static Id_txt_xpath
	private static Username_txt_xpath
	private static Email_txt_xpath
	private static Firstname_txt_xpath
	private static Lastname_txt_xpath
	private static Phone_txt_xpath
	private static Birthday_txt_xpath
	private static Passpord_txt_xpath
	private static GeneralEditGear_icon_xpath
	private static AddressEditGear_icon_xpath
	private static GeneralUpdate_btn_xpath
	private static GeneralCancel_btn_xpath
	private static AddressUpdate_btn_xpath
	private static AddressCancel_btn_xpath
	private static Address1_txt_xpath
	private static Address2_txt_xpath
	private static City_txt_xpath
	private static State_txt_xpath
	private static PostalCode_txt_xpath
	private static Country_txt_xpath
	private static Refer_lnk_xpath
	private static ValidationFirstName_msg_xpath
	private static ValidationLastName_msg_xpath
	private static ValidationPhone_msg_xpath
	private static ValidationPassport_msg_xpath
	private static ValidationPostal_msg_xpath




	public UserProfilePage() {
		webDriver = DriverFactory.getWebDriver();
		if(GlobalVariable.glb_SiteName=='Bec') {
			Id_txt_xpath	= "//input[@placeholder='ID']"
			Username_txt_xpath	= "//input[@placeholder='User Name']"
			Email_txt_xpath	= "//input[@type='email']"
			Firstname_txt_xpath	= "//input[@name='firstname']"
			Lastname_txt_xpath	= "//input[@name='lastname']"
			Phone_txt_xpath	= "//input[@name='phone']"
			Birthday_txt_xpath	= "//input[@name='birthday']"
			Passpord_txt_xpath	= "//input[@name='passport']"
			GeneralEditGear_icon_xpath	= "//div[@id='general-form']//a[@class='actions__item ion ion-gear-a']"
			AddressEditGear_icon_xpath	= "//div[@id='address-form']//a[@class='actions__item ion ion-gear-a']"
			GeneralUpdate_btn_xpath	="//div[@id='general-form']//button[text()='Update']"
			GeneralCancel_btn_xpath	= "//div[@id='general-form']//button[text()='Cancel']"
			AddressUpdate_btn_xpath	= "//div[@id='address-form']//button[text()='Update']"
			AddressCancel_btn_xpath	= "//div[@id='address-form']//button[text()='Cancel']"
			Address1_txt_xpath	= "//input[@name='address']"
			Address2_txt_xpath	= "//input[@name='address2']"
			City_txt_xpath	= "//input[@name='city']"
			State_txt_xpath	= "//input[@name='state']"
			PostalCode_txt_xpath	= "//input[@name='postal_code']"
			Country_txt_xpath	= "//label[text()='Country']/../select"
			Refer_lnk_xpath	= "//input[@id='copy-input']"
			ValidationFirstName_msg_xpath	= "//ul[@data-field='first_name']/li"
			ValidationLastName_msg_xpath	= "//ul[@data-field='last_name']/li"
			ValidationPhone_msg_xpath	= "//ul[@data-field='phone']/li"
			ValidationPassport_msg_xpath	= "//ul[@data-field='passport']/li"
			ValidationPostal_msg_xpath = "//ul[@data-field='postal_code']/li"
		}

		if(GlobalVariable.glb_SiteName=='Vinestate'
		||GlobalVariable.glb_SiteName=='WWR') {
			Id_txt_xpath	= "//label[text()='ID']/../input"
			Username_txt_xpath	= "//input[@placeholder='User Name']"
			Email_txt_xpath	= "//input[@type='email']"
			Firstname_txt_xpath	= "//input[@name='first_name']"
			Lastname_txt_xpath	= "//input[@name='last_name']"
			Phone_txt_xpath	= "//input[@name='phone']"
			Birthday_txt_xpath	= "//input[@id='datepicker']"
			Passpord_txt_xpath	= "//input[@name='passport']"
			GeneralEditGear_icon_xpath	= "//div[@id='general-form']//a[@class='actions__item fi-ellipsis switch-panel-mode']"
			AddressEditGear_icon_xpath	= "//div[@id='address-form']//a[@class='actions__item fi-ellipsis switch-panel-mode']"
			GeneralUpdate_btn_xpath	="//form[@id='form-update-profile-general']//button[text()='Update']"
			GeneralCancel_btn_xpath	= "//form[@id='form-update-profile-general']//button[text()='Cancel']"
			AddressUpdate_btn_xpath	= "//form[@id='form-update-profile-address']//button[text()='Update']"
			AddressCancel_btn_xpath	= "//form[@id='form-update-profile-address']//button[text()='Cancel']"
			Address1_txt_xpath	= "//input[@name='address']"
			Address2_txt_xpath	= "//input[@name='address2']"
			City_txt_xpath	= "//input[@name='city']"
			State_txt_xpath	= "//input[@name='state']"
			PostalCode_txt_xpath	= "//input[@name='postal_code']"
			Country_txt_xpath	= "//label[text()='Country']/../select"
			Refer_lnk_xpath	= "//input[@id='copy-input']"
			ValidationFirstName_msg_xpath	= "//ul[@data-field='first_name']/li"
			ValidationLastName_msg_xpath	= "//ul[@data-field='last_name']/li"
			ValidationPhone_msg_xpath	= "//ul[@data-field='phone']/li"
			ValidationPassport_msg_xpath	= "//ul[@data-field='passport']/li"
			ValidationPostal_msg_xpath = "//ul[@data-field='postal_code']/li"
		}
		else if(GlobalVariable.glb_SiteName=='CLP') {
			Id_txt_xpath	= "//label[text()='ID']/../input"
			Username_txt_xpath	= "//input[@placeholder='User Name']"
			Email_txt_xpath	= "//input[@type='email']"
			Firstname_txt_xpath	= "//input[@name='first_name']"
			Lastname_txt_xpath	= "//input[@name='last_name']"
			Phone_txt_xpath	= "//input[@name='phone']"
			Birthday_txt_xpath	= "//input[@id='datepicker']"
			Passpord_txt_xpath	= "//input[@name='passport']"
			GeneralEditGear_icon_xpath	= "//div[@id='general-form']//a[@class='actions__item fas fa-cog switch-panel-mode']"
			AddressEditGear_icon_xpath	= "//div[@id='address-form']//a[@class='actions__item fas fa-cog switch-panel-mode']"
			GeneralUpdate_btn_xpath	="//form[@id='form-update-profile-general']//button[text()='Update']"
			GeneralCancel_btn_xpath	= "//form[@id='form-update-profile-general']//button[text()='Cancel']"
			AddressUpdate_btn_xpath	= "//form[@id='form-update-profile-address']//button[text()='Update']"
			AddressCancel_btn_xpath	= "//form[@id='form-update-profile-address']//button[text()='Cancel']"
			Address1_txt_xpath	= "//input[@name='address']"
			Address2_txt_xpath	= "//input[@name='address2']"
			City_txt_xpath	= "//input[@name='city']"
			State_txt_xpath	= "//input[@name='state']"
			PostalCode_txt_xpath	= "//input[@name='postal_code']"
			Country_txt_xpath	= "//label[text()='Country']/../select"
			Refer_lnk_xpath	= "//input[@id='copy-input']"
			ValidationFirstName_msg_xpath	= "//ul[@data-field='first_name']/li"
			ValidationLastName_msg_xpath	= "//ul[@data-field='last_name']/li"
			ValidationPhone_msg_xpath	= "//ul[@data-field='phone']/li"
			ValidationPassport_msg_xpath	= "//ul[@data-field='passport']/li"
			ValidationPostal_msg_xpath = "//ul[@data-field='postal_code']/li"
		}
	}




	public static WebElement ValidationPostal_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationPostal_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationFirstName_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationFirstName_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationLastName_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationLastName_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationPhone_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationPhone_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationPassport_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationPassport_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Id_txt() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Id_txt_xpath));
			return temp_element;
		}

		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Username_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Username_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Email_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Email_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Firstname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Firstname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Lastname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Lastname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Phone_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Phone_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Birthday_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Birthday_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Passpord_txt() {
		try {
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Passpord_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GeneralEditGear_icon() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(GeneralEditGear_icon_xpath));
		return temp_element;
	}

	public static WebElement AddressEditGear_icon() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(AddressEditGear_icon_xpath));
		return temp_element;
	}

	public static WebElement GeneralUpdate_btn() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GeneralUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GeneralCancel_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(GeneralCancel_btn_xpath));
		return temp_element;
	}

	public static WebElement AddressUpdate_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(AddressUpdate_btn_xpath));
		return temp_element;
	}

	public static WebElement AddressCancel_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(AddressCancel_btn_xpath));
		return temp_element;
	}

	public static WebElement Address1_txt() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Address1_txt_xpath));
		return temp_element;
	}

	public static WebElement Address2_txt() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Address2_txt_xpath));
		return temp_element;
	}

	public static WebElement City_txt() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(City_txt_xpath));
		return temp_element;
	}

	public static WebElement State_txt() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(State_txt_xpath));
		return temp_element;
	}

	public static WebElement PostalCode_txt() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(PostalCode_txt_xpath));
		return temp_element;
	}

	public static WebElement Country_txt() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Country_txt_xpath));
		return temp_element;
	}

	public static WebElement Refer_lnk() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Refer_lnk_xpath));
		return temp_element;
	}


	////======METHOD
	public static Boolean IsTextFieldEditable(WebElement e,String value_str) {
		WebDriver webDriver = DriverFactory.getWebDriver();

		((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", e);

		Actions actions = new Actions(webDriver);
		Action action = actions.sendKeys(e,value_str).build();
		action.perform();
		if(e.getAttribute("value").contains(value_str)) {
			return true
		}
		return false
	}
}
