package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.Activity
import objects.Client
import objects.Mailinfo
import objects.NoteInfo
import objects.Transferee
import objects.Vendor
import internal.GlobalVariable

public class HeadingMenu {
	private static String NewTrans_btn_xpath= "//div[@class='page-menu']//a/p[text()='Transferee']"
	private static String NewVendor_btn_xpath= "//div[@class='page-menu']//a/p[text()='Vendor']"
	private static String NewClient_btn_xpath= "//div[@class='page-menu']//a/p[text()='Client']"
	//private static String NewActivity_btn_xpath= "//div[@class='page-menu']//a/p[text()='Activity']" - Obsolete
	private static String NewNote_btn_xpath= "//div[@class='page-menu']//a/p[text()='Note']"
	private static String Print_btn_xpath= "//div[@class='page-menu']//a/p[text()='Print']"
	private static String Email_btn_xpath= "//div[@class='page-menu']//a/p[text()='Email']"
	private static String NewTask_btn_xpath = "//div[@class='page-menu']//button//span[text()='Task']"
	private static String NewFollow_btn_xpath = "//div[@class='page-menu']//button//span[text()='Follow Up']"
	
	private static String Entity_ddl_xpath ="//div[@class='btn-group open']//ul[@class='list-unstyled']"


	public HeadingMenu() {
	}
	
	public static WebElement NewTask_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewTask_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	
	public static WebElement NewFollow_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewFollow_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	
	public static WebElement Entity_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Entity_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewTrans_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewTrans_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewVendor_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendor_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewClient_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewClient_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement NewNote_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewNote_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Print_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Print_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Email_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Email_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	//============================METHOD

	public static void CreateTransf_func(Transferee trf, Boolean submit = true)
	{
		this.NewTrans_btn().click()

		NewTransfModal modal = new NewTransfModal()

		Common.WaitForElementDisplay(modal.Create_btn())

		NewTransfModal.FillInfoModal_func(trf,submit)


	}//end void

	public static void CreateVendor_func(Vendor newvendor,String button)
	{
		this.NewVendor_btn().click()
		newvendor.FillModalInfo_func(button)

	}//end void

	public static void CreateClient_func(Client client_info,String button)
	{
		this.NewClient_btn().click()
		client_info.CreateClient_func(button)
	}//end void


	public static void CreateEmail_func(Mailinfo mail_info, String button = "")//button = "Submit"/"Cancel"/""
	{
		//Common.ScrollElementtoViewPort_func(this.Email_btn())

		Common.HandleElementClick(this.Email_btn())


		Common.WaitForElementDisplay(NewEmailModal.Service_ddl())

		NewEmailModal.FillInfoModal_func(mail_info, button)



	}//end void

	public static void CreateCustomEmail_func(Mailinfo mail_info, String button = "")//button = "Submit"/"Cancel"/""
	{
		Common.HandleElementClick(this.Email_btn())

		Common.WaitForElementDisplay(NewEmailModal.Service_ddl())

		NewEmailModal.FillInfoModal_func(mail_info, button,true)



	}//end void

	public static void CreateTransfNote_func(NoteInfo note, String button= "Submit")
	{
		Common.HandleElementClick(this.NewNote_btn())


		NoteTab.FillInfoModalTransf_func(note,button)

	}//end void



	public static void CreateClientNote_func(NoteInfo note, String button= "Submit")
	{
		Common.HandleElementClick(this.NewNote_btn())

		NoteTab.FillInfoModalClient_func(note,button)
	}

	public static void CreateVendorNote_func(NoteInfo note, String button= "Submit")
	{
		Common.HandleElementClick(this.NewNote_btn())

		NoteTab.FillInfoModalVendor_func(note)
	}

	public static void  Print_func()
	{

	}//end void
	
	
	public static void CreateTaskActivity_func(Activity activity_arg,String button = "Submit")
	{
		Common.HandleElementClick(this.NewTask_btn())
		
		Common.WaitForElementDisplay(this.Entity_ddl())
		
		this.FillModalDetails_func(activity_arg,button)
			
	}
	
	
	public static void CreateFollowActivity_func(Activity activity_arg,String button = "Submit")
	{
		
		
		Common.HandleElementClick(this.NewFollow_btn())
		
		Common.WaitForElementDisplay(this.Entity_ddl())
		
		this.FillModalDetails_func(activity_arg,button)
		
	}
	
	public static void FillModalDetails_func(Activity activity_arg, String button)
	{
		ActivityTab tab = new ActivityTab()
		
		String entity_str = ""
		//Wait for modal is displayed
		//-Select Client Name/Vendor Name
		
		if(activity_arg.BelongTo=="Transf")
		{
			entity_str = "Transferee"
		}
		
		else
		{
			entity_str = activity_arg.BelongTo
		}
		
		//Select The Assginee: Transf Name, Client Company, VEndor Name
		Common.SelectItemULElement_func(this.Entity_ddl(), entity_str)
		
		if(activity_arg.BelongTo =="Vendor")
		{
			Common.HandleElementClick(tab.VendorName_vlue())
			
			Common.sendkeyTextToElement(tab.VendorName_txt(), activity_arg.Assignee)
			
			Common.SelectItemULElement_func(tab.VendorList_ddl(), entity_str)
		}
		
		
		else if(activity_arg.BelongTo =="Client")
		{
			Common.sendkeyTextToElement(tab.ClientName_txt(), activity_arg.Assignee)
			
			Common.SelectItemULElement_func(tab.ClientList_ddl(), entity_str)
		}
		
		else
		
		{
			Common.sendkeyTextToElement(tab.TransfName_txt(), activity_arg.Assignee)
			
			Common.SelectItemULElement_func(tab.TransfList_ddl(), entity_str)
		}
		
		//Provide other info
		tab.FillModalDetails(activity_arg, button)
	}

}
