package pages_elements

import org.junit.Assert
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import functions.Navigation
import internal.GlobalVariable
import objects.Activity

public class ActivityTab {

	private static String activity_tab_xpath = "//a[contains(text(),'Activities')]"
	private static String newTitleActivity_msg_xpath = "//li[contains(text(),'The title field is required.')]"
	private static String newSheduledWithActivity_msg_xpath = "//ul[@data-field='scheduled_with']"
	private static String newActivity_btn_xpath = "//button[contains(text(),'New Activity')]"
	private static String submitNewActivity_btn_xpath = "//button[contains(text(),'Submit')]"
	private static String CreateNew_btn_xpath= "//button[text() = 'New Activity']"
	private static String Search_btn_xpath= "//button[contains(@id,'btn-search')]"
	private static String Clear_btn_xpath= "//button[text() = 'Clear']"
	private static String SearchFromDate_txt_xpath= "//input[contains(@id,'from_date')]"
	private static String SearchToDate_txt_xpath= "//input[contains(@id,'to_date')]"
	private static String SearchType_slect_xpath = "//select[contains(@placeholder,'Types')]"
	private static String SearchPriority_slect_xpath = "//select[contains(@placeholder,'Priorities')]"
	private static String SearchStatus_slect_xpath = "//select[contains(@placeholder,'Status')]"
	private static String ActivityType_slect_xpath= "//div[@class = 'modal-body clearfix']//select[@placeholder = 'Activity Type']"
	private static String ServiceType_slect_xpath= "//div[@class = 'modal-body clearfix']//select[@placeholder = 'Service type']"
	private static String Start_txt_xpath= "//div[@class = 'modal-body clearfix']//input[contains(@id,'ActivityCreate_from_date')]"
	private static String End_txt_xpath= "//div[@class = 'modal-body clearfix']//input[contains(@id,'ActivityCreate_to_date')]"
	private static String Title_txt_xpath= "//div[contains(@class,'content-body container-fluid')]//input[@placeholder = 'Title']"//"//div[contains(@class,'modal-body')]//input[@placeholder = 'Title']"
	private static String Content_txt_xpath= "//p/.."
	private static String Submit_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'create']"
	private static String Cancel_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'cancel-create']"
	private static String Attach_lnk_xpath= "//div[@id = 'new-activity-attachment']//div[@class = 'content dz-clickable']"
	private static String ActivityList_tbl_xpath= "//table[@class = 'VueTables__table btable table dataTable table-striped table-hover']//tbody | //table[@id='table-list-activities']"
	private static String Edit_btn_xpath = "//div[@id ='loading-modal-edit-activity']//a[@data-original-title='Edit']"
	private static String Close_opt_xpath = "//div[@id ='loading-modal-edit-activity']//input[@type = 'checkbox']"
	private static String Activity_tab_xpath = "//a[@href='#tab-vendor-activities'] | //a[@href='#tab-vendor-activities'] | //a[@href ='#activities']"
	private static String ActivityPriority_slect_xpath = "//select[@placeholder='Priority']"
	private static String ScheduledWith_slect_xpath = "//select[@placeholder='Scheduled With']"
	private static String ScheduledWith_listDrd_xpath = "//select[@placeholder='Scheduled With']/option"
	private static String NotiRemider_slect_xpath = "//select[@placeholder='Notification Reminder']"
	private static String NotiRemider_listDrd_xpath = "//select[@placeholder='Notification Reminder']/option"
	private static String EditActivityVendor_btn_xpath = "//div[@class ='modal-header']//a[@data-original-title = 'Edit']"
	private static String CloseActivityVendor_opt_xpath = "//div[@class ='modal-body clearfix']//input[@type = 'checkbox']"
	private static String ResultActivity_slect_xpath = "//select[@placeholder='Result']"
	private static String ResultActivity_listDrd_xpath = "//select[@placeholder='Result']/option"
	private static String ReasonClosing_txt_xpath = "//textarea[@placeholder='Reason']"
	private static String Task_icon_xpath = "//i[@data-original-title='Task']"
	private static String FollowUp_icon_xpath = "//i[@data-original-title='Follow Up']"
	private static String Delete_icon_xpath = "//div[@class='table-responsive']//i[@class='icon-close']"

	private static String ActivityName_lnk_xpath = "//a[contains(@href,'activity/edit')]"
	private static String Update_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'update']"
	private static String CancelEdit_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'cancel-update']"
	private static String ModalClose_btn_xpath = "//div[contains(@id,'modal-edit')]//i[@class='icon-close']"

	private static String SubmitTaskVendor_btn_xpath = "//button[@id='create-new-transferee-activity']"
	
	private static String ClientName_txt_xpath = "//div[@class='input-control-wrap']//input[@placeholder='Company Name']"
	
	private static String VendorName_txt_xpath = "//input[contains(@placeholder,'Vendor Name')]"
	
	private static String TransfName_txt_xpath = "//input[contains(@placeholder,'Transferee')]"
	
	private static String ClientList_ddl_xpath = ClientName_txt_xpath+"//ancestor::div[@class='b__components b__multi__select']//ul"
	
	private static String VendorList_ddl_xpath = VendorName_txt_xpath+"//ancestor::div[@class='b__components b__multi__select']//ul"

	private static String TransfList_ddl_xpath = TransfName_txt_xpath+"//ancestor::div[@id='b-combobox-transferee']//ul"
	
	private static String VendorName_vlue_xpath = "//label[text() = 'Vendor Name']//ancestor::div[@class='col-md-4 form-group']//span[@class='thumb']"
	
	private static String ClientName_vlue_xpath = "//label[text() = 'Company Name']//ancestor::div[@class='col-md-4 form-group']//span[@class='thumb']"
	
	//private static String TransfName_vlue_xpath = ""


	public ActivityTab() {
	}
	
	public static WebElement VendorName_vlue(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(VendorName_vlue_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement ClientName_vlue(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientName_vlue_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement TransfName_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransfName_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement TransfList_ddl(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransfList_ddl_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement ClientName_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientName_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement VendorName_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(VendorName_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement VendorList_ddl(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(VendorList_ddl_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement ClientList_ddl(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientList_ddl_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement activity_tab(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(activity_tab_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newActivity_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newActivity_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement submitNewActivity_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(submitNewActivity_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newTitleActivity_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newTitleActivity_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newSheduledWithActivity_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newSheduledWithActivity_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	public static WebElement ModalClose_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalClose_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CancelEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CancelEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ActivityName_lnk(String title) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			String xpath_str = ActivityName_lnk_xpath + "//span[text() ='"+title+"'] | "+ActivityName_lnk_xpath+"[text() ='"+title+"']"
			//println(xpath_str)
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			//	String xpath_str = ActivityName_lnk_xpath + "span[text() ='"+title+"']"
			//println(xpath_str)
			return null
		}
	}


	public static WebElement ActivityNameOfVendor_lnk(String title) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			ActivityName_lnk_xpath = "//a[contains(text(),'"+title+"')]"
			WebElement temp_element = webDriver.findElement(By.xpath(ActivityName_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Close_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Close_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CloseActivityVendor_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CloseActivityVendor_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ResultActivity_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ResultActivity_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ReasonClosing_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ReasonClosing_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Task_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Task_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FollowUp_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FollowUp_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Delete_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Delete_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CreateNew_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CreateNew_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Clear_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Clear_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchFromDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchFromDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchToDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchToDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchType_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchType_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchPriority_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchPriority_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchStatus_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchStatus_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ActivityType_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ActivityType_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ActivityPriority_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ActivityPriority_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ScheduledWith_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ScheduledWith_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> ScheduledWith_listDrd() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(ScheduledWith_listDrd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NotiRemider_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NotiRemider_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> NotiRemider_listDrd() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(NotiRemider_listDrd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> ResultActivity_listDrd() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(ResultActivity_listDrd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement EditActivityVendor_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element =webDriver.findElement(By.xpath(EditActivityVendor_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ServiceType_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element =webDriver.findElement(By.xpath(ServiceType_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Start_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Start_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement End_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(End_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Title_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Title_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Content_txt() {
		try{
			WebDriver webDriver = Navigation.SwitchIframe_func(Navigation.TinyIframe_txt())
			WebElement temp_element = webDriver.findElement(By.xpath(Content_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Attach_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Attach_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ActivityList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ActivityList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Activity_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Activity_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SubmitTaskVendor_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SubmitTaskVendor_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SubmitActivity_btn(String typeUser) {
		try{
			String SubmitActivity_btn = "//button[@id='create-new-" + typeUser + "-activity']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SubmitActivity_btn));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	//=================================METHOD

	public static void SearchActivity_func(String StartDate_str="", String EndDate_str="")
	{
		if(StartDate_str!="")
		{

			Common.ActionSendkey(this.SearchFromDate_txt(), StartDate_str)

		}
		if(EndDate_str!="")
		{
			Common.ActionSendkey(this.SearchToDate_txt(), EndDate_str)
		}

		Common.HandleElementClick(this.Search_btn())

		WebUI.delay(10)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(ActivityTab.ActivityList_tbl())

	}

	public static void VerifyActivityExistTable_func(Activity Act)
	{
		Boolean exist = false
		this.SearchActivity_func(Act.StartDate)
		List<String> records = Common.GetTableRecordsPerPaging_func(this.ActivityList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(Act.Title))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Activity["+Act.Title+"] NOT exist in table.\n"
		}//end if
	}

	public static void OpenDetailsModal_func(Activity Act)
	{
		this.SearchActivity_func(Act.StartDate)
		Common.WaitForElementDisplay(this.ActivityList_tbl())
		println(this.ActivityName_lnk_xpath + "span[text()='"+Act.Title+"']")
		this.ActivityName_lnk(Act.Title).click()


	}


	public static void VerifyModalDetailsViewMode_func(Activity act_info)
	{
		String area = "Modal Detail - ViewMode - "
		String field = ""

		this.ActivityName_lnk(act_info.Title).click()

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.Title_txt())

		Thread.sleep(5000)

		field = "Title_txt"
		Common.VerifyFieldValueEqual_func(area+field, this.Title_txt(), act_info.Title)

		field = "Content_txt"
		Common.VerifyTinyContentEqual_func(area+field, this.Content_txt(),act_info.Content)

		field = "Close_opt"
		Common.VerifyFieldTextEqual_func(field, this.Close_opt(), act_info.Closed.toString())

	}
	
	
	public void FillModalDetails(Activity activity_arg,String button) {
		
		ActivityTab.Title_txt().sendKeys(Keys.TAB)
		
		if(activity_arg.BelongTo=="Transf")
		{
			//Enter Service Type for Transferee Type
			ActivityTab.ServiceType_slect().click()
			Common.SelectDropdownItem(ActivityTab.ServiceType_slect(), activity_arg.Service)
			
		}
		
		else
		{
			//Enter Schedule for Client or Vendor Type
			Common.SelectDropdownItem(ActivityTab.ScheduledWith_slect(), activity_arg.ScheduleWith)
		}
		
		
		ActivityTab.Title_txt().sendKeys(Keys.TAB)

		Common.ActionSendkey(ActivityTab.Start_txt(), activity_arg.StartDate)
		WebUI.delay(1)

		Common.ActionSendkey(ActivityTab.End_txt(), activity_arg.EndDate)
		WebUI.delay(1)

		Common.sendkeyTextToElement(ActivityTab.Title_txt(), activity_arg.Title)
		
		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action = actions.sendKeys(ActivityTab.Title_txt(), Keys.TAB).sendKeys(Keys.TAB).sendKeys(activity_arg.Content).build()

		action.perform()

		WebUI.delay(2)

		if(button =="Submit")
			ActivityTab.Submit_btn().click()

		WebUI.delay(4)

		String msg = Messages.notification_msg().getText()

		String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
		//println(msg)
		if(msg ==expected_msg)
		{
			GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Tasklist - Activity Creation.Msg: '"+msg+"'.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}
		
			}//end void


}
