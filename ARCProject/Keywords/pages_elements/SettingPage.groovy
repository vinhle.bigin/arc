package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action


public class SettingPage {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private static OldPassword_txt_xpath
	private static NewPassword_txt_xpath
	private static ConfirmNewPassword_txt_xpath
	private static EditPassGear_icon_xpath
	private static Update_btn_xpath
	private static Cancel_btn_xpath
	private static  ValidationOldPass_msg_xpath
	private static  ValidationNewPass_msg_xpath
	private static  ValidationConfirmPass_msg_xpath




	public SettingPage() {
		webDriver = DriverFactory.getWebDriver();
		if(GlobalVariable.glb_SiteName=='Bec') {
			OldPassword_txt_xpath	= "//input[@id='inputPasswordOld']"
			NewPassword_txt_xpath	= "//input[@id='inputPasswordNew']"
			ConfirmNewPassword_txt_xpath	= "//input[@id='inputPasswordConfirm']"
			EditPassGear_icon_xpath	= "//a[@class='actions__item ion ion-gear-a']"
			Update_btn_xpath	= "//button[@id='savePassword']"
			Cancel_btn_xpath	= "//button[text()='Cancel']"
			ValidationOldPass_msg_xpath = "//ul[@data-field='old-password']/li"
			ValidationNewPass_msg_xpath = "//ul[@data-field='password']/li"
			ValidationConfirmPass_msg_xpath = "//ul[@data-field='confirm-pwd']/li"
		}

		else if(GlobalVariable.glb_SiteName=='Vinestate'
		||GlobalVariable.glb_SiteName=='WWR') {
			OldPassword_txt_xpath	= "//input[@name='old-password']"
			NewPassword_txt_xpath	= "//input[@name='password']"
			ConfirmNewPassword_txt_xpath	= "//input[@name='confirm-pwd']"
			EditPassGear_icon_xpath	= "//a[@class='actions__item fi-ellipsis switch-panel-mode']"
			Update_btn_xpath	= "//form[@id='form-update-password']//button[@data-control='submit']"
			Cancel_btn_xpath	= "//form[@id='form-update-password']//button[@data-control='cancel']"
			ValidationOldPass_msg_xpath = "//ul[@data-field='old-password']/li"
			ValidationNewPass_msg_xpath = "//ul[@data-field='password']/li"
			ValidationConfirmPass_msg_xpath = "//ul[@data-field='confirm-pwd']/li"
		}
		else if(GlobalVariable.glb_SiteName=='CLP') {
			OldPassword_txt_xpath	= "//input[@name='old-password']"
			NewPassword_txt_xpath	= "//input[@name='password']"
			ConfirmNewPassword_txt_xpath	= "//input[@name='confirm-pwd']"
			EditPassGear_icon_xpath	= "//div[@id ='change-pw-form']//a"
			Update_btn_xpath	= "//form[@id='form-update-password']//button[@data-control='submit']"
			Cancel_btn_xpath	= "//form[@id='form-update-password']//button[@data-control='cancel']"
			ValidationOldPass_msg_xpath = "//ul[@data-field='old-password']/li"
			ValidationNewPass_msg_xpath = "//ul[@data-field='password']/li"
			ValidationConfirmPass_msg_xpath = "//ul[@data-field='confirm-pwd']/li"
		}
	}

	public static WebElement ValidationOldPass_msg() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationOldPass_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationNewPass_msg() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationNewPass_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationConfirmPass_msg() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationConfirmPass_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement OldPassword_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OldPassword_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewPassword_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewPassword_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ConfirmNewPassword_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ConfirmNewPassword_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement EditPassGear_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(EditPassGear_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	////======METHOD
	public static Boolean IsTextFieldEditable(WebElement e,String value_str) {
		WebDriver webDriver = DriverFactory.getWebDriver();
		Actions actions = new Actions(webDriver);
		Action action = actions.sendKeys(e,value_str).build();
		action.perform();
		if(e.getAttribute("value").contains(value_str)) {
			return true
		}
		return false
	}

}