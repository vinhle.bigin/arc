package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.Transferee
import internal.GlobalVariable

public class Transf_OtherInfoTab {


	private static String Other_tab_xpath = "//a[@href = '#transferee-other-info']"

	private static String PropertyType_ddl_xpath= "//select[@placeholder = 'Property Type']"
	private static String MoveReason_ddl_xpath= "//select[@placeholder = 'Move Reason']"
	private static String FileType_ddl_xpath= "//select[@placeholder = 'Type of File']"
	private static String SaleType_ddl_xpath= "//select[@placeholder = 'Type of Sale']"
	private static String MoveType_ddl_xpath= "//select[@placeholder = 'Move Type']"
	private static String ProgramType_ddl_xpath= "//select[@placeholder = 'Type of Program']"
	private static String Move_Edit_btn_xpath= "//a[@id ='icon-edit-transferee-moving-panel-other-info']"
	private static String Move_Update_btn_xpath= "//div[@id ='edit-transferee-moving-panel-other-info']//button[@data-control = 'update']"
	private static String Move_Cancel_btn_xpath= "//div[@id ='edit-transferee-moving-panel-other-info']//button[@data-control = 'cancel-update']"
	private static String Date_Edit_btn_xpath= "//a[@id ='icon-edit-transferee-dates-panel-other-info']"
	private static String Date_Update_btn_xpath= "//div[@id ='edit-transferee-dates-panel-other-info']//button[@data-control = 'update']"
	private static String Date_Cancel_btn_xpath= "//div[@id ='edit-transferee-dates-panel-other-info']//button[@data-control = 'cancel-update']"
	private static String InitDate_txt_xpath= "//input[@id ='initiationDate']"
	private static String EffectDate_txt_xpath= "//input[@id ='effectiveDate']"
	private static String InterMoveInDate_txt_xpath= "//input[@id ='interimMoveInDate']"
	private static String InterMoveOutDate_txt_xpath= "//input[@id ='interimMoveOutDate']"
	private static String FullConvrsDate_txt_xpath= "//input[@id ='fullConversationDate']"
	private static String ExpectDate_txt_xpath= "//input[@id ='expectedMoveDate']"
	private static String MoveEndDate_txt_xpath= "//input[@id ='moveEndDate']"
	private static String JobStart_txt_xpath= "//input[@id ='jobStartDate']"
	private static String EmplNumb_txt_xpath= "//div[@org-placeholder = 'Employee Number']//input"
	private static String VendorNumb_txt_xpath= "//div[@org-placeholder = 'Vendor Number']//input"
	private static String FileNumb_txt_xpath= "//div[@org-placeholder = 'File Number']//input"
	private static String GeneralLedger_txt_xpath= "//div[@org-placeholder = 'General Ledger']//input"
	private static String Empl_Edit_btn_xpath= "//a[@id ='icon-edit-transferee-employee-panel-other-info']"
	private static String Empl_Update_btn_xpath= "//div[@id ='edit-transferee-employee-panel-other-info']//button[@data-control = 'update']"
	private static String Empl_Cancel_btn_xpath= "//div[@id ='edit-transferee-employee-panel-other-info']//button[@data-control = 'cancel-update']"
	private static String Curcy_Edit_xpath= "//a[@id ='icon-edit-transferee-currency-and-countries-panel-other-info']"
	private static String Curcy_Update_btn_xpath= "//div[@id ='edit-transferee-currency-and-countries-panel-other-info']//button[@data-control = 'update']"
	private static String Curcy_Cancel_btn_xpath= "//div[@id ='edit-transferee-currency-and-countries-panel-other-info']//button[@data-control = 'cancel-update']"
	private static String PrefCurrency_dll_xpath= "//select[@placeholder = 'Pref Currency']"
	private static String HomeCurrency_dll_xpath= "//select[@placeholder = 'Home Currency']"
	private static String HostCurrency_dll_xpath= "//select[@placeholder = 'Host Currency']"
	private static String PriCountry_dll_xpath= "//select[@placeholder = 'Primary Country']"
	private static String SecCountry_dll_xpath= "//select[@placeholder = 'Secondary Country']"
	private static String HomeCountry_dll_xpath= "//select[@placeholder = 'Home Country']"
	private static String HostCountry_dll_xpath= "//select[@placeholder = 'Host Country']"
	private static String OldCountry_dll_xpath= "//select[@placeholder = 'Old Country']"
	private static String NewCountry_dll_xpath= "//select[@placeholder = 'New Country']"
	private static String Other_Edit_xpath= "//a[@id ='icon-edit-transferee-other-panel']"
	private static String Other_Update_btn_xpath= "//div[@id ='edit-transferee-other-panel']//button[@data-control = 'update']"
	private static String Other_Cancel_btn_xpath= "//div[@id ='edit-transferee-other-panel']//button[@data-control = 'cancel-update']"
	private static String SSN_txt_xpath= "//input[@type ='password']"
	private static String POBox_txt_xpath= "//div[@org-placeholder = 'Po Box']//input"
	private static String BirthDate_txt_xpath= "//div[@org-placeholder = 'Date of Birth']//input"
	private static String Nation_txt_xpath= "//div[@org-placeholder = 'Nationality']//select"



	public Transf_OtherInfoTab() {
	}


	public static WebElement Other_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Other_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PropertyType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PropertyType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoveReason_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveReason_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FileType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FileType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SaleType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SaleType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoveType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ProgramType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ProgramType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Move_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Move_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Move_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Move_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Move_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Move_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Date_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Date_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Date_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Date_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Date_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Date_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement InitDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(InitDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement EffectDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(EffectDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement InterMoveInDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(InterMoveInDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement InterMoveOutDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(InterMoveOutDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FullConvrsDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FullConvrsDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ExpectDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ExpectDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoveEndDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveEndDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement JobStart_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(JobStart_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement EmplNumb_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(EmplNumb_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement VendorNumb_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(VendorNumb_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FileNumb_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FileNumb_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GeneralLedger_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GeneralLedger_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Empl_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Empl_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Empl_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Empl_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Empl_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Empl_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Curcy_Edit() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Curcy_Edit_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Curcy_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Curcy_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Curcy_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Curcy_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PrefCurrency_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PrefCurrency_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HomeCurrency_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HomeCurrency_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HostCurrency_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HostCurrency_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PriCountry_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PriCountry_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SecCountry_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SecCountry_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HomeCountry_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HomeCountry_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HostCountry_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HostCountry_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement OldCountry_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OldCountry_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewCountry_dll() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewCountry_dll_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Other_Edit() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Other_Edit_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Other_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Other_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Other_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Other_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SSN_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SSN_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement POBox_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(POBox_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BirthDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BirthDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Nation_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Nation_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}







	/////////================================METHOD
	public static void VerifyInfoViewMode_func(Transferee tf)
	{

		if(LandingPage.LoadingArc()!=null)
		Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.HandleElementClick(this.Other_tab())

		//Common.WaitForElementDisplay(this.Move_Edit_btn())

		Common.WaitForElementDisplay(this.PropertyType_ddl())

		WebUI.delay(3)

		//Common.VerifyFieldTextEqual_func("PropertyType_ddl", this.PropertyType_ddl(), tf.Other_info.PropertyType, " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("PropertyType_ddl", this.PropertyType_ddl(), tf.Other_info.PropertyType , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("MoveReason", this.MoveReason_ddl(), tf.Other_info.MoveReason, " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("FileType", this.FileType_ddl(), tf.Other_info.FileType, " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("MoveType", this.MoveType_ddl(), tf.Other_info.MoveType, " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("SaleType", this.SaleType_ddl(), tf.Other_info.SaleType, " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("ProgramType", this.ProgramType_ddl(), tf.Other_info.ProgramType , " - Moves Info - View Mode.")


		//================DATES INFO
		Common.VerifyFieldValueEqual_func("InitDate", this.InitDate_txt(), tf.Other_info.InitDate , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("EffectDate", this.EffectDate_txt(), tf.Other_info.EffectDate , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("InterMoveInDate", this.InterMoveInDate_txt(), tf.Other_info.InterMoveInDate , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("InterMoveOutDate", this.InterMoveOutDate_txt(), tf.Other_info.InterMoveOutDate , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("FullConvers", this.FullConvrsDate_txt(), tf.Other_info.FullConvers , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("ExpectedMoveByDate", this.ExpectDate_txt(), tf.Other_info.ExpectedMoveByDate , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("MoveEnd", this.MoveEndDate_txt(), tf.Other_info.MoveEnd , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("JobStart", this.JobStart_txt(), tf.Other_info.JobStart , " - Moves Info - View Mode.")


		//==================EMPLOYEE INFO

		Common.VerifyFieldValueEqual_func("EmployeeNumb", this.EmplNumb_txt(), tf.Other_info.EmployeeNumb , " - Moves Info - View Mode.")

		//Common.VerifyFieldValueEqual_func("VendorNumb", this.VendorNumb_txt(), tf.Other_info.VendorNumb , " - Moves Info - View Mode.")

		//Common.VerifyFieldValueEqual_func("FileNumb",this.FileNumb_txt(), tf.Other_info.FileNumb , " - Moves Info - View Mode.")

		//Common.VerifyFieldValueEqual_func("GeneralLedger", this.GeneralLedger_txt(), tf.Other_info.GeneralLedger , " - Moves Info - View Mode.")

		//Common.VerifyFieldValueEqual_func("JobStart", this.JobStart_txt(), tf.Other_info.JobStart , " - Moves Info - View Mode.")


		//===========Currency and Countries
		Common.VerifyFieldTextEqual_func("PrefCurrency",this.PrefCurrency_dll(), tf.Other_info.PrefCurrency , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("HomeCurrency", this.HomeCurrency_dll(),tf.Other_info.HomeCurrency , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("HostCurrency",this.HostCurrency_dll(),tf.Other_info.HostCurrency , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("PriCountry", this.PriCountry_dll(),tf.Other_info.PriCountry , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("SecCountry", this.SecCountry_dll(),tf.Other_info.SecCountry , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("HomeCountry", this.HomeCountry_dll(),tf.Other_info.HomeCountry , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("HostCountry",this.HostCountry_dll(),tf.Other_info.HostCountry , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("OldCountry", this.OldCountry_dll(),tf.Other_info.OldCountry , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("NewCountry", this.NewCountry_dll(),tf.Other_info.NewCountry , " - Moves Info - View Mode.")


		//======================Transferee Other Info
		Common.WaitForElementDisplay(this.SSN_txt())

		Common.VerifyFieldValueEqual_func("SSN", this.SSN_txt(),tf.Other_info.SSN , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("PoBox", this.POBox_txt(),tf.Other_info.PoBox , " - Moves Info - View Mode.")

		Common.VerifyFieldValueEqual_func("BirthDate", this.BirthDate_txt(),tf.Other_info.BirthDate , " - Moves Info - View Mode.")

		Common.VerifyFieldTextEqual_func("Nation", this.Nation_txt(),tf.Other_info.Nation , " - Moves Info - View Mode.")

	}//end void


}//end class
