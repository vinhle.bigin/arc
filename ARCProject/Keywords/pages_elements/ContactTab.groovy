package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException
import functions.Common
import internal.GlobalVariable
import objects.ContactInfo
import objects.AddressInfo

public class ContactTab {


	private static String Contact_Tab_xpath = "//a[contains(@href,'-contacts')]"
	private static String ModalEdit_btn_xpath= "//div[@class='modal-header']//a[@data-original-title='Edit']"
	private static String ModalSubmit_btn_xpath= "//div[@class = 'modal-footer']//button[@type = 'submit']"
	private static String ModalCancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control,'cancel')]"
	private static String New_btn_xpath = "//button[contains(text(),'New Contact')]"
	private static String ContactList_tbl_xpath = "//table[@id='table-list-contract']/tbody"



	private static String Salutation_ddl_xpath= "//div[@org-placeholder ='Salutation']//select"
	private static String ModalFName_txt_xpath= "//input[@placeholder ='First Name']" //div[@org-placeholder ='First Name']//input | 
	private static String ModalLName_txt_xpath= "//input[@placeholder ='Last Name']" //div[@org-placeholder ='Last Name']//input  |
	private static String ModalCtactType_ddl_xpath= "//select[@placeholder ='Contact Type']" //div[@org-placeholder ='Contact Type']//select | 
	private static String ModalisDefault_chk_xpath= "//label[text() ='Default']/../input"
	private static String ModalAddr1_txt_xpath= "//div[@org-placeholder ='Address 1']//input | //input[@placeholder ='Address1']"
	private static String ModalCity_txt_xpath= "//div[@org-placeholder ='City']//input | //input[@placeholder ='City']"
	private static String ModalState_ddl_xpath= "//select[@placeholder ='State']"//div[@org-placeholder ='State']//select | "
	private static String ModalZip_txt_xpath= "//div[@org-placeholder ='Zip Code']//input  | //input[@placeholder ='Zip']"
	private static String ModalCountry_ddl_xpath= "//div[@org-placeholder ='Country']//select | //select[contains(@placeholder,'Country')]"  
	private static String ModalEmailType_ddl_xpath= "//div[@class='email-wrap extension-fields']//div[@class='row']//div[@org-placeholder='Type']//select"
	private static String ModalEmailAddr_txt_xpath= "//div[@class='email-wrap extension-fields']//div[@class='row']//div[@org-placeholder='E-mail']//input"
	private static String FullName_lnk_xpath = "/.//a[@class ='btn-edit-contact']"
	private static String IsDefault_icon_xpath = "/.//div[@class='check-items-default checked']//i"
	private static String SetDefault_lnk_xpath = "/.//a[contains(@href,'contact/setDefault')]/span"
	private static String Remove_icon_xpath = "/.//span[@title ='Remove Contact']"

	private static String ModalPhoneType_ddl_xpath= "//div[@class='mobile-wrap']//div[@class='row']//div[@org-placeholder='Type']//select"
	private static String ModalPhoneNumb_txt_xpath= "//div[@class='mobile-wrap']//div[@class='row']//div[@org-placeholder='Number']//input"

	private static String ModalServiceType_ddl_xpath = "//select[@placeholder ='Service Type']"


	public ContactTab() {
	}

	public static WebElement ModalServiceType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalServiceType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalPhoneType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalPhoneType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement ModalPhoneNumb_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalPhoneNumb_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement Salutation_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Salutation_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalFName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalFName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalLName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalLName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCtactType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCtactType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalisDefault_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalisDefault_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalAddr1_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalAddr1_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCity_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCity_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalState_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalState_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalZip_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalZip_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCountry_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCountry_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalEmailType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalEmailType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalEmailAddr_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalEmailAddr_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Contact_Tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Contact_Tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalSubmit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalSubmit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement New_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(New_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContactList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContactList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FullName_lnk(String fullname_str) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = Common.GetElementInTable(fullname_str, this.ContactList_tbl(), FullName_lnk_xpath);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement IsDefault_icon(String fullname_str) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = Common.GetElementInTable(fullname_str, this.ContactList_tbl(), IsDefault_icon_xpath);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SetDefault_lnk(String fullname_str) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = Common.GetElementInTable(fullname_str, this.ContactList_tbl(), SetDefault_lnk_xpath);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Remove_icon(String fullname_str) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = Common.GetElementInTable(fullname_str, this.ContactList_tbl(), Remove_icon_xpath);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	//=========================METHODS
	public static void FillModalInfo(ContactInfo ctact_info, String button)
	{
		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.ModalFName_txt())

		Common.sendkeyTextToElement(this.ModalFName_txt(), ctact_info.firstname)

		Common.sendkeyTextToElement(this.ModalLName_txt(), ctact_info.lastname)

		String status = Common.GetFieldText_func(this.ModalisDefault_chk())
		if(status!=ctact_info.IsDefault.toString())
			Common.HandleElementClick(this.ModalisDefault_chk())


		if(ctact_info.VendorName!="")
			Common.SelectDropdownItem(this.ModalServiceType_ddl(), ctact_info.ServiceType)

		Common.sendkeyTextToElement(this.ModalAddr1_txt(), ctact_info.AddrInfo.Addr1)

		Common.sendkeyTextToElement(this.ModalCity_txt(), ctact_info.AddrInfo.City)

		Common.sendkeyTextToElement(this.ModalZip_txt(), ctact_info.AddrInfo.Zip)

		if(ctact_info.AddrInfo.State!="")
			Common.SelectDropdownItem(this.ModalState_ddl(),  ctact_info.AddrInfo.State)

		if(ctact_info.AddrInfo.Country!="")
			Common.SelectDropdownItem(this.ModalCountry_ddl(), ctact_info.AddrInfo.Country)


		Common.SelectDropdownItem(this.ModalPhoneType_ddl(), ctact_info.PhoneList.get(0).Type)

		Common.sendkeyTextToElement(this.ModalPhoneNumb_txt(), ctact_info.PhoneList.get(0).Numb)

		Common.SelectDropdownItem(this.ModalEmailType_ddl(), ctact_info.MailList.get(0).Type)

		Common.sendkeyTextToElement(this.ModalEmailAddr_txt(), ctact_info.MailList.get(0).EmailAddr)


		if(button=="Submit" ||button=="submit")
			Common.HandleElementClick(this.ModalSubmit_btn())
		else if(button =="Cancel")
			Common.HandleElementClick(this.ModalCancel_btn())

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When Submit Edit Contact Info")


	}//end void

	public static void VerifyModalViewMode_func(ContactInfo ctact_info)
	{
		String area = ""
		String field_name = ""
		String mode = "View Mode"



		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Click on ADDRESS TAB
		Common.HandleElementClick(this.Contact_Tab())

		//Common.WaitForElementNotDisplay(LandingPage.LoadingArc())
		// CLICK ON FullName link
		this.FullName_lnk(ctact_info.FullName).click()
		
		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.ModalEdit_btn())

		WebUI.delay(5)
		area = ""


		field_name = "ModalFirstName"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalFName_txt(),ctact_info.firstname, mode)
	

		field_name = "ModalLastName"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalLName_txt(),ctact_info.lastname, mode)
		
		
		field_name = "ModalisDefault"
		Common.VerifyFieldTextEqual_func(area+field_name , this.ModalisDefault_chk(),ctact_info.IsDefault.toString(), mode)
	
		field_name = "ModalAddrs1"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalAddr1_txt(),ctact_info.AddrInfo.Addr1, mode)
			
		field_name = "ModalCity"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalCity_txt(),ctact_info.AddrInfo.City , mode)
	
		
		field_name = "ModalZip"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalZip_txt(),ctact_info.AddrInfo.Zip , mode)

		
		field_name = "ModalState"
		Common.VerifyFieldTextEqual_func(area+field_name , this.ModalState_ddl(),ctact_info.AddrInfo.State , mode)

		field_name = "ModalCountry"
		Common.VerifyFieldTextEqual_func(area+field_name , this.ModalCountry_ddl(),ctact_info.AddrInfo.Country , mode)

	}


}
