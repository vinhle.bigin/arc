package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.Transferee
import internal.GlobalVariable

public class TransfereePage {
	private static String NewTransf_btn_xpath = "//button[text() = 'New Transferee']"
	private static String TransfList_tbl_xpath = "//div[@id = 'table-list-transferee']//table/tbody"
	private static String MyTranfOnly_chk_xpath = "//div[@class = 'b__components b-checkbox m-b-md']/span"
	private static String SearchTranfName_txt_xpath = "//input[@name ='transferee_name']"
	private static String Search_btn_xpath = "//button[text() = 'Search']"
	//private static String
	/*
	 private static String 
	 private static String 
	 private static String 
	 private static String 
	 */
	public TransfereePage() {
	}

	public static WebElement TransfName_link(String tranffullname_str) {
		try{
			String TransfName_lnk_xapth = "//a/span[text() = '"+tranffullname_str+"']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransfName_lnk_xapth));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchTranfName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchTranfName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewTransf_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewTransf_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TransfList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransfList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MyTranfOnly_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MyTranfOnly_chk_xpath));
			return temp_element;
		}//end try
		catch(NoSuchElementException e) {
			return null
		}//end catch
	}



	//METHOD

	public static void SearchTransferee_func(Transferee tf)
	{
		Common.WaitForElementDisplay(this.SearchTranfName_txt())

		Common.sendkeyTextToElement(this.SearchTranfName_txt(), tf.firstname)

		Common.HandleElementClick(this.Search_btn())

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		WebUI.delay(3)

	}

	public static void VerifyTranfExistTable_func(Transferee tf)
	{
		Boolean exist = false
		this.SearchTransferee_func(tf)
		List<String> records = Common.GetTableRecordsPerPaging_func(this.TransfList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(tf.firstname))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Transferee["+tf.FullName+"] NOT exist in table.\n"
		}//end if

	}
}
