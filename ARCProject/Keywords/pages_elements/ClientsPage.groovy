package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.Client
import objects.Transferee
import internal.GlobalVariable

public class ClientsPage {
	private static String NewClient_btn_xpath = "//button[text() = 'New Client']"
	private static String ClientList_tbl_xpath = "//table[@id = 'table-list-client']/tbody"
	//private static String MyTranfOnly_chk_xpath = "//div[@class = 'b__components b-checkbox m-b-md']/span"
	private static String SearchClientName_txt_xpath = "//input[@name ='company_name']"
	private static String Search_btn_xpath = "//button[text() = 'Search']"

	//NEW MODAL

	private static String Comp_txt_xpath= "//div[@class='modal-body clearfix']//input[@class = 'form-control company_name b__input 2']"
	private static String State_ddl_xpath= "//div[@class='modal-body clearfix']//select[@placeholder ='State']"
	private static String Zip_txt_xpath= "//div[@class='modal-body clearfix']//input[@placeholder = 'Zip']"
	private static String City_txt_xpath= "//div[@class='modal-body clearfix']//input[@placeholder = 'City']"
	private static String Submit_btn_xpath= "//div[@class='modal-footer']//button[@data-control='create']"
	private static String Cancel_btn_xpath= "//div[@class='modal-footer']//button[@data-control='cancel-create']"
	private static String Descr_txt_xpath= "//div[@class='modal-body clearfix']//div[@org-placeholder='Description']//textarea"






	public ClientsPage() {

	}

	public static WebElement Descr_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Descr_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}//end void


	public static WebElement ClientName_link(String clientfullname_str) {
		try{
			String TransfName_lnk_xapth = "//a[text() = '"+clientfullname_str+"']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransfName_lnk_xapth));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchClientName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchClientName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewClient_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewClient_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ClientList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	//METHOD

	public static void SearchClient_func(Transferee tf)
	{
		Common.WaitForElementDisplay(this.SearchClientName_txt())

		this.SearchClientName_txt().sendKeys(tf.firstname)
		Common.HandleElementClick(this.Search_btn())
		WebUI.delay(3)

	}

	public static void VerifyClientExistTable_func(Transferee tf)
	{
		Boolean exist = false
		this.SearchClient_func(tf)
		List<String> records = Common.GetTableRecordsPerPaging_func(this.ClientList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(tf.firstname))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Client["+tf.FullName+"] NOT exist in table.\n"
		}//end if

	}

	public static WebElement Comp_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Comp_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement State_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(State_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Zip_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Zip_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement City_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(City_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	//METHODS
	public static void SearchClient_func(Client client)
	{
		Common.WaitForElementDisplay(this.SearchClientName_txt())

		Common.sendkeyTextToElement(this.SearchClientName_txt(), client.CompanyName)
		Common.HandleElementClick(this.Search_btn())
		WebUI.delay(3)

	}

	public static void VerifyClientExistTable_func(Client client)
	{
		Boolean exist = false
		this.SearchClient_func(client)
		List<String> records = Common.GetTableRecordsPerPaging_func(this.ClientList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(client.firstname))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Client["+client.Fullname+"] NOT exist in table.\n"
		}//end if

	}
}
