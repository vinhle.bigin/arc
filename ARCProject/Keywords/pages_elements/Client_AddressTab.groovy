package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.AddressInfo
import objects.Transferee
import internal.GlobalVariable

public class Client_AddressTab {


	private static String Address_Tab_xpath = "//a[@href = '#client-address']"
	private static String ModalEdit_btn_xpath= "//a[@data-original-title='Edit']"
	private static String ModalUpdate_btn_xpath= "//div[@class = 'modal-footer']//button[@type = 'submit']"
	private static String ModalCancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control,'cancel')]"
	private static String ModalAddrs1_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Address 1']//input"
	private static String ModalAddrs2_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Address 2']//input"
	private static String ModalCity_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'City']//input"
	private static String ModalState_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'State']//select"
	private static String ModalZip_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Zip']//input"
	private static String ModalCountry_xpath= "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Country']//select"
	private static String New_btn_xpath = "//div[@slot = 'modal-button-create form-group']/a"
	private static String AddressList_tbl_xpath = "//table[@id ='address-table']/tbody"
	private static String AddrEdit_icon_xpath = ".//span[@data-original-title='Edit Address']"
	private static String AddrRemove_icon_xpath = ".//span[@class ='btn-remove-address']"
	private static String ModalCode_txt_xpath = "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Code']//input"
	private static String ModalDescrt_txt_xpath = "//div[contains(@id,'address-modal')]//div[@org-placeholder = 'Description']//textarea"
	






	public Client_AddressTab() {
	}
	
	public static WebElement ModalDescrt_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalDescrt_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCode_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCode_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Address_Tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Address_Tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalAddrs1() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalAddrs1_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalAddrs2() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalAddrs2_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCity() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCity_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalState() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalState_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalZip() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalZip_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCountry() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCountry_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement New_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(New_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement AddressList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddressList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement AddrEdit_icon(String addr_code) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()

			WebElement temp_element =  Common.GetElementInTable(addr_code, this.AddressList_tbl(), AddrEdit_icon_xpath)

			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement AddrRemove_icon(String addr_code) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element =  Common.GetElementInTable(addr_code, this.AddressList_tbl(), AddrRemove_icon_xpath)

			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}





	/////////================================METHOD
	public static void VerifyInfoViewMode_func(AddressInfo addr)
	{
		String area = ""
		String field_name = ""
		String mode = "View Mode"



		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Click on ADDRESS TAB
		Common.HandleElementClick(this.Address_Tab())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		//Common.WaitForElementNotDisplay(LandingPage.LoadingArc())
		// CLICK ON EDIT ICON
		this.AddrEdit_icon(addr.Code).click()

		Common.WaitForElementDisplay(this.ModalEdit_btn())

		WebUI.delay(5)

		area = ""

		field_name = "ModalCode"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalCode_txt(),addr.Code, mode)

		field_name = "ModalAddrs1"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalAddrs1(),addr.Addr1, mode)

		field_name = "ModalAddrs2"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalAddrs2(),addr.Addr2 , mode)

		field_name = "ModalCity"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalCity(),addr.City , mode)

		field_name = "ModalZip"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalZip(),addr.Zip , mode)

		field_name = "ModalState"
		Common.VerifyFieldTextEqual_func(area+field_name , this.ModalState(),addr.State , mode)

		field_name = "ModalCountry"
		Common.VerifyFieldTextEqual_func(area+field_name , this.ModalCountry(),addr.Country , mode)

	}

	public static void FillModalInfo_func(AddressInfo addr,String button)
	{
		Common.ScrollElementtoViewPort_func(this.ModalCode_txt())

		Common.sendkeyTextToElement(this.ModalDescrt_txt(), addr.Description)
		
		
		Common.sendkeyTextToElement(this.ModalCode_txt(), addr.Code)
		
		Common.sendkeyTextToElement(this.ModalAddrs1(), addr.Addr1)

		Common.sendkeyTextToElement(this.ModalAddrs2(), addr.Addr2)

		Common.sendkeyTextToElement(this.ModalCity(), addr.City)

		Common.sendkeyTextToElement(this.ModalZip(), addr.Zip)

		Common.SelectDropdownItem(this.ModalState(),  addr.State)

		Common.SelectDropdownItem(this.ModalCountry(), addr.Country)
		
		
		//this.Country = "Viet Nam"
		//Common.sendkeyTextToElement()

		if(button=="Submit")
		{
			//Common.ScrollElementtoViewPort_func(this.ModalUpdate_btn())
			Common.HandleElementClick(this.ModalUpdate_btn())

		}

		else if (button == "Cancel")
			this.ModalCancel_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg,"Unsuccessfull Submit Create/Edit Address.")
	}





}
