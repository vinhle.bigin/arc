package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.Transferee
import internal.GlobalVariable

public class TransfGenInfoPage {

	private static String Generall_tab_xpath = "//a[@href = '#transferee-general-info']"


	private static String Bank_tab_xpath = "//a[@href = '#transferee-dependents']"

	private static String Custom_tab_xpath = "//a[@href = '#transferee-custom-fields']"


	//TRANSFERE INFO
	private static String Salulation_slect_xpath = "//select[@placeholder = 'Salutation']"
	private static String Fname_txt_xpath = "//input[@placeholder = 'First Name']"

	private static String Lname_txt_xpath = "//input[@placeholder = 'Last Name']"
	private static String Trf_Mid_txt_xpath= "//input[@placeholder = 'Mid']"
	private static String Trf_Pronu_txt_xpath= "//div[@org-placeholder='Pronunciation']//input"
	private static String Jobtle_txt_xpath= "//input[@placeholder = 'Job Title']"
	private static String Trf_Nickname_txt_xpath= "//input[@placeholder = 'Nickname']"
	private static String Trf_IsExecOff_opt_xpath= "//label[text() ='Executive Officer']/..//input[@type = 'checkbox']"
	private static String Trf_Coord_txt_xpath= "//input[@id = 'input-list-coordinators']"
	private static String Trf_Coord_ddl_xpath= "//div[@id = 'list-coordinators']//ul"
	private static String Trf_CoordAss_txt_xpath= "//input[@id = 'input-list-coordinator-assistants']"
	private static String Trf_CoordAss_ddl_xpath= "//div[@id = 'list-coordinator-assistants']//ul"
	private static String Trf_TeamLead_txt_xpath= "//input[@id = 'input-list-team-leads']"
	private static String Trf_TeamLead_ddl_xpath= "//div[@id = 'list-team-leads']//ul"
	private static String Trf_AdditleNote_txt_xpath= "//div[@org-placeholder='Additional Notes']//textarea"
	private static String Trf_UrgentInstr_txt_xpath= "//div[@org-placeholder='Urgent Instructions']//textarea"
	private static String TransInfoEdit_btn_xpath= "//a[@id ='icon-edit-transferee-infomation-panel']"
	private static String TransInfoUpdate_btn_xpath= "//div[@id ='edit-transferee-infomation-panel']//button[@data-control = 'update']"
	private static String TransInfoCancel_btn_xpath= "//div[@id ='edit-transferee-infomation-panel']//button[@data-control = 'cancel-update']"

	//CLIENT
	private static String Client_txt_xpath= "//input[@id = 'list-clients']"
	private static String Client_ddl_xpath= "//input[@id = 'input-list-clients']"
	private static String ClientContact_ddl_xpath= "//select[@placeholder = 'Client Contact']"
	private static String ClientEdit_btn_xpath= "//a[@id = 'icon-edit-client-panel']"
	private static String ClientUpdate_btn_xpath= "//div[@id = 'edit-client-panel']//button[@data-control = 'update']"
	private static String ClientCancel_btn_xpath= "//div[@id = 'edit-client-panel']//button[@data-control = 'cancel-update']"

	//STATUS
	private static String StatusEdit_btn_xpath= "//a[@id = 'icon-edit-status-infomation-panel']"
	private static String Status_ddl_xpath= "//select[@placeholder = 'Status']"
	private static String Stt_ChangeDate_vlue_xpath= "//input[@placeholder = 'Status Change Date']"
	private static String Stt_IsOverride_opt_xpath= ""
	private static String Stt_IsQualified_opt_xpath= ""
	private static String Stt_FileEnterDate_txt_xpath= ""
	private static String Stt_IsRecvSgnABAD_opt_xpath= ""
	private static String Stt_IsRefLstSide_opt_xpath= ""

	private static String Stt_Update_btn_xpath= "//div[contains(@id,'edit-status')]//button[@data-control = 'update']"
	private static String Stt_Cancel_btn_xpath= "//div[contains(@id,'edit-status')]//button[@data-control = 'cancel-update']"

	private static String PhoneList_xpath = "//div[@class = 'mobile-wrap']"

	private static String EMailList_xpath = "//div[@class='email-wrap extension-fields']"

	//VIEW MODE
	private static String Fname_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'First Name']/input"
	private static String Lname_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Last Name']/input"
	private static String Trf_Mid_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Mid']"
	private static String Trf_Pronu_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Pronunciation']//input"
	private static String Jobtle_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Job Title']/.."
	private static String Trf_Nickname_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@org-placeholder = 'Nickname']//input"
	private static String Trf_IsExecOff_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//label[text() = 'Executive Officer']/../input"
	private static String Trf_Coord_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@id = 'list-coordinators']//div[@class = 'result']//div[@class = 'content']//div[@class = 'name']"
	private static String Trf_CoordAss_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@id = 'list-coordinator-assistants']/.."///div[@class = 'result']//div[@class = 'content']//div[@class = 'name']"
	private static String Trf_TeamLead_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//div[@id = 'list-team-leads']//input"//div[@class = 'result']//div[@class = 'content']//div[@class = 'name']"
	private static String Trf_AdditleNote_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//pre[@placeholder = 'Additional Notes']"
	private static String Trf_UrgentInstr_view_xpath= "//div[@id = 'edit-transferee-infomation-panel']//pre[@placeholder = 'Urgent Instructions']"
	private static String Client_view_xpath= "//input[@id = 'input-list-clients']"
	private static String ClientContact_view_xpath=  "//div[@org-placeholder ='Client Contact']"       //"//select[@placeholder = 'Client Contact']"
	private static String Status_view_xpath= "//div[@org-placeholder ='Status']//select"
	private static String Stt_IsOverride_view_xpath= "//label[text() = 'Override']/../input"
	private static String Stt_IsQualified_view_xpath= "//label[text() = 'Qualified']/../input"
	private static String Stt_FileEnterDate_view_xpath= "//div[@org-placeholder ='File Entered Date']//input"
	private static String Stt_IsRecvSgnABAD_view_xpath= "//label[text() = 'Received Signed ABAD']/../input"
	private static String Stt_IsRefLstSide_view_xpath= "//label[text() = 'Referred List Side']/../input"


	private static String TransInfo_heading_lbl_xpath= "//div[@id ='edit-transferee-infomation-panel']//div[@class ='panel-heading']"
	private static String ClientInfo_heading_lbl_xpath= "//div[@id ='edit-client-panel']//div[@class ='panel-heading']"
	//private static String ClientInfo_heading_lbl= "//div[@id ='edit-client-panel']//div[@class ='panel-heading']"
	private static String StatusInfo_heading_lbl_xpath= "//div[@id ='edit-status-infomation-panel']//div[@class ='panel-heading']"

	
	//PHONE
	private static String Phone_Edit_btn_xpath = "//a[@id ='icon-edit-phones-panel']"
	private static String Phone_Update_btn_xpath = "//div[@id='edit-phones-panel']//button[@data-control ='update']"
	private static String Phone_Cancel_btn_xpath = "//div[@id='edit-phones-panel']//button[@data-control ='cancel-update']"
	
	//EMAIL
	private static String Email_Edit_btn_xpath = "//a[@id ='icon-edit-emails-panel']"
	private static String Email_Update_btn_xpath = "//div[@id='edit-emails-panel']//button[@data-control ='update']"
	private static String Email_Cancel_btn_xpath = "//div[@id='edit-emails-panel']//button[@data-control ='cancel-update']"
	
	public TransfGenInfoPage() {
	}

	public static WebElement StatusInfo_heading() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(StatusInfo_heading_lbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ClientInfo_heading() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientInfo_heading_lbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement TransInfo_heading() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransInfo_heading_lbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Generall_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Generall_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Lname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Lname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Fname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Fname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Salulation_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Salulation_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Mid_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Mid_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Pronu_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Pronu_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Jobtle_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Jobtle_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Nickname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Nickname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_IsExecOff_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_IsExecOff_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Coord_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Coord_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Coord_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Coord_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_CoordAss_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_CoordAss_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_CoordAss_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_CoordAss_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_TeamLead_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_TeamLead_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_TeamLead_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_TeamLead_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_AdditleNote_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_AdditleNote_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_UrgentInstr_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_UrgentInstr_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement TransInfoEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransInfoEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement TransInfoUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransInfoUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement TransInfoCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TransInfoCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Client_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Client_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Client_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Client_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static Select ClientContact_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = new Select (webDriver.findElement(By.xpath(ClientContact_ddl_xpath)))
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ClientEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ClientUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ClientCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement StatusEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(StatusEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Status_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Status_ddl_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_ChangeDate_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_ChangeDate_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_Override_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsOverride_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_Qualified_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsQualified_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_FileEnterDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_FileEnterDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_IsRecvSgnABAD_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsRecvSgnABAD_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_IsRefLstSide_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsRefLstSide_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PhoneList() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PhoneList_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PhoneType_ddl(int index) {
		try{
			String e_xpath = ".//select[@placeholder = 'Type']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = this.PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index)
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Phone_txt(int index) {
		try{
			String e_xpath = ".//input[@type = 'tel']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = this.PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index)
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PhonePrefer_opt(int index) {
		try{
			String e_xpath = ".//input[@type = 'checkbox']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = this.PhoneList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index)
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement EmailList() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(EMailList_xpath));


			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement EmailType_ddl(int index) {
		try{
			String e_xpath = ".//select[@placeholder = 'Type']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = this.EmailList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index)
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement EmailAddr_txt(int index) {
		try{
			String e_xpath = ".//div[@org-placeholder='E-mail']//input"
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = this.EmailList().findElements(By.xpath(e_xpath));

			WebElement e_return = temp_element.get(index)
			return e_return;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	//VIEW MODE
	public static WebElement Fname_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Fname_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Lname_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Lname_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Mid_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Mid_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Pronu_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Pronu_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Jobtle_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Jobtle_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Nickname_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Nickname_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_IsExecOff_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_IsExecOff_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_Coord_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_Coord_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_CoordAss_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_CoordAss_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_TeamLead_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_TeamLead_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_AdditleNote_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_AdditleNote_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Trf_UrgentInstr_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Trf_UrgentInstr_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Client_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Client_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ClientContact_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ClientContact_view_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Status_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Status_view_xpath))

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_IsOverride_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsOverride_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_IsQualified_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsQualified_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_FileEnterDate_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_FileEnterDate_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_IsRecvSgnABAD_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsRecvSgnABAD_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_IsRefLstSide_view() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_IsRefLstSide_view_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PhoneEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Phone_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	public static WebElement PhoneUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Phone_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	
	public static WebElement PhoneCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Phone_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	public static WebElement EmailEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Email_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	
	public static WebElement EmailUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Email_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	
	public static WebElement EmailCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Email_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	
	
	/////////================================METHOD
	public static void VerifyGeneralInfoViewMode_func(Transferee tf)
	{
		if(LandingPage.Loading_icon()==null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		if(this.Status_view()==null)
			Common.WaitForElementDisplay(this.Status_view())

		String result_str
		result_str = this.Fname_view().getAttribute("value")
		Common.VerifyFieldValueEqual_func("FirstName",this.Fname_view(),tf.firstname," - ViewMode")



		println("1." + result_str)

		result_str = this.Lname_view().getAttribute("value")
		Common.VerifyFieldValueEqual_func("LastName",this.Lname_view(),tf.lastname," - ViewMode")


		println("2." + result_str)

		result_str = this.Client_view().getAttribute("value")
		Common.VerifyFieldValueEqual_func("Client_view",this.Client_view(),tf.ClientName," - ViewMode")

		//result_str = this.Client_view().getText()
		println("3." + result_str)

		//result_str = this.ClientContact_view().getAttribute("value") //CLIENT CONTACT
		result_str = this.ClientContact_view().getText().replace("CLIENT CONTACT\n", "")
		if(tf.ClientContactFullName!="")
			if(result_str!=tf.ClientContactFullName)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage+="Incorrect Client Contact.[Observed: "+result_str+" - Expected:"+tf.ClientContactFullName+"].\n"
			}
			else
			if(result_str!="-")
		{
			GlobalVariable.glb_TCStatus=false
			GlobalVariable.glb_TCFailedMessage+="Incorrect Client Contact.[Observed: '"+result_str+"' - Expected:'-'].\n"
		}

		println("4." + result_str)

		//result_str = this.Jobtle_view().getAttribute("value")
		result_str = this.Jobtle_view().getText()
		result_str = result_str.toString().replace("JOB TITLE", "")
		
		println("5." + result_str)
		
		if(result_str!=tf.Jobtitle)
		{
			GlobalVariable.glb_TCStatus=false
			GlobalVariable.glb_TCFailedMessage+="Incorrect Jobtitle.[Observed: "+result_str+" - Expected:"+tf.Jobtitle+"].\n"
		}
		
//		Common.VerifyFieldTextEqual_func("Jobtitle",this.Jobtle_view(),tf.Jobtitle," - ViewMode")

		result_str = Common.GetFieldText_func(this.Status_view())

		println("6." + result_str)

		Common.VerifyFieldTextEqual_func("Status",this.Status_view(),tf.Status," - ViewMode")

		result_str = this.Stt_FileEnterDate_view().getAttribute("value")

		println("7." + result_str)
		Common.VerifyFieldValueEqual_func("Client_view",this.Stt_FileEnterDate_view(),tf.FileEnterDate," - ViewMode")

		result_str = this.Stt_IsOverride_view().isSelected()

		println("8." + result_str)


		result_str = this.Stt_IsQualified_view().isSelected()

		println("9." + result_str)

		result_str = this.Stt_IsRecvSgnABAD_view().isSelected()

		println("10." + result_str)

		result_str = this.Stt_IsRefLstSide_view().isSelected()

		println("11." + result_str)

		result_str = this.Trf_AdditleNote_view().getText()

		println("12." + result_str)

		result_str = this.Trf_Coord_view().getText()

		println("13." + result_str)

		Common.VerifyFieldTextEqual_func("Coordinator",this.Trf_Coord_view(),tf.Coordinator," - ViewMode")

		Common.VerifyFieldTextEqual_func("CoordinatorAss",this.Trf_CoordAss_ddl(),tf.CoordinatorAssistant," - ViewMode")

		Common.VerifyFieldTextEqual_func("IsExecutiveOfficer",this.Trf_IsExecOff_opt(),tf.IsExecutiveOfficer.toString()," - ViewMode")

		Common.VerifyFieldValueEqual_func("Trf_Pronu_view",this.Trf_Pronu_txt(),tf.Pronunciation," - ViewMode")

		Common.VerifyFieldTextEqual_func("Trf_TeamLead_ddl",this.Trf_TeamLead_ddl(),tf.TeamLeader," - ViewMode")

		Common.VerifyFieldTextEqual_func("Trf_UrgentInstr_txt",this.Trf_UrgentInstr_txt(),tf.UrgentInstructions," - ViewMode")

		Common.VerifyFieldTextEqual_func("PhoneType_ddl",this.PhoneType_ddl(0),tf.PhoneList.get(0).Type," - ViewMode")

		Common.VerifyFieldValueEqual_func("Phone_txt",this.Phone_txt(0),tf.PhoneList.get(0).Numb," - ViewMode")

		Common.VerifyFieldTextEqual_func("EmailType_ddl",this.EmailType_ddl(0),tf.MailList.get(0).Type," - ViewMode")

		Common.VerifyFieldValueEqual_func("EmailAddr_txt",this.EmailAddr_txt(0),tf.MailList.get(0).EmailAddr," - ViewMode")


	}




}
