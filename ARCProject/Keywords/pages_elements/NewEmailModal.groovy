package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.Navigation
import internal.GlobalVariable
import objects.Mailinfo
import objects.Transferee

public class NewEmailModal {

	private static String Service_ddl_xpath= "//div[@org-placeholder='Service']//select"
	private static String Recipient_ddl_xpath= "//div[@org-placeholder='Recipient']//select"
	private static String Template_ddl_xpath= "//div[@org-placeholder='Template']//select"
	private static String Contact_tbl_xpath= "//div[@class ='table-responsive']//table/tbody"
	private static String From_ddl_xpath= "//label[text()='From']/..//ul[@class ='b__multi__select__list']"
	private static String To_txt_xpath= "//input[@placeholder ='To']"
	private static String CC_txt_xpath= "//input[@placeholder ='CC']"
	private static String Bcc_txt_xpath= "//input[@placeholder ='BCC']"
	private static String Subject_txt_xpath= "//input[@placeholder ='Subject']"
	private static String Content_txt_xpath= "//p"
	private static String Attach_lnk_xpath= "//div[@class ='content dz-clickable']"
	private static String Next_btn_xpath= "//div[@class='modal-footer']//button[@type ='submit']"
	private static String Send_btn_xpath= "//div[@class='modal-footer']//button[@type='submit']"
	private static String Cancel_btn_xpath= "//button[@data-dismiss='modal']"
	private static String From_txt_xpath= "//input[@placeholder ='From']"
	private static String From_vlue_xpath= "//span[@class='thumb']"
	private static String ContactSelect_chk_xpath= ".//input[@type ='radio']"

	private static String DocReview_lnk_xpath = "//a[contains(text(),'doc_name_arg')]"


	public NewEmailModal() {
	}

	public static WebElement DocReview_lnk(String doc_name) {
		try{
			String xpath_str = DocReview_lnk_xpath.replace("doc_name_arg", doc_name)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CC_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CC_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Service_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Service_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Recipient_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Recipient_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Template_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Template_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Contact_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Contact_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement From_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(From_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement To_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(To_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Bcc_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Bcc_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Subject_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Subject_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Content_txt() {
		try{
			WebDriver webDriver = Navigation.SwitchIframe_func(Navigation.TinyIframe_txt())

			WebElement temp_element = webDriver.findElement(By.xpath(Content_txt_xpath+"/.."));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Attach_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Attach_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Next_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Next_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Send_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Send_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement From_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(From_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement From_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(From_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContactSelect_chk(String name_str) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = Common.GetElementInTable(name_str,this.Contact_tbl(),this.ContactSelect_chk_xpath)
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	//METHOD

	public static void FillInfoModal_func(Mailinfo mail_info,String button = "Submit",Boolean customEmail = false)
	{

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(this.Service_ddl())

		//Select Recipienttype
		Common.SelectDropdownItem( this.Recipient_ddl(),mail_info.RecipientType)

		WebUI.delay(3)

		//Select Service type
		Common.SelectDropdownItem( this.Service_ddl(),mail_info.ServiceType)

		WebUI.delay(2)

		Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(this.Template_ddl())

		//Select Template Name
		Common.SelectDropdownItem(this.Template_ddl(),mail_info.TemplateName)

		//Select Contact
		this.ContactSelect_chk(mail_info.EmailAddr).click()

		//Click Next button
		this.Next_btn().click()

		WebUI.delay(2)

		//Select From
		//Common.ScrollElementtoViewPort_func(this.From_vlue())

		//Common.HandleElementClick(this.From_vlue())

		//Common.sendkeyTextToElement(this.From_txt(), mail_info.From)

		//Common.SelectDropdownItem(this.From_ddl(),mail_info.From)

		//fill "To" field

		Common.sendkeyTextToElement(this.To_txt(),mail_info.To_list.get(0))

		if(mail_info.CC_list.size()!=0)
			Common.sendkeyTextToElement(this.CC_txt(),mail_info.CC_list.get(0))

		if(mail_info.BCC_list.size()!=0)
			Common.sendkeyTextToElement(this.Bcc_txt(),mail_info.BCC_list.get(0))


		if(customEmail==true)
		{
			WebDriver webDriver = DriverFactory.getWebDriver()

			Actions actions = new Actions(webDriver)

			Action action_key = actions.sendKeys(this.Subject_txt(), Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(mail_info.Content).build()

			action_key.perform()

			//attachment
			if(mail_info.Attach_List.get(0)!="")
			{
				String file_name = mail_info.Attach_List.get(0)
				Common.UploadFile_func(this.Attach_lnk(), file_name)
			}


		}//end if boolean = true

		//Click Button
		if(button == "Submit")
		{
			Common.ScrollElementtoViewPort_func(this.Send_btn())
			this.Send_btn().click()

			WebUI.delay(5)

			if(Messages.notification_msg()!=null)
			{
				String msg = Messages.notification_msg().getText()
				String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
				if(msg ==expected_msg)
				{
					GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull TRANSF Creation.Msg: '"+msg+"'.\n"
					Assert.fail(GlobalVariable.glb_TCFailedMessage)
				}
			}//end if of message

			else if(button == "Cancel")
			{
				this.Cancel_btn().click()
			}


		}//end if of submit



	}//end void

	//METHOD VERIFY SEND EMAIL MODAL
	public static void VerifyModalDetails_func(Mailinfo mail_info)
	{
		String area = "Email modal"
		String field = ""
		String Content_str = this.Content_txt().getText()

		WebDriver webDriver = DriverFactory.getWebDriver()

		webDriver = Navigation.SwitchIframe_func()
		field = "Content"
		if(Content_str!=mail_info.Content)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += area+"-"+field+"Incorrect:[Observed: "+Content_str+" - Expected: "+mail_info.Content+"].\n"
		}




	}//end voids

}
