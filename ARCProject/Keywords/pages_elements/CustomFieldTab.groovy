package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.CustomField
import objects.Transferee
import internal.GlobalVariable

public class CustomFieldTab {
	public static String Custom_tab_xpath = "//a[contains(@href,'custom-fields')]"
	public static String Edit_btn_xpath = "//a[@data-original-title='Edit']"
	public static String Update_btn_xpath = "//button[@data-control='update']"
	public static String Cancel_btn_xpath = "//button[@data-control='cancel-update']"



	public CustomFieldTab() {
	}



	public static WebElement TextBox_txt(String fieldname) {
		try{

			String 	TextBox_txt_xpath = "//div[@org-placeholder='"+fieldname+"']//input"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TextBox_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dropdown_ddl(String fieldname) {
		try{
			String 	Dropdown_ddl_xpath = "//div[@org-placeholder='"+fieldname+"']//select"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dropdown_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Checkbox_chk(String fieldname) {
		try{
			String 	Checkbox_chk_xpath = "//div[@org-placeholder='"+fieldname+"']//input"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Checkbox_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TextArea_txt(String fieldname) {
		try{
			String 	TextArea_txt_xpath = "//div[@org-placeholder='"+fieldname+"']//textarea"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TextArea_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Radio_rad(String fieldname) {
		try{
			String 	Radio_rad_xpath = "//div[@org-placeholder='"+fieldname+"']//input"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Radio_rad_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MultiDropdown_ddl(String fieldname) {
		try{
			String 	MultiDropdown_ddl_xpath = "//div[@org-placeholder='"+fieldname+"']//ul"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MultiDropdown_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MultiDropdown_arrow(String fieldname) {
		try{
			String 	MultiDropdown_arrow_xpath = "//div[@org-placeholder='"+fieldname+"']//div[@class='iconC']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MultiDropdown_arrow_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement Custom_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Custom_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement GroupEdit_btn(String groupname) {
		try{
			String 	Group_xpath = "//h3[contains(text(),'"+groupname+"')]/.."+Edit_btn_xpath
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Group_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement GroupUpdate_btn(String groupname) {
		try{
			String 	Group_xpath = "//h3[contains(text(),'"+groupname+"')]/../.."+Update_btn_xpath
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Group_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GroupCancel_btn(String groupname) {
		try{
			String 	Group_xpath = "//h3[contains(text(),'"+groupname+"')]/../.."+Cancel_btn_xpath
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Group_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	/////////================================METHOD
	//Edit CUSTOM FIELD
	public static void EditCustomField_func(CustomField field)
	{
		if(this.Custom_tab().getAttribute("aria-expanded")!="true")
		{
			Common.HandleElementClick(this.Custom_tab())
		
		}

		if(LandingPage.Loading_icon()!=null)
		{
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)
		}

		Common.WaitForElementDisplay(this.GroupEdit_btn(field.custom_group.Name))

			
		Common.ScrollElementtoViewPort_func(this.GroupEdit_btn(field.custom_group.Name))

		this.GroupEdit_btn(field.custom_group.Name).click()

		WebUI.delay(2)

		String controltype = field.ControlType
		switch(controltype)
		{
			case "Textbox":
				Common.sendkeyTextToElement(this.TextBox_txt(field.FieldName), field.ValueList.get(0))
				break

			case "Checkbox":
				String value_str = Common.GetFieldText_func(this.Checkbox_chk(field.FieldName))

				if(value_str!=field.ValueList.get(0))
					this.Checkbox_chk(field.FieldName).click()
				break

			case "Date Picker":
				Common.ActionSendkey(this.TextBox_txt(field.FieldName), field.ValueList.get(0))
				break

			case "Dropdown":
				Common.SelectDropdownItem(this.Dropdown_ddl(field.FieldName), field.ValueList.get(0))
				break

			case "Multiselect":

				break

			case "Numeric Field":
				Common.sendkeyTextToElement(this.TextBox_txt(field.FieldName), field.ValueList.get(0))
				break

			case "Radio Button":
				String value_str = Common.GetFieldText_func(this.Radio_rad(field.FieldName))

				if(value_str!=field.ValueList.get(0))
					this.Radio_rad(field.FieldName).click()

				break

			case "TextArea":
				Common.sendkeyTextToElement(this.TextArea_txt(field.FieldName), field.ValueList.get(0))
				break
		}

		this.GroupUpdate_btn(field.custom_group.Name).click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "Edit Custom Field")


	}

	//VERIFY VIEW MODE FOR 1 ELEMENT
	public static void VerifyViewModePerElement(CustomField field)
	{
		if(this.Custom_tab().getAttribute("aria-expanded")!="true")
		{
			this.Custom_tab().click()
		}


		String controltype = field.ControlType

		switch(controltype)
		{
			case "Textbox":
				Common.VerifyFieldValueEqual_func(field.FieldName, this.TextBox_txt(field.FieldName), field.ValueList.get(0))
				break

			case "Checkbox":
				Common.VerifyFieldTextEqual_func(field.FieldName, this.Checkbox_chk(field.FieldName), field.ValueList.get(0))
				break

			case "Date Picker":
				Common.VerifyFieldValueEqual_func(field.FieldName, this.TextBox_txt(field.FieldName), field.ValueList.get(0))
				break

			case "Dropdown":
				Common.VerifyFieldTextEqual_func(field.FieldName, this.Dropdown_ddl(field.FieldName), field.ValueList.get(0))

				break

			case "Multiselect":

				break

			case "Numeric Field":
				Common.VerifyFieldValueEqual_func(field.FieldName, this.TextBox_txt(field.FieldName), field.ValueList.get(0))

				break

			case "Radio Button":
				Common.VerifyFieldTextEqual_func(field.FieldName, this.Radio_rad(field.FieldName), field.ValueList.get(0))

				break

			case "TextArea":
				Common.VerifyFieldTextEqual_func(field.FieldName, this.TextArea_txt(field.FieldName), field.ValueList.get(0))

				break
		}
	}


	//VERIFY CUSTOM FIELD DISPLAY AS EXPECTED
	public static void VerifyCustomFieldDisplay(CustomField field)
	{
		if(this.Custom_tab().getAttribute("aria-expanded")!="true")
		{
			this.Custom_tab().click()
		}
		
		String controltype = field.ControlType

		WebElement e_temp = null

		switch(controltype)
		{
			case "Textbox":
				e_temp = this.TextBox_txt(field.FieldName)

				break

			case "Checkbox":
				e_temp = this.Checkbox_chk(field.FieldName)

				break

			case "Date Picker":
				e_temp = this.TextBox_txt(field.FieldName)

				break

			case "Dropdown":
				e_temp = this.Dropdown_ddl(field.FieldName)
				break

			case "Multiselect":

				break

			case "Numeric Field":
				e_temp = this.TextBox_txt(field.FieldName)
				break

			case "Radio Button":
				e_temp = this.Radio_rad(field.FieldName)
				break

			case "TextArea":
				e_temp = this.TextArea_txt(field.FieldName)
				break
		}

		Common.VerifyFieldDisplayed_func(field.FieldName, e_temp)

	}//end void


	//VERIFY CUSTOM FIELD NOT DISPLAY AS EXPECTED
	public static void VerifyCustomFieldNotDisplay(CustomField field)
	{
		if(this.Custom_tab().getAttribute("aria-expanded")!="true")
		{
			this.Custom_tab().click()
		}
		
		String controltype = field.ControlType

		WebElement e_temp = null

		switch(controltype)
		{
			case "Textbox":
				e_temp = this.TextBox_txt(field.FieldName)

				break

			case "Checkbox":
				e_temp = this.Checkbox_chk(field.FieldName)

				break

			case "Date Picker":
				e_temp = this.TextBox_txt(field.FieldName)

				break

			case "Dropdown":
				e_temp = this.Dropdown_ddl(field.FieldName)
				break

			case "Multiselect":

				break

			case "Numeric Field":
				e_temp = this.TextBox_txt(field.FieldName)
				break

			case "Radio Button":
				e_temp = this.Radio_rad(field.FieldName)
				break

			case "TextArea":
				e_temp = this.TextArea_txt(field.FieldName)
				break
		}

		Common.VerifyFieldNotDisplayed_func(field.FieldName, e_temp)

	}//end void


}
