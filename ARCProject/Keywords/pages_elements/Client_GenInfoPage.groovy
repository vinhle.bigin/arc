package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import objects.Client
import objects.Transferee
import internal.GlobalVariable

public class Client_GenInfoPage {

	private static String Generall_tab_xpath = "//a[contains(@href,'general-info')]"
	private static String Address_tab_xpath = "//a[contains(@href,'#client-address')]"

	private static String Custom_tab_xpath = "//a[contains(@href,'#custom-fields')]"


	//CLIENT INFO
	private static String CompName_txt_xpath = "//input[@placeholder = 'Company name']"
	private static String Prof_Edit_btn_xpath= "//div[@id = 'edit-client-profile-panel-general-info']//a[contains(@id,'icon-edit')]"
	private static String Prof_Submit_btn_xpath= "//div[@id = 'edit-client-profile-panel-general-info']//button[@data-control = 'update']"
	private static String Prof_Cancel_btn_xpath= "//div[@id = 'edit-client-profile-panel-general-info']//button[@data-control = 'cancel-update']"

	//STATUS
	private static String Stt_Edit_btn_xpath= "//div[contains(@id,'edit-client-status')]//a[contains(@id,'icon-edit')]"
	private static String Stt_Submit_btn_xpath= "//div[contains(@id,'edit-client-status')]//button[@data-control = 'update']"
	private static String Stt_Cancel_btn_xpath= "//div[contains(@id,'edit-client-status')]//button[@data-control = 'cancel-update']"
	private static String Status_ddl_xpath= "//div[contains(@id, 'edit-client-status')]//select[@placeholder = 'Status']"


	//HEADING ELEMENT
	private static String HeadingNewClient_btn_xpath = "//div[@class ='page-menu']//a[contains(@href,'create')]"
	private static String HeadingNewNote_btn_xpath = "//div[@class ='page-menu']//a/p[text() ='Note']"
	private static String HeadingPrint_btn_xpath = "//div[@class ='page-menu']//a/p[text() ='Print']"






	public Client_GenInfoPage() {
	}
	
	
	public static WebElement HeadingNewClient_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HeadingNewClient_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	public static WebElement HeadingNewNote_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HeadingNewNote_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}
	
	public static WebElement HeadingPrint_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HeadingPrint_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Prof_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Prof_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Prof_Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Prof_Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Prof_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Prof_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement CompName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CompName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Stt_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Stt_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Stt_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Status_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Status_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	/////////================================METHOD
	public static void VerifyGeneralInfoViewMode_func(Client client)
	{
		
		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		if(this.Status_ddl()==null)
			Common.WaitForElementDisplay(this.Status_ddl())

		String result_str
		result_str = this.CompName_txt().getAttribute("value")

		Common.VerifyFieldValueEqual_func("CompanyName",this.CompName_txt(),client.CompanyName," - ViewMode")

		println("1." + result_str)

		result_str = this.Status_ddl().getText()
		println("2." + result_str)
		Common.VerifyFieldTextEqual_func("Status",this.Status_ddl(),client.Status," - ViewMode")






	}




}
