package pages_elements

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import internal.GlobalVariable
import objects.Activity

public class TaskListPage {

	private static String Search_btn_xpath= "//button[@id='task-search-activity']"
	private static String Clear_btn_xpath= "//button[text() = 'Clear']"
	private static String SearchFromDate_txt_xpath = "//input[@id='search_from_date']"
	private static String SearchToDate_txt_xpath= "//input[@id='search_to_date']"
	private static String SearchPriority_slect_xpath = "//select[contains(@placeholder,'Priorities')]"
	private static String SearchStatus_slect_xpath = "//select[contains(@placeholder,'Status')]"
	private static String SearchTitle_txt_xpath = "//div[@class='form-group col-md-2']//input[@placeholder='Title']"
	private static String SearchOverdueOnly_swit_xpath = "//div[@class='switch__touch']"
	//private static String ActivityType_slect_xpath= "//div[@class = 'modal-body clearfix']//select[@placeholder = 'Activity Type']"
	//private static String ServiceType_slect_xpath= "//div[@class = 'modal-body clearfix']//select[@placeholder = 'Service type']"
	private static String Start_txt_xpath= "//div[@class = 'modal-body clearfix']//input[contains(@id,'ActivityCreate_from_date')]"
	private static String End_txt_xpath= "//div[@class = 'modal-body clearfix']//input[contains(@id,'ActivityCreate_to_date')]"
	private static String Title_txt_xpath= "//div[@class = 'modal-body clearfix']//input[@placeholder = 'Title']"
	private static String Content_txt_xpath= "//iframe[@id = 'client-activity-create-content_ifr']//html//body/p"
	private static String Submit_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'create']"
	private static String Cancel_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'cancel-create']"
	private static String Attach_lnk_xpath= "//div[@id = 'new-activity-attachment']//div[@class = 'content dz-clickable']"
	private static String ActivityList_tbl_xpath= "//table[@class = 'VueTables__table btable table dataTable table-striped table-hover']//tbody"
	private static String Edit_btn_xpath = "//div[@id ='loading-modal-edit-activity']//a[@data-original-title='Edit']"
	private static String Close_opt_xpath = "//div[@id ='loading-modal-edit-activity']//input[@type = 'checkbox']"
	private static String Activity_tab_xpath = "//a[@href='#tab-vendor-activities']"
	private static String ActivityPriority_slect_xpath = "//select[@placeholder='Priority']"
	private static String ScheduledWith_slect_xpath = "//select[@placeholder='Scheduled With']"
	private static String ScheduledWith_listDrd_xpath = "//select[@placeholder='Scheduled With']/option"
	private static String NotiRemider_slect_xpath = "//select[@placeholder='Notification Reminder']"
	private static String NotiRemider_listDrd_xpath = "//select[@placeholder='Notification Reminder']/option"
	private static String EditActivityVendor_btn_xpath = "//div[@class ='modal-header']//a[@data-original-title = 'Edit']"
	private static String CloseActivityVendor_opt_xpath = "//div[@class ='modal-body clearfix']//input[@type = 'checkbox']"
	private static String ResultActivity_slect_xpath = "//select[@placeholder='Result']"
	private static String ResultActivity_listDrd_xpath = "//select[@placeholder='Result']/option"
	private static String ReasonClosing_txt_xpath = "//input[@placeholder='Reason']"
	private static String Task_icon_xpath = "//i[@data-original-title='Task']"
	private static String FollowUp_icon_xpath = "//i[@data-original-title='Follow Up']"
	private static String Delete_icon_xpath = "//div[@class='table-responsive']//i[@class='icon-close']"

	private static String ActivityName_lnk_xpath = "//a[contains(@href,'activity/edit')]//"
	private static String Update_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'update']"
	private static String CancelEdit_btn_xpath= "//div[@class = 'modal-footer']//button[@data-control = 'cancel-update']"
	private static String ModalClose_btn_xpath = "//div[contains(@id,'modal-edit')]//i[@class='icon-close']"


	public TaskListPage() {
	}

	public static WebElement ModalClose_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalClose_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CancelEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CancelEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ActivityName_lnk(String title) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			String xpath_str = ActivityName_lnk_xpath + "span[text() ='"+title+"']"
			//println(xpath_str)
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			String xpath_str = ActivityName_lnk_xpath + "span[text() ='"+title+"']"
			println(xpath_str)
			return null
		}
	}


	public static WebElement ActivityNameOfVendor_lnk(String title) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			ActivityName_lnk_xpath = "//a[contains(text(),'"+title+"')]"
			WebElement temp_element = webDriver.findElement(By.xpath(ActivityName_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Close_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Close_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CloseActivityVendor_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CloseActivityVendor_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ResultActivity_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ResultActivity_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ReasonClosing_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ReasonClosing_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Task_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Task_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FollowUp_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FollowUp_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Delete_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Delete_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Clear_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Clear_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchFromDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchFromDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchToDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchToDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement SearchPriority_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchPriority_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchStatus_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchStatus_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement ActivityPriority_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ActivityPriority_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ScheduledWith_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ScheduledWith_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> ScheduledWith_listDrd() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(ScheduledWith_listDrd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NotiRemider_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NotiRemider_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> NotiRemider_listDrd() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(NotiRemider_listDrd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> ResultActivity_listDrd() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(ResultActivity_listDrd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement EditActivityVendor_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element =webDriver.findElement(By.xpath(EditActivityVendor_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement Start_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Start_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement End_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(End_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Title_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Title_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Content_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Content_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Attach_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Attach_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ActivityList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ActivityList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Activity_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Activity_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	//=================================METHOD

	public static void SearchActivity_func(String StartDate_str="", String EndDate_str="")
	{
		if(StartDate_str!="")
		{

			Common.ActionSendkey(this.SearchFromDate_txt(), StartDate_str)

		}
		if(EndDate_str!="")
		{
			Common.ActionSendkey(this.SearchToDate_txt(), EndDate_str)
		}

		Common.HandleElementClick(this.Search_btn())

		WebUI.delay(10)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		Common.WaitForElementDisplay(ActivityTab.ActivityList_tbl())

	}

	public static void VerifyActivityExistTable_func(Activity Act)
	{
		Boolean exist = false
		this.SearchActivity_func(Act.StartDate)
		List<String> records = Common.GetTableRecordsPerPaging_func(this.ActivityList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(Act.Title))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Activity["+Act.Title+"] NOT exist in table.\n"
		}//end if
	}

	public static void OpenDetailsModal_func(Activity Act)
	{
		this.SearchActivity_func(Act.StartDate)
		Common.WaitForElementDisplay(this.ActivityList_tbl())
		println(this.ActivityName_lnk_xpath + "span[text()='"+Act.Title+"']")
		this.ActivityName_lnk(Act.Title).click()


	}


	public static void VerifyModalDetailsViewMode_func(Activity act_info)
	{
		String area = "Modal Detail - ViewMode - "
		String field = ""

		this.ActivityName_lnk(act_info.Title).click()

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.Title_txt())

		field = "Title_txt"
		Common.VerifyFieldValueEqual_func(area+field, this.Title_txt(), act_info.Title)

		field = "Content_txt"
		Common.VerifyTinyContentEqual_func(area+field, this.Content_txt())

		field = "Close_opt"
		Common.VerifyFieldTextEqual_func(field, this.Close_opt(), act_info.Closed.toString())

	}


	public static void CreateActivity_func(Activity act)
	{
		HeadingMenu menu  = new HeadingMenu()
		
		menu.Create
	}


}
