package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException

public class Usersetting_menu {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private  static String SettingGear_icon_xpath;
	private  static String LogOut_mnuitem_xpath;
	private  static String Profile_mnuitem_xpath;
	private  static String Setting_mnuitem_xpath;

	public Usersetting_menu() {
		webDriver = DriverFactory.getWebDriver();



		if(GlobalVariable.glb_SiteName=='Bec') {
			SettingGear_icon_xpath="//i[@class='ion ion-ios-gear']";
			LogOut_mnuitem_xpath="//a[contains(text(),'Logout')]";
			Profile_mnuitem_xpath="//a[contains(text(),'Profile')]";
			Setting_mnuitem_xpath="//a[contains(text(),'Settings')]";
		}

		if(GlobalVariable.glb_SiteName=='Vinestate'||GlobalVariable.glb_SiteName=='CLP'
		||GlobalVariable.glb_SiteName=='WWR') {
			SettingGear_icon_xpath="//i[@class='fi-cog noti-icon']";
			LogOut_mnuitem_xpath="//a[contains(@href,'logout')]";
			Profile_mnuitem_xpath="//a[contains(@href,'profile')]//span[text()='Profile']";
			Setting_mnuitem_xpath="//a[contains(@href,'security')]//span[text()='Settings']";
		}
	}


	//public  WebElement username_txt = webDriver.findElement(By.xpath(xpath_str));


	public static WebElement SettingGear_icon()
	{
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SettingGear_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}

	}

	public static WebElement LogOut_mnuitem()
	{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(LogOut_mnuitem_xpath));
		return temp_element;

	}

	public static WebElement Profile_mnuitem()
	{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Profile_mnuitem_xpath));
		return temp_element;

	}


	public static WebElement Setting_mnuitem()
	{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Setting_mnuitem_xpath));
		return temp_element;
	}

}