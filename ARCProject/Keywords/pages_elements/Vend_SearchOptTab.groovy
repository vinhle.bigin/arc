package pages_elements

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import functions.Navigation
import internal.GlobalVariable
import objects.Activity
import objects.Vendor

public class Vend_SearchOptTab {

private static String SearchOpt_tab_xpath ="//a[contains(@href,'#vendor-search-option')]"
private static String Edit_btn_xpath ="//div[@id ='search-infomation-panel']//a[@data-original-title='Edit']"
private static String Update_btn_xpath ="//div[@id ='search-infomation-panel']//button[@data-control='update']"
private static String Cancel_btn_xpath ="//div[@id ='search-infomation-panel']//button[@data-control='cancel-update']"
private static String IsPreferVendor_opt_xpath ="//div[@id ='search-infomation-panel']//input[@type='checkbox']"
private static String IsUseZip_opt_xpath ="//div[@id ='search-infomation-panel']//input[@name ='check-mile']"
private static String IsUSA_opt_xpath ="//div[@id ='search-infomation-panel']//input[@name ='in-usa']"
private static String IsInternation_opt_xpath ="//div[@id ='search-infomation-panel']//input[@name ='international']"
private static String ExcludeClient_ddl_xpath ="//div[@id ='search-infomation-panel']//div[@class ='b__components b__multi__select']"
private static String IncludeZip_txt_xpath ="//input[@id ='vendor-detail-zip-code']"
	
	public Vend_SearchOptTab() {
	}
	
	public static WebElement SearchOpt_tab(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(SearchOpt_tab_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement Edit_btn(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Edit_btn_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement Update_btn(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Update_btn_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement Cancel_btn(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement IsPreferVendor_opt(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(IsPreferVendor_opt_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement IsUseZip_opt(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(IsUseZip_opt_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement IsUSA_opt(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(IsUSA_opt_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement IsInternation_opt(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(IsInternation_opt_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}
public static WebElement ExcludeClient_ddl(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(ExcludeClient_ddl_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}


public static WebElement IncludeZip_txt(){
	try{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(IncludeZip_txt_xpath))
		return temp_element;
	}
	catch(NoSuchElementException e){
		return null
	}
}



//===========================================METHOD

public static void UpdateSearchOption_fun(Vendor vendor,String button ="Submit")
	{
		Common.HandleElementClick(this.SearchOpt_tab())
		WebUI.delay(2)
		Common.HandleElementClick(this.Edit_btn())
		String isprefer = Common.GetFieldText_func(this.IsPreferVendor_opt())	
	
		if(vendor.SearchOpt_IsPrefered.toString()!=isprefer)
		{
			Common.HandleElementClick(this.IsPreferVendor_opt())
		}
		
		if(vendor.SearchOpt_IsZip==true)
		{Common.HandleElementClick(this.IsUseZip_opt())
			
			int count = vendor.SearchOpt_ZipList.size()
			for(int i =0;i<count;i++)
			{
				this.IncludeZip_txt().sendKeys(vendor.SearchOpt_ZipList.get(i))
				if(i!=count-1)
				{
					this.IncludeZip_txt().sendKeys(",")
				}
			}//end for
		}//end if
			
		else if(vendor.SearchOpt_IsUSA==true)
		Common.HandleElementClick(this.IsUSA_opt())
			
		else if(vendor.SearchOpt_IsInter==true)
		Common.HandleElementClick(this.IsInternation_opt())
						
		if(button == "Submit")
			Common.HandleElementClick(this.Update_btn())
			
		else if(button =="Cancel")
			Common.HandleElementClick(this.Cancel_btn())
		
	}	//end void

}
