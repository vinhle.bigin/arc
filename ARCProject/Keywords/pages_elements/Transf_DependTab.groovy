package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.DependentInfo
import objects.Transferee
import internal.GlobalVariable

public class Transf_DependTab {

	private static String Dependent_tab_xpath = "//a[@href = '#transferee-dependents']"

	private static String New_btn_xpath= "//button[@data-toggle = 'modal']"
	private static String Modal_Fname_txt_xpath= "//div[@org-placeholder = 'Enter First Name']//input"
	private static String Modal_Lname_txt_xpath= "//div[@org-placeholder = 'Enter Last Name']//input"
	private static String Modal_State_txt_xpath= "//div[@org-placeholder = 'State']//select"
	private static String Modal_EmailType_ddl_xpath= "//div[@class = 'email-wrap extension-fields']//div[@org-placeholder = 'Type']//select"
	private static String Modal_Email_txt_xpath= "//div[@org-placeholder = 'E-mail']//input"
	private static String Modal_Submit_btn_xpath= "//div[@class = 'modal-footer']//button[@type = 'submit']"
	private static String Modal_Cancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control, 'cancel')]"
	private static String Dependent_List_xpath= "//div[@id = 'table-list-transferee-dependent']//table/tbody"

	private static String Modal_Edit_btn_xpath= "//a[@data-original-title = 'Edit']"




	public Transf_DependTab() {
	}



	public static WebElement Dependent_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dependent_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	public static WebElement New_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(New_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_Fname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_Fname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_Lname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_Lname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_State_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_State_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_EmailType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_EmailType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_Email_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_Email_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dependent_List() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dependent_List_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement DependentName_lnk(String fullname_str) {
		try{
			String DependentName_lnk_xpath= "//a[text() = '"+fullname_str+"']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DependentName_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	/////////================================METHOD
	public static void VerifyModalDetailsViewMode_func(DependentInfo D_info)
	{
		String area = ""
		String field_name = ""
		String mode = "View Mode"

		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		this.DependentName_lnk(D_info.firstname).click()

		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.Modal_Edit_btn())

		field_name = "Modal_Fname_txt"
		Common.VerifyFieldValueEqual_func(area+field_name , this.Modal_Fname_txt(),D_info.firstname, mode)

		field_name = "Modal_Lname_txt"
		Common.VerifyFieldValueEqual_func(area+field_name , this.Modal_Lname_txt(),D_info.lastname, mode)

		field_name = "Modal_State_txt"
		Common.VerifyFieldTextEqual_func(area+field_name , this.Modal_State_txt(),D_info.AddrInfo.State, mode)

		field_name = "Modal_Email_txt"
		Common.VerifyFieldValueEqual_func(area+field_name , this.Modal_Email_txt(),D_info.MailList.get(0).EmailAddr, mode)


	}//end void

	public static void VerifyItemDisplayInTable_func(DependentInfo D_info)
	{
		List tble_list = Common.GetTableRecordsPerPaging_func(this.Dependent_List())

		int size_int = tble_list.size()
		Boolean exist = false
		for(int i:size_int-1)
		{
			String item_text = tble_list.get(i)
			if(item_text.contains(D_info.firstname))
			{
				exist = true
				return
			}//end if

		}//end for

		if(exist==false)
		{
			GlobalVariable.glb_TCStatus=false
			GlobalVariable.glb_TCFailedMessage += "Dependency["+D_info.FullName+"] does not exist.Observed: "+tble_list+".\n"
		}

	}//end void




}
