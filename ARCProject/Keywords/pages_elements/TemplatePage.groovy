package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import functions.Navigation
import objects.Template
import objects.Transferee
import internal.GlobalVariable

public class TemplatePage {


	//Landing page

	private static String Search_btn_xpath= "//button[@data-control ='search']"
	private static String New_btn_xpath= "//a[@href ='#/new']"
	private static String Search_TemplName_txt_xpath= "//input[@placeholder ='Template Name']"
	private static String Templ_List_tbl_xpath= "//table[@id = 'template-table']/tbody"
	private static String Search_TemplType_ddl_xpath= "//input[@placeholder ='Type']"
	private static String Search_Target_ddl_xpath= "//input[@placeholder ='Target Record']"
	private static String Search_Status_ddl_xpath= "//input[@placeholder ='Status']"
	private static String SearchClear_btn_xpath= "//button[@data-control ='cancel-search']"


	//Edit Page
	private static String Edit_Type_ddl_xpath= "//select[@placeholder = 'Type']"
	private static String Edit_Target_ddl_xpath= "//select[@placeholder = 'Target Record']"
	private static String Edit_EmailFrom_ddl_xpath= "//ul[@class ='b__multi__select__list']"
	private static String Edit_ServiceType_ddl_xpath= "//select[@placeholder = 'Service Type']"
	private static String Edit_EmailFrom_txt_xpath= "//input[@placeholder = 'Email From']"
	private static String Edit_EmailFrom_vlue_xpath = "//span[@class='thumb']"
	private static String Edit_Name_txt_xpath= "//input[@placeholder = 'Template Name']"
	private static String Edit_Descr_txt_xpath= "//input[@placeholder = 'Description']"
	private static String Edit_Subject_txt_xpath= "//input[@placeholder = 'Subject']"
	private static String Edit_Search_txt_xpath= "//input[@placeholder = 'Search box']"
	private static String Edit_Attach_lnk_xpath= "//div[@class = 'content dz-clickable']"
	private static String Edit_Submit_btn_xpath= "//button[@id = 'submit-template']"
	private static String Edit_Cancel_btn_xpath= "//button[@cancel = 'cancel-template']"

	private static String Edit_Content_txt_xpath= "//p"

	private static String New_IsDocProcess_xpath = "//span[contains(text(),'doc_name_arg')]/..//input[contains(@name,'is-process')]"

	private static String New_DocRemove_icon_xpath = "//span[contains(text(),'doc_name_arg')]/..//a[@class='remove-archive']"

	private static String New_ProcessDocX_opt_xpath = "//span[contains(text(),'doc_name_arg')]/..//input[@value='docx']"

	private static String New_ProcessPDF_opt_xpath = "//span[contains(text(),'doc_name_arg')]/..//a[@class='pdf']"


	private static String Edit_ProcessDocX_opt_xpath = "//span[contains(text(),'doc_name_arg')]/../../..//div[@value='docx']/input"

	private static String Edit_ProcessPDF_opt_xpath = "//span[contains(text(),'doc_name_arg')]/../../..//div[@value='pdf']/input"


	//	Type = Address Label
	private static String Edit_CtentAddr1_txt_xpath = "//input[@placeholder = 'Address 1']"
	private static String Edit_CtentAddr2_txt_xpath = "//input[@placeholder = 'Address 2']"
	private static String Edit_CtentAddr3_txt_xpath = "//input[@placeholder = 'Address 3']"
	private static String Edit_CtentAddr4_txt_xpath = "//input[@placeholder = 'Address 4']"
	private static String Edit_CtentAddr5_txt_xpath = "//input[@placeholder = 'Address 5']"

	private static String Edit_IsActive_chk_xpath = "//input[@type = 'checkbox']"

	public TemplatePage() {
	}


	public static WebElement Edit_ProcessDocX_opt(String doc_name) {
		try{
			String xpath_str = Edit_ProcessDocX_opt_xpath.replace("doc_name_arg", doc_name)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Edit_ProcessPDF_opt(String doc_name) {
		try{
			String xpath_str = Edit_ProcessPDF_opt_xpath.replace("doc_name_arg", doc_name)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement New_ProcessDocX_opt(String doc_name) {
		try{
			String xpath_str = New_ProcessDocX_opt_xpath.replace("doc_name_arg", doc_name)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement New_ProcessPDF_opt(String doc_name) {
		try{
			String xpath_str = New_ProcessPDF_opt_xpath.replace("doc_name_arg", doc_name)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement New_IsDocProcess(String doc_name) {
		try{
			String xpath_str = New_IsDocProcess_xpath.replace("doc_name_arg", doc_name)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement New_DocRemove_icon(String doc_name) {
		try{

			String xpath_str = New_DocRemove_icon_xpath.replace("doc_name_arg", doc_name)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement SearchClear_btn() {
		try{

			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchClear_btn_xpath));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Search_TemplType_ddl() {
		try{

			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_TemplType_ddl_xpath));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Search_Target_ddl() {
		try{

			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_Target_ddl_xpath));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Search_Status_ddl() {
		try{

			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_Status_ddl_xpath));

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_Content_txt() {
		try{

			WebDriver webDriver = Navigation.SwitchIframe_func(Navigation.TinyIframe_txt())

			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Content_txt_xpath+"/.."));
			//((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", Navigation.TinyIframe_txt());
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_IsActive_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_IsActive_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_CtentAddr1_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_CtentAddr1_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_CtentAddr2_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_CtentAddr2_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_CtentAddr3_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_CtentAddr3_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_CtentAddr4_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_CtentAddr4_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_CtentAddr5_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_CtentAddr5_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Remove_icon(String tpl_name) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			String xpath_str = this.Templ_List_tbl_xpath +"//a[text()='"+tpl_name+"']/././/a[@data-original-title= 'Remove']"
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement TemplName_link(String tpl_name) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			String xpath_str = this.Templ_List_tbl_xpath +"//a[text()='"+tpl_name+"']"
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Edit_EmailFrom_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_EmailFrom_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement New_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(New_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Search_TemplName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_TemplName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Templ_List_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Templ_List_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}



	public static WebElement Edit_Type_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Type_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Target_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Target_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_EmailFrom_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_EmailFrom_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_ServiceType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_ServiceType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_EmailFrom_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_EmailFrom_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Name_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Name_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Descr_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Descr_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Subject_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Subject_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Search_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Search_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Attach_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Attach_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}




	/////////================================METHOD
	public static void Search_func(Template tpl)
	{
		Navigation.GotoTemplatePage()

		Common.sendkeyTextToElement(this.Search_TemplName_txt(), tpl.Name)

		this.Search_btn().click()

		WebUI.delay(3)

		Boolean exist = Common.IsItemExistTable(this.Templ_List_tbl(),tpl.Name)

		if(exist==false)
		{
			GlobalVariable.glb_TCFailedMessage+="Template["+tpl.Name+"] NOT found.\n"
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}

	}

	public static void Search_func(String SearchType = "Name",String Search_vlue = "")
	{
		Navigation.GotoTemplatePage()

		switch(SearchType)
		{
			case "Name":
				Common.sendkeyTextToElement(this.Search_TemplName_txt(), Search_vlue)
				break
			case "Type":
				Common.SelectDropdownItem(this.Search_TemplType_ddl(), Search_vlue )
				break
			case "Target":
				Common.SelectDropdownItem(this.Search_Target_ddl(), Search_vlue )
				break
			case "Status":
				Common.SelectDropdownItem(this.Search_Status_ddl(), Search_vlue )
				break
		}

		this.Search_btn().click()

		WebUI.delay(3)

	}


	public static void VerifyDetails_func(Template tpl)
	{
		String area = "Template Details-"
		String field_name = ""
		String mode = ""

		//Common.WaitForElementNotDisplay(LandingPage.LoadingArc())

		this.Search_func(tpl)

		this.TemplName_link(tpl.Name).click()

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.Edit_ServiceType_ddl())

		//WebUI.delay(5)
		field_name = "Edit_Type_ddl"
		Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_Type_ddl(),tpl.Type, mode)
		//Common.VerifyFieldValueEqual_func(area+field_name , this.Edit_Type_ddl(),tf.OriResAddr_info.Addr1, mode)

		field_name = "Edit_Target_ddl"
		Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_Target_ddl(),tpl.Target, mode)

		switch(tpl.Type)
		{
			case "Address Label":

				field_name = "Edit_CtentAddr1_txt"
				Common.VerifyFieldValueEqual_func(area+field_name , this.Edit_CtentAddr1_txt(), tpl.Content , mode)

				field_name = "Edit_CtentAddr2_txt"
				Common.VerifyFieldValueEqual_func(area+field_name , this.Edit_CtentAddr2_txt(), tpl.Content , mode)

				field_name = "Edit_CtentAddr3_txt"
				Common.VerifyFieldValueEqual_func(area+field_name , this.Edit_CtentAddr3_txt(), tpl.Content , mode)

				field_name = "Edit_CtentAddr4_txt"
				Common.VerifyFieldValueEqual_func(area+field_name , this.Edit_CtentAddr4_txt(), tpl.Content , mode)

				field_name = "Edit_CtentAddr5_txt"
				Common.VerifyFieldValueEqual_func(area+field_name , this.Edit_CtentAddr5_txt(), tpl.Content , mode)

				break

			case "Email":
				field_name = "Edit_ServiceType_ddl"
				Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_ServiceType_ddl(),tpl.ServiceType, mode)


				field_name = "Edit_EmailFrom_txt"
				Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_EmailFrom_vlue(),tpl.EmailFrom, mode)

				field_name = "Edit_Subject_txt"
				Common.VerifyFieldValueEqual_func(area+field_name , this.Edit_Subject_txt(),tpl.Subject , mode)

				break

		}

		if(tpl.Type !="Address Label")
		{
			//Navigation.SwitchIframe_func(Navigation.TinyIframe_txt())
			field_name = "Edit_Content_txt"

			String Content_str = this.Edit_Content_txt().getText()

			WebDriver webDriver = DriverFactory.getWebDriver()

			webDriver = Navigation.SwitchIframe_func()

			if(Content_str!=tpl.Content)
			{
				GlobalVariable.glb_TCStatus = false
				GlobalVariable.glb_TCFailedMessage += area+field_name+"Incorrect:[Observed: "+Content_str+" - Expected: "+tpl.Content+"].\n"
			}

			//Common.VerifyFieldTextEqual_func(field_name, this.Edit_Content_txt(), tpl.Content, mode)

			//Navigation.SwitchIframe_func()


		}//end if

		if(tpl.DocName!="")
		{

			mode = "When Document set as Proccess"
			if(tpl.DocProcessType=="DOCX")

			{
				field_name = "Edit_ProcessDocX_opt"
				Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_ProcessDocX_opt(tpl.DocName), "true", mode)
			}
			else if(tpl.DocProcessType=="DOCX")
			{
				field_name = "Edit_ProcessPDF_opt"
				Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_ProcessPDF_opt(tpl.DocName), "true", mode)

			}//else
			else
			{
				mode = "When Document not set as Proccess"
				field_name = "Edit_ProcessDocX_opt"
				Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_ProcessDocX_opt(tpl.DocName), "false", mode)

				field_name = "Edit_ProcessPDF_opt"
				Common.VerifyFieldTextEqual_func(area+field_name , this.Edit_ProcessPDF_opt(tpl.DocName), "false", mode)
			}//end final else

		}//if Docname!=""


	}//end void



	//FILL INFO INTO CREATE NEW TEMPLATE
	public void FillInfo_func(String action_str = "Create",Template tpl,String button_str = "Submit")
	{
		if(action_str == "Create")
		{
			Common.ScrollElementtoViewPort_func(this.Edit_Type_ddl())

			Common.SelectDropdownItem(this.Edit_Type_ddl(), tpl.Type)

			Common.SelectDropdownItem(this.Edit_Target_ddl(),tpl.Target)
		}

		Common.sendkeyTextToElement(this.Edit_Name_txt(),tpl.Name)

		if(tpl.IsActive!=this.Edit_IsActive_chk().isSelected())
			Common.HandleElementClick(this.Edit_IsActive_chk())

		switch(tpl.Type)
		{
			case "Address Label":
				Common.sendkeyTextToElement(this.Edit_CtentAddr1_txt(), tpl.Content)

				Common.sendkeyTextToElement(this.Edit_CtentAddr2_txt(), tpl.Content)

				Common.sendkeyTextToElement(this.Edit_CtentAddr3_txt(), tpl.Content)

				Common.sendkeyTextToElement(this.Edit_CtentAddr4_txt(), tpl.Content)

				Common.sendkeyTextToElement(this.Edit_CtentAddr5_txt(), tpl.Content)

				break

			case "Email":
				Common.SelectDropdownItem( this.Edit_ServiceType_ddl(),tpl.ServiceType)

				this.Edit_EmailFrom_vlue().click()

				WebUI.delay(1)

				Common.sendkeyTextToElement(this.Edit_EmailFrom_txt(), tpl.EmailFrom)

				Common.SelectDropdownItem(this.Edit_EmailFrom_ddl(),tpl.EmailFrom)

				Common.sendkeyTextToElement(this.Edit_Subject_txt(),tpl.Subject)

				break

		}

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		if(tpl.Type =="Email")
		{

			Action action_key = actions.sendKeys(this.Edit_Subject_txt(), Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(tpl.Content).build()

			action_key.perform()

		}
		else if(tpl.Type =="Letter")
		{
			Action action_key = actions.sendKeys(this.Edit_Name_txt(),Keys.TAB).sendKeys(Keys.TAB).sendKeys(Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(tpl.Content).build()

			action_key.perform()
		}

		WebUI.delay(3)

		//UPLOAD DOCUMENT / PROCESS DOCUMENT
		if(tpl.DocName!="")
		{
			Common.UploadFile_func(this.Edit_Attach_lnk(), tpl.DocName)



			if(tpl.DocProcessType=="DOCX")
			{
				Common.HandleElementClick(this.New_IsDocProcess(tpl.DocName))

				Common.HandleElementClick(this.New_ProcessDocX_opt(tpl.DocName))

			}//end if IsProcess

			else if(tpl.DocProcessType=="PDF")
			{
				Common.HandleElementClick(this.New_IsDocProcess(tpl.DocName))

				Common.HandleElementClick(this.New_ProcessPDF_opt(tpl.DocName))
			}

		}//end if Docment!=""

		if(button_str=="Submit")
			this.Edit_Submit_btn().click()
		else if(button_str=="Cancel")
			this.Edit_Cancel_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg)



	}//end void


	public static void VeifyTemplateNotExistTable_func(Template tpl)
	{
		Boolean exist = Common.IsItemExistTable(this.Templ_List_tbl(), tpl.Name)

		if(exist==true)
		{
			GlobalVariable.glb_TCStatus=false
			GlobalVariable.glb_TCFailedMessage += "Template["+tpl.Name+"] SHOULD NOT Show after search.\n"
		}//end if
	}//end void



	public static void VerifyTemplateExistTable_func(Template tpl,String note = "")
	{
		Boolean exist = false
		//Search template
		//this.Search_func(tpl)

		List<String> list_tmp = Common.GetTableRecordsPerPaging_func(this.Templ_List_tbl())

		int count = list_tmp.size()

		if(count==0)
		{
			GlobalVariable.glb_TCStatus=false
			GlobalVariable.glb_TCFailedMessage += "No record shown after search "+note+".\n"
		}
		else
		{
			for(int i=0;i<count;i++)
			{
				if(list_tmp.get(i).contains(tpl.Name))
				{
					exist = true
				}
			}//end for
		}//end else

		if(exist==false)
		{
			GlobalVariable.glb_TCStatus=false
			GlobalVariable.glb_TCFailedMessage += "Template["+tpl.Name+"] NOT found after search "+note+".\n"
		}
	}//end void






}//end class

