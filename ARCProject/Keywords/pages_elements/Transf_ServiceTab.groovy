package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.Transferee
import internal.GlobalVariable

public class Transf_ServiceTab {

	private static String Srv_tab_xpath = "//a[contains(@href, 'transferee-services')]"
	private static String Edit_btn_xpath= "//a[contains(@id, 'icon-panel-edit')]"
	private static String HHG_opt_xpath= "//label[text() ='Household goods']/..//input[@type = 'checkbox']"
	private static String Morgt_opt_xpath= "//label[text() ='Mortgage']/..//input[@type = 'checkbox']"
	private static String HP_opt_xpath= "//label[text() ='Home purchase']/..//input[@type = 'checkbox']"
	private static String HS_opt_xpath= "//label[text() ='Home sale']/..//input[@type = 'checkbox']"
	private static String TQ_opt_xpath= "//label[text() ='Temporary quarter']/..//input[@type = 'checkbox']"
	private static String Update_btn_xpath= "//button[@data-control = 'update']"
	private static String Cancel_btn_xpath= "//button[@data-control = 'cancel-update']"


	public Transf_ServiceTab() {
	}



	public static WebElement Srv_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Srv_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HHG_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HHG_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Morgt_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Morgt_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HP_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HP_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}




	/////////================================METHOD
	public static void VerifyServiceInfoViewMode_func(Transferee tf)
	{
		if(LandingPage.Loading_icon()!=null)
		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.Edit_btn())


		String result_str_1 = this.HHG_opt().isSelected().toString()
		println("result_str_1: "+result_str_1)
		String result_str_2 = this.Morgt_opt().isSelected().toString()
		println("result_str_2:"+result_str_2)
		String result_str_3 = this.HP_opt().isSelected().toString()
		println("result_str_3: "+result_str_3)
		String result_str_4 = this.HS_opt().isSelected().toString()
		println("result_str_4: "+result_str_4)
		String result_str_5 = this.TQ_opt().isSelected().toString()
		println("result_str_5: "+result_str_5)
		
		int count = tf.ServiceList.size()
		for(int i =0;i<count;i++)
		{
			String serivce_tmp =tf.ServiceList.get(i).Type
			String status = tf.ServiceList.get(i).Status

			switch(serivce_tmp)
			{
				case "HouseHoldGood":
					Common.VerifyFieldTextEqual_func(serivce_tmp,this.HHG_opt(),status,"View Mode")
					break;
				case "Mortgage":
					Common.VerifyFieldTextEqual_func(serivce_tmp,this.Morgt_opt(),status,"View Mode")
					break;

				case "HomePurchase":
					Common.VerifyFieldTextEqual_func(serivce_tmp,this.HP_opt(),status,"View Mode")
					break;

				case "HomeSale":
					Common.VerifyFieldTextEqual_func(serivce_tmp, this.HS_opt(),status,"View Mode")
					break;

				case "TemporaryQuarter":
					Common.VerifyFieldTextEqual_func(serivce_tmp,this.TQ_opt(),status,"View Mode")
					break;
			}//end switch

		}//end for




	}




}
