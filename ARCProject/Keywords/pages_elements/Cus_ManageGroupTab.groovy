package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.firefox.FirefoxProfile
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.CustomGroup
import objects.Transferee
import internal.GlobalVariable

public class Cus_ManageGroupTab {

	private static String ManageGroup_tab_xpath= "//label[@id='manage-group-option']"
	private static String FieldType_ddl_xpath= "//select[@placeholder ='Field Type']"
	private static String Tab_ddl_xpath= "//select[@placeholder ='Tab']"
	private static String Filter_btn_xpath= "//button[@id ='filter-layout']"
	private static String NewGroup_btn_xpath= "//button[@id ='create-group']"
	private static String Search_txt_xpath= "//input[@type='search']"
	private static String GroupList_tbl_xpath= "//table[@id='tb-list-group']/tbody"
	private static String ModalFieldType_ddl_xpath= "//div[@class='modal-body clearfix']//select[@placeholder ='Field Type']"
	private static String ModalTab_ddl_xpath= "//div[@class='modal-body clearfix']//select[@placeholder ='Tab']"
	private static String ModalGroupName_txt_xpath= "//div[@class='modal-body clearfix']//input[@placeholder ='Group Name']"
	private static String ModalCreate_btn_xpath= "//div[@id='group-custom-field-form']//button[@data-control ='create']"
	private static String ModalCancel_btn_xpath= "//div[@id='group-custom-field-form']//button[@data-dismiss ='modal'][1]"
	private static String Edit_icon_xpath= ".//span[@data-toggle='modal']"
	private static String Remove_icon_xpath= ".//span[@title='Remove group']"
	private static String ModalUpdate_btn_xpath= "//div[@id='group-custom-field-form']//button[@data-control ='update']"




	public Cus_ManageGroupTab() {
	}

	public static WebElement ManageGroup_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ManageGroup_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FieldType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FieldType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Tab_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Tab_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Filter_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Filter_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewGroup_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewGroup_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GroupList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GroupList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalFieldType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalFieldType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalTab_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalTab_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalGroupName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalGroupName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCreate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCreate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_icon(String groupname) {
		try{

			WebElement temp_element = Common.GetElementInTable(groupname, this.GroupList_tbl(), Edit_icon_xpath)
			//webDriver.findElement(By.xpath());
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Remove_icon(String groupname) {
		try{

			WebElement temp_element = Common.GetElementInTable(groupname, this.GroupList_tbl(), Remove_icon_xpath)
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ModalUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ModalUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}







	/////////================================METHOD
	public static void VerifyModalViewMode_func(CustomGroup group)
	{
		String area = ""
		String field_name = ""
		String mode = "View Mode"

		//Common.WaitForElementNotDisplay(LandingPage.LoadingArc())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.HandleElementClick(this.Edit_icon(group.Name))

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.ModalGroupName_txt())

		///ORG RESIDENCE
		area = "Custom Group - "

		field_name = "ModalFieldType_ddl"
		Common.VerifyFieldTextEqual_func(area+field_name , this.ModalFieldType_ddl(),group.FieldType, mode)

		field_name = "OrgRes_Addrs2"
		Common.VerifyFieldTextEqual_func(area+field_name , this.ModalTab_ddl(),group.Tab , mode)

		field_name = "ModalGroupName_txt"
		Common.VerifyFieldValueEqual_func(area+field_name , this.ModalGroupName_txt(),group.Name , mode)


	}

	public static void FillModalInfo_func(CustomGroup group,String Action = "Create")
	{

		Common.WaitForElementDisplay(this.ModalGroupName_txt())

		if(Action=="Create")
		{
			Common.SelectDropdownItem(this.ModalFieldType_ddl(), group.FieldType)

			Common.SelectDropdownItem(this.ModalTab_ddl(), group.Tab)
		}

		Common.sendkeyTextToElement(this.ModalGroupName_txt(),group.Name)
	}


}
