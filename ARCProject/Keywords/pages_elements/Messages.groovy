package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import functions.Common


import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import java.lang.Math as Math

import org.openqa.selenium.NoSuchElementException

public class Messages {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private static ConfirmNo_btn_xpath	= "//button[text() ='Cancel']" //Updated: 5/29/2018
	private static ConfirmYes_btn_xpath	= "//button[text() ='Yes']" //Updated: 5/29/2018
	private static modalconfirmationheader_xpath	= "//h4[@class ='modal-title']";
	private static modalmessagecontent_xpath	= "//div[@class = 'modal-body']"
	private static PopupMessage_xpath	= "//div[@id='error-modal']//div[@class ='modal-body']"// | //div[@class ='modal-body']"
	private static OppsOk_btn_xpath	= "//button[contains(text(),'OK')]"
	private static SuccessSubmitMessage_xpath	= "//div[contains(@class,'noti-success-content')]"
	private static Servercache_msg_xpath = "//span[contains(text(), 'No such file or directory')]"

	private static ContLastMove_lnk_xpath ="//a[@id = 'has-last-move-plan-in-modal']"
	private static StartNewPlan_btn_xpath = "//button[@id='submit-exactly-in-modal']"
	private static Close_btn_xpath = "//div[@class = 'modal-content']//button[@class ='close']"

	private static notification_msg_xpath = "//div[@class = 'toast-message']"

	private static Delete_btn_xpath = "//button[contains(@id,'delete-action')] | //button[contains(text(),'Remove')]"
	private static DeleteItem_btn_xpath = "//button[@data-control='delete']"

	private static String NoResultMsg_lb_xpath = "//td[@class='text-center']"
	//private static String Close_btn_xpath = "//div[@class='modal-dialog modal-md']//i[@class='icon-close']"


	//==================Message Type
	private static SystemError_msg = "Server error Request. Please try again or contact IT helpdesk."
	private static noResutl_msg = "No matching records"
	private static newContactToast_msg = "New Vendor Contact has been created successfully."
	private static updateVendor_msg = "Vendor has been updated successfully."
	private static updateContact_msg = "Vendor Contact has been updated successfully."




	public Messages() {
		webDriver = DriverFactory.getWebDriver();

	}

	public static WebElement Delete_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Delete_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement DeleteItem_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DeleteItem_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement notification_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(notification_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Close_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Close_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement StartNewPlan_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(StartNewPlan_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContLastMove_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContLastMove_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Servercache_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Servercache_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement ConfirmNo_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(ConfirmNo_btn_xpath));
		return temp_element;
	}

	public static WebElement ConfirmYes_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ConfirmYes_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement modalconfirmationheader() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(modalconfirmationheader_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement modalmessagecontent() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(modalmessagecontent_xpath));
			return temp_element;
		}

		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PopUpMessage() {
		try {
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PopupMessage_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement OppsOk_btn() {
		try {
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OppsOk_btn_xpath));
			return temp_element;
		}

		catch(Exception e) {
			return null
		}
	}

	public static WebElement SuccessSubmitMessage() {
		try {
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SuccessSubmitMessage_xpath));
			return temp_element;
		}

		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NoResultMsg_lb() {
		try {
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NoResultMsg_lb_xpath));
			return temp_element;
		}

		catch(NoSuchElementException e) {
			return null
		}
	}

	//=====================================================FUNCTION=================
	//======================================================
	//=============================

	public static void VerifyNotificationMessageDisplay_func(String Expected_msg,String note = "")
	{

		if(this.notification_msg()==null)
		{

			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "The Notification disappeared too fast or is not found. "
		}
		else
		{
			if(!this.notification_msg().getText().contains(Expected_msg))
			{
				GlobalVariable.glb_TCStatus = false
				GlobalVariable.glb_TCFailedMessage += "Incorrect Notification content.[Observed:"+this.notification_msg().getText()+"-Expected:"+Expected_msg+"]."+ " "+note
			}
		}
	}

	public static void VerifyNotifyMsgNotDisplay_func(String Expected_msg,String note = "")
	{
		Common.WaitForElementDisplay(this.notification_msg())

		if(this.notification_msg()!=null)
		{
			if(this.notification_msg().getText().contains(Expected_msg))
			{
				GlobalVariable.glb_TCStatus = false
				GlobalVariable.glb_TCFailedMessage += "Notify Message Should NOT Display.[Observed:"+this.notification_msg().getText()+"-Expected:"+Expected_msg+"]."+ " "+note+"\n"
			}
		}


	}
	public static void VerifyPopUpMessageDisplay_func(String Expected_msg)
	{
		if(this.PopUpMessage()==null)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Opps message pop-up does not display."
			return
		}

		if (this.PopUpMessage().getText() != Expected_msg) {
			String result_message = ((((('The Oops messages do not match.Observed: ' + this.PopUpMessage().getText()) + '-') + 'Expected: ') +
					Expected_msg) + '.')

			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += result_message

			if(this.OppsOk_btn()!=null)
			{
				this.OppsOk_btn().click()
				WebUI.delay(2)
			}

		}
	}//and func
}//END CLASS