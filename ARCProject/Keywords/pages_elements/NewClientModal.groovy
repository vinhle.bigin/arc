package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Assert
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import internal.GlobalVariable
import objects.Transferee

public class NewClientModal {
	private static String Fname_txt_xpath = "//input[@placeholder = 'First Name']"
	private static String Lname_txt_xpath = "//input[@placeholder = 'Last Name']"
	private static String Client_slect_xpath = "//div[@class='form-group col-md-4 multi-client']//div[contains(@class,'dropdown-main addBorder')]/ul"
	private static String Coordinate_slect_xpath = "//div[@id='list-coordinators']//ul"
	private static String InitDate_txt_xpath = "//input[@id = 'create-transferee-other-info-initiation-date']"
	private static String OrigState_slect_xpath = "//h4[text() = 'Home Address']/..//select[@placeholder = 'State']"
	private static String DestState_slect_xpath = "//h4[text() = 'Destination Office Address']/..//select[@placeholder = 'State']"
	private static String Create_btn_xpath = "//button[@data-control = 'create']"

	private static String Coorp_txt_xpath = "//input[@id = 'input-list-coordinators']"

	private static String Client_txt_xpath = "//input[@placeholder = 'Client']"

	private static String CoordExpand_icon_xpath = "//div[@id='list-coordinators']//div[@class = 'control']"

	/*
	 private static String 
	 private static String 
	 private static String 
	 private static String 
	 */
	public NewClientModal() {
	}

	public static WebElement Coord_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Coorp_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CoordExpand_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CoordExpand_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Client_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Client_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Coordinate_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Coordinate_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Fname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Fname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Lname_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Lname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Client_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Client_slect_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement InitDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(InitDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static Select OrigState_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			Select temp_element = new Select( webDriver.findElement(By.xpath(OrigState_slect_xpath)))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static Select DestState_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			Select temp_element = new Select(webDriver.findElement(By.xpath(DestState_slect_xpath)))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Create_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Create_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	//METHOD

	public static void FillInfoModal_func(Transferee trf,Boolean submit = true)
	{
		WebUI.delay(2)

		Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		this.Fname_txt().sendKeys(trf.firstname)

		WebUI.delay(1)

		this.Lname_txt().sendKeys(trf.lastname)

		WebUI.delay(1)

		this.Client_txt().sendKeys(trf.Client_info.CompanyName)

		Common.SelectItemULElement_func(this.Client_slect(), trf.Client_info.CompanyName)

		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc())

		//this.Coordinator = "em2 2"
		Common.WaitForElementDisplay(this.Coord_txt())
		this.Coord_txt().sendKeys(trf.Coordinator)

		Common.SelectItemULElement_func(this.Coordinate_slect(), trf.Coordinator)

		WebUI.delay(1)

		Common.ActionSendkey(this.InitDate_txt(), trf.FileEnterDate)

		WebUI.delay(1)

		this.InitDate_txt().sendKeys(Keys.TAB)

		this.OrigState_slect().selectByVisibleText(trf.OriResAddr_info.State)

		WebUI.delay(5)

		this.DestState_slect().selectByVisibleText(trf.DesOffAddr_info.State)

		WebUI.delay(5)

		if(submit)
		{
			this.Create_btn().click()

			WebUI.delay(5)

			if(Messages.notification_msg()!=null)
			{
				String msg = Messages.notification_msg().getText()
				String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
				println(msg)
				if(msg ==expected_msg)
				{
					GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull TRANSF Creation.Msg: '"+msg+"'.\n"
					Assert.fail(GlobalVariable.glb_TCFailedMessage)
				}
			}//end if of message


		}//end if of submit
	}
}
