package pages_elements

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.Select
import org.testng.Assert

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import functions.Navigation
import internal.GlobalVariable
import objects.Mailinfo
import objects.NoteInfo
public class NoteTab {
	private static String Note_tab_xpath = "//ul[@role='tablist']//a[contains(@href, '-notes')]"
	private static String CreateNew_btn_xpath= "//button[text() = 'New Note']"
	private static String CreateNew_ic_xpath= "//span[@class='menu-icon fas fa-sticky-note']"
	private static String Search_btn_xpath= "//button[contains(@id,'btn-search')]"
	private static String Clear_btn_xpath= "//button[text() = 'Clear']"
	private static String SearchFromDate_txt_xpath= "//input[contains(@id,'from_date')]"
	private static String SearchToDate_txt_xpath= "//input[contains(@id,'to_date')]"
	private static String SearchSubject_txt_xpath = "//input[@name = 'subject']"
	private static String SearchContent_txt_xpath = "//input[@placeholder='Content']"
	//private static String ActivityType_slect_xpath= "//select[@placeholder = 'Activity Type']"
	private static String ServiceType_slect_xpath= "//select[@placeholder = 'Service']"
	private static String Contact_slect_xpath= "//select[@placeholder = 'Contact']"
	private static String Start_txt_xpath= "//input[@id = 'clientActivityCreate_from_date']"
	private static String End_txt_xpath= "//input[@id = 'clientActivityCreate_to_date']"
	private static String Title_txt_xpath= "//input[contains(@id,'subject')]"
	private static String Content_txt_xpath= "//p"
	private static String Submit_btn_xpath= "//div[@class = 'modal-footer']//button[@type = 'submit']"
	private static String Cancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control, 'cancel')]"
	private static String Attach_lnk_xpath= "//div[@id = 'new-activity-attachment']//div[@class = 'content dz-clickable']"
	private static String NoteList_tbl_xpath= "//table[@class = 'VueTables__table btable table dataTable table-striped table-hover']//tbody | //table[@id = 'table-list-notes-tab']/tbody"

	//private static String Transf_NoteList_tbl_xpath= "//table[@class = 'VueTables__table btable table dataTable table-striped table-hover']//tbody | //table[@id = 'table-list-notes-tab']/tbody"
	//private static String Client_NoteList_tbl_xpath= "//table[@id = 'table-list-notes-tab']/tbody"
	//private static String NoteTitle_lnk_xpath = "//a[contains(@href,'note/edit')]"
	private static String Edit_btn_xpath = "//div[@class ='modal-header']//a[@data-original-title = 'Edit']"

	//private static String Remove_icon_xpath = "//a[contains(@href,'note/delete')]"
	private static String Remove_icon_xpath = "/.//i[@class='icon-close']"


	public NoteTab() {
	}

	public static WebElement Remove_icon(String subject) {
		//WebElement temp_element  = Common.GetElementInTable(subject,this.Transf_NoteList_tbl(), this.Remove_icon_xpath)
		WebElement temp_element  = Common.GetElementInTable(subject,this.NoteList_tbl(), this.Remove_icon_xpath)
		return temp_element
	}

	public static WebElement Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchSubject_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchSubject_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchContent_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchContent_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Note_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Note_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CreateNew_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CreateNew_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CreateNew_ic() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CreateNew_ic_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Clear_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Clear_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchFromDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchFromDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchToDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchToDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ServiceType_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ServiceType_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Contact_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Contact_slect_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Start_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Start_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement End_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(End_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Title_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Title_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Content_txt() {
		try{
			WebDriver webDriver = Navigation.SwitchIframe_func(Navigation.TinyIframe_txt())

			WebElement temp_element = webDriver.findElement(By.xpath(Content_txt_xpath));
			//((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", temp_element);
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Attach_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Attach_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NoteList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NoteList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	/*
	 public static WebElement Transf_NoteList_tbl() {
	 try{
	 WebDriver webDriver = DriverFactory.getWebDriver()
	 WebElement temp_element = webDriver.findElement(By.xpath(Transf_NoteList_tbl_xpath));
	 return temp_element;
	 }
	 catch(NoSuchElementException e) {
	 return null
	 }
	 }
	 public static WebElement Client_NoteList_tbl() {
	 try{
	 WebDriver webDriver = DriverFactory.getWebDriver()
	 WebElement temp_element = webDriver.findElement(By.xpath(Client_NoteList_tbl_xpath));
	 return temp_element;
	 }
	 catch(NoSuchElementException e) {
	 return null
	 }
	 }
	 */
	public static WebElement noteTitle_lnk(String titleNote) {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			String xpath_str =  "//a//span[text()='"+titleNote+"'] | //a[contains(text(),'" + titleNote + "')]"
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str))
			/*
			 WebDriver webDriver = DriverFactory.getWebDriver()
			 String xpath_str =  ".//a//span[text()='"+titleNote+"']"
			 WebElement temp_element
			 if(this.Transf_NoteList_tbl()!=null)
			 {
			 temp_element = Common.GetElementInTable(titleNote, this.Transf_NoteList_tbl(), xpath_str)
			 }
			 else if(this.Client_NoteList_tbl()!=null)
			 {
			 temp_element = Common.GetElementInTable(titleNote, this.Client_NoteList_tbl(), xpath_str)
			 }
			 else
			 {
			 temp_element = webDriver.findElement(By.xpath("//a[contains(text(),'" + titleNote + "')]"));
			 }
			 */
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	//=================================METHOD

	public static void SearchNote_func(String subject_str)
	{
		Common.WaitForElementDisplay(this.SearchSubject_txt())

		Common.sendkeyTextToElement(this.SearchSubject_txt(), subject_str)

		Common.HandleElementClick(this.Search_btn())

		//this.Search_btn().click()

		WebUI.delay(3)

	}

	public static void searchNote(String searchField, WebElement element, String subject_str)
	{
		Common.sendkeyTextToElement(element, subject_str)

		Common.HandleElementClick(this.Search_btn())

		WebUI.delay(3)
	}

	public static void searchNoteDate()
	{
		WebUI.delay(3)
		String FileEnterDate = DateTimeFuncs.GetCurrentLocalDate_Str()

		Common.ActionSendkey(this.SearchFromDate_txt(), FileEnterDate)
		WebUI.delay(1)

		this.SearchFromDate_txt().sendKeys(Keys.TAB)

		Common.ActionSendkey(this.SearchToDate_txt(), FileEnterDate)
		WebUI.delay(1)

		Common.HandleElementClick(this.Search_btn())

		WebUI.delay(3)
	}

	public static void VerifyNoteExistTable_func(NoteInfo note)
	{

		this.Note_tab().click()

		WebUI.delay(3)

		Boolean exist = false

		List<String> records
		records = Common.GetTableRecordsPerPaging_func(this.NoteList_tbl())

		/*
		 if(note.TransfFullName!="")
		 records = Common.GetTableRecordsPerPaging_func(this.Transf_NoteList_tbl())
		 else if(note.Contact!="")
		 records = Common.GetTableRecordsPerPaging_func(this.Client_NoteList_tbl())
		 */
		for(String index_str: records)
		{
			if(index_str.contains(note.Title))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Note["+note.Title+"] NOT exist in table.\n"
		}//end if
	}


	public static void VerifyNoteNotExistTable_func(NoteInfo note)
	{
		//this.SearchNote_func(note.Title)

		Boolean exist = false
		//this.SearchNote_func(note.Title)
		List<String> records

		records = Common.GetTableRecordsPerPaging_func(this.NoteList_tbl())
		/*
		 if(note.TransfFullName!="")
		 records = Common.GetTableRecordsPerPaging_func(this.Transf_NoteList_tbl())
		 else if(note.Contact!="")
		 records = Common.GetTableRecordsPerPaging_func(this.Client_NoteList_tbl())
		 */

		if(records.size()>0)
		{
			for(String index_str: records)
			{
				if(index_str.contains(note.Title))
				{
					exist = true
					break
				}//end if
			}//end foreach
		}


		if(exist==true)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Note["+note.Title+"] SHOULD NOT exist in table.\n"
		}//end if
	}


	public static void FillInfoModalTransf_func(NoteInfo note,String button="Submit")
	{
		//=================INPUT NOTE INFO

		Common.WaitForElementDisplay(this.ServiceType_slect())

		Common.HandleElementClick(this.ServiceType_slect())
		Common.SelectDropdownItem(this.ServiceType_slect(), note.Service)

		//this.ServiceType_slect().selectByVisibleText(this.Service)

		Common.sendkeyTextToElement(this.Title_txt(), note.Title)

		//=======INPUT FOR CONTENT

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action_key = actions.sendKeys(this.Title_txt(), Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(note.Content).build();

		action_key.perform()

		WebUI.delay(2)

		if(button=="Submit")
			this.Submit_btn().click()

		WebUI.delay(1)

		if( Messages.notification_msg()!=null)
		{
			String msg = Messages.notification_msg().getText()

			String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
			println(msg)
			if(msg ==expected_msg)
			{
				GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Note Creation.Msg: '"+msg+"'.\n"
				Assert.fail(GlobalVariable.glb_TCFailedMessage)
			}
		}


	}



	public static void FillInfoModalClient_func(NoteInfo note,String button="Submit")
	{
		//=================INPUT NOTE INFO

		Common.WaitForElementDisplay(this.Title_txt())

		Common.SelectDropdownItem(this.Contact_slect(), note.Contact)


		//this.ServiceType_slect().selectByVisibleText(this.Service)

		Common.sendkeyTextToElement(this.Title_txt(), note.Title)

		//=======INPUT FOR CONTENT

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action_key = actions.sendKeys(this.Title_txt(), Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(note.Content).build();

		action_key.perform()

		WebUI.delay(2)

		if(button=="Submit")
			this.Submit_btn().click()

		WebUI.delay(1)

		if( Messages.notification_msg()!=null)
		{
			String msg = Messages.notification_msg().getText()

			String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
			println(msg)
			if(msg ==expected_msg)
			{
				GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Note Creation.Msg: '"+msg+"'.\n"
				Assert.fail(GlobalVariable.glb_TCFailedMessage)
			}
		}


	}


	public static void FillInfoModalVendor_func(NoteInfo note)
	{
		//=================INPUT NOTE INFO

		//Common.WaitForElementDisplay(VendorDetail.newContactNote_drp())
		WebUI.delay(5)

		Common.selectRandomItemDropdown(VendorDetail.newContactNote_drp(), VendorDetail.newContactNote_listDrp())

		//this.ServiceType_slect().selectByVisibleText(this.Service)
		//WebUI.delay(3)
		Common.sendkeyTextToElement(this.Title_txt(), note.Title)

		//=======INPUT FOR CONTENT

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		Action action_key = actions.sendKeys(this.Title_txt(), Keys.TAB).keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(note.Content).build();

		action_key.perform()

		WebUI.delay(2)

		this.Submit_btn().click()

		WebUI.delay(1)

		if( Messages.notification_msg()!=null)
		{
			String msg = Messages.notification_msg().getText()

			String expected_msg = "Server error Request. Please try again or contact IT helpdesk."
			println(msg)
			if(msg ==expected_msg)
			{
				GlobalVariable.glb_TCFailedMessage+= "Unsuccessfull Note Creation.Msg: '"+msg+"'.\n"
				Assert.fail(GlobalVariable.glb_TCFailedMessage)
			}
		}
	}


	//METHOD: VERIFY INFO ON DETAILS MODAL
	public static void VerifyModalDetailsInfo_func(NoteInfo note)
	{
		this.Note_tab().click()
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		this.SearchNote_func(note.Title)


		this.noteTitle_lnk(note.Title).click()

		String field_str = ""
		String area  = "Note"

		if(note.TransfFullName!="")
		{
			field_str = "Service_ddl"
			Common.VerifyFieldTextEqual_func(area+field_str, this.ServiceType_slect(), note.Service)

		}//end if
		else if(note.Contact!="")
		{
			field_str = "Contact_ddl"
			Common.VerifyFieldTextEqual_func(area+field_str, this.Contact_slect(), note.Contact)
		}//end else if

		field_str = "Title_txt"
		Common.VerifyFieldValueEqual_func(area+field_str, this.Title_txt(), note.Title)

		Common.VerifyTinyContentEqual_func(area, this.Content_txt(), note.Content)

	}//end void

	public static void VerifyInfoViewModeNoteVendor_func(NoteInfo note)
	{
		this.Note_tab().click()
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		this.SearchNote_func(note.Title)


		this.noteTitle_lnk(note.Title).click()

		String field_str = ""
		String area  = "Note"

		if(note.Contact!="")
		{
			field_str = "Contact_ddl"
			Common.VerifyFieldTextEqual_func(area+field_str, this.Contact_slect(), note.Contact)
		}//end else if

		field_str = "Title_txt"
		Common.VerifyFieldValueEqual_func(area+field_str, this.Title_txt(), note.Title)

		Common.VerifyTinyContentEqual_func(area, this.Content_txt(), note.Content)

	}//end void

	public static void VerifyModalDetailsInfo_func(Mailinfo mail_info)
	{
		String field_str = ""
		String area  = "Note"

		this.Note_tab().click()
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

		this.SearchNote_func(mail_info.Subject)


		this.noteTitle_lnk(mail_info.Subject).click()

		Common.WaitForElementEnable(this.Edit_btn())

		if(mail_info.RecipientType=="Transferee")
		{
			field_str = "Service_ddl"
			Common.VerifyFieldTextEqual_func(area+field_str, this.ServiceType_slect(), mail_info.ServiceType)
		}//end if

		else if(mail_info.RecipientType=="Client")
		{
			field_str = "Contact_ddl"
			Common.VerifyFieldTextEqual_func(area+field_str, this.Contact_slect(), mail_info.EmailAddr)
		}//end else if

		//	field_str = "To_txt"
		//	Common.VerifyFieldValueEqual_func(area+field_str, this.Title_txt(), mail_info.Subject)

		field_str = "Title_txt"
		Common.VerifyFieldValueEqual_func(area+field_str, this.Title_txt(), mail_info.Subject)


		Common.VerifyTinyContentEqual_func(area, this.Content_txt(), mail_info.Content)

	}//end void

}
