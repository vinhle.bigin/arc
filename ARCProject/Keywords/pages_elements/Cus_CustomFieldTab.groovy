package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import functions.Navigation
import objects.CustomField
import objects.CustomGroup
import objects.Transferee
import internal.GlobalVariable
import junit.framework.Assert

public class Cus_CustomFieldTab {

	private static String ManageCustField_tab_xpath= "//label[@href ='#manage-custom-fields']"
	private static String Search_FieldName_txt_xpath= "//input[@placeholder = 'Field Name']"
	private static String Search_btn_xpath= "//button[@data-control ='search']"
	private static String Clear_btn_xpath= "//button[@data-control ='cancel-search']"
	private static String New_btn_xpath= "//button[text() = 'New Custom Field']"
	private static String CustFieldList_tbl_xpath= "//table[@id = 'customization-table']/tbody"
	private static String Dtail_FieldType_ddl_xpath= "//select[@placeholder = 'Field Type']"
	private static String Dtail_FieldName_txt_xpath= "//input[@placeholder = 'Field Name']"
	private static String ControlType_ddl_xpath= "//select[@placeholder = 'Control Type']"
	private static String Dtail_Group_ddl_xpath= "//select[@placeholder = 'Group']"
	private static String Dtail_IsActive_opt_xpath= "//div[@class='col-md-2 checkbox-inline']//label[text() = 'Active']/../input"
	private static String Dtail_IsRequire_opt_xpath= "//label[text() = 'Required']/../input"
	private static String Dtail_Submit_btn_xpath= "//button[@id ='submit-template']"
	private static String Dtail_Cancel_btn_xpath= "//button[@cancel ='cancel-template']"
	private static String Dtail_Option_txt_xpath= "//input[@placeholder ='Option']"
	private static String Dtail_OptionAdd_btn_xpath= "//button[@id = 'submit-new-option']"
	private static String Dtail_OptionList_tbl_xpath= "//div[@class = 'container-fluid table']//div[contains(@class,'row option-item move')]"
	private static String FieldName_lnk_xpath = "//a"
	private static String Remove_icon_xpath = "//span[@data-original-title='Remove']"


	public Cus_CustomFieldTab() {
	}

	public static WebElement ManageCustField_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ManageCustField_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_FieldName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_FieldName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Clear_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Clear_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement New_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(New_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CustFieldList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CustFieldList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_FieldType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_FieldType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_FieldName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_FieldName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ControlType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ControlType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Group_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Group_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_IsActive_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_IsActive_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_IsRequire_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_IsRequire_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Option_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Option_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_OptionAdd_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_OptionAdd_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> Dtail_OptionList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(Dtail_OptionList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement FieldName_lnk(String fieldname_str) {
		try{

			String xpath_str = FieldName_lnk_xpath+"[text()='"+fieldname_str+"']"

			WebDriver webDriver = DriverFactory.getWebDriver()

			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Remove_icon(String fieldname_str) {
		try{

			WebElement temp_element = Common.GetElementInTable(fieldname_str, this.CustFieldList_tbl(), Remove_icon_xpath)
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	/////////================================METHOD

	//VERIFY INFO IN DETAILS PAGE
	public static void VerifyInfoDetailPage_func(CustomField cus_field)
	{
		String area = ""
		String field_name = ""

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.HandleElementClick(this.FieldName_lnk(cus_field.FieldName))

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.Dtail_FieldName_txt())

		///ORG RESIDENCE
		area = "Details Custom Fields - "

		field_name = "Dtail_FieldType_ddl"

		Common.VerifyFieldTextEqual_func(area+field_name, this.Dtail_FieldType_ddl(), cus_field.FieldType)

		field_name = "ControlType_ddl"
		Common.VerifyFieldTextEqual_func(area+field_name, this.ControlType_ddl(), cus_field.ControlType)

		field_name = "Dtail_Group_ddl"
		Common.VerifyFieldTextEqual_func(area+field_name, this.Dtail_Group_ddl(), cus_field.custom_group)

		field_name = "Dtail_FieldName_txt"
		Common.VerifyFieldValueEqual_func(area+field_name, this.Dtail_FieldName_txt(), cus_field.FieldName)


		if(cus_field.ControlType=="Dropdown" || cus_field.ControlType=="Multiselect" || cus_field.ControlType=="Radio Button")
		{
			int size_int = cus_field.OptionList.size()
			String tmp_str = ""
			for(int i=0;i<size_int;i++)
			{
				tmp_str = cus_field.OptionList.get(i)
				this.VerifyOptionExistDetails(tmp_str)
			}//for

		}//end if

	}//end void



	//VERIFY OPTION IS DISPLAYED IN THE OPTION LIST IN DETAILS PAGE

	public static void VerifyOptionExistDetails(String opt_name)
	{
		Boolean exist = false
		String observed = ""
		int e_size = this.Dtail_OptionList_tbl().size()
		for(int j =0;j<e_size;j++)
		{
			observed +=this.Dtail_OptionList_tbl().get(j).getText()+"\n"
			if(this.Dtail_OptionList_tbl().get(j).getText().contains(opt_name))
			{
				exist = true
			}
		}

		if(exist==false)
		{
			GlobalVariable.glb_TCStatus=false
			GlobalVariable.glb_TCFailedMessage+="Option does not exist.[Observed: "+observed+"- Expected: "+opt_name+"].\n"
		}


	}


	//FILL INFO TO DETAIL PAGE
	public static void FillModalInfo_func(CustomField cus_field,String Action = "Create")
	{

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.Dtail_FieldName_txt())

		if(Action=="Create")
		{
			Common.SelectDropdownItem(this.Dtail_FieldType_ddl(), cus_field.FieldType)

			Common.SelectDropdownItem(this.ControlType_ddl(), cus_field.ControlType)
		}

		
		String group_str = cus_field.custom_group.Tab+" - " + cus_field.custom_group.Name
		Common.SelectDropdownItem(this.Dtail_Group_ddl(), group_str)

		Common.sendkeyTextToElement(this.Dtail_FieldName_txt(),cus_field.FieldName)
		String vlue_str = Common.GetFieldText_func(this.Dtail_IsActive_opt())
		if(vlue_str!=cus_field.IsActive.toString())
			this.Dtail_IsActive_opt().click()


		if(cus_field.ControlType=="Dropdown" || cus_field.ControlType=="Multiselect" || cus_field.ControlType=="Radio Button")
		{
			int size_int = cus_field.OptionList.size()
			for(int i=0;i<size_int;i++)
			{
				Common.sendkeyTextToElement(this.Dtail_Option_txt(),cus_field.OptionList.get(i))
				this.Dtail_OptionAdd_btn().click()
				WebUI.delay(1)

			}
		}
	}



	//SEARCH THE CUSTOM FIELD

	public static SearchCustomField_func(String fieldname_str)
	{
		Navigation.GotoCustomFieldPage()

		Common.HandleElementClick(this.ManageCustField_tab())

		Common.sendkeyTextToElement(this.Search_FieldName_txt(), fieldname_str)

		Common.HandleElementClick(this.Search_btn())

		Boolean exist = Common.IsItemExistTable(this.CustFieldList_tbl(), fieldname_str)

		if(exist==false)
		{
			GlobalVariable.glb_TCFailedMessage+="CustomField["+fieldname_str+"] NOT found."
			Assert.fail(GlobalVariable.glb_TCFailedMessage)
		}//end if exist


	}//end void

}
