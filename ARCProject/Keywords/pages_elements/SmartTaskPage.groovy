package pages_elements

import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions

import org.openqa.selenium.interactions.Action as SelAction

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.DateTimeFuncs
import functions.Navigation
import internal.GlobalVariable
import objects.Activity
import objects.SmartTask
import objects.Action

public class SmartTaskPage {

	private static String Search_Name_txt_xpath= "//input[@placeholder ='Smart Task Name']"
	private static String Search_btn_xpath= "//button[@id='search-smart-task-btn']"
	private static String New_btn_xpath= "//button[text()='New Smart Task']"
	private static String SmartTaskList_tbl_xpath= "//table[@id='smart-task-table']/tbody"
	private static String Dtail_Name_txt_xpath= "//input[@placeholder='Name']"
	private static String Dtail_ServiceType_ddl_xpath= "//select[@placeholder='Service Type']"
	private static String Dtail_FieldChange_opt_xpath= "//label[text()='Field Changed']/..//input"
	private static String Dtail_MailProcess_opt_xpath= "//label[text()='Email Processed']/..//input"
	private static String Dtail_ActivityComplete_opt_xpath= "//label[text()='Activity Completed']/..//input"
	private static String Dtail_FileCreate_opt_xpath= "//label[text()='File Created']/..//input"
	private static String Dtail_EmailTemplate_ddl_xpath= "//form//select[contains(@placeholder,'Email Template')]"
	private static String Dtail_ActivityName_txt_xpath= "//form//input[contains(@placeholder,'Activity Name')]"
	private static String Dtail_CompareValue_txt_xpath= "//form//input[contains(@placeholder,'Value')]"

	private static String Dtail_TriggerType_ddl_xpath= "//label[text()='Trigger Type']/..//select[@placeholder ='Trigger Type']"
	private static String Dtail_Fieldname_ddl_xpath= "//form//select[contains(@placeholder,'Field Name')]"
	private static String Dtail_Operator_ddl_xpath= "//div[@class='form-group col-md-3 operator']//select[@placeholder ='Operator']"
	private static String Dtail_ConditionList_tbl_xpath= ""
	private static String Dtail_ActionType_ddl_xpath= "//select[@placeholder='Action Type']"
	private static String Dtail_ConditionAdd_btn_xpath= "//div[@class='col-md-2']/button"
	private static String Dtail_ActionAdd_btn_xpath= "//div[@class='add-action d-flex']/button"
	private static String Dtail_Submit_btn_xpath= "//div[@class='col-md-12']//button[contains(@type,'submit')]"
	private static String Dtail_Cancel_btn_xpath= "//div[@class='col-md-12']/button[text() = 'Cancel']"
	private static String Dtail_ActEmailTemplate_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Template']"
	private static String Dtail_ActFieldList_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Update Value Of']"
	private static String Dtail_ActFieldValueTo_txt_xpath= "//div[@class='task-content']//input[@placeholder ='To']"
	private static String Dtail_ActActivityType_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Activity Type']"
	private static String Dtail_ActAssignTo_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Assigned To']"
	private static String Dtail_ActPriority_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Priority']"
	private static String Dtail_ActDays_txt_xpath= "//div[@class='task-content']//input[@placeholder ='Days']"
	private static String Dtail_ActDate_ddl_xpath= "//div[@class='task-content']//select[@placeholder ='Date']"
	private static String Dtail_ActActivityTitle_txt_xpath= "//div[@class='task-content']//input[@placeholder ='Title']"
	private static String Dtail_TinyEditor_txt_xpath= "//p/.."
	private static String Dtail_ActUpdate_btn_xpath= "//div[@class='task-btn-group']//button[@type='button'][contains(text(),'Update')]"
	private static String Dtail_ActCancel_btn_xpath= "//div[@class='task-content']//button[text() ='Cancel']"

	private static String TaskName_lnk_xpath = "//a[text()='name_argument']"

	private static String Dtail_ActionDescr_value_xpath = "//div[@class='task-content']"

	private static String Dtail_ActionEdit_btn_xpath = "//div[@class='task-btn-group']//button[text()='Edit Action']"



	public SmartTaskPage() {
	}

	public static WebElement Dtail_ActionDescr_value() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActionDescr_value_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActionEdit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActionEdit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement TaskName_lnk(String name_str) {
		try{
			String xpath_str = TaskName_lnk_xpath.replace("name_argument", name_str)
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(xpath_str));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_CompareValue_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_CompareValue_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	public static WebElement Dtail_EmailTemplate_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_EmailTemplate_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActivityName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActivityName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_Name_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_Name_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement New_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(New_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SmartTaskList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SmartTaskList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Name_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Name_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ServiceType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ServiceType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_FieldChange_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_FieldChange_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_MailProcess_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_MailProcess_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActivityComplete_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActivityComplete_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_FileCreate_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_FileCreate_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_TriggerType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_TriggerType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Fieldname_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Fieldname_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Operator_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Operator_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ConditionList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ConditionList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActionType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActionType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ConditionAdd_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ConditionAdd_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActionAdd_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActionAdd_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActEmailTemplate_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActEmailTemplate_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActFieldList_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActFieldList_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActFieldValueTo_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActFieldValueTo_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActActivityType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActActivityType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActAssignTo_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActAssignTo_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActPriority_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActPriority_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActDays_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActDays_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActDate_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActDate_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActActivityTitle_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActActivityTitle_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_TinyEditor_txt() {
		try{
			WebDriver webDriver = Navigation.SwitchIframe_func(Navigation.TinyIframe_txt())
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_TinyEditor_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Dtail_ActCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Dtail_ActCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	///=====METHOD: Search Button

	public static void SearchSmartTask_func(String search_str)
	{
		Common.sendkeyTextToElement(this.Search_Name_txt(), search_str)

		this.Search_btn().click()

		WebUI.delay(2)

		//String get_str = Common.GetFieldText_func(this.SmartTaskList_tbl())

	}



	//==METHOD: CREATE NEW
	public static void FillDetailsInfo_func(SmartTask task,String button)
	{
		Common.sendkeyTextToElement(this.Dtail_Name_txt(), task.Name)

		Common.SelectDropdownItem(this.Dtail_ServiceType_ddl(), task.Service)


		if(task.IsFieldchange_opt ==true)
		{
			this.Dtail_FieldChange_opt().click()

			Common.SelectDropdownItem(this.Dtail_TriggerType_ddl(), task.TriggerType)

			Common.SelectDropdownItem(this.Dtail_Fieldname_ddl(), task.Condt_FieldName)

			Common.SelectDropdownItem(this.Dtail_Operator_ddl(), task.Condt_Operator)

			if(task.Condt_Operator !="Has any change")
			{
				Common.sendkeyTextToElement(this.Dtail_CompareValue_txt(), task.Condt_Value)
			}

			this.Dtail_ConditionAdd_btn().click()

		}//end if


		//
		else if(task.IsEmailProcess_opt==true)
		{
			this.Dtail_MailProcess_opt().click()
			Common.SelectDropdownItem(this.Dtail_EmailTemplate_ddl(), task.Condt_EMailTemplName)

		}

		else if(task.IsActivityComplete_opt==true)
		{
			this.Dtail_ActivityComplete_opt().click()

			Common.sendkeyTextToElement(this.Dtail_ActivityName_txt(), task.Condt_ActivityName)
		}
		else if(task.IsFileCreate_opt==true)
			this.Dtail_FileCreate_opt().click()



		//============ACTIONS
		if(task.action.Type!="")
			this.AddAction_func(task.action)


		if(button=="Submit")
		{
			this.Dtail_Submit_btn().click()

			Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When adding new SmartTask")
		}
		else if(button=="Cancel")
			this.Dtail_Cancel_btn().click()




	}

	public static void AddAction_func(Action act,String button = "Submit")
	{
		Common.SelectDropdownItem(this.Dtail_ActionType_ddl(), act.Type)

		this.Dtail_ActionAdd_btn().click()

		switch(act.Type)
		{

			case "Sending Email Action":
				this.FillSendEmailAction_func(act)
				break;

			case "Updating Field Action":
				this.FillFieldUpdateAction_func(act)
				break;

			case "Creating Activity Action":
				this.FillCreateActivityAction_func(act)
				break;

		}//end switch

		if(button=="Submit")
		{
			this.Dtail_ActUpdate_btn().click()

			Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "When adding new Action")
		}
		else if(button=="Cancel")
			this.Dtail_ActCancel_btn().click()
	}


	//ADD ACTION TYPE SEND EMAIL TEMPLATE
	public static void FillSendEmailAction_func(Action act)
	{


		Common.SelectDropdownItem(this.Dtail_ActEmailTemplate_ddl(), act.Act_TemplateName)




	}//END VOID

	//ADD ACTION TYPE SEND EMAIL TEMPLATE
	public static void FillCreateActivityAction_func(Action act)
	{
		Common.SelectDropdownItem(this.Dtail_ActActivityType_ddl(), act.Act_Activity.Type)

		Common.SelectDropdownItem(this.Dtail_ActAssignTo_ddl(), act.Act_Activity.ScheduleWith)

		Common.sendkeyTextToElement(this.Dtail_ActDays_txt(), act.Act_Activity.Days)


		Common.SelectDropdownItem(this.Dtail_ActDate_ddl(), act.Act_Activity.Date)


		Common.sendkeyTextToElement(this.Dtail_ActActivityTitle_txt(), act.Act_Activity.Title)

		WebDriver webDriver = DriverFactory.getWebDriver()

		Actions actions = new Actions(webDriver)

		SelAction action = actions.sendKeys(this.Dtail_ActActivityTitle_txt(), Keys.TAB).sendKeys(act.Act_Activity.Content).build()

		action.perform()

		WebUI.delay(2)


	}//END VOID


	public static void FillFieldUpdateAction_func(Action act)
	{
		Common.SelectDropdownItem(this.Dtail_ActFieldList_ddl(), act.Act_Field)

		Common.sendkeyTextToElement(this.Dtail_ActFieldValueTo_txt(), act.Act_ValueTo)
	}


	public static void VerifyDetailsInfo(SmartTask smarttask)
	{


		String fieldname = ""
		String Area = "SmartTask Dtails-"

		Navigation.GotoSmartTaskPage()

		this.SearchSmartTask_func(smarttask.Name)

		this.TaskName_lnk(smarttask.Name).click()

		WebUI.delay(3)

		fieldname = "Dtail_Name_txt"
		Common.VerifyFieldValueEqual_func(Area+fieldname, this.Dtail_Name_txt(), smarttask.Name)

		fieldname = "Dtail_ServiceType_ddl"
		Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ServiceType_ddl(), smarttask.Service)

		fieldname = "Dtail_FieldChange_opt"
		Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_FieldChange_opt(), smarttask.IsFieldchange_opt.toString())

		fieldname = "Dtail_ActivityComplete_opt"
		Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActivityComplete_opt(), smarttask.IsActivityComplete_opt.toString())

		fieldname = "Dtail_FileCreate_opt"
		Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_FileCreate_opt(), smarttask.IsFileCreate_opt.toString())

		fieldname = "Dtail_MailProcess_opt"
		Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_MailProcess_opt(), smarttask.IsEmailProcess_opt.toString())

		//fieldname = "Dtail_ActionType_ddl"
		//Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActionType_ddl(), smarttask.action.Type)

		//fieldname = "Dtail_ActionDescr_value"
		//Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActionDescr_value(), smarttask.action.)

		Common.HandleElementClick(this.Dtail_ActionEdit_btn())

		WebUI.delay(1)

		switch(smarttask.action.Type)
		{
			case "Sending Email Action":
				fieldname = "Dtail_ActEmailTemplate_ddl"
				Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActEmailTemplate_ddl(), smarttask.action.Act_TemplateName)
				break

			case "Updating Field Action":
				fieldname = "Dtail_ActFieldList_ddl"
				Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActFieldList_ddl(), smarttask.action.Act_Field)

				fieldname = "Dtail_ActFieldValueTo_txt"
				Common.VerifyFieldValueEqual_func(Area+fieldname, this.Dtail_ActFieldValueTo_txt(), smarttask.action.Act_ValueTo)
				break


			case "Creating Activity Action":

				fieldname = "Dtail_ActActivityType_ddl"
				Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActActivityType_ddl(), smarttask.action.Act_Activity.Type)

				fieldname = "Dtail_ActAssignTo_ddl"
				Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActAssignTo_ddl(), smarttask.action.Act_Activity.ScheduleWith)

				fieldname = "Dtail_ActDays_txt"
				Common.VerifyFieldValueEqual_func(Area+fieldname, this.Dtail_ActDays_txt(), smarttask.action.Act_Activity.Days)

				fieldname = "Dtail_ActDate_ddl"
				Common.VerifyFieldTextEqual_func(Area+fieldname, this.Dtail_ActDate_ddl(), smarttask.action.Act_Activity.Date)

				fieldname = "Dtail_ActActivityTitle_txt"
				Common.VerifyFieldValueEqual_func(Area+fieldname, this.Dtail_ActActivityTitle_txt(), smarttask.action.Act_Activity.Title)

				fieldname = "Activity Content"
				String Content_str = this.Dtail_TinyEditor_txt().getText()

				WebDriver webDriver = DriverFactory.getWebDriver()


				webDriver = Navigation.SwitchIframe_func()
				fieldname = "Activity Content"
				if(Content_str!=smarttask.action.Act_Activity.Content)
				{
					GlobalVariable.glb_TCStatus = false
					GlobalVariable.glb_TCFailedMessage += Area+fieldname+"Incorrect:[Observed: "+Content_str+" - Expected: "+smarttask.action.Act_Activity.Content+"].\n"
				}

				break
		}




	}



}
