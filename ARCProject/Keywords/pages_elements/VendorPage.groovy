package pages_elements
import org.openqa.selenium.By
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common


public class VendorPage {
	/**
	 * Refresh browser
	 */

	// Vendor page
	private static WebDriver webDriver = DriverFactory.getWebDriver()
	private static String NewVendorTop_btn_xpath = "//span[@class='menu-icon arc-user-tie-plus']";
	private static String NoteTop_btn_xpath = "//span[@class='menu-icon fas fa-sticky-note']";
	private static String PrintTop_btn_xpath = "//span[@class='menu-icon fas fa-print']";
	private static String Search_btn_xpath = "//button[contains(text(),'Search')]";
	private static String Clear_btn_xpath = "//button[contains(text(),'Clear')]";
	private static String OnlyMainContact_ck_xpath = "//input[contains(@name,'is_main_contact')]";
	private static String SearchName_txt_xpath = "//input[@placeholder='Vendor name']"
	private static String SearchContact_txt_xpath = "//input[@placeholder='Contact Name']"
	private static String SearchAddress1_txt_xpath = "//input[@placeholder='Address Line 1']"
	private static String SearchAddress2_txt_xpath = "//input[@placeholder='Address Line 2']"
	private static String SearchCity_txt_xpath = "//input[@placeholder='City']"
	private static String SearchZipcode_num_xpath = "//input[@placeholder='Zip Code']"
	private static String Status_drd_xpath = "//select[@id='search-vendor-status']"
	private static String Status_listDrd_xpath = "//select[@id='search-vendor-status']//option"
	private static String Status_active_xpath = "//span[@data-original-title='Active']"
	private static String Status_inactive_xpath = "//span[@data-original-title='Inactive']"
	private static String ServiceType_drd_xpath = "//select[@placeholder='Service Type']"
	private static String ServiceType_listDrd_xpath = "//select[@placeholder='Service Type']//option"
	private static String Country_drd_xpath = "//select[@name='country']"
	private static String Country_listDrd_xpath = "//select[@name='country']//option"
	private static String State_drd_xpath = "//select[@placeholder='State']"
	private static String State_listDrd_xpath = "//select[@placeholder='State']//option"
	private static String OnlyMainVendor_icon_xpath = "//i[@class='glyphicon glyphicon-ok']"
	private static String Noresults_xpath ="//td[contains(@class,'text-center')]"
	private static String RowsTable_xpath = "//div[@class='table-responsive']//tbody//tr[@class='VueTables__no-results']"
	private static String VendorList_tbl_xpath = "//div[@class='table-responsive']//tbody"
	private static String FirstVendorName_link_xpath = "//tr[1]//td[1]//span"


	// New Vendor Modal
	private static String NewVendor_ic_xpath = "//span[@class='menu-icon arc-user-tie-plus']"
	private static String NewVendor_btn_xpath = "//button[contains(text(),'New Vendor')]"
	private static String TitleNewVendor_modal_xpath = "//h4[contains(text(),'New Vendor')]"
	private static String SubmitNewVendor_btn_xpath = "//button[contains(text(),'Submit')]"
	private static String CancelNewVendor_btn_xpath = "//div[@id='loading-modal-vendor-create']//button[@type='button'][contains(text(),'Cancel')]"
	private static String NewVendor_modal_xpath = "//div[@id='loading-modal-vendor-create']"
	private static String NewVendorName_txt_xpath = "//input[@placeholder='Enter vendor name']"
	private static String NewVendorCode_txt_xpath = "//input[contains(@placeholder,'Enter vendor code')]"
	private static String NewVendorWebsite_txt_xpath = "//input[@placeholder='Enter website']"
	private static String NewVendorService_drd_xpath = "//select[@id='new-vendor-serivce']"
	private static String NewVendorService_listDrd_xpath = "//select[@id='new-vendor-serivce']//option"
	private static String NewVendorActive_sw_xpath = "//input[@id='new-vendor-is-active']"
	private static String NewVendorNameMsg_xpath = "//li[contains(text(),'The vendor name field is required.')]"
	private static String NewVendorCodeMsg_xpath = "//li[contains(text(),'The vendor code field is required.')]"
	private static String NewVendorServiceMsg_xpath = "//li[contains(text(),'The service type field is required.')]"
	private static String NoResultMsg_lb_xpath = "//td[@class='text-center']"
	private static String ExistVendor_lb_xpath = "//h4[@id='myModalLabel']"
	private static String CreateNewVendor_btn_xpath = "//button[contains(text(),'Create New Vendor')"



	// Vendor detail
	private static String CompanyNameTitle_lb_xpath = "//a[@id='company_name']"

	//Dash board
	private static String AvatarUser_lb_xpath = "//img[@id='nav-avatar']"
	private static String Logout_menu_xpath= "//a[contains(text(),'Sign out')]"


	public VendorPage() {
	}


	public static WebElement SearcName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchContact_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchContact_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement OnlyMainContact() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OnlyMainContact_ck_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Clear_btn() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Clear_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchZipcode_num(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchZipcode_num_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement SearchName_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchName_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement NoResultMsg_lb(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NoResultMsg_lb_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement Status_drd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Status_drd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> Status_listDrd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(Status_listDrd_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement ServiceType_drd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ServiceType_drd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> ServiceType_listDrd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(ServiceType_listDrd_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement Country_drd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Country_drd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> Country_listDrd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(Country_listDrd_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement State_drd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(State_drd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> State_listDrd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(State_listDrd_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> Status_active(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(Status_active_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement Status_inactive(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Status_inactive_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement SearchCity_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchCity_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement SearchAddress1_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchAddress1_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement OnlyMainVendor_icon(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OnlyMainVendor_icon_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement Noresults() {
		try{
			WebElement temp_element = webDriver.findElement(By.xpath(Noresults_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static List<WebElement> RowsTable(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(RowsTable_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement VendorList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(VendorList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewVendor_ic() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendor_ic_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewVendor_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendor_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TitleNewVendor_modal() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TitleNewVendor_modal_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewVendor_modal() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendor_modal_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CancelNewVendor_btn() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CancelNewVendor_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SubmitNewVendor_btn() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SubmitNewVendor_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SelectServiceNewVendor(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement selectStatus = webDriver.findElement(By.xpath(NewVendorService_drd_xpath))
			return selectStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> NewVendorService_listDrd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(NewVendorService_listDrd_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement NewVendorName(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement VendorName = webDriver.findElement(By.xpath(NewVendorName_txt_xpath))
			return VendorName;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement NewVendorCode(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendorCode_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement NewVendorWebsite_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendorWebsite_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement ExistVendor_lb(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ExistVendor_lb_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement CreateNewVendor_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CreateNewVendor_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement CompanyNameTitle(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CompanyNameTitle_lb_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement AvatarUser(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AvatarUser_lb_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement UserLogout(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Logout_menu_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement NewVendorNameMsg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendorNameMsg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement NewVendorCodeMsg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendorCodeMsg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement NewVendorServiceMsg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewVendorServiceMsg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}


	public static WebElement vendorName_link(String vendorName) {
		try{
			String vendorName_lnk_xapth = "//a/span[text() = '"+vendorName+"']"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(vendorName_lnk_xapth));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement firstVendorName_link() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FirstVendorName_link_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement zipCode(String zipCode) {
		try{
			String zipCode_xpath = "//div[@class='table-responsive']//tbody//tr//span[contains(text(),'"+ zipCode +"')]"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(zipCode_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement city(String city) {
		try{
			String city_xpath = "//div[@class='table-responsive']//tbody//tr//span[contains(text(),'"+ city +"')]"
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(city_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



}