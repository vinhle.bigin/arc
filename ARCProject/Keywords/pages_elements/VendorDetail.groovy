package pages_elements
import java.util.List

import org.openqa.selenium.By
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.webui.driver.DriverFactory

import functions.Common

public class VendorDetail {
	/**
	 * Refresh browser
	 */

	// Vendor detail
	private static WebDriver webDriver = DriverFactory.getWebDriver()
	private static String companyNameTitle_lb_xpath = "//a[@id='company_name']"
	private static String toastMsg_xpath = "//div[@class='toast-message']"


	// General Info _ Vendor Information
	private static String vendorEdit_btn_xpath = "//a[@id='icon-general-infomation-panel']"
	private static String vendorName_txt_xpath = "//input[@placeholder='Enter vendor name']"
	private static String vendorCode_num_xpath = "//div[@org-placeholder='Enter vendor code']//input"
	private static String vendorService_drd_xpath = "//select[@id='vendor-services']"
	private static String vendorOwnership_drd_xpath = "//select[@id='vendor-ownership']"
	private static String vendorService_listDrd_xpath = "//select[@id='vendor-services']//option"
	private static String vendorOwnership_listDrd_xpath = "//select[@id='vendor-ownership']//option"
	private static String vendorWebSite_txt_xpath = "//div[@org-placeholder='Enter website']//input"
	private static String updateVendorInfo_btn_xpath = "//div[@class='panel panel-border general-infomation-panel']//button[@data-control='update']"
	private static String cancelVendorInfo_btn_xpath = "//div[@class='panel panel-border general-infomation-panel']//button[@data-control='cancel']"

	// General Info _ Status
	private static String updateVendorStatus_btn_xpath = "//div[@id='status-panel']//button[@data-control='update']"
	private static String editVendorStatus_btn_xpath = "//a[@id='icon-status-panel']"
	private static String active_toggle_xpath = "//input[@id='vendor_is_active']"
	private static String available_toggle_xpath = "//input[@id='vendor_is_available']"
	private static String international_toggle_xpath = "//input[@id='vendor_is_international']"

	// General Info _ Rating
	private static String updateVendorRating_btn_xpath = "//div[@id='rating-panel']//button[@data-control='update']"
	private static String editVendorRating_btn_xpath = "//a[@id='icon-rating-panel']"
	private static String whyVendorRating_txt_xpath = "//textarea[@id='vendor-why']"
	private static String whyVendorRating_view_xpath = "//pre[@placeholder='Why']"
	private static String fiveVendorRating_icon_xpath = "//div[@class='panel panel-white tab-top']//div[@class='b__components b-ios b-rating']//li[4]"
	private static String fiveVendorRating_att_xpath = "//div[@class='panel panel-white tab-top']//div[@class='b__components b-ios b-rating']//li[4][@class='active']"

	// Search Option
	private static String searchOption_tap_xpath = "//a[@href='#vendor-search-option']"
	private static String editSearchOption_btn_xpath = "//a[@id='icon-search-infomation-panel']"
	private static String preferredVendor_chk_xpath = "//div[@class='form-group']//input[@type='checkbox']"
	private static String useZipCodeVendor_chk_xpath = "//label[contains(text(),'Use Zip Code')]"
	private static String inputUseZipCodeVendor_chk_xpath = "//input[@id='vendor-detail-zip-code']"
	private static String excludedClients_multi_xpath = "//ul[@class='b__multi__select__list']"
	private static String updateSearchOptionVendor_btn_xpath = "//div[@id='search-infomation-panel']//button[@data-control='update']"

	//Contact
	private static String contacts_tap_xpath = "//a[@href='#tab-vendor-contacts']"
	private static String newContacts_btn_xpath = "//button[contains(text(),'New Contact')]"
	private static String submitNewContacts_btn_xpath = "//button[contains(text(),'Submit')]"
	private static String newFirstNameContact_msg_xpath = "//ul[@data-field='first_name']"
	private static String newLastNameContact_msg_xpath = "//ul[@data-field='last_name']"
	private static String newStateContact_msg_xpath = "//ul[@data-field='state']"
	private static String newFirstNameContact_txt_xpath = "//input[@id='contact_firstname']"
	private static String newLastNameContact_txt_xpath = "//input[@id='contact_lastname']"
	private static String newStateContact_drp_xpath = "//select[@placeholder='State']"
	private static String newAddress1Contact_txt_xpath = "//input[@id='contact_address1']"
	private static String newCityContact_txt_xpath = "//input[@id='contact_city']"
	private static String newZipCodeContact_txt_xpath = "//input[@id='contact_zip']"
	private static String toastMsgNewContact_xpath = "//div[@class='toast-message']"
	private static String editContact_btn_xpath = "//div[@class='modal-header']//a[@data-original-title='Edit']"
	private static String updateContact_btn_xpath = "//div[@class='modal-footer']//button[@data-control='update']"
	private static String firstNameContact_txt_xpath = "//input[@placeholder='First name']"
	private static String lastNameContact_txt_xpath = "//input[@placeholder='Last name']"
	private static String emailContact_txt_xpath = "//div[@class='modal-content modal-lg']//input[@placeholder='E-mail']"
	private static String typeEmailContact_drp_xpath = "//div[@id='loading-modal-vendor-contact-edit']//fieldset[@class='email-extension extension-fields']//select[@placeholder='Type']"
	private static String contactList_tbl_xpath = "//div[@id='table_vendor_contact']//tbody"
	private static String setDefaultContact_lnk_xpath = "//span[contains(text(),'Set as Default')]"
	private static String defaultContact_ic_xpath = "//i[contains(@class,'glyphicon glyphicon-ok')]"
	private static String deleteContact_ic_xpath = "//span[contains(@class,'action-table action-delete')]//i[contains(@class,'icon-close')]"

	//Activity


	//Document
	private static String document_tab_xpath = "//a[@href='#tab-vendor-documents']"
	private static String newDocument_btn_xpath = "//button[contains(text(),'New Document')]"
	private static String submitNewDocument_btn_xpath = "//button[contains(text(),'Submit')]"
	private static String newFileDoucment_msg_xpath = "//ul[@data-field='file']"

	//Note
	private static String note_tab_xpath = "//a[@href='#tab-vendor-notes']"
	private static String newNote_btn_xpath = "//button[contains(text(),'New Note')]"
	private static String submitNewNote_btn_xpath = "//button[contains(text(),'Submit')]"
	private static String newContactNote_msg_xpath = "//ul[@data-field='contact_id']"
	private static String newSubjectNote_msg_xpath = "//ul[@data-field='subject']"
	private static String newContentNote_msg_xpath = "//ul[@data-field='content']"
	private static String newContactNote_drp_xpath = "//select[@placeholder='Contact']"
	private static String newContactNote_listDrp_xpath = "//select[@placeholder='Contact']/option"



	public VendorDetail() {
	}


	public static WebElement companyNameTitle(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(companyNameTitle_lb_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement vendorEdit_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(vendorEdit_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement vendorName_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(vendorName_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}


	public static WebElement vendorCode_num(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(vendorCode_num_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement vendorService_drd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(vendorService_drd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> vendorService_listDrd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(vendorService_listDrd_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement vendorOwnership_drd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(vendorOwnership_drd_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> vendorOwnership_listDrd(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver
					.findElements(By.xpath(vendorOwnership_listDrd_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}


	public static WebElement vendorWebSite_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(vendorWebSite_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement updateVendorInfo_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(updateVendorInfo_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement cancelVendorInfo_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(cancelVendorInfo_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement updateVendorRating_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(updateVendorRating_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement editVendorRating_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(editVendorRating_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement whyVendorRating_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(whyVendorRating_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement whyVendorRating_view(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(whyVendorRating_view_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement toastMsg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(toastMsg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement fiveVendorRating_icon(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(fiveVendorRating_icon_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement fiveVendorRating_att(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(fiveVendorRating_att_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement searchOption_tap(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(searchOption_tap_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement editSearchOption_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(editSearchOption_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement preferredVendor_chk(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(preferredVendor_chk_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement useZipCodeVendor_chk(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(useZipCodeVendor_chk_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement inputUseZipCodeVendor_chk(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(inputUseZipCodeVendor_chk_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement excludedClients_multi(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(excludedClients_multi_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement updateSearchOptionVendor_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(updateSearchOptionVendor_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	// STATUS
	public static WebElement updateVendorStatus_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(updateVendorStatus_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement editVendorStatus_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(editVendorStatus_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement active_toggle(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(active_toggle_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}


	public static WebElement available_toggle(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(available_toggle_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement international_toggle(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(international_toggle_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	//CONTACTS
	public static WebElement contacts_tap(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(contacts_tap_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newContacts_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newContacts_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement submitNewContacts_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(submitNewContacts_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}


	public static WebElement newFirstNameContact_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newFirstNameContact_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newLastNameContact_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newLastNameContact_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newStateContact_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newStateContact_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newFirstNameContact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newFirstNameContact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newLastNameContact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newLastNameContact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newStateContact_drp(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newStateContact_drp_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement toastMsgNewContact(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(toastMsg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newAddress1Contact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newAddress1Contact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newCityContact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newCityContact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newZipCodeContact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newZipCodeContact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement editContact_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(editContact_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement updateContact_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(updateContact_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement firstNameContact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(firstNameContact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement lastNameContact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(lastNameContact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement emailContact_txt(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(emailContact_txt_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement typeEmailContact_drp(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(typeEmailContact_drp_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement contactList_tbl(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(contactList_tbl_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement setDefaultContact_lnk(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(setDefaultContact_lnk_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement defaultContact_ic(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(defaultContact_ic_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement deleteContact_ic(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(deleteContact_ic_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement fullNameContact(String fullContactName){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			String fullContactName_xpath = "//span[contains(text(),'"+fullContactName+"')]"
			WebElement temp_element = webDriver.findElement(By.xpath(fullContactName_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}

	}


	// ACTIVITY



	//NOTE
	public static WebElement note_tab(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(note_tab_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newNote_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newNote_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement submitNewNote_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(submitNewNote_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newContactNote_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newContactNote_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newSubjectNote_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newSubjectNote_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newContentNote_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newContentNote_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newContactNote_drp(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newContactNote_drp_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static List<WebElement> newContactNote_listDrp(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(newContactNote_listDrp_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	//DOCUMENT

	public static WebElement document_tab(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(document_tab_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newDocument_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newDocument_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement submitNewDocument_btn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(submitNewDocument_btn_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement newFileDoucment_msg(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(newFileDoucment_msg_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e){
			return null
		}
	}


}