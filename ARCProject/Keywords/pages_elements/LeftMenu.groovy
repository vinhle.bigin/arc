package pages_elements
import org.openqa.selenium.By
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import functions.Common

public class LeftMenu {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver
	private static String Transferee_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/transferee/transferees')]"
	private static String Client_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/client/clients')]"
	private static String Vendor_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/vendors/vendors')]"
	private static String TaskList_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/task/task')]"
	private static String Template_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/template')]"
	private static String SmartTask_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/smart-task/tasks')]"
	private static String Custom_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/customization')]"
	private static String Role_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/arc-employee/role')]"
	private static String User_mnuitem_xpath = "//ul[@class = 'menu accordion-menu']//a[contains(@href, 'backend/arc-employee/user')]"
	private static String Admin_mnu_xpath = "//ul[@class = 'menu accordion-menu']//a/p[contains(text(), 'Admin')]"

	private static username_lbl_xpath
	private static email_lbl_xpath
	private static logo_xpath

	public LeftMenu() {
	}

	public static WebElement Transferee_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(Transferee_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Client_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(Client_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement Vendor_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(Vendor_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TaskList_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(TaskList_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Template_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(Template_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SmartTask_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(SmartTask_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Custom_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(Custom_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Role_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(Role_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement User_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(User_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Admin_mnu() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(Admin_mnu_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static void openVendorPage(){
		WebDriver webDriver = DriverFactory.getWebDriver();
		Common.waitForControlClickable(webDriver, Vendor_mnuitem_xpath)
		WebElement vendor = webDriver.findElement(By.xpath(Vendor_mnuitem_xpath));
		vendor.click()
	}
}

