package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import functions.Convert
import objects.HHGService
import objects.HPService
import objects.HSService
import objects.MortgService
import objects.ServicesInfo
import objects.TQService
import objects.Transferee
import internal.GlobalVariable

public class Transf_DomesticTab {

	private static String Domestic_tab_xpath = "//a[contains(text(),'Domestic')]"
	private static String VendorType_ddl_xpath= "//div[@org-placeholder ='Select Type']//select"
	private static String VendorList_ddl_xpath= "//div[@class='filter-heading form-inline default-form']//ul"
	private static String Vendor_txt_xpath= "//div[@class='filter-heading form-inline default-form']//div[@class='input-control-wrap']//input"
	private static String Add_btn_xpath= "//div[@class='filter-heading form-inline default-form']//button[contains(@id,'add-vendor')]"
	private static String CustomSearch_btn_xpath= "//div[@class='filter-heading form-inline default-form']//button[contains(@data-toggle,'modal')]"
	private static String Edit_btn_xpath= "//a[@data-original-title ='Edit']"

	private static String HS_ListPrice_txt_xpath= "//input[@placeholder ='List Price']"
	private static String HS_SalePrice_txt_xpath= "//input[@placeholder ='Sale Price']"
	private static String HS_CmsionPercnt_txt_xpath= "//input[@placeholder ='Commission Percent']"
	private static String HS_ListStartDate_txt_xpath= "//input[@id ='listing_start_date']"
	private static String HS_ListEndDate_txt_xpath= "//input[@id ='listing_end_date']"
	private static String HS_CloseDate_txt_xpath= "//input[@id ='closing_date']"
	private static String HS_RebateAmount_txt_xpath= "//div[@org-placeholder='Rebate Amount']//input"
	private static String HS_BuyOutDate_txt_xpath= "//div[@org-placeholder='Buy-Out Accepted Date']//input"
	private static String HS_EscrowPercent_txt_xpath= "//div[@org-placeholder='Escrow Percentage']//input"
	private static String HS_EscrowFeeAmount_txt_xpath= "//div[@org-placeholder='Escrow Fee Amount']//input"
	private static String HS_IsRebateSent_chk_xpath= "//label[text()='Rebate Sent']/..//input"
	private static String HS_IsTookBOut_chk_xpath= "//label[text()='Took Buy-Out']/..//input"
	private static String HS_IsInvoiceCreate_chk_xpath= "//label[text()='Invoice Created']/..//input"
	private static String HS_IsInvoiceClose_chk_xpath= "//label[text()='Invoice Closed Out']/..//input"
	private static String HS_IsEscrReceive_chk_xpath= "//label[text()='Escrow Fee Received']/..//input"
	private static String HS_IsSignRefAgree_chk_xpath= "//label[text()='Signed Referral Fee Agreement']/..//input"
	private static String Update_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='update']"
	private static String Cancel_btn_xpath= "//div[@class='btn-group-action']//button[@data-control='cancel-update']"
	private static String AddedVendor_tbl_xpath= "//div[@id='transferee-domestic-table']//table/tbody"

	private static String HP_LoanAmount_txt_xpath= "//div[@org-placeholder='Loan Amount']//input"
	private static String HP_HouseTripDate_txt_xpath= "//div[@org-placeholder='House Hunting Trip Date']//input"
	private static String HP_IsTransfRent_chk_xpath= "//label[text()='Transferee Renting']/..//input"
	private static String HP_SalesPrice_txt_xpath= "//input[@placeholder ='Sales Price']"
	private static String HP_CloseDate_txt_xpath= "//div[@org-placeholder ='Close Date']//input"
	private static String HP_EscrowPercent_txt_xpath= "//div[@org-placeholder='Escrow Fee Percent']//input"

	private static String MG_MortgAmount_txt_xpath= "//div[@org-placeholder='Mortgage Amount']//input"
	private static String MG_IndexRate_txt_xpath= "//div[@org-placeholder='Index Rate']//input"
	private static String MG_AmountClose_txt_xpath= "//div[@org-placeholder='Amount Needed at Closing']//input"
	private static String MG_DownPayment_txt_xpath= "//div[@org-placeholder='Down Payment']//input"
	private static String MG_LockDate_txt_xpath= "//div[@org-placeholder='Lock In Date']//input"

	private static String HHG_OrgAddrs_vlue_xpath= "//div[@class='form-group col-md-3 form-view'][1]/div"
	private static String HHG_DesAddrs_vlue_xpath= "//div[@class='form-group col-md-3 form-view'][2]/div"
	private static String HHG_PaymentType_txt_xpath= "//div[@org-placeholder='HHG Payment Type']//input"
	private static String HHG_MoveDate_txt_xpath= "//div[@org-placeholder='HHG Move Date']//input"
	private static String HHG_DeliveryDate_txt_xpath= "//div[@org-placeholder='HHG Delivery Date']//input"

	private static String TQ_ServiceDesrc_txt_xpath= "//div[@org-placeholder ='Service Description']//input"
	private static String TQ_Currency_ddl_xpath= "//div[@org-placeholder ='Currency']//select"
	private static String TQ_AccountPurpose_ddl_xpath= "//div[@org-placeholder ='Account Purpose']//select"
	private static String TQ_NotifyDate_txt_xpath= "//div[@org-placeholder ='Notification Date']//input"
	private static String TQ_ServiceDate_txt_xpath= "//div[@org-placeholder ='Service Date']//input"
	private static String TQ_MoveInDate_txt_xpath= "//div[@org-placeholder ='Move In Date']//input"
	private static String TQ_MoveOutDate_txt_xpath= "//div[@org-placeholder ='Move Out Date']//input"
	private static String TQ_DateAuth_txt_xpath= "//div[@org-placeholder ='Days Authorized']//input"
	private static String TQ_ApartmentType_ddl_xpath= "//div[@org-placeholder ='Appartment Type']//select"
	private static String TQ_ApartmentFinish_ddl_xpath= "//div[@org-placeholder ='Appartment Finish']//select"
	private static String TQ_Bath_txt_xpath= "//div[@org-placeholder ='Baths']//input"
	private static String TQ_Budget_txt_xpath= "//div[@org-placeholder ='Budget']//input"
	private static String TQ_Pet_txt_xpath= "//div[@org-placeholder ='Pets']//textarea"
	private static String TQ_SpecicialInstruct_txt_xpath= "//div[@org-placeholder ='Special Instructions']//textarea"
	private static String TQ_WhoPay_ddl_xpath= "//div[contains(@org-placeholder,'s Paying')]//select"
	private static String TQ_PayType_txt_xpath= "//div[contains(@org-placeholder,'Payment Type')]//input"
	private static String TQ_IsBillFirstDay_chk_xpath= "//label[text()='Bill First Day']/..//input"



	private static String Menu_List_xpath = "//ul[@class = 'dropdown-menu services-dropdown-menu']"


	public Transf_DomesticTab() {
	}



	public static WebElement Menu_List() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Menu_List_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Domestic_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Domestic_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement VendorType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(VendorType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement VendorList_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(VendorList_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Vendor_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Vendor_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Add_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Add_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CustomSearch_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CustomSearch_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_ListPrice_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_ListPrice_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_SalePrice_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_SalePrice_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_CmsionPercnt_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_CmsionPercnt_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_ListStartDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_ListStartDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_ListEndDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_ListEndDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_CloseDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_CloseDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_RebateAmount_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_RebateAmount_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_BuyOutDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_BuyOutDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_EscrowPercent_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_EscrowPercent_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_EscrowFeeAmount_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_EscrowFeeAmount_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_IsRebateSent_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_IsRebateSent_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_IsTookBOut_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_IsTookBOut_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_IsInvoiceCreate_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_IsInvoiceCreate_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_IsInvoiceClose_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_IsInvoiceClose_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_IsEscrReceive_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_IsEscrReceive_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HS_IsSignRefAgree_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HS_IsSignRefAgree_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement AddedVendor_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddedVendor_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HP_LoanAmount_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HP_LoanAmount_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HP_HouseTripDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HP_HouseTripDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HP_IsTransfRent_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HP_IsTransfRent_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	
	public static WebElement HP_SalesPrice_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HP_SalesPrice_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	
	public static WebElement HP_CloseDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HP_CloseDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	
	public static WebElement HP_EscrowPercent_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HP_EscrowPercent_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	
	
	public static WebElement MG_MortgAmount_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MG_MortgAmount_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MG_IndexRate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MG_IndexRate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MG_AmountClose_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MG_AmountClose_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MG_DownPayment_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MG_DownPayment_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MG_LockDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MG_LockDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HHG_OrgAddrs_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HHG_OrgAddrs_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HHG_DesAddrs_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HHG_DesAddrs_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HHG_PaymentType_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HHG_PaymentType_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HHG_MoveDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HHG_MoveDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement HHG_DeliveryDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(HHG_DeliveryDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement TQ_ServiceDesrc_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_ServiceDesrc_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_Currency_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_Currency_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_AccountPurpose_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_AccountPurpose_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_NotifyDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_NotifyDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_ServiceDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_ServiceDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_MoveInDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_MoveInDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_MoveOutDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_MoveOutDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_DateAuth_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_DateAuth_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_ApartmentType_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_ApartmentType_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_ApartmentFinish_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_ApartmentFinish_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_Bath_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_Bath_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_Budget_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_Budget_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_Pet_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_Pet_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_SpecicialInstruct_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_SpecicialInstruct_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_WhoPay_ddl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_WhoPay_ddl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_PayType_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_PayType_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement TQ_IsBillFirstDay_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TQ_IsBillFirstDay_chk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}





	/////////================================METHOD
	public VerifyServiceDisplayMenu_func(List<ServicesInfo> servlist)
	{
		for(int i:servlist.size()-1)
		{
			Common.VerifyItemExistDropdown(this.Menu_List(), servlist.get(i).Type)
		}

	}//end void

	
	//METHOD FILL HS INFO
	public static void FillHSInfo_func(HSService service,String button)
	{
		Common.HandleElementClick(this.Edit_btn())
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)


		Common.WaitForElementDisplay(this.HS_BuyOutDate_txt())


		Common.sendkeyTextToElement(this.HS_ListPrice_txt(), service.ListPrice)

		Common.sendkeyTextToElement(this.HS_SalePrice_txt(), service.SalePrice)

		Common.sendkeyTextToElement(this.HS_CmsionPercnt_txt(), service.CommissionPercent)

		Common.ActionSendkey(this.HS_ListStartDate_txt(), service.ListEndDate)
	
		Common.ActionSendkey(this.HS_ListEndDate_txt(), service.ListEndDate)

		Common.ActionSendkey(this.HS_CloseDate_txt(), service.CloseDate)

		Common.sendkeyTextToElement(this.HS_RebateAmount_txt(), service.RebateAmount)

		Common.ActionSendkey(this.HS_BuyOutDate_txt(), service.BuyOutAcceptDate)

		Common.sendkeyTextToElement(this.HS_EscrowPercent_txt(), service.EscrowFeePercent)

		Common.sendkeyTextToElement(this.HS_EscrowFeeAmount_txt(), service.EscrowFeeAmount)

		Common.HandleElementClick(this.HS_IsRebateSent_chk())

		Common.HandleElementClick(this.HS_IsTookBOut_chk())

		Common.HandleElementClick(this.HS_IsInvoiceCreate_chk())

		Common.HandleElementClick(this.HS_IsInvoiceClose_chk())

		Common.HandleElementClick(this.HS_IsEscrReceive_chk())

		Common.HandleElementClick(this.HS_IsSignRefAgree_chk())

		if(button=="Submit")
			this.Update_btn().click()
		else if(button=="Cancel")
			this.Cancel_btn().click()
			
		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "when update HS_ServiceInfo")


	}//end void

	
	
	//METHOD FILL HP INFO
	public static void FillHPInfo_func(HPService service,String button)
	{
		Common.HandleElementClick(this.Edit_btn())
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)


		Common.WaitForElementDisplay(this.HS_BuyOutDate_txt())

		Common.sendkeyTextToElement(this.HP_SalesPrice_txt(), service.SalePrice)

		Common.sendkeyTextToElement(this.HP_LoanAmount_txt(), service.LoanAmount)

		Common.sendkeyTextToElement(this.HS_CmsionPercnt_txt(), service.CommissionPercent)

		Common.sendkeyTextToElement(this.HP_CloseDate_txt(), service.CloseDate)
		WebUI.delay(2)
		
		Common.ActionSendkey(this.HP_HouseTripDate_txt(), service.HouseHuntTripDate)

		WebUI.delay(2)
		String input_str = service.RebateAmount.replace("\$","")
	
		input_str = service.RebateAmount.replace(",","")
		Common.ActionSendkey(this.HS_RebateAmount_txt(), input_str)
		
		WebUI.delay(2)
		input_str = service.EscrowFeePercent.replace("%","")
		input_str = service.EscrowFeePercent.replace(",","")
		Common.ActionSendkey(this.HP_EscrowPercent_txt(), input_str)
		
	//	Common.sendkeyTextToElement()

		WebUI.delay(2)
		input_str = service.EscrowFeeAmount.replace("\$","")
		input_str = service.EscrowFeeAmount.replace(",","")
		Common.sendkeyTextToElement(this.HS_EscrowFeeAmount_txt(), input_str)

		Common.HandleElementClick(this.HS_IsRebateSent_chk())

		Common.HandleElementClick(this.HS_IsInvoiceCreate_chk())

		Common.HandleElementClick(this.HS_IsInvoiceClose_chk())

		Common.HandleElementClick(this.HS_IsEscrReceive_chk())

		Common.HandleElementClick(this.HS_IsSignRefAgree_chk())

		Common.HandleElementClick(this.HP_IsTransfRent_chk())

		if(button=="Submit")
			this.Update_btn().click()
		else if(button=="Cancel")
			this.Cancel_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "when update HP_ServiceInfo")

	}//end void

	
	
	//METHOD FILL MG INFO
	public static void FillMortgageInfo_func(MortgService service,String button)
	{
		Common.HandleElementClick(this.Edit_btn())
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)


		//Common.WaitForElementDisplay(this.HS_BuyOutDate_txt())
		String input_str = service.MortgageAmount.replace("\$","")
		input_str = service.MortgageAmount.replace(",","")
		input_str = service.MortgageAmount.replace(".","")
		Common.sendkeyTextToElement(this.MG_MortgAmount_txt(), input_str)

		Common.sendkeyTextToElement(this.MG_IndexRate_txt(), service.IndexRate)

		input_str = service.AmountAtClose.replace("\$","")
		input_str = service.AmountAtClose.replace(",","")
		input_str = service.AmountAtClose.replace(".","")
		Common.sendkeyTextToElement(this.MG_AmountClose_txt(), service.AmountAtClose)

		input_str = service.DownPayment.replace("\$","")
		input_str = service.DownPayment.replace(",","")
		input_str = service.DownPayment.replace(".","")
		Common.sendkeyTextToElement(this.MG_DownPayment_txt(), service.DownPayment)

		Common.ActionSendkey(this.HS_CloseDate_txt(), service.CloseDate)

		Common.ActionSendkey(this.MG_LockDate_txt(), service.LockInDate)

		if(button=="Submit")
			this.Update_btn().click()
		else if(button=="Cancel")
			this.Cancel_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "when update Mortgage_ServiceInfo")


	}//end void

	
	
	//METHOD FILL HGG INFO
	public static void FillHHGInfo_func(HHGService service,String button)
	{
		Common.HandleElementClick(this.Edit_btn())
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)


		Common.WaitForElementDisplay(this.HS_BuyOutDate_txt())

		Common.sendkeyTextToElement(this.HHG_PaymentType_txt(), service.HHGPaymentType)

		Common.ActionSendkey(this.HHG_MoveDate_txt(), service.HHGMoveDate)
		
		Common.ActionSendkey(this.HHG_DeliveryDate_txt(), service.HHGDeliverDate)
		
		Common.HandleElementClick(this.HS_IsInvoiceCreate_chk())

		Common.HandleElementClick(this.HS_IsInvoiceClose_chk())

		Common.HandleElementClick(this.HS_IsEscrReceive_chk())

		if(button=="Submit")
			this.Update_btn().click()
		else if(button=="Cancel")
			this.Cancel_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "when update HHG_ServiceInfo")


	}//end void

	
	//METHOD FILL TQ INFO
	public static void FillTQInfo_func(TQService service,String button)
	{
		Common.HandleElementClick(this.Edit_btn())
		if(LandingPage.LoadingArc()!=null)
			Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)


		//Common.WaitForElementDisplay(this.HS_BuyOutDate_txt())


		Common.sendkeyTextToElement(this.TQ_ServiceDesrc_txt(),service.ServiceDesr)

		Common.ScrollElementtoViewPort_func(this.TQ_Currency_ddl())
		
		Common.SelectDropdownItem(this.TQ_Currency_ddl(),service.Currency)
		
		Common.ScrollElementtoViewPort_func(this.TQ_AccountPurpose_ddl())

		Common.SelectDropdownItem(this.TQ_AccountPurpose_ddl(),service.AccountPurpose)

		Common.ActionSendkey(this.TQ_NotifyDate_txt(),service.NotiDate)

		Common.ActionSendkey(this.TQ_ServiceDate_txt(),service.ServiceDate)

		Common.ActionSendkey(this.TQ_MoveInDate_txt(),service.MoveInDate)

		Common.ActionSendkey(this.TQ_MoveOutDate_txt(),service.MoveOutDate)

		Common.sendkeyTextToElement(this.TQ_DateAuth_txt(),service.DayAuth)

		Common.ScrollElementtoViewPort_func(this.TQ_ApartmentType_ddl())
		Common.SelectDropdownItem(this.TQ_ApartmentType_ddl(),service.ApartmentType)

		Common.ScrollElementtoViewPort_func(this.TQ_ApartmentFinish_ddl())
		Common.SelectDropdownItem(this.TQ_ApartmentFinish_ddl(),service.ApartmentFinish)

		Common.sendkeyTextToElement(this.TQ_Bath_txt(),service.Baths)

		Common.sendkeyTextToElement(this.TQ_Budget_txt(),service.Budget)

		Common.sendkeyTextToElement(this.TQ_Pet_txt(),service.Pets)

		Common.sendkeyTextToElement(this.TQ_SpecicialInstruct_txt(),service.SpecialInstruct)

		Common.ScrollElementtoViewPort_func(this.TQ_WhoPay_ddl())
		Common.SelectDropdownItem(this.TQ_WhoPay_ddl(),service.WhoPay)

		Common.sendkeyTextToElement(this.TQ_PayType_txt(),service.PaymentType)


		Common.HandleElementClick(this.TQ_IsBillFirstDay_chk())

		Common.HandleElementClick(this.HS_IsInvoiceCreate_chk())

		Common.HandleElementClick(this.HS_IsInvoiceClose_chk())

		Common.HandleElementClick(this.HS_IsEscrReceive_chk())

		if(button=="Submit")
			this.Update_btn().click()
		else if(button=="Cancel")
			this.Cancel_btn().click()

		Messages.VerifyNotifyMsgNotDisplay_func(Messages.SystemError_msg, "when update TQ_ServiceInfo")


	}//end void
	
	
	//METHOD: VERIFY HHG DETAILS INFO
	public static void Verify_HHGServiceDetail_func(HHGService service)
	{
		
		String field_name = ""
		
		field_name = "HHG_OrgAddrs_vlue"
		Common.VerifyFieldTextEqual_func(field_name,this.HHG_OrgAddrs_vlue(), service.OriAddr)
		
		field_name = "HHG_DesAddrs_vlue"
		Common.VerifyFieldTextEqual_func(field_name,this.HHG_DesAddrs_vlue(), service.DesAddr)
		
		field_name = "HHG_PaymentType_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HHG_PaymentType_txt(), service.HHGPaymentType)
		
		
		field_name = "HHG_PaymentType_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HHG_PaymentType_txt(), service.HHGPaymentType)

		field_name = "HHG_MoveDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HHG_MoveDate_txt(), service.HHGMoveDate)

		field_name = "HHG_DeliveryDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HHG_DeliveryDate_txt(), service.HHGDeliverDate)

		field_name = "HHG_IsInvoiceCreate_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceCreate_chk(),service.IsInvoceCreate.toString())

		field_name = "HHG_IsInvoiceClose_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceClose_chk(),service.IsInvoiceClose.toString())

		field_name = "HHG_IsEscrReceive_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsEscrReceive_chk(),service.IsEscrowFeeReceive.toString())
	}//end void
	
	
	
	//METHOD: VERIFY HS DETAILS INFO
	public static void Verify_HSServiceDetail_func(HSService service)
	{
		String field_name = ""
		
		field_name = "HS_ListPrice_txt"
	
		
		Common.VerifyFieldValueEqual_func(field_name,this.HS_ListPrice_txt(), service.ListPrice)

		field_name = "HS_SalePrice_txt"
		
		Common.VerifyFieldValueEqual_func(field_name,this.HS_SalePrice_txt(), service.SalePrice)

		field_name = "HS_CmsionPercnt_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_CmsionPercnt_txt(), service.CommissionPercent)

		field_name = "HS_ListStartDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_ListStartDate_txt(), service.ListEndDate)

		field_name = "HS_ListEndDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_ListEndDate_txt(), service.ListEndDate)

		field_name = "HS_CloseDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_CloseDate_txt(), service.CloseDate)

		field_name = "HS_RebateAmount_txt"
	
		Common.VerifyFieldValueEqual_func(field_name,this.HS_RebateAmount_txt(), service.RebateAmount)

		field_name = "HS_BuyOutDate_txt"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_BuyOutDate_txt(), service.BuyOutAcceptDate)

		field_name = "HS_EscrowPercent_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_EscrowPercent_txt(), service.EscrowFeePercent)

		field_name = "HS_EscrowFeeAmount_txt"
		
		Common.VerifyFieldValueEqual_func(field_name,this.HS_EscrowFeeAmount_txt(), service.EscrowFeeAmount)

		field_name = "HS_IsRebateSent_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsRebateSent_chk(),service.IsRebaseSent.toString())

		field_name = "HS_IsTookBOut_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsTookBOut_chk(),service.IsTookBuyOut.toString())

		field_name = "HS_IsInvoiceCreate_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceCreate_chk(),service.IsInvoiceCreate.toString())

		field_name = "HS_IsInvoiceClose_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceClose_chk(),service.IsInvoiceClose.toString())

		field_name = "HS_IsEscrReceive_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsEscrReceive_chk(),service.IsEscrowFeeReceive.toString())

		field_name = "HS_IsSignRefAgree_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsSignRefAgree_chk(),service.IsSignRefFeeAgree.toString())
	}//end void
	
	
	//METHOD: VERIFY HP DETAILS INFO
	public static void Verify_HPServiceDetail_func(HPService service)
	{
		String field_name = ""
		
		field_name = "HP_SalePrice_txt"
		
		Common.VerifyFieldValueEqual_func(field_name,this.HP_SalesPrice_txt(), service.SalePrice)

		field_name = "HP_LoanAmount_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HP_LoanAmount_txt(), service.LoanAmount)

		field_name = "HP_CmsionPercnt_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_CmsionPercnt_txt(), service.CommissionPercent)

		field_name = "HP_CloseDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HP_CloseDate_txt(), service.CloseDate)

		field_name = "HP_HouseTripDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HP_HouseTripDate_txt(), service.HouseHuntTripDate)

		field_name = "HP_RebateAmount_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_RebateAmount_txt(), service.RebateAmount)

		field_name = "HP_EscrowPercent_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HP_EscrowPercent_txt(), service.EscrowFeePercent)

		field_name = "HP_EscrowFeeAmount_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_EscrowFeeAmount_txt(), service.EscrowFeeAmount)

		field_name = "HP_IsRebateSent_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsRebateSent_chk(),service.IsRebateSent.toString())

		field_name = "HP_IsInvoiceCreate_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceCreate_chk(), service.IsInvoiceCreate.toString())

		field_name = "HP_IsInvoiceClose_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceClose_chk(), service.IsInvoiceClose.toString())

		field_name = "HP_IsEscrReceive_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsEscrReceive_chk(), service.IsEscrowFeeReceive.toString())

		field_name = "HP_IsSignRefAgree_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsSignRefAgree_chk(), service.IsSignRefFeeAgree.toString())

		field_name = "HP_IsTransfRent_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HP_IsTransfRent_chk(), service.IsTransfRent.toString())
	}//end void
	
	
	public static void Verify_MGServiceDetail_func(MortgService service)
	{
		String field_name = ""
		
		field_name = "MG_MortgAmount_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.MG_MortgAmount_txt(), service.MortgageAmount)
		
		field_name = "MG_IndexRate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.MG_IndexRate_txt(), service.IndexRate)

		field_name = "MG_AmountClose_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.MG_AmountClose_txt(), service.AmountAtClose)

		field_name = "MG_DownPayment_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.MG_DownPayment_txt(), service.DownPayment)

		field_name = "MG_CloseDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.HS_CloseDate_txt(), service.CloseDate)

		field_name = "MG_LockDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.MG_LockDate_txt(), service.LockInDate)
	}//end void
	
	
	//METHOD: VERIFY TQDETAILS INFO
	public static void Verify_TQServiceDetail_func(TQService service)
	{
		
		String field_name = ""
		
		field_name = "TQ_ServiceDesrc_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_ServiceDesrc_txt(),service.ServiceDesr)
		
		field_name = "TQ_Currency_ddl"
		Common.VerifyFieldTextEqual_func(field_name,this.TQ_Currency_ddl(),service.Currency)

		field_name = "TQ_AccountPurpose_ddl"
		Common.VerifyFieldTextEqual_func(field_name,this.TQ_AccountPurpose_ddl(),service.AccountPurpose)

		field_name = "TQ_NotifyDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_NotifyDate_txt(),service.NotiDate)

		field_name = "TQ_ServiceDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_ServiceDate_txt(),service.ServiceDate)

		field_name = "TQ_MoveInDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_MoveInDate_txt(),service.MoveInDate)

		field_name = "TQ_MoveOutDate_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_MoveOutDate_txt(),service.MoveOutDate)

		field_name = "TQ_DateAuth_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_DateAuth_txt(),service.DayAuth)

		field_name = "TQ_ApartmentType_ddl"
		Common.VerifyFieldTextEqual_func(field_name,this.TQ_ApartmentType_ddl(),service.ApartmentType)

		field_name = "TQ_ApartmentFinish_ddl"
		Common.VerifyFieldTextEqual_func(field_name,this.TQ_ApartmentFinish_ddl(),service.ApartmentFinish)

		field_name = "TQ_Bath_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_Bath_txt(),service.Baths)

		field_name = "TQ_Budget_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_Budget_txt(),service.Budget)

		field_name = "TQ_Pet_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_Pet_txt(),service.Pets)

		field_name = "TQ_SpecicialInstruct_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_SpecicialInstruct_txt(),service.SpecialInstruct)

		field_name = "TQ_WhoPay_ddl"
		Common.VerifyFieldTextEqual_func(field_name,this.TQ_WhoPay_ddl(),service.WhoPay)

		field_name = "TQ_PayType_txt"
		Common.VerifyFieldValueEqual_func(field_name,this.TQ_PayType_txt(),service.PaymentType)

		field_name = "TQ_IsBillFirstDay_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.TQ_IsBillFirstDay_chk(),service.IsBillFirstDay.toString())

		field_name = "TQ_IsInvoiceCreate_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceCreate_chk(),service.IsInvoiceCreate.toString())

		field_name = "TQ_IsInvoiceClose_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsInvoiceClose_chk(),service.IsInvoiceClose.toString())

		field_name = "TQ_IsEscrReceive_chk"
		Common.VerifyFieldTextEqual_func(field_name,this.HS_IsEscrReceive_chk(),service.IsEscrowFeeReceive.toString())
	}//end void
	

	public void AddVendorContact_func(){
		
	}


}
