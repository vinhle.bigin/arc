package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import functions.Navigation
import objects.Activity
import objects.DocumentInfo
import objects.NoteInfo
import objects.Transferee
import internal.GlobalVariable

public class DocumentTab {
	private static String Tab_xpath = "//a[contains(@href, 'documents') and @data-toggle = 'tab']"

	private static String CreateNew_btn_xpath= "//button[@data-target = '#modal-new-documents']"

	private static String Search_btn_xpath= "//button[contains(@id,'btn-search')]"

	private static String Clear_btn_xpath= "//button[text() = 'Clear']"

	private static String Modal_Edit_btn_xpath = "//div[@class ='modal-header']//a[@data-original-title = 'Edit']"

	private static String SearchSubject_txt_xpath = "//input[@placeholder = 'Document name']"

	private static String SearchDescription_txt_xpath = "//input[@placeholder='Description']"

	private static String Modal_ServiceType_ddl_xpath= "//div[@class ='modal-body clearfix']//select[@placeholder = 'Service']"

	private static String Modal_DocName_txt_xpath= "//div[contains(@org-placeholder,'Document name')]/input | //input[@placeholder ='Document name']"
	
	private static String Modal_DocNameVendor_txt_xpath= "//div[@class='form-group col-md-6 pl-0']//input[@placeholder='Document name']"

	private static String Submit_btn_xpath= "//div[@class = 'modal-footer']//button[@type = 'submit']"

	private static String Cancel_btn_xpath= "//div[@class = 'modal-footer']//button[contains(@data-control, 'cancel')]"

	private static String Attach_lnk_xpath= "//div[@id = 'email-process-attachment']//div[@class = 'content dz-clickable']"

	private static String DocList_tbl_xpath= "//table[@id = 'table-list-document']//tbody"

	//private static String Edit_icon_xpath = ".//a[contains(@href,'document/edit')]"

	private static String Edit_icon_xpath = "//span[@data-original-title='Edit']"

	//private static String Remove_icon_xpath = ".//a[contains(@href,'document/delete')]"

	private static String Remove_icon_xpath = "//span[@data-toggle='tooltip']//i[@class='icon-close']"

	private static String DownLoad_icon_xpath = ".//a[contains(@href,'assets/media/document')]"

	private static String DocumentVendor_tab_xpath = "//a[contains(@href, '#tab-vendor-documents')]"

	private static String NewDocumentVendor_btn_xpath = "//button[contains(text(),'New Document')]"

	private static String DocListVendor_tbl_xpath = "//table[@class = 'VueTables__table btable table dataTable table-striped table-hover']//tbody"

	private static String UpdateDescriptionDoc_txt_xpath = "//div[@org-placeholder='Description']//input"

	private static String Close_icon_xpath = "//div[@id='loading-modal-document-edit']//i[@class='icon-close']"


	public DocumentTab() {
	}

	public static WebElement Modal_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Doc_Tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement DocumentVendor_tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DocumentVendor_tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewDocumentVendor_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewDocumentVendor_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement DownLoad_icon(String docname_str) {
		WebElement temp_element  = Common.GetElementInTable(docname_str,this.DocList_tbl(), this.Remove_icon_xpath)
	}

	public static WebElement Remove_icon(String docname_str) {
		WebElement temp_element  = Common.GetElementInTable(docname_str,this.DocList_tbl(), this.Remove_icon_xpath)

		return temp_element
	}

	public static WebElement Edit_icon(String docname_str) {
		WebElement temp_element  = Common.GetElementInTable(docname_str,this.DocList_tbl(), this.Edit_icon_xpath)
	}

	public static WebElement Edit_ic() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Edit_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Remove_ic() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Remove_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchSubject_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchSubject_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SearchDescription_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SearchDescription_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement CreateNew_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(CreateNew_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Search_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Search_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Clear_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Clear_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement Modal_ServiceType_slect() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_ServiceType_ddl_xpath))
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Modal_DocName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_DocName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
	
	public static WebElement Modal_DocNameVendor_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Modal_DocNameVendor_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Attach_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Attach_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement DocList_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DocList_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement DocListVendor_tbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DocListVendor_tbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement UpdateDescriptionDoc_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(UpdateDescriptionDoc_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Close_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Close_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	//=================================METHOD

	public static void Search_func(String docname_str)
	{
		Common.WaitForElementDisplay(SearchSubject_txt())

		Common.sendkeyTextToElement(SearchSubject_txt(), docname_str)

		Common.HandleElementClick(this.Search_btn())

		WebUI.delay(3)

	}

	public static void searchDocument(String searchField, WebElement element, String subject_str)
	{
		Common.sendkeyTextToElement(element, subject_str)

		Common.HandleElementClick(this.Search_btn())

		WebUI.delay(3)
	}

	public static void VerifyDocExistTable_func(DocumentInfo doc)
	{
		Boolean exist = false
		this.Search_func(doc.file_name)
		List<String> records = Common.GetTableRecordsPerPaging_func(this.DocList_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(doc.file_name))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Doc["+doc.file_name+"] NOT exist in table.\n"
		}//end if
	}

	public static void VerifyDocExistTableVendor_func(DocumentInfo doc)
	{
		Boolean exist = false
		List<String> records = Common.GetTableRecordsPerPaging_func(this.DocListVendor_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(doc.file_name))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(!exist)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Doc["+doc.file_name+"] NOT exist in table.\n"
		}//end if
	}

	public static void VerifyDocNotExistTable_func(DocumentInfo doc)
	{
		Boolean exist = false
		this.Search_func(doc.file_name)

		List<String> records = Common.GetTableRecordsPerPaging_func(this.DocList_tbl())


		for(String index_str: records)
		{
			if(index_str.contains(doc.file_name))
			{
				exist = true
				break
			}//end if
		}//end foreach

		if(exist==true)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Doc["+doc.file_name+"] SHOULD NOT exist in table.\n"
		}//end if
	}

	public static void VerifyModalInfoViewMode_func(DocumentInfo doc_info)
	{
		String area = ""
		String field_name = ""
		String mode = "View Mode"

		Navigation.GotoDocumentTab_func()

		Common.WaitForElementDisplay(this.CreateNew_btn())

		this.Search_func(doc_info.file_name)

		this.Edit_icon(doc_info.file_name).click()

		WebUI.delay(2)

		field_name = "Modal_DocName_txt"
		Common.VerifyFieldValueEqual_func(area+field_name , this.Modal_DocName_txt(),doc_info.file_name, mode)

		field_name = "Modal_ServiceType_slect"
		Common.VerifyFieldTextEqual_func(area+field_name , this.Modal_ServiceType_slect(),doc_info.ServiceType, mode)

	}//end void
	
	public static void VerifyInfoViewModeDocVendor_func(DocumentInfo doc_info)
	{
		String area = ""
		String field_name = ""
		String mode = "View Mode"

		Common.WaitForElementDisplay(this.CreateNew_btn())

		this.Search_func(doc_info.file_name)

		this.Edit_ic().click()

		WebUI.delay(2)

		field_name = "Modal_DocName_txt"
		Common.VerifyFieldValueEqual_func(area+field_name , this.Modal_DocNameVendor_txt(),doc_info.file_name, mode)


	}//end void

	public static void VerifyDocNotExistTableVendor_func(DocumentInfo doc)
	{
		Boolean exist = false

		List<String> records = Common.GetTableRecordsPerPaging_func(this.DocListVendor_tbl())

		for(String index_str: records)
		{
			if(index_str.contains(doc.file_name))
			{
				exist = true
				break
			}//end if
		}//end foreach


		if(exist==true)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Doc["+doc.file_name+"] SHOULD NOT exist in table.\n"
		}//end if
	}


}
