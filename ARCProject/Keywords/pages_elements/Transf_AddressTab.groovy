package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.NoSuchElementException

import functions.Common
import objects.Transferee
import internal.GlobalVariable

public class Transf_AddressTab {


	private static String Address_Tab_xpath = "//a[@href = '#transferee-address']"
	private static String OrgRes_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-origin-residence-address']"
	private static String OrgRes_Update_btn_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//button[@data-control = 'update']"
	private static String OrgRes_Cancel_btn_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//button[@data-control = 'cancel-update']"
	private static String OrgRes_Addrs1_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Address 1']//input"
	private static String OrgRes_Addrs2_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Address 2']//input"
	private static String OrgRes_City_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter City']//input"
	private static String OrgRes_State_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter State']//select"
	private static String OrgRes_Zip_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Zip']//input"
	private static String OrgRes_Country_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@org-placeholder = 'Enter Country']//select"
	private static String OrgRes_IsBilling_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[1]//input"
	private static String OrgRes_IsShipping_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[2]//input"
	private static String OrgRes_IsMailing_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[3]//input"
	private static String OrgRes_IsPrimary_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][1]/div[4]//input"
	private static String OrgRes_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][2]/div[1]//input"
	private static String OrgRes_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][2]/div[2]//input"
	private static String OrgRes_IsSendMailService_xpath= "//div[@id = 'edit-transferee-origin-residence-address']//div[@class = 'col-md-6'][2]/div[3]//input"
	private static String DesRes_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-destination-residence-address']"

	///DESTINATION RESIDENCE
	private static String DesRes_Update_btn_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//button[@data-control = 'update']"
	private static String DesRes_Cancel_btn_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//button[@data-control = 'cancel-update']"
	private static String DesRes_Addrs1_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Address 1']//input"
	private static String DesRes_Addrs2_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Address 2']//input"
	private static String DesRes_City_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter City']//input"
	private static String DesRes_State_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter State']//select"
	private static String DesRes_Zip_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Zip']//input"
	private static String DesRes_Country_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@org-placeholder = 'Enter Country']//select"
	private static String DesRes_IsBilling_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[1]//input"
	private static String DesRes_IsShipping_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[2]//input"
	private static String DesRes_IsMailing_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[3]//input"
	private static String DesRes_IsPrimary_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][1]/div[4]//input"
	private static String DesRes_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][2]/div[1]//input"
	private static String DesRes_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][2]/div[2]//input"
	private static String DesRes_IsSendMailService_xpath= "//div[@id = 'edit-transferee-destination-residence-address']//div[@class = 'col-md-6'][2]/div[3]//input"

	///ORIGINAL OFF
	private static String OrgOff_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-origin-office-address']"
	private static String OrgOff_Update_btn_xpath= "//div[@id = 'edit-transferee-origin-office-address']//button[@data-control = 'update']"
	private static String OrgOff_Cancel_btn_xpath= "//div[@id = 'edit-transferee-origin-office-address']//button[@data-control = 'cancel-update']"
	private static String OrgOff_Addrs1_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Address 1']//input"
	private static String OrgOff_Addrs2_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Address 2']//input"
	private static String OrgOff_City_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter City']//input"
	private static String OrgOff_State_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter State']//select"
	private static String OrgOff_Zip_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Zip']//input"
	private static String OrgOff_Country_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@org-placeholder = 'Enter Country']//select"
	private static String OrgOff_IsBilling_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[1]//input"
	private static String OrgOff_IsShipping_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[2]//input"
	private static String OrgOff_IsMailing_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[3]//input"
	private static String OrgOff_IsPrimary_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][1]/div[4]//input"
	private static String OrgOff_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][2]/div[1]//input"
	private static String OrgOff_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][2]/div[2]//input"
	private static String OrgOff_IsSendMailService_xpath= "//div[@id = 'edit-transferee-origin-office-address']//div[@class = 'col-md-6'][2]/div[3]//input"


	//DESTINATION OFF
	private static String DesOff_Edit_btn_xpath= "//a[@id = 'icon-edit-transferee-destination-office-address']"
	private static String DesOff_Update_btn_xpath= "//div[@id = 'edit-transferee-destination-office-address']//button[@data-control = 'update']"
	private static String DesOff_Cancel_btn_xpath= "//div[@id = 'edit-transferee-destination-office-address']//button[@data-control = 'cancel-update']"
	private static String DesOff_Addrs1_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Address 1']//input"
	private static String DesOff_Addrs2_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Address 2']//input"
	private static String DesOff_City_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter City']//input"
	private static String DesOff_State_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter State']//select"
	private static String DesOff_Zip_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Zip']//input"
	private static String DesOff_Country_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@org-placeholder = 'Enter Country']//select"
	private static String DesOff_IsBilling_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[1]//input"
	private static String DesOff_IsShipping_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[2]//input"
	private static String DesOff_IsMailing_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[3]//input"
	private static String DesOff_IsPrimary_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][1]/div[4]//input"
	private static String DesOff_IsSendMailPayment_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][2]/div[1]//input"
	private static String DesOff_IsSendMailTaxDoc_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][2]/div[2]//input"
	private static String DesOff_IsSendMailService_xpath= "//div[@id = 'edit-transferee-destination-office-address']//div[@class = 'col-md-6'][2]/div[3]//input"




	public Transf_AddressTab() {
	}

	public static WebElement Address_Tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Address_Tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	//DEFINE ELEMENT
	public static WebElement OrgRes_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_Addrs1() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_Addrs1_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_Addrs2() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_Addrs2_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_City() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_City_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_State() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_State_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_Zip() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_Zip_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_Country() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_Country_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_IsBilling() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_IsBilling_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_IsShipping() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_IsShipping_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_IsMailing() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_IsMailing_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_IsPrimary() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_IsPrimary_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_IsSendMailPayment() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_IsSendMailPayment_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_IsSendMailTaxDoc() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_IsSendMailTaxDoc_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgRes_IsSendMailService() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgRes_IsSendMailService_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_Addrs1() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_Addrs1_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_Addrs2() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_Addrs2_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_City() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_City_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_State() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_State_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_Zip() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_Zip_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_Country() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_Country_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_IsBilling() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_IsBilling_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_IsShipping() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_IsShipping_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_IsMailing() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_IsMailing_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_IsPrimary() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_IsPrimary_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_IsSendMailPayment() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_IsSendMailPayment_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_IsSendMailTaxDoc() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_IsSendMailTaxDoc_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesRes_IsSendMailService() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesRes_IsSendMailService_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_Addrs1() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_Addrs1_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_Addrs2() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_Addrs2_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_City() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_City_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_State() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_State_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_Zip() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_Zip_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_Country() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_Country_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_IsBilling() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_IsBilling_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_IsShipping() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_IsShipping_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_IsMailing() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_IsMailing_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_IsPrimary() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_IsPrimary_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_IsSendMailPayment() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_IsSendMailPayment_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_IsSendMailTaxDoc() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_IsSendMailTaxDoc_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement OrgOff_IsSendMailService() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OrgOff_IsSendMailService_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_Edit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_Edit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_Update_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_Update_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_Cancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_Cancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_Addrs1() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_Addrs1_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_Addrs2() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_Addrs2_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_City() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_City_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_State() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_State_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_Zip() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_Zip_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_Country() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_Country_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_IsBilling() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_IsBilling_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_IsShipping() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_IsShipping_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_IsMailing() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_IsMailing_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_IsPrimary() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_IsPrimary_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_IsSendMailPayment() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_IsSendMailPayment_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_IsSendMailTaxDoc() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_IsSendMailTaxDoc_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement DesOff_IsSendMailService() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(DesOff_IsSendMailService_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}





	/////////================================METHOD
	public static void VerifyInfoViewMode_func(Transferee tf)
	{
		String area = ""
		String field_name = ""
		String mode = "View Mode"

		//Common.WaitForElementNotDisplay(LandingPage.LoadingArc())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.HandleElementClick(this.Address_Tab())

		if(LandingPage.Loading_icon()!=null)
			Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

		Common.WaitForElementDisplay(this.OrgRes_Edit_btn())

		WebUI.delay(10)

		///ORG RESIDENCE
		area = "Org Res - "

		field_name = "OrgRes_Addrs1"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgRes_Addrs1(),tf.OriResAddr_info.Addr1, mode)

		field_name = "OrgRes_Addrs2"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgRes_Addrs2(),tf.OriResAddr_info.Addr2 , mode)

		field_name = "OrgRes_City"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgRes_City(),tf.OriResAddr_info.City , mode)

		field_name = "OrgRes_Zip"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgRes_Zip(),tf.OriResAddr_info.Zip , mode)

		field_name = "OrgRes_State"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_State(),tf.OriResAddr_info.State , mode)

		field_name = "OrgRes_Country"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_Country(),tf.OriResAddr_info.Country , mode)

		field_name = "OrgRes_IsBilling"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_IsBilling(),tf.OriResAddr_info.IsAtri_Billing.toString() , mode)

		field_name = "OrgRes_IsShipping"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_IsShipping(),tf.OriResAddr_info.IsAtri_Shipping.toString() , mode)

		field_name = "OrgRes_IsMailing"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_IsMailing(),tf.OriResAddr_info.IsAtri_Mailing.toString() , mode)

		field_name = "OrgRes_IsPrimary"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_IsPrimary(),tf.OriResAddr_info.IsAtri_Primary.toString() , mode)

		field_name = "OrgRes_IsSendMailPayment"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_IsSendMailPayment(),tf.OriResAddr_info.IsSendMailPayment.toString() , mode)

		field_name = "OrgRes_IsSendMailService"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_IsSendMailService(),tf.OriResAddr_info.IsSendMailServices.toString() , mode)

		field_name = "OrgRes_IsSendMailTaxDoc"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgRes_IsSendMailTaxDoc(),tf.OriResAddr_info.IsSendMailTaxDoc.toString() , mode)



		///===================================================DESTINATION OFF
		area = "Des Off - "
		field_name = "DesOff_Addrs1"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesOff_Addrs1(),tf.DesOffAddr_info.Addr1 , mode)

		field_name = "DesOff_Addrs2"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesOff_Addrs2(),tf.DesOffAddr_info.Addr2 , mode)

		field_name = "DesOff_City"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesOff_City(),tf.DesOffAddr_info.City , mode)

		field_name = "DesOff_Zip"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesOff_Zip(),tf.DesOffAddr_info.Zip , mode)

		field_name = "DesOff_State"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_State(),tf.DesOffAddr_info.State , mode)

		field_name = "DesOff_Country"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_Country(),tf.DesOffAddr_info.Country , mode)

		field_name = "DesOff_IsBilling"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_IsBilling(),tf.DesOffAddr_info.IsAtri_Billing.toString() , mode)

		field_name = "DesOff_IsShipping"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_IsShipping(),tf.DesOffAddr_info.IsAtri_Shipping.toString() , mode)

		field_name = "DesOff_IsMailing"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_IsMailing(),tf.DesOffAddr_info.IsAtri_Mailing.toString() , mode)

		field_name = "DesOff_IsPrimary"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_IsPrimary(),tf.DesOffAddr_info.IsAtri_Primary.toString() , mode)

		field_name = "DesOff_IsSendMailPayment"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_IsSendMailPayment(),tf.DesOffAddr_info.IsSendMailPayment.toString() , mode)

		field_name = "DesOff_IsSendMailService"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_IsSendMailService(),tf.DesOffAddr_info.IsSendMailServices.toString() , mode)

		field_name = "DesOff_IsSendMailTaxDoc"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesOff_IsSendMailTaxDoc(),tf.DesOffAddr_info.IsSendMailTaxDoc.toString() , mode)


		//==========================================ORG OFF
		area = "Org Off - "

		field_name = "OrgOff_Addrs1"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgOff_Addrs1(),tf.OriOffAddr_info.Addr1 , mode)

		field_name = "OrgOff_Addrs2"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgOff_Addrs2(),tf.OriOffAddr_info.Addr2 , mode)

		field_name = "OrgOff_City"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgOff_City(),tf.OriOffAddr_info.City , mode)

		field_name = "OrgOff_Zip"
		Common.VerifyFieldValueEqual_func(area+field_name , this.OrgOff_Zip(),tf.OriOffAddr_info.Zip , mode)

		field_name = "OrgOff_State"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_State(),tf.OriOffAddr_info.State , mode)

		field_name = "OrgOff_Country"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_Country(),tf.OriOffAddr_info.Country , mode)

		field_name = "OrgOff_IsBilling"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_IsBilling(),tf.OriOffAddr_info.IsAtri_Billing.toString() , mode)

		field_name = "OrgOff_IsShipping"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_IsShipping(),tf.OriOffAddr_info.IsAtri_Shipping.toString() , mode)

		field_name = "OrgOff_IsMailing"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_IsMailing(),tf.OriOffAddr_info.IsAtri_Mailing.toString() , mode)

		field_name = "OrgOff_IsPrimary"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_IsPrimary(),tf.OriOffAddr_info.IsAtri_Primary.toString() , mode)

		field_name = "OrgOff_IsSendMailPayment"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_IsSendMailPayment(),tf.OriOffAddr_info.IsSendMailPayment.toString() , mode)

		field_name = "OrgOff_IsSendMailService"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_IsSendMailService(),tf.OriOffAddr_info.IsSendMailServices.toString() , mode)

		field_name = "OrgOff_IsSendMailTaxDoc"
		Common.VerifyFieldTextEqual_func(area+field_name , this.OrgOff_IsSendMailTaxDoc(),tf.OriOffAddr_info.IsSendMailTaxDoc.toString() , mode)


		///===================================================DESTINATION RESIDENCE
		area = "Des Res - "
		field_name = "DesRes_Addrs1"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesRes_Addrs1(),tf.DesResAddr_info.Addr1 , mode)

		field_name = "DesRes_Addrs2"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesRes_Addrs2(),tf.DesResAddr_info.Addr2 , mode)

		field_name = "DesRes_City"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesRes_City(),tf.DesResAddr_info.City , mode)

		field_name = "DesRes_Zip"
		Common.VerifyFieldValueEqual_func(area+field_name , this.DesRes_Zip(),tf.DesResAddr_info.Zip , mode)

		field_name = "DesRes_State"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_State(),tf.DesResAddr_info.State , mode)

		field_name = "DesRes_Country"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_Country(),tf.DesResAddr_info.Country , mode)

		field_name = "DesRes_IsBilling"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_IsBilling(),tf.DesResAddr_info.IsAtri_Billing.toString() , mode)

		field_name = "DesRes_IsShipping"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_IsShipping(),tf.DesResAddr_info.IsAtri_Shipping.toString() , mode)

		field_name = "DesRes_IsMailing"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_IsMailing(),tf.DesResAddr_info.IsAtri_Mailing.toString() , mode)

		field_name = "DesRes_IsPrimary"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_IsPrimary(),tf.DesResAddr_info.IsAtri_Primary.toString() , mode)

		field_name = "DesRes_IsSendMailPayment"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_IsSendMailPayment(),tf.DesResAddr_info.IsSendMailPayment.toString() , mode)

		field_name = "DesRes_IsSendMailService"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_IsSendMailService(),tf.DesResAddr_info.IsSendMailServices.toString() , mode)

		field_name = "DesRes_IsSendMailTaxDoc"
		Common.VerifyFieldTextEqual_func(area+field_name , this.DesRes_IsSendMailTaxDoc(),tf.DesResAddr_info.IsSendMailTaxDoc.toString() , mode)

	}




}
