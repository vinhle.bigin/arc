import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.Activity
import objects.Vendor
import pages_elements.ActivityTab
import pages_elements.LoginPage
import pages_elements.VendorPage


Vendor vendor = new Vendor()

LoginPage.Login_func()
Navigation.GotoVendorPage()

vendor.createNewVendor()
vendor.createNewContactVendor()

Activity activity = new Activity()

activity.CreateActivityVendor_func("Task", "Medium")

activity.searchType("Task")

Common.VerifyFieldDisplayed_func("Task icon", ActivityTab.Task_icon())

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
  WebUI.closeBrowser()
}



