import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ActivityTab
import pages_elements.ClientPortal_LoginPage
import pages_elements.DocumentTab
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.NoteTab
import pages_elements.ActivityTab
import pages_elements.TransfGenInfoPage
import pages_elements.TransfereePage
import objects.Activity
import objects.DocumentInfo
import objects.NoteInfo
import objects.Transferee
import objects.User as User
import static org.junit.Assert.*

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT
import pages_elements.Messages

String client_username = ""
String client_pass = ""

Transferee tranf = new Transferee() 

LoginPage.Login_func()

Navigation.GotoTransfereePage()

tranf.Create_func()

//Log into Client Portal
ClientPortal_LoginPage.Login_func(client_username, client_pass)

//Go to Transfree Details screen
Navigation.GotoTransfereePage()

TransfereePage.SearchTransferee_func(tranf)

Navigation.GotoTransDetailsPage(tranf.FullName)

DocumentInfo doc_info = new DocumentInfo()

doc_info.TransfCreate_func()

doc_info.TransfDelete_func()

DocumentTab.VerifyDocNotExistTable_func(doc_info)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
 // WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



