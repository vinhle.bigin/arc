import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.TransfGenInfoPage
import pages_elements.Transf_DomesticTab
import pages_elements.Transf_ServiceTab
import pages_elements.TransfereePage
import pages_elements.Vend_SearchOptTab
import objects.ContactInfo
import objects.Transferee
import objects.User as User
import objects.Vendor

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT


Transferee tranf = new Transferee()

Vendor vend = new Vendor()

vend.SearchOpt_IsUSA = true

ContactInfo vend_contact = new ContactInfo()
vend_contact.VendorName = vend.vendorName
vend_contact.ServiceType = "Van Lines"
vend_contact.AddrInfo.City = ""
vend_contact.AddrInfo.Zip = ""
vend_contact.AddrInfo.State = tranf.OriResAddr_info.State

//STEPS:
LoginPage.Login_func()

//1. Create Vendor,Contact with City Info:
Navigation.GotoVendorPage()

vend.createNewVendor()

vend_contact.CreateContact_func("Submit")


//Setting the SearchOption for vendor is USA only
Vend_SearchOptTab.UpdateSearchOption_fun(vend)


//2. Create new Transf
Navigation.GotoTransfereePage()

tranf.Create_func()

//3. Edit the Origin Residence Address that match with Vendor Address Info: City
tranf.EditAddressInfo_func()

//4. Select HHG Service
tranf.EditService_func()

Transf_DomesticTab.Domestic_tab().click()

Common.SelectDropdownItem(Transf_DomesticTab.Menu_List(), tranf.HHG_service.Type)

String expect_item = vend_contact.VendorName  + " - " + vend_contact.FullName

Common.sendkeyTextToElement(Transf_DomesticTab.Vendor_txt(), vend_contact.FullName)

Common.VerifyItemExistDropdown(Transf_DomesticTab.VendorList_ddl(), expect_item)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true

	GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
   WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



