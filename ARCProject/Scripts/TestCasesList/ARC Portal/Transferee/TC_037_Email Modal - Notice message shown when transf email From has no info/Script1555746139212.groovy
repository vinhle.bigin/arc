import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.DocumentTab
import pages_elements.HeadingMenu
import pages_elements.LandingPage
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.Messages
import pages_elements.NewEmailModal
import pages_elements.NoteTab
import pages_elements.TemplatePage
import pages_elements.TransfGenInfoPage
import pages_elements.Transf_AddressTab
import pages_elements.Transf_DependTab
import pages_elements.Transf_OtherInfoTab
import pages_elements.TransfereePage
import objects.DocumentInfo
import objects.NoteInfo
import objects.Template
import objects.Transferee
import objects.User as User
import static org.junit.Assert.*
import objects.Mailinfo

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.EmailConnect
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT

//=============INIT TEST DATA
String msg = "Unable to process email without a valid Assistant/Counselor email address or selected assistant/Counselor"
Transferee transf = new Transferee()


Template newtpl = new Template()
newtpl.EmailFrom = "Assistant"

EmailConnect mail_connector = new EmailConnect("Gmail")

//================

//Login to ARC
LoginPage.Login_func()

//Go to Template Page
Navigation.GotoTemplatePage()

//Create Template with Transferee General Field Code
newtpl.Create_func()

//Go to Transferee page
Navigation.GotoTransfereePage()

transf.Create_func()

//Send Email to Transfree
NewEmailModal modal = new NewEmailModal()
transf.MailList.get(0).AssignTemplateToEmail_func(newtpl,true)

Common.HandleElementClick(HeadingMenu.Email_btn())
	

Common.WaitForElementDisplay(NewEmailModal.Service_ddl())


if(LandingPage.Loading_icon()!=null)
Common.WaitForElementNotDisplay(LandingPage.Loading_icon_xpath)

if(LandingPage.LoadingArc()!=null)
Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

Common.WaitForElementDisplay(modal.Service_ddl())

//Select Recipienttype
Common.SelectDropdownItem( modal.Recipient_ddl(),transf.MailList.get(0).RecipientType)

WebUI.delay(3)

//Select Service type
Common.SelectDropdownItem( modal.Service_ddl(),transf.MailList.get(0).ServiceType)

WebUI.delay(2)

Common.WaitForElementNotDisplay(LandingPage.LoadingArc_xpath)

Common.WaitForElementDisplay(modal.Template_ddl())

//Select Template Name
Common.SelectDropdownItem(modal.Template_ddl(),transf.MailList.get(0).TemplateName)

//Select Contact
modal.ContactSelect_chk(transf.MailList.get(0).EmailAddr).click()

//Click Next button
modal.Next_btn().click()

Messages.VerifyPopUpMessageDisplay_func(msg)

Messages.Close_btn().click()

//Verify Modal is closed

Common.VerifyFieldNotDisplayed_func("Invalid Process Email Msg modal", Messages.PopUpMessage())

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
 // WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



