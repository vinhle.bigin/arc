import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ActivityTab
import pages_elements.DocumentTab
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.NoteTab
import pages_elements.ActivityTab
import pages_elements.TransfGenInfoPage
import pages_elements.TransfereePage
import objects.Activity
import objects.Client
import objects.DocumentInfo
import objects.NoteInfo
import objects.Transferee
import objects.User as User
import static org.junit.Assert.*

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.FileAccess
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT
import pages_elements.Messages

//==TEST DATA
String download_filename = "DownloadFile_doc.docx"
String Expect_content = "Document's Testing Content"

DocumentInfo doc_info = new DocumentInfo()


FileAccess file_access = new FileAccess()
file_access.CreateWordFile_func(doc_info.file_name,Expect_content)

//=========================================
//PRE-CONDITION: CHANGE DOWNLOAD SETTING IN FIREFOX BROWSER
Common.ChangeFFDownloadSetting_func()

Client client = new Client() 

LoginPage.Login_func()

Navigation.GotoClientPage()

client.CreateClient_func()

//Navigation.GoToBrowserTab()

doc_info.TransfCreate_func()

if(GlobalVariable.glb_TCStatus==true)
{
	DocumentTab.Search_func(doc_info.Name)
	
	WebUI.delay(2)
	
	Common.DownloadFile_func(DocumentTab.DownLoad_icon(doc_info.Name), download_filename)
	
	file_access.VerifyFileContent_func(download_filename, Expect_content)
	
	
}
	

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
 // WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



