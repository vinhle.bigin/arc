import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.DocumentTab
import pages_elements.HeadingMenu
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.NewEmailModal
import pages_elements.NoteTab
import pages_elements.TemplatePage
import pages_elements.TransfGenInfoPage
import pages_elements.Transf_AddressTab
import pages_elements.Transf_DependTab
import pages_elements.Transf_OtherInfoTab
import pages_elements.TransfereePage
import objects.Client
import objects.ContactInfo
import objects.DocumentInfo
import objects.NoteInfo
import objects.Template
import objects.Transferee
import objects.User as User
import static org.junit.Assert.*
import objects.Mailinfo

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.EmailConnect
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT

//=============INIT TEST DATA
Client client = new Client() 
ContactInfo new_ctact = new ContactInfo()
new_ctact.ClientName = client.CompanyName
new_ctact.MailList.get(0).EmailAddr = GlobalVariable.glb_usermail

Transferee transf = new Transferee()
//ADD the client to transferee
transf.ClientName = new_ctact.ClientName
transf.ClientContactFullName = new_ctact.FullName

EmailConnect mail_connector = new EmailConnect("Gmail")

Template newtpl = new Template()
newtpl.Target = "Client"
newtpl.ServiceType = "Transferee"
//================


LoginPage.Login_func()

//CREATE TEMPLATE FOR CLIENT

//Go to Template Page
Navigation.GotoTemplatePage()

//Create Template with Transferee General Field Code
newtpl.Create_func()

//Go to Client Page
Navigation.GotoClientPage()

//Create new client
client.CreateClient_func()

//Create new contact
new_ctact.CreateContact_func()

//Go to Transferee page
Navigation.GotoTransfereePage()

transf.Create_func()

//Edit Service to the correct
transf.EditService_func()

//Send Email to Transfree
new_ctact.MailList.get(0).AssignTemplateToEmail_func(newtpl,true)

HeadingMenu.CreateEmail_func(new_ctact.MailList.get(0), "Submit")

Mailinfo data_mail = new_ctact.MailList.get(0)

//GET THE SENT EMAIL
WebUI.delay(60)
mail_connector.FetchEmail_func()

Mailinfo receivemail = mail_connector.FindExistEmail(data_mail)

receivemail.VerifyContentEmail(data_mail)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
 // WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



