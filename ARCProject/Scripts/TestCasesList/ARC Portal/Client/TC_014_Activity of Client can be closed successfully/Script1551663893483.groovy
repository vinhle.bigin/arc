import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ActivityTab
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.ActivityTab
import pages_elements.TransfGenInfoPage
import pages_elements.TransfereePage
import objects.Activity
import objects.Client
import objects.ContactInfo
import objects.Transferee
import objects.User as User
import static org.junit.Assert.*

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT

Client client = new Client() 
ContactInfo new_ctact = new ContactInfo()
new_ctact.ClientName = client.CompanyName

LoginPage.Login_func()

Navigation.GotoClientPage()

client.CreateClient_func()

//Create new contact
new_ctact.CreateContact_func()

Activity act_1 = new Activity()
act_1.Type = "Follow Up"
act_1.ScheduleWith = new_ctact.FullName


Activity act_2 = new Activity()
act_2.Type = "Follow Up"
act_2.ScheduleWith = new_ctact.FullName
act_2.Closed = true

act_1.CreateActivity_func("Client","Submit")

ActivityTab.SearchActivity_func(act_1.StartDate)

act_2.EditActivity_func(act_1.Title)


List<String> record = Common.GetTableRecordsPerPaging_func(ActivityTab.ActivityList_tbl())

	if(!record.get(0).contains(act_2.Closed.toString()))
	{
		GlobalVariable.glb_TCStatus = false
		GlobalVariable.glb_TCFailedMessage +="Activity Record - Closed Status is not correct.[Observed:'"+record.get(0)+"' - Expected: '"+act_1.Closed.toString()+"'].\n"
	}

ActivityTab.OpenDetailsModal_func(act_2)

Common.VerifyFieldTextEqual_func("Closed Status", ActivityTab.Close_opt(),act_2.Closed.toString(), " - For Closed Activity")

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
   //WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



