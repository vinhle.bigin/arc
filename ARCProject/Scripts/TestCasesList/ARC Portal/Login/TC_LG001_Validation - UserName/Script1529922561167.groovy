import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal
import pages_elements.LoginPage



WebUI.comment('PRECONDITON: CREATE NEW INACTIVE USER')
User user = new User()
WebUI.callTestCase(findTestCase('Method/Signup/SignupUser'), [('referal_url') : '', ('firstname') : user.firstname, ('lastname') : user.lastname
	,('username'):user.username, ('email') : user.email, ('password') : user.password, ('phone') : user.phone], FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser()

WebUI.openBrowser(GlobalVariable.glb_URL)

WebUI.comment('VERIFY VALIDATION FOR INPUT: INACTIVE USER')

validation_msg = 'The selected name is invalid or the account is not active.'

WebUI.callTestCase(findTestCase('Method/Login/TestValidationUsername'), [('input_str') : user.username, ('Expected_msg') : validation_msg],
	FailureHandling.CONTINUE_ON_FAILURE)


WebUI.comment('VERIFY VALIDATION FOR INPUT: EMPTY')

user.username = ''

validation_msg = 'The username field is required.'

WebUI.callTestCase(findTestCase('Method/Login/TestValidationUsername'), [('input_str') : user.username, ('Expected_msg') : validation_msg], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('VERIFY VALIDATION FOR INPUT: Not Existing username')

user.username = 'NotExistedUsername'

validation_msg = 'The selected name is invalid or the account is not active.'

WebUI.callTestCase(findTestCase('Method/Login/TestValidationUsername'), [('input_str') : user.username, ('Expected_msg') : validation_msg], 
    FailureHandling.CONTINUE_ON_FAILURE)


WebUI.comment('VERIFY VALIDATION FOR INPUT: Valid user')

user.username = GlobalVariable.glb_adminuser

validation_msg = ''

WebUI.callTestCase(findTestCase('Method/Login/TestValidationUsername'), [('input_str') : user.username, ('Expected_msg') : validation_msg],
	FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('VERIFY VALIDATION FOR INPUT: valid username, incorrect password')

user.username = GlobalVariable.glb_adminuser

validation_msg = ' These credentials do not match our records.'
LoginPage currentpage = new LoginPage()
currentpage.Password_txt().sendKeys("incorrectpass")

WebUI.callTestCase(findTestCase('Method/Login/TestValidationUsername'), [('input_str') : user.username, ('Expected_msg') : validation_msg],
	FailureHandling.CONTINUE_ON_FAILURE)

WebUI.closeBrowser()


Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

