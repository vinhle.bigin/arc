import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.Activity
import objects.Client
import objects.ContactInfo
import objects.Transferee
import objects.Vendor
import pages_elements.ActivityTab
import pages_elements.HeadingMenu
import pages_elements.LoginPage
import pages_elements.TaskListPage
import pages_elements.VendorPage


Transferee transf = new Transferee()

Activity activity = new Activity()
activity.Type = "Task"
activity.AssigneeType = "Transf"
activity.Assignee = transf.FullName

LoginPage.Login_func()

Navigation.GotoTransfereePage()

transf.Create_func()

Navigation.GotoTaskListPage()

TaskListPage.CreateNewActivity_func(activity, "Submit")

TaskListPage.VerifyActivityExistTable_func(activity)

TaskListPage.SearchActivity_func("","",activity.Title)

TaskListPage.VerifyModalDetailsViewMode_func(activity)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
  WebUI.closeBrowser()
}



