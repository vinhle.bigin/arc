import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.Activity
import objects.Client
import objects.ContactInfo
import objects.Vendor
import pages_elements.ActivityTab
import pages_elements.HeadingMenu
import pages_elements.LoginPage
import pages_elements.TaskListPage
import pages_elements.VendorPage


Client client = new Client()

ContactInfo contact = new ContactInfo()
contact.ClientName = client.CompanyName

Activity activity = new Activity()
activity.ScheduleWith = contact.FullName
activity.Type = "Follow"
activity.AssigneeType = "Client"
activity.Assignee = client.CompanyName

LoginPage.Login_func()

Navigation.GotoClientPage()

client.CreateClient_func()

contact.CreateContact_func()

Navigation.GotoTaskListPage()

TaskListPage.CreateNewActivity_func(activity, "Submit")

TaskListPage.VerifyActivityExistTable_func(activity)

TaskListPage.SearchActivity_func("","",activity.Title)

TaskListPage.VerifyModalDetailsViewMode_func(activity)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
  WebUI.closeBrowser()
}



