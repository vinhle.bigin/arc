import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.Activity
import objects.Transferee
import objects.Vendor
import pages_elements.ActivityTab
import pages_elements.LoginPage
import pages_elements.TaskListPage
import pages_elements.VendorPage


Transferee transf = new Transferee()

Activity activity = new Activity()
activity.Type = "Follow"
activity.AssigneeType = "Transf"
activity.Assignee = transf.FullName

Activity activity2 = new Activity()
activity2.Type = "Follow"
activity2.AssigneeType = "Transf"
activity2.Assignee = transf.FullName

LoginPage.Login_func()

Navigation.GotoTransfereePage()

transf.Create_func()

Navigation.GotoTaskListPage()

TaskListPage.CreateNewActivity_func(activity, "Submit")

//Update the Activity
TaskListPage.SearchActivity_func("","",activity.Title)

TaskListPage.EditActivity_func(activity.Title,activity2,"Submit")

//Verify the Updated activity info is shown on List
TaskListPage.SearchActivity_func("","",activity2.Title)

TaskListPage.VerifyActivityExistTable_func(activity2)

//Verify The updated activity modal details
TaskListPage.VerifyModalDetailsViewMode_func(activity2)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
  WebUI.closeBrowser()
}



