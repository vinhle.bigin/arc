import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.Activity
import objects.ContactInfo
import objects.Vendor
import pages_elements.ActivityTab
import pages_elements.LoginPage
import pages_elements.TaskListPage
import pages_elements.VendorPage


Vendor vendor = new Vendor()

ContactInfo contact = new ContactInfo()
contact.VendorName = vendor.vendorName

Activity activity = new Activity()
activity.ScheduleWith = contact.FullName
activity.Type = "Task"
activity.AssigneeType = "Vendor"
activity.Assignee = vendor.vendorName

Activity activity2 = new Activity()
activity2.ScheduleWith = contact.FullName
activity2.Type = "Task"
activity2.AssigneeType = "Vendor"
activity2.Assignee = vendor.vendorName

LoginPage.Login_func()

Navigation.GotoVendorPage()

vendor.createNewVendor()

contact.CreateContact_func()

Navigation.GotoTaskListPage()

TaskListPage.CreateNewActivity_func(activity, "Submit")

//Verify the Create activity info is shown on List
TaskListPage.SearchActivity_func("","",activity.Title)

//Close the Activity
activity.Closed  = true
TaskListPage.SearchActivity_func("","",activity.Title)

TaskListPage.EditActivity_func(activity.Title,activity,"Submit")


//Verify the Closed Activity cannot be Updated activity info is shown on List
TaskListPage.SearchActivity_func("","",activity.Title)

TaskListPage.OpenDetailsModal_func(activity)

Common.VerifyFieldNotDisplayed_func("Edit button", TaskListPage.ModalEdit_btn(), " - For Closed Activity")

TaskListPage.FillModalDetails_func(activity2)

TaskListPage.VerifyModalDetailsViewMode_func(activity,"Closed Activity")


Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
 // WebUI.closeBrowser()
}



