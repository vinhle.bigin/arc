import org.junit.Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Common
import functions.Navigation
import internal.GlobalVariable
import objects.Activity
import objects.Vendor
import pages_elements.ActivityTab
import pages_elements.LoginPage
import pages_elements.VendorDetail

Vendor vendor = new Vendor()

LoginPage.Login_func()
Navigation.GotoVendorPage()

vendor.createNewVendor()

Navigation.GotoActivityTab_func()

Common.HandleElementClick(ActivityTab.newActivity_btn())

WebUI.delay(2)
Common.HandleElementClick(ActivityTab.submitNewActivity_btn())

Activity.VerifyValidationOnNewActivityModal()


Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true
	GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
   WebUI.closeBrowser()
}





