import org.junit.Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable
import objects.Vendor
import pages_elements.LoginPage
import pages_elements.VendorPage

Vendor vendor = new Vendor()
Vendor vendor2 = new Vendor()

LoginPage.Login_func()
Navigation.GotoVendorPage()

vendor.createNewVendor()
vendor.createNewContactVendor()
vendor2.createNewContactVendor()
vendor2.deleteContact()
vendor2.VerifyVendorNOTExistTable_func(vendor2.fullContactName)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true
	GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
   WebUI.closeBrowser()
}





