import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.NoteInfo
import objects.Vendor
import pages_elements.HeadingMenu
import pages_elements.LoginPage
import pages_elements.NoteTab
import pages_elements.VendorPage


Vendor vendor = new Vendor()

LoginPage.Login_func()
Navigation.GotoVendorPage()

vendor.createNewVendor()
vendor.createNewContactVendor()

NoteInfo note = new NoteInfo()
HeadingMenu.CreateVendorNote_func(note)
NoteTab.VerifyNoteExistTable_func(note)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
	
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
  WebUI.closeBrowser()
}



