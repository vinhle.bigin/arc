import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.DocumentInfo
import objects.Vendor
import pages_elements.DocumentTab
import pages_elements.LoginPage
import pages_elements.VendorPage


Vendor vendor = new Vendor()

LoginPage.Login_func()
Navigation.GotoVendorPage()

vendor.createNewVendor()

DocumentInfo doc_info = new DocumentInfo()

doc_info.CreateNewDocumentVendor_func()

doc_info.DeleteDocumentVendor_func()

DocumentTab.VerifyDocNotExistTableVendor_func(doc_info)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
  WebUI.closeBrowser()
}



