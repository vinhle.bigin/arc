import static org.junit.Assert.*

import org.junit.Assert as Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable as GlobalVariable
import objects.History
import objects.Vendor
import pages_elements.LoginPage
import pages_elements.VendorPage


Vendor vendor = new Vendor()

LoginPage.Login_func()
Navigation.GotoVendorPage()

vendor.createNewVendor()
vendor.createNewContactVendor()
vendor.editGeneralInfoVendor()

Navigation.GotoHistoryTab_func()

History history = new History()

history.searchHistoryDate()

history.VerifyHistoryVendorExistTable_func("Search Date", history.StartDate )

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true
    GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
  WebUI.closeBrowser()
}



