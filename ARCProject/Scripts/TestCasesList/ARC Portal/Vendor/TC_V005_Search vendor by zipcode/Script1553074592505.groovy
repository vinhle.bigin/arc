import org.junit.Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable
import pages_elements.LoginPage
import pages_elements.VendorPage
import objects.Vendor

LoginPage.Login_func()
Navigation.GotoVendorPage()

Vendor vendor = new Vendor()
vendor.createNewVendor()
vendor.createNewContactVendor()
Navigation.GotoVendorPage()

vendor.searchVendor(vendor.zipCode, VendorPage.SearchZipcode_num())
vendor.VerifyVendorExistTable_func("Search Zip Code Vendor", vendor.zipCode)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true
	GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
   WebUI.closeBrowser()
}