import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.annotation.TearDown
import functions.Common
import pages_elements.LoginPage
import pages_elements.VendorPage


LoginPage.Login_func() 
Common.HandleElementClick(VendorPage.AvatarUser())
Common.HandleElementClick(VendorPage.UserLogout())

@TearDown
void AfterTest() {
   WebUI.closeBrowser()
}



