import org.junit.Assert

import com.kms.katalon.core.annotation.SetUp
import com.kms.katalon.core.annotation.TearDown
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import functions.Navigation
import internal.GlobalVariable
import objects.NoteInfo
import objects.Vendor
import pages_elements.LoginPage
import pages_elements.VendorPage

LoginPage.Login_func()
Navigation.GotoVendorPage()

Vendor vendor = new Vendor()
vendor.createNewVendor()

NoteInfo note_info = new NoteInfo()
note_info.clearAllDataInSearchbox()
note_info.verifyEmptyDatainSearchbox()

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true
	GlobalVariable.glb_TCFailedMessage = ''
}

@TearDown
void AfterTest() {
   WebUI.closeBrowser()
}

