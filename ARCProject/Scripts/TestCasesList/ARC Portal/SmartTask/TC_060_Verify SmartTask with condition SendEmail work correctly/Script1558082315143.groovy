import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ActivityTab
import pages_elements.HeadingMenu
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.SmartTaskPage
import pages_elements.ActivityTab
import pages_elements.TransfGenInfoPage
import pages_elements.TransfereePage
import objects.Activity
import objects.Mailinfo
import objects.SmartTask
import objects.Template
import objects.Transferee
import objects.User as User
import static org.junit.Assert.*

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.EmailConnect
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT


Template newtpl = new Template()
Transferee tranf = new Transferee()
tranf.MailList.get(0).EmailAddr = GlobalVariable.glb_usermail
tranf.MailList.get(0).AssignTemplateToEmail_func(newtpl,true)

SmartTask newtask = new SmartTask()
newtask.IsEmailProcess_opt = true
newtask.action.InitUpdateFieldAction_func("Transferee - Last Name", "value_from_smarTTask")
newtask.SetEmailProcessCondt_func(newtpl.Name)

EmailConnect mail_connector = new EmailConnect("Gmail")

//==Pre-Conditioin: Create EMail template
LoginPage.Login_func()

Navigation.GotoTemplatePage()

newtpl.Create_func("Submit")

//===================Steps
newtask.Create_func("Submit")

//2. Create new Transferee:
Navigation.GotoTransfereePage()

tranf.Create_func()

//Send Email to Transfree
HeadingMenu.CreateEmail_func(tranf.MailList.get(0), "Submit")

Mailinfo data_mail = tranf.MailList.get(0)

//GET THE SENT EMAIL
WebUI.delay(60)
mail_connector.FetchEmail_func()

Mailinfo receivemail = mail_connector.FindExistEmail(data_mail)

if(receivemail!=null)
{
	tranf.lastname = newtask.action.Act_ValueTo
	TransfGenInfoPage.Generall_tab().click()
	
	TransfGenInfoPage.VerifyGeneralInfoViewMode_func(tranf)
}

else
{
	  GlobalVariable.glb_TCStatus = false

	  GlobalVariable.glb_TCFailedMessage += "Cannot verify smarttask due to: Email is not received.\n"
}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
   //WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



