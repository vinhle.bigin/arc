import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User
import pages_elements.UserProfilePage 
import org.junit.Assert as Assert

import org.openqa.selenium.WebDriver
import org.openqa.selenium.JavascriptExecutor
import com.kms.katalon.core.webui.driver.DriverFactory


User user = new User()
WebUI.comment('STEP#1: lOG INTO SITE')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('username_str') : GlobalVariable.glb_username, ('password_str') : GlobalVariable.glb_password], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.comment('STEP#2: GO TO USER PROFILE PAGE')

WebUI.callTestCase(findTestCase('Method/Navigation/GotoUserProfile'), [:], FailureHandling.STOP_ON_FAILURE)

UserProfilePage currentpage = new UserProfilePage()

WebUI.comment('CLICK ON EDIT ADDRESS ICON')
currentpage.AddressEditGear_icon().click()
WebUI.delay(2)


WebUI.comment("SCROLL TO ADDRESS FORM ")
WebDriver driver = DriverFactory.getWebDriver()

((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", currentpage.AddressUpdate_btn());
WebUI.delay(2)

WebUI.comment('VERIFY ADDRESS1 IS ENABLE')
println(currentpage.Address1_txt().getText())
status = currentpage.IsTextFieldEditable(currentpage.Address1_txt(), "abc")
if(status == false)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Address1 should enable.\n"
}

println(currentpage.Address1_txt().getText())

WebUI.comment('VERIFY ADDRESS2 IS ENABLE')
status = currentpage.IsTextFieldEditable(currentpage.Address2_txt(), "abc")
if(status == false)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Address2 should enable.\n"
}

WebUI.comment('VERIFY CITY IS ENABLE')
status = currentpage.IsTextFieldEditable(currentpage.City_txt(), "abc")
if(status == false)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The CITY should enable.\n"
}

WebUI.comment('VERIFY STATE IS ENABLE')
status = currentpage.IsTextFieldEditable(currentpage.State_txt(), "1507")
if(status == false)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The STATE should enable.\n"
}

WebUI.comment('VERIFY POSTAL IS ENABLE')
status = currentpage.IsTextFieldEditable(currentpage.PostalCode_txt(), "98989")
if(status == false)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The POSTAL should enable.\n"
}

WebUI.closeBrowser()

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}
