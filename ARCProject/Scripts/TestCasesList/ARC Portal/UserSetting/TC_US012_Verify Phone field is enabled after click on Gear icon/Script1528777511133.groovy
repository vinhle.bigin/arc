import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User
import pages_elements.UserProfilePage 
import org.junit.After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

WebUI.comment('STEP#1: lOG INTO SITE')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('username_str') : GlobalVariable.glb_username, ('password_str') : GlobalVariable.glb_password], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.comment('STEP#2: GO TO USER PROFILE PAGE')

WebUI.callTestCase(findTestCase('Method/Navigation/GotoUserProfile'), [:], FailureHandling.STOP_ON_FAILURE)
WebUI.delay(1)

UserProfilePage currentpage = new UserProfilePage()


WebUI.comment('STEP#3: CLICK ON GENERAL EDIT ICON')
currentpage.GeneralEditGear_icon().click()
WebUI.delay(1)

currentpage.Phone_txt().clear()

WebDriver webDriver = DriverFactory.getWebDriver();
Actions actions = new Actions(webDriver);
Action action = actions.sendKeys(currentpage.Phone_txt() ,"123456789").build();
action.perform();


if(currentpage.Phone_txt().isEnabled()!=true)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Phone should enable after click on Edit icon.\n"
}

String Observed_str = currentpage.Phone_txt().getAttribute("value")

WebUI.callTestCase(findTestCase('Method/CoreMethod/VerifyFieldEditable'), [('fieldname_str') : 'Phone', ('observed_str') : Observed_str
	, ('expected_str') : '123456789'], FailureHandling.CONTINUE_ON_FAILURE)


/*
WebUI.comment('STEP#4: VERIFY THE FIRSTNAME VALUE IS CORRECT')
User user = new User()
user.username = GlobalVariable.glb_username
user.GetUserInfoFromDB()

if(user.userId != currentpage.Id_txt().getAttribute("value"))
{
	GlobalVariable.glb_TCStatus = false
	GlobalVariable.glb_TCFailedMessage += "Incorrect UserID. Observed: "+currentpage.Id_txt().getAttribute("value")+"Expected: "+user.userId+".\n"
}
*/
WebUI.closeBrowser()

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}
