import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LeftMenu as LeftMenu
import pages_elements.Messages as Messages
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import pages_elements.ManageUserLanding as ManageUserLanding
import pages_elements.EditUser
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.junit.Assert as Assert


WebUI.comment('PRE-CONDITON: SIGNUP NEW USER')

	User newuser = new User()

	WebUI.callTestCase(findTestCase('Method/Signup/SignupUser'), [('referal_url') : '', ('firstname') : newuser.firstname, ('lastname') : newuser.lastname
		, ('username') : newuser.username, ('email') : newuser.email, ('password') : newuser.password, ('phone') : newuser.phone],
	FailureHandling.STOP_ON_FAILURE)


WebUI.comment('Log into site')

	WebUI.callTestCase(findTestCase('Method/Login/Login'), [('password_str') : GlobalVariable.glb_adminpassword, ('username_str') : GlobalVariable.glb_adminuser], 
		FailureHandling.OPTIONAL)

WebUI.comment('NAVIGATE TO MANAGE USER LANDING PAGE')
	
	WebUI.callTestCase(findTestCase('Method/Navigation/GotoLandingManageUser'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('SEARCH USER')
	
//	WebUI.callTestCase(findTestCase('Method/ManageUser/SearchUser'), [('email_par') : '', ('username_par') : newuser.username], FailureHandling.STOP_ON_FAILURE)


WebUI.comment('DELETE USER')
WebUI.callTestCase(findTestCase('Method/ManageUser/DeleteUser'), [('username_str') : newuser.username], FailureHandling.STOP_ON_FAILURE)

	

WebUI.comment('Verify Error Message is shown')
msg_str = 'User does not exist'
//'Verify Point: verify the message Opps is shown that makes test case failed'

Messages msg = new Messages() 	
ManageUserLanding landingpage = new ManageUserLanding()
	
	if(msg.SuccessSubmitMessage().getText()==msg_str)
	
	{
		GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage +="Error message '"+msg_str+"' should not be shown when delete user. "
		
		WebUI.comment('Click on Ok button on Pop-up')
		
		//landingpage.OppsOk_btn().click()
		
	}

WebUI.comment('CHECK USER IS FOUND')

result = WebUI.callTestCase(findTestCase('Method/ManageUser/IsUserFound'), [('Expected_str') : newuser.username], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('VERIFY POINT: VERIFY THE DELETED USER SHOULD NOT BE FOUND')

if (result == true) {
	
	GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage+='The deleted user can still be found. '
    
}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true

	GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
   WebUI.closeBrowser()
}
