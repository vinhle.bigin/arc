import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.DocumentTab
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.NoteTab
import pages_elements.TemplatePage
import pages_elements.TransfGenInfoPage
import pages_elements.Transf_AddressTab
import pages_elements.Transf_DependTab
import pages_elements.Transf_OtherInfoTab
import pages_elements.TransfereePage
import objects.DocumentInfo
import objects.NoteInfo
import objects.Template
import objects.Transferee
import objects.User as User
import static org.junit.Assert.*

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT
import objects.AddressTemplate


LoginPage.Login_func()

Template newtpl1 = new Template()
newtpl1.Target = "Client"
Template newtpl2 = new Template()
newtpl2.Target = "Transferee"
Template newtpl3 = new Template()
newtpl3.Target = "Vendor"
List<Template> list = new ArrayList<>()

list.add(newtpl1)

list.add(newtpl2)

list.add(newtpl3)

Navigation.GotoTemplatePage()

for(int i =0;i<list.size();i++)
	list.get(i).Create_func()
	
TemplatePage.Search_func("Target", newtpl1.Target)
	
TemplatePage.VerifyTemplateExistTable_func(newtpl1, "when Search Type: "+newtpl1.Target)

TemplatePage.VerifyTemplateNotExistTable_func(newtpl2,"when Search Type: "+newtpl1.Target)

TemplatePage.VerifyTemplateNotExistTable_func(newtpl3,"when Search Type: "+newtpl1.Target)

//Click Clear button
TemplatePage.SearchClear_btn().click()

TemplatePage.VerifyTemplateExistTable_func(newtpl1, "when Clear Search Type: "+newtpl1.Target)

TemplatePage.VerifyTemplateExistTable_func(newtpl2,"when Clear Search Type: "+newtpl1.Target)

TemplatePage.VerifyTemplateExistTable_func(newtpl3,"when Clear Search Type: "+newtpl1.Target)



Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)
@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true

	GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



