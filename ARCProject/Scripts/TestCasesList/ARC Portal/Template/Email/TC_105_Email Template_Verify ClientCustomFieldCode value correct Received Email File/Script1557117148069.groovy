import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.CustomFieldTab
import pages_elements.DocumentTab
import pages_elements.HeadingMenu
import pages_elements.LeftMenu as LeftMenu
import pages_elements.LoginPage
import pages_elements.NewEmailModal
import pages_elements.NoteTab
import pages_elements.TemplatePage
import pages_elements.TransfGenInfoPage
import pages_elements.Transf_AddressTab
import pages_elements.Transf_DependTab
import pages_elements.Transf_OtherInfoTab
import pages_elements.TransfereePage
import objects.Client
import objects.ContactInfo
import objects.CustomField
import objects.CustomGroup
import objects.DocumentInfo
import objects.Mailinfo
import objects.NoteInfo
import objects.Template
import objects.Transferee
import objects.User as User
import objects.Vendor

import static org.junit.Assert.*

import org.eclipse.persistence.jpa.jpql.parser.DatetimeExpressionBNF
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.EmailConnect
import functions.FileAccess
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT


//===TEST DATA INIT
CustomGroup group = new CustomGroup()
group.FieldType = "Transferee"

CustomField customfield = new CustomField()
customfield.AssignGroup_func(group)

Client client = new Client()
ContactInfo new_ctact = new ContactInfo()
new_ctact.ClientName = client.CompanyName
new_ctact.MailList.get(0).EmailAddr = GlobalVariable.glb_usermail

Transferee transf = new Transferee()

Template newtpl = new Template()
newtpl.DocName = "Email_Template.docx"

Mailinfo data_mail = new_ctact.MailList.get(0)

data_mail.RenewContent_CustomField_func(customfield)

EmailConnect mail_connector = new EmailConnect("Gmail")

//=========================Login to ARC
LoginPage.Login_func()

//CREATE CUSTOMFIELD FOR TRANSFEREE
Navigation.GotoCustomFieldPage()

group.CreateGroup_func()

customfield.Create_func()

//Create Content_str with Field Code
customfield.GetFieldID_func()

//ADD FIELD CODE TO CONTENT OF TEMPLATE
newtpl.AddFieldCode_CustomField_func(customfield)

FileAccess file_access = new FileAccess()
file_access.CreateWordFile_func(newtpl.DocName, newtpl.Content)

//CREAT TEMPLATE FIELD
Navigation.GotoTemplatePage()

//Create Template with Transferee General Field Code
newtpl.Create_func()

//CREATE CLIENT, CLIENT CONTACT
Navigation.GotoClientPage()

//Create new client
client.CreateClient_func()

//Create new contact
new_ctact.CreateContact_func()

//UPDATE CLIENT - CUSTOM FIELD INFO
CustomFieldTab.EditCustomField_func(customfield)

//CREATE NEW TRANSFREE
Navigation.GotoTransfereePage()

transf.Create_func()

//Send Email to Transfree
data_mail.AssignTemplateToEmail_func(newtpl,false)

HeadingMenu.CreateEmail_func(data_mail, "Submit")

//GET THE SENT EMAIL
WebUI.delay(60)
mail_connector.FetchEmail_func()

Mailinfo receivemail = mail_connector.FindExistEmail(data_mail)

//VERIFY ATTACHMENT CONTENT
receivemail.VerifyAttachmentContent_func(data_mail.Content)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
  WebUI.closeBrowser()

 //   DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



