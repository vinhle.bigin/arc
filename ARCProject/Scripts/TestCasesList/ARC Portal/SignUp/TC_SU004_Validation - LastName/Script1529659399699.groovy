import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.LoginPage
User user = new User()

WebUI.openBrowser(GlobalVariable.glb_URL)

LoginPage loginpage = new LoginPage()
loginpage.Signup_link().click()
WebUI.delay(5)


WebUI.comment('VERIFY VALIDATION FOR INPUT: EMPTY')

user.lastname = ''

validation_msg = 'Last Name is required.'

WebUI.callTestCase(findTestCase('Method/SignUp/TestValidationLastName'), [('input_str') : user.lastname, ('Expected_msg') : validation_msg], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('VERIFY VALIDATION FOR INPUT: <2 CHARS')

user.lastname = 'a'

validation_msg = 'The last name must be at least 2 characters.'

WebUI.callTestCase(findTestCase('Method/SignUp/TestValidationLastName'), [('input_str') : user.lastname, ('Expected_msg') : validation_msg], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('VERIFY INPUT = VALID VALUE')

user.lastname = 'updatedlastname'

WebUI.callTestCase(findTestCase('Method/SignUp/TestValidationLastName'), [('input_str') : user.lastname, ('Expected_msg') : ''], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.closeBrowser()

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

