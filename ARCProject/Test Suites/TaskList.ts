<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TaskList</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>8725d7af-cae3-4b76-a7aa-9a1fc6da4933</testSuiteGuid>
   <testCaseLink>
      <guid>7a62996a-4b27-4158-a06c-1d29bfd3612e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V037_The closed Activity of vendor cannot be editted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4984fb8b-e226-406b-b93a-7350bebd86c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V001_Validation message when Task modal is empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b2b2635-7d1d-4f56-a0d7-e91f8696c752</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V003_New Task Activity can be added successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dfdae361-473f-41e7-95e0-d15aff7c7c78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V035_Follow Up Activity can be eidited for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e61912b3-d7e8-41f7-99e4-4c99af5f39e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V036_The Activity of vendor can be closed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cee2b8eb-58db-433b-99df-4cb0916042f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V039_3_The Activity can be deleted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc4ab3c2-976a-4877-8a95-864666823147</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V039_1_The Activity can be searched by priority</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfe10ce5-6272-4884-873e-c88bcd248630</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V039_The Activity can be searched by type</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f198c2c-05c6-4245-81ea-27e41e02b9c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V039_2_The Activity can be searched by status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5b33494-b7b1-4e92-9a96-978d0b1a9a6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V004_New Follow Up Activity can be added successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47626fa0-d275-46d1-935c-79dd42d5f25d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V039_4_User can clear all data in Search box of Activity tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a2277dc-d4a7-4ae6-b0fa-1862cc6884b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V032_New Task Activity can be added for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dbf1fd5-e7c5-4718-ad86-39bafb964f19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V002_Validation message when Follow Up modal is empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>285646af-c5b8-40bd-afde-31997209d699</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V034_Task Activity can be eidited for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>653a706d-2d5c-43cb-9e84-b55a4ce87aeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Task List/TC_V038_The Activity can be searched by date</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
