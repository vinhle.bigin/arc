<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Client</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>2a07fa41-c82a-4fd0-b606-2aa7e8b22b21</testSuiteGuid>
   <testCaseLink>
      <guid>0b60c313-170b-4063-ab4c-e7b4c5867bc1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_048_Default Address is generated after client is created</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6087fc38-45fd-4900-9100-6e4f40289838</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_047_Address can be removed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6a52469-f3ef-4506-8905-3199957d3578</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_006_Contact Client can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0d13dc8-be89-456b-b1be-a10766086946</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_032_Note can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fc5d190-af72-48c0-b93d-2b5efc059b0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_033_Note can be deleted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ac92d1a-9bb2-4ece-a113-b2347339b0f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_001_New Client can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab677fa6-3a95-4006-bb31-da4fc7942fc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_031_New Note can be added for Client successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1b57fca-f850-49b9-8306-dea2d91ee831</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_039_Document can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcef0bfb-060c-405a-8f8a-e15bf850c09d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_010_New Follow Activity can be added for Client successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>782aa597-1d8c-4c5a-b7d3-13f34e66cae1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_012_Follow Activity of Client can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6e292ad-c8de-4e78-8dc4-902f329c8429</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_015_The closed Activity of Client cannot be editted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cba0cefd-b94d-4d1d-ba2a-e585b5258496</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_011_New Task Activity can be added for Client successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b4beed77-a304-4559-9701-dfe161e4b30b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_008_Contact Client can be set as default successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c90dee78-8146-478c-bf20-200baa7af339</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_046_Address can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>645bd343-c1b3-4924-a024-aeb1bc162c10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_049_Unique Contact cannot be uncheck Default</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60b25790-ffd2-4164-b4cd-a054e4dc4b82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_007_Contact Client can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82930e60-bcf6-4d83-8edd-603365e183a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_009_Contact Client can be removed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6d650bd-e274-4372-8869-a3ef8b323757</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_018_Activity of Client can be searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>879e2dde-5b08-4afd-bdac-3b05516a8c7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_014_Activity of Client can be closed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2fca3175-61fa-4d7d-88fe-7776edd4db14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_044_User can create new Client when clicking button at top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15d3967f-797f-47fe-b765-f1a619d8e20f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_040_Document can be removed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>911fcf60-f101-4694-8761-f47e4fc9716a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_002_Client can searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99118a07-e013-4be6-9513-e8358354b4b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_035_New Document can be added for Client successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a267dd0-bb7a-4260-9a3d-5abc8d073c10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_027_Note of Client can be searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c781699-57a2-48f3-8f08-a815e5d23313</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_005_Client Address cant be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74ad3c5b-d786-414a-ac41-49e2e2c606aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_013_Task Activity of Client can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97aafe2c-c219-4e95-b3db-e5972913ad87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_045_User can create new note when clicking button at top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5d61ca8-9b02-4d3d-9129-c1495d16fe03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_003_New Client can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa9752d9-f8d6-4c06-83bc-5c2977ed9354</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_036_Document of Client can be searched by Name successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77c40341-21e6-458d-928c-c6fc69412b4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_051_Template Email without document can be sent to Client successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd68e6f4-bfc9-439e-8578-5ac6f4862758</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_050_Unique Contact cannot be removed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ad8c772-8ae9-481f-8c49-5dcd535c2bbb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Client/TC_052_Customed Email without document can be sent to Client successfully</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
