<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Transferee</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d1afdb64-d5a0-4f03-990c-5b684a069867</testSuiteGuid>
   <testCaseLink>
      <guid>3205ae63-d463-4376-b591-8ef2bbfdee6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_024_01_Transferee Document can be download</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbc27e9f-ae27-466c-bfae-7543b43e8b29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_056_HS_ Country Mapping_Shown when Vendor_Transf matched and SearchOption is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1935712f-b0de-4918-9dfe-b42837d05456</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_084_HP_Zip Mapping_Vendor Not shown if Vend_Transf_ SearchOpt ZipList not matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d81a3acf-597f-4c9a-8d10-c1d98af0e569</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_017_The closed Activity of transferee cannot be editted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98ea766a-dcae-4510-b23f-dac2f60558ff</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_023_The sent Email is saved as a note after user receive email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d3ec1af-f84f-4756-9f05-7737a849726e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_036_Transferee - TQ_Detail info can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8463361-3abe-4f56-8f0a-c843181b6848</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_070_MG - Zip Mapping - Vendor shown when Transferee Zip match Vendor Zip</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6731408a-6eff-4c3b-9959-b700b03135ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_024_New Document can be added for transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39bd352c-5e44-418a-9168-d5971b6ddb09</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_076_MG - City Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>486b9a51-3316-4907-bfab-bf784889804a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_020_Note of transferee can be searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce698965-d7a8-4bf6-8027-8c66b67ed4d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_097_TQ - State Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2cb4bd5-10b6-4bf9-8486-7024d1c88b18</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_051_HS_State Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdfefdbd-c4c0-46e4-88be-cf04a2c5ea70</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_071_MG_Zip Mapping_Vendor shown when Transf Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89a5cfea-d7e2-4c65-aae9-419c6cfed327</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_079_MG_ Country Mapping_Shown if Vendor_Transf matched and SearchOpt is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b3272ab-75d5-4c0d-8339-94183d336a4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_102_TQ_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>183411a7-1940-406a-9ce2-462befad45b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_018_Activity of transferee can be searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>324cfa71-89d6-4ee3-980e-38816cd420be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_089_HP_ Country Mapping_Shown when Vendor_Transf matched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9e5e62b-8f5c-4ace-b592-4af4dcc44472</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_007_Transferee - DependentInfo can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f251830e-fe70-42f5-82d7-4d4d8f51bd15</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_038_Template Email without document can be sent to Transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1da203d1-197e-4a36-aca0-6b483fb811e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_057_HS_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOption is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4a23eef-31d3-46a9-bb8d-1ce98f0bf27d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_058_HHG_Zip Mapping - Vendor shown when Transferee Zip match Vendor Zip</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2b83937-ac4c-42d1-8203-9ceaace05409</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_004_Transferee - OtherInfo can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17563f35-b625-41f3-b7d1-77865ccdc5ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_080_MG_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOpt is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c2dcd77-a260-43e9-b0fd-9f30ebedc1c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_027_Document of transferee can be removed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>581d75d5-c4f5-4a46-ac3a-cfb78af663bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_099_TQ - City Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95f08ef5-f601-4174-a638-ac026e074a21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_048_HomeSale_Zip Mapping_Vendor shown when Vendor Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>99ec9764-ff8b-4789-8cad-9afacb5ef3d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_014_New Follow Activity can be added for transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82108c83-928c-402e-855a-7aec945289d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_104_TQ_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOpt is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b3fcd5a-19a3-4ddc-af7c-04c58f34d1b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_088_HP - City Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbd6278d-d2c0-4f23-bfc9-ae7c3cc1a601</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_054_HS_ Country Mapping_Shown when Vendor_Transf matched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31345cd3-4a12-4d69-b296-83087456e4cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_086_HP_State Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>837bdde5-343e-425e-ada9-971721dd2eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_096_TQ_Zip Mapping_Vendor Not shown if Vend_Transf_ SearchOpt ZipList not matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2d89d89-4eda-4c85-b23b-0642d861eeeb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_019_New Note can be added for transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b05aeda-32d9-4b68-9eef-792caa1784e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_041_Home Sale - Vendor shown base on Origin - Residence Address matching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>101b5574-b431-4c5f-8b19-0d998c103cde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_003_Transferee - General info can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>663e3358-e10f-472f-8329-76f186585d4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_067_HHG_ Country Mapping_Shown when Vendor_Transf matched and SearchOpt is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48d25c1e-d760-4c4e-8714-348b934fbee1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_029_User can send Template email to transferee with proccess document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc44a4e3-1fcf-4098-8372-8c6f9550ced2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_026_Document of transferee can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b165da03-118a-4bbb-9605-39633c159546</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_047_HomeSale_Zip Mapping_Vendor shown when Transf Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80356a75-8ab7-4f1b-8a0e-213d670a42e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_063_HHG_State Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07955144-7c0d-4dc8-a7f5-3c55d3647833</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_101_TQ_ Country Mapping_Shown when Vendor_Transf matched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67079ecc-1c3c-42d7-8261-4bd8aebe59ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_092_HP_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOpt is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84ea2238-e892-497f-a7ca-bcc3b3ae1d2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_078_MG_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cac2849-348a-4ec5-9619-bf6cc2b59dae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_098_TQ_State Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0800b0ec-b65b-4bb5-be1f-55e30dbea6fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_100_TQ - City Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>279a4b1b-2098-41fe-b9a2-130c82d76d9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_006_Transferee - DependentInfo can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5029cd87-f368-4792-ab06-228c03766f29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_015_Follow Activity of transferee can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>55fb2f20-8a31-42da-a50b-23b793b0651a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_073_MG_Zip Mapping_Vendor Not shown if Vend_Transf_ SearchOpt ZipList not matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41d1da02-a3a4-4a1b-b302-aed3edf50832</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_037_Email Modal - Notice message shown when transf email From has no info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7118c22b-a3d4-40da-b138-6bce1a1cd672</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_021_Note can be updated - Transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f39b956-7ff9-4305-935e-5bde01c71d71</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_042_Household Good_Vendor shown base on Origin_Residence Address match</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da5e182d-2990-4253-aca6-9370e298e88a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_028_User can send Template email to transferee with attached document</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19039761-75b7-466c-8bb3-1ed11471cbf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_059_HHG_Zip Mapping_Vendor shown when Transf Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c885886-d195-41d6-a97d-26533cf6f57d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_074_MG - State Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3be31920-980c-4928-b134-28b496a7fb53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_005_Transferee - AddressInfo can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>35ca8fa6-7f47-4153-b23e-4de5c9440f90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_077_MG_ Country Mapping_Shown when Vendor_Transf matched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>139644ec-012d-4bed-80f9-6b73ecd33f2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_061_HHG_Zip Mapping_Vendor Notshown if Vendor_Transf_ SearchOpt ZipList not match</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63ddd69f-76b4-4274-b3ae-3e58912ac0af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_025_Document of transferee can be searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f25875f6-17ac-4f7d-a9b1-703b61fd482f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_011_Transferee - HHG_Detail info can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2304c32e-1661-49ef-a74f-093c59f00f9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_082_HP_Zip Mapping_Vendor shown when Transf Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4820a726-04a1-4d5a-8942-b2d092cea6b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_072_MG_Zip Mapping_Vendor shown when Vendor Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22751c1c-f6bb-429a-9fe8-5921b345a5ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_083_HP_Zip Mapping_Vendor shown when Vendor Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>736446c5-fba9-4f56-aac2-89dacba650bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_022_Note can be deleted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb7df73f-f3a1-46df-bab6-20ba630785df</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_033_Transferee - HS_Detail info can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcb510a7-9283-470e-9636-8f70f966b1fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_044_Home Purchase - Vendor shown base on Destination - Office Address matching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77193452-4986-4eb6-8fb0-cb34f202b5b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_076_MG - City Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ba5662d-a611-4c5b-bc0a-b869f2c30230</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_012_Transferee - Custom Field info can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43dc5ef9-97db-401d-a0d5-fde2f3f18471</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_043_Mortgage - Vendor shown base on Destination - Office Address matching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74c93f1f-eac7-4181-8ccc-12a887c5ce06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_050_HomeSale - State Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbdfe56e-bb33-4817-b7f7-9294bc40dc59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_064_HHG - City Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>228e2220-f856-40bc-b20b-ab88f112a914</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_053_HomeSale - City Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>012070ae-920a-46a4-9d9c-ded2b2d6ae77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_040_Customed Email with attachment can be sent to Transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7eae3cfe-8b70-403d-b761-71cfbe739bf8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_049_HS_Zip Mapping_Vendor Not shown if Vendor_Transferee_ SearchOption ZipList not matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f01bb768-2685-409b-958a-7ed54ee5f2e3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_013_New Follow Activity can be added for transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44715b61-b9e3-4bf6-8f1a-e6280db12b64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_060_HHG_Zip Mapping_Vendor shown when Vendor Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13a2af6f-6c55-49f3-8680-3e8b423505fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_075_MG_State Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aaa42880-1e87-40de-860e-d7503179b947</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_087_HP - City Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>261d21cc-d629-47c6-be3f-971a23d1d988</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_095_TQ_Zip Mapping_Vendor shown when Vendor Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12695f7b-7631-4c35-97cf-59c648aae4e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_066_HHG_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOpt is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3cce71f-62b8-4a56-877c-2bb5f20af5af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_062_HHG - State Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f581978-0201-41d0-94f6-7a24aa04c87d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_010_Transferee - Service can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05e8fef2-ec6a-4271-a98c-eb5aeb1346da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_001_New Transferee can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>664bdc89-a068-4a7b-9ecd-8da48485c7a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_094_TQ_Zip Mapping_Vendor shown when Transf Zip match SearchOption Zip List</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ffda44bd-ddb4-4d65-879c-9815ecfb5e7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_002_Transferee can searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d230fb4b-d196-4f3c-b531-094c5c320032</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_085_HP - State Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b48d2a2-b5e9-41f7-9830-a314c471546b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_093_TQ - Zip Mapping - Vendor shown when Transferee Zip match Vendor Zip</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60263473-d67a-497f-97e8-be9840196216</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_052_HomeSale - City Mapping - Shown when Vendor_Transferee matched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f492377-c40e-44a0-a810-9639eee55337</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_091_HP_ Country Mapping_Shown if Vendor_Transf matched and SearchOpt is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff576d94-769d-4577-9473-c5ba8daa908f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_039_Customed Email without document can be sent to Transferee successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94f3fcbe-ec88-47f9-b63f-c0b99c63298a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_090_HP_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6960e67-8621-4dd2-8aa6-bcde0d7712f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_035_Transferee - MG_Detail info can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d953044-cf08-4957-b9a0-b6301f4d5aa2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_016_Task Activity of transferee can be editted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1d41d70-53aa-41fd-862e-a65364e642e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_046_HomeSale_Zip Mapping_Vendor shown when Transferee Zip match Vendor Zip</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0ca8e0f-88a3-46ba-9fc8-b3efaa7864e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_055_HS_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>373326e4-6090-498e-9195-768c65fa82a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_081_HP - Zip Mapping - Vendor shown when Transferee Zip match Vendor Zip</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0854fac0-684f-4cf5-8d29-7c2423ae1d87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_068_HHG_ Country Mapping_UnShown if Vendor_Transf Unmatched and SearchOpt is International</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0acfc06c-9e93-4239-8e6c-c3b9b46fd0e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_045_TQ - Vendor shown base on Destination - Office Address matching</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>210c5936-b7fb-4556-b492-d3586243ec01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_065_HHG - City Mapping - NotShown when Vendor_Transferee Unmatched</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88def77c-0c92-494a-8896-f62715a4c8dd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_065_HHG_ Country Mapping_Shown when Vendor_Transf matched and SearchOption is USA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>228f63bb-49d7-48cc-903d-0111aa8c6ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Transferee/TC_103_TQ_ Country Mapping_Shown if Vendor_Transf matched and SearchOpt is International</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
