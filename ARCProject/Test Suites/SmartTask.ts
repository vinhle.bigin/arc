<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SmartTask</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>22d7b1db-3826-4d2a-86aa-f5d34b3c4856</testSuiteGuid>
   <testCaseLink>
      <guid>36315024-0978-4e0f-a397-1e369af4d2e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_008_Verify creating Smarttask cannot submit without submiting action edition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9ccc5d8-1c7b-437b-82df-f6204f352e3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_009_Verify SmartTask is searched successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aefa69a4-c0db-4788-b8ae-c09552ffdedd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_004_Verify Smarttask condition Email is created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>174cec1b-8945-4896-9b4a-745030cbc460</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_002_Verify Smarttask condition ActivityComplete is created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f78d8cee-106d-4945-8ea4-e92fa60445a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_017_Verify SmartTask with action FieldUpdate_TransfFname work correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87279b28-5c23-498b-a8e9-d2049f15ec60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_006_Verify SmartTask with action FieldUpdate created correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>70f427bf-008e-492b-9a36-60a5b245895b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_060_Verify SmartTask with condition SendEmail work correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54da55f5-0091-4065-91c6-3efd6f0f24a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_005_Verify SmartTask with action SendEmail created correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9d183cf-a61f-4aa7-86ce-bcae2c330cf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_003_Verify Smarttask condition FieldChange is created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea788b68-68ef-48d1-8a75-d4316e5c9697</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_016_Verify SmartTask with action CreateActivity work correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b38d730-6ee8-4d95-9310-0a760e9506b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_061_Verify SmartTask with condition ActivityComplete work correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c34fbb1c-7f67-4052-aa99-680677882db7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_001_Verify Smarttask condition FileCreated is created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a528259c-cec9-4088-b102-4a14d4107423</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_015_Verify SmartTask with action SendEmail work correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a92ae914-aa38-4947-9fc2-8ebca392884c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_007_Verify SmartTask with action CreateActivity created correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8818e478-9173-4c09-8a2a-da5b6950a8b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_064_Verify SmartTask with TriggerType MeetAny work correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2128800c-8b86-4588-b50a-cf688faf8165</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/SmartTask/TC_059_Verify SmartTask with condition Fieldchange work correctly</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
