<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>CustomField</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f668a0f4-f26b-4735-b2c3-7d35bef6aee3</testSuiteGuid>
   <testCaseLink>
      <guid>039524eb-8ba2-4ccc-a639-de4287002545</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_055_Client New Group can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0082144d-4f67-4c32-81e1-68d9b5dcd228</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_016_Client New Custom Field can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebff94a7-e9eb-4adb-a39e-40ce258c23ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_059_Group can be removed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f39b3af-0a1e-4ffa-b5de-61f65a57d876</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_017_Transfe - New Custom Field can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd01a49d-f42e-401b-97d2-47b227979b93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_020_User cannot remove refered custom field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f30516e-dff4-418e-bdb8-a5aead44c89d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_022_User can change from active to inactive status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>651a0243-9e81-469a-8d5a-e0a8293f4fc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_056_Transferee New Group can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5daf4c55-6589-4763-94b8-7024955b8c9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_021_User can editted custom field successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0367647-1992-4ae4-b78b-506b20f791b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_058_Group can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed5aefcf-73ab-4254-ae7a-199a47e3501e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_019_User can remove custom field successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74353948-2f5e-4706-b399-8c0dfa80749a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_057_Vendor New Group can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67b05bb8-62c3-490c-864b-88edee967612</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/CustomField/TC_018_Vendor - New Custom Field can be created successfully</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
