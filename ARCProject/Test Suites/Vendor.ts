<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Vendor</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>1fb15d2b-203a-4a28-92ee-57d6a1c823d4</testSuiteGuid>
   <testCaseLink>
      <guid>33379b25-9ad8-4ba9-ac2c-36cba126b2c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Sandbox/TC_test_ET</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11eec311-86a3-413d-a407-35c75cfeb563</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V000_Logout ARC page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15867538-5d8e-4f07-9f4e-fe63d009a1b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V001_Search vendor by name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5512c847-e755-4a5c-806d-05b416e45809</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V002_Search vendor by contact name</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cca3d7db-878a-45dc-a4d1-2144a0a5cd77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V003_Search vendor by status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dbc1f93-39bc-4e32-b033-475c19e57311</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V004_Search vendor by service type</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56a108ab-4552-4044-836d-87928bbf2270</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V005_Search vendor by zipcode</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>25fbe8a9-e3bc-4eec-a72e-16c459df5cf6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V006_Search vendor by city</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4950eece-5685-4c8d-aed2-67cda1fecf9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V007_Search vendor by address</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>562ffbf7-f27d-40ca-beb2-eb51bb905b08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V008_Search vendor by only main contact</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f18f2d83-a8a4-443e-8604-cae046b0eda8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V009_User can clear all data in Search box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69ff54fe-047a-4728-9665-964b1fbeef25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V010_Create new vendor successfully when clicking on Vendor icon at the top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b665d5d-a5f1-4729-8d1d-2d42538dcf96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V011_Create new vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a315e861-afe5-4847-8338-b69eafe26527</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V012_Validation message when New Vendor is empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cdafa1c-4429-4702-b75b-a12ec761635c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V013_Update General Info vendor detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29c99a51-a390-4b8d-b79b-4a23e6535fb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V014_Update rating in vendor detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0be43e1-4b57-48bb-a8ee-3c4278f96e1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V015_Update search option in Vendor detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36817144-8d6b-4e83-af2f-00f832cbecb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V016_Update status in vendor detail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c3705d9f-4dfb-4d1c-8d6d-4811f6507c58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V020_Validation message when New Contact Vendor is empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9e3fa0c-e0e6-4ba0-afe9-a52f5d8c75ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V021_Create New Contact Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d397b4c-7732-4acb-b077-8af9ba7aa425</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V022_Update Contact Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5eb70409-aa5a-48cf-a20b-2b2dde690e4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V023_Set Contact Vendor as default successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12b0c452-8514-40e1-b352-bd796fddeec6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V024_Delete Contact Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab693aab-cf51-46ef-ab04-70417d0942e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V025_Validation message when New Activity Vendor is empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82eb3eb4-fff9-4f5b-934e-87ff21f23a45</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V026_New Task Activity can be added for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>828cad23-3a63-4f10-8e70-32cb424a09fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V027_New Follow Up Activity can be added for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86d0a1d3-5724-4470-8850-ceec4f82d40f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V028_Task Activity can be eidited for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43f60552-80fd-4240-a054-6b5d277c18e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V029_Follow Up Activity can be eidited for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8197d586-fb8a-4ab7-87a7-08ce01390be5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V030_The Activity of vendor can be closed successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a24edeb1-715e-4e32-a7f7-0864510ebc64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V031_The closed Activity of vendor cannot be editted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9eff892c-dec7-49ee-a53c-f845c74b2c3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V032_The Activity can be searched by date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27cba68f-68f8-4514-85be-15c13a23010e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V033_The Activity can be searched by priority</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a9041c8-866d-43b3-9e9c-351b93b70f3c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V034_The Activity can be searched by status</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dbe2259-ac79-4d94-ab8f-309ff7534cc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V035_The Activity can be deleted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0874187b-04b3-4b31-befd-295ea06048cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V036_User can clear all data in Search box of Activity tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>700b81f4-e2ae-4bd8-bd72-b9c1469c65cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V039_The Activity can be searched by type</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7557d781-ad07-4adc-82f1-f7249e9399fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V040_Validation message when New Note Vendor is empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5cc17e0-c675-4fdf-8da9-e11513e2db9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V041_New Note can be added successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acb9c6a8-d85b-4225-9d5d-3fb0e40fd492</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V042_Note can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d93fab28-910a-4bb1-9fe3-f3ef26345a37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V043_Note can be deleted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c47cf09-8c4e-4002-88cb-fb5613c7773d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V044_Note can be search by date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d103c658-9eb3-4e03-bc70-be3e3f49719e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V045_Note can be search by subject</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b917154-bd82-4e51-b50b-7dcbd84a8c3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V046_Note can be search by content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29d93444-9b04-4633-8b65-8d28bbb787bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V047_New Note can be added successfully when clicking on button at top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2c83a8c-ead0-4a0c-af75-45226494a8ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V048_User can clear all data in Search box of Note tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e2ddb7e-f4d9-4228-8d02-8b2438545937</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V050_Validation message when New Document Vendor is empty</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d77318f3-a6ec-434c-8629-fc240024ad49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V051_New Document can be added for Vendor successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74a7703c-a3fe-4de6-b852-477db7d22c23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V052_Document can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a7bbea2-2863-47bb-ac28-89b82a30263f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V053_Document can be deleted successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b39258b5-2840-421e-b330-c200ac0e068b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V054_Document can be searched by name successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7719437c-1cde-4733-a704-aa0741e88a32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V055_Document can be searched by description successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8526f9af-d395-4d19-90df-2f8ab835254e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V056_User can clear all data in Search box of Document tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aeff51f5-4c5f-4868-8807-fb4677896c07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V060_The History can be searched by date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ec30859-d376-47a1-b3f9-44750251e8da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V061_The History can be searched by type</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4619f76b-22b7-488c-89f3-ac7e70e93227</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V062_The History can be searched by description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b818de2f-467a-45ac-ae8e-d2898287ccd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V063_The History can be searched by user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8025845-080b-462e-b800-c961d9e5f041</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Vendor/TC_V064_User can clear all data in Search box of History tab</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
