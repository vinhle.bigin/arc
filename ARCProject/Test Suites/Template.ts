<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Template</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f79e8d83-c909-476b-8f29-3d53d2d21ec7</testSuiteGuid>
   <testCaseLink>
      <guid>129946e3-99d4-4017-b6ae-ac83fcaf88ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_080_Email Template_Client_Verify MG field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b5caa28-7cfb-447e-9961-af7bfe4cfd24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_012_Address Client_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d4c3ac0-8ffe-4f97-9cb0-7658090c6def</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_002_User can search Email type successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1eeb4ae-db3c-4c68-9c8c-9167b183bf90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_028_Email Template_Verify Transferee General field code value correctly on Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa1bc487-9e6d-4f81-a9a1-1aa49ec4e802</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_033_Email Template_Verify Transf Mortgage FieldCode value correct Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93a437e3-41af-4930-8d70-8628bfd83f99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_079_Email Template_Client_Verify TQ field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0f0c9f0-39ba-4903-8114-fcb5f16bf9bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_036_Email Template_Verify Transf CustomFieldCode value correct Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46cf4756-4b49-4674-9e9b-b0b126b3a5d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_024_User can remove template successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0aa40e31-b5f4-4f88-bed2-05cc335de4b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_016_Email Transferee_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51f3e5f5-84c8-431e-8ad5-ff6319a2b045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_045_Email Template_Verify Transferee HHG code value correctly on Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be3f5e6f-dadf-4bb1-a61a-693a337a0239</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_077_Email Template_Client_Verify HP field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91ad1646-becb-43ef-9ed5-b025a0db4749</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_015_Email Client_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1caa979b-d19e-4804-a9c2-96fc9399d704</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_004_User can search Client Target Record successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>973041d4-db94-4db5-a209-d40d1441e190</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_078_Email Template_Client_Verify HHG field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f4ea54f-5985-4c63-9213-befd74069da8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_095_Email Template_Client_Verify TQ field code show correct Email Modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6fba18d7-eccd-4b5f-975e-5bfc43ecb36c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_013_Address Transferee_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12c5098f-5617-4854-a089-f17efb2c2275</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_014_Address Vendor_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88dadd6b-6e97-4a56-972c-23a0bb78bd4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_044_Email Template_Verify Transf Mortgage FieldCode value correct Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f2063ca-b92b-4e02-9d82-3176ef1fc10b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_081_Email Template_Verify ClientCustomFieldCode value correct Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e179b31-714e-41f4-b38e-87361e49b579</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_017_Email Vendor_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6606b17f-b068-4312-a85e-f68fdbfd239b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_093_Email Template_Client_Verify HP field code show correct Email Modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef214b43-b6ae-4c80-8cd7-948cf01ce76c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_029_Email Template_Verify Transf OtherInfo field code correctly on Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b07bcd92-2bad-43e2-b808-dd91e8a2632e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_005_User can search Transferee Target Record successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d594cc1b-7f94-41e2-bb10-71c68c504fe6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_006_User can search VendorTarget Record successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa61f77f-032a-490e-9804-16fdfbe56dbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_018_Letter Client_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee035b2d-61d2-4247-aaba-19e433ddb0ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_094_Email Template_Client_Verify HHG field code show correct Email Modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38f6b545-90a4-4863-a264-d945b78b0a87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_076_Email Template_Client_Verify HS field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39164ee1-ca4d-47bc-9cf2-a6ad90eb8db1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_074_Email Template_Client_Verify Client Info field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a92a4c0-197e-4b01-bf2f-a6291aff15c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_046_Email Template_Verify Transferee TQ code value correctly on Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93b2b886-88c3-431c-b20d-c3a1e0a2328e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_043_Email Template_Verify Transf HP FieldCode value show correct on Emailmodal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54c61f7c-96d2-4607-8ee7-2863eba25b84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_035_Email Template_Verify Transf TQ_FieldCode value correct Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40b95c1e-b3cd-49a8-b7e4-00974c9a28be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_008_User can search Inactive status successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2a4799f-6187-4d7e-95b9-8b9316e4c914</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_075_Email Template_Client_Verify TransfGenInfo field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebebb40e-cf2d-4eae-b0ce-86a7bbe27a84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_021_Template can be updated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93bb2111-5a2e-475e-8b79-17224e0c2b53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_090_Email Template_Client_Verify Client Info field code correct Email Modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c063dbfb-feae-4797-9a50-7a01dd8ba074</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_030_Email Template_Verify Transf Addr field code correctly on Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1fe54ac-79dc-42ee-a5b3-6c301f71dfb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_025_User can process docx document in template</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7006ac92-8d02-4800-98d5-a433292b0c01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_026_User can process pdf document in template</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c8a7472-8648-4526-ad29-0be568e7a71f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_003_User can search Letter type successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c965f93-093f-49de-b6ef-5b50f177b882</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_096_Email Template_Client_Verify MG field code show correct Email Modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f1c3db7-466b-4f9f-8ae1-7f9f67a6ad01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_034_Email Template_Verify Transf HHG_FieldCode value correct Email modal Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13f06f6b-dee2-4a81-9dd9-d5e40c70a35a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_031_Email Template_Verify Transf HS FieldCode value show correct on EmailmodalContent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c35dbb19-0e47-4fb4-aac3-b05e79ee64c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_047_Email Template_Verify Transf CustomFieldCode value correct Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f97054cf-6659-463a-a784-92f627eac354</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_009_User can clear all data in searchbox successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f7854ff-7a76-4d85-9161-1efd20dbbc21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_070_Email Template_Transf_Verify ClientInfo field code show correct Email Modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09680d53-d417-46b1-91ec-46ac4bab8caa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_027_User can attach document in template</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31bdf150-12b9-432f-87bc-4de2c229e7c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_097_Email Template_Verify Client CustomFieldCode value correct Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d606f66a-f8cb-4b26-8a9b-d449f59ff5c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_020_Letter Vendor_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2ce3eee-ccbb-458b-b423-4435e2e65888</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_038_Email Template_Verify Transf OtherInfo field code correctly on Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48964595-bf6a-4b4c-8b4f-64c95e1fd50b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_022_User can inactive template successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a504e2f-e41d-4f6d-aa72-3ad181360a6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_039_Email Template_Verify Transf Addr field code correctly on Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04a166a4-9800-4203-bc03-52db02fde17a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_001_User can search Address Lable type successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3e2e24c-d269-4127-862c-d48a63e87950</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_037_Email Template_Verify Transferee General field code value correctly on Email modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfb7e2be-de50-4ff6-ab30-6d58d0e4ec79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_032_Email Template_Verify Transf HP FieldCode value show correct on EmailmodalContent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1f4372d-8b88-48af-922f-f03163c8b6bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_091_Email Template_Client_Verify TransfGenInfo field code correct Email Modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52cc6fb7-8b11-4885-a794-a8771585dd07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_019_Letter Transferee_Template can be created successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00376114-741d-4778-9ded-890cb7639b05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_092_Email Template_Client_Verify HS field code show correct Email Modal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06880880-9046-4d79-b42b-33b2e6f27f16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_023_User can cancel updating template successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c29f0c7-fd4a-46e1-875a-23bb4dd91f4e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_087_Email Template_Client_Verify TQ field code show correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ad7c8b2a-35a5-4d3b-a06f-080529d1fea8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_104_Email Template_Client_Verify MG field code show correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa458e76-e366-44fb-9e49-8d578ec61968</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_065_Email Template_Verify Transf HP FieldCode value show correct on Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>968aa61a-fada-42ed-a31d-bf7b95f83569</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_057_Email Template_Verify Transf TQ_FieldCode value correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bb75fb1-472f-42b2-aa4d-d0a3bf9c2bf1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_089_Email Template_Verify ClientCustomFieldCode value correct Received Email content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc593332-1ffd-4ba0-b730-d806ea43d4eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_084_Email Template_Client_Verify HS field code show correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>134a1bf7-a112-4da0-981f-c6ee0e8ee899</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_069_Email Template_Verify Transf CustomFieldCode value correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acff1090-e462-45cb-8265-b5551014adb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_083_Email Template_Transf_Verify TransfGenInfo field code correct ReceivedEmail Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41af5e4b-44b6-4d98-bb59-d5e7b7ef007b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_067_Email Template_Verify Transf HHG_FieldCode value correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>684bbc74-37f8-4759-9f2f-7121f2e0f75f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_050_Email Template_Verify Transf Addr field code correctly on Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47804d05-f797-468f-84b6-58da8f5ad07e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_042_Email Template_Verify Transf HS FieldCode value show correct on Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6273f0f-1bba-4418-884b-e10945270105</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_088_Email Template_Client_Verify MG field code show correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c5fe942-f5cb-425f-8416-2a1f07399fa5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_060_Email Template_Verify Transf OtherInfo field code correctly on Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d295474c-ed9d-4f2a-a7b7-4c73017adb5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_055_Email Template_Verify Transf Mortgage FieldCode value correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d11fbaca-796a-41ec-9f2c-a8f77283ad35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_082_Email Template_Client_Verify ClientInfo field code correct Received Email Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f47e1a16-db91-4a28-aaea-1ed3f84d38db</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_056_Email Template_Verify Transf HHG_FieldCode value correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5d3e08c-ad65-49c0-b9c6-3ad09edd384c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_058_Email Template_Verify Transf CustomFieldCode value correct Received Email Content</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>558337b4-f263-4920-9f67-677f0d4cf103</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_049_Email Template_Verify Transf OtherInfo field code correctly on Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46e1fc07-0fc4-4757-b85f-95ba7a5b9830</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_085_Email Template_Client_Verify HP field code show correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a1fb55d-0f46-4140-8996-dd505e4cfff5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_100_Email Template_Client_Verify HS field code show correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be713d3b-9165-478d-9b0e-b317351d04a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_099_Email Template_Transf_Verify TransfGenInfo field code correct ReceivedEmail File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91333385-2ad9-457b-b4b8-aa9f45281335</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_086_Email Template_Client_Verify HHG field code show correct Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d7f36de-8c46-4af2-8016-dc33cd9235a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_048_Email Template_Verify Transf General field code value correct on Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>267c4071-006e-4891-b61c-e0bd30108114</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_061_Email Template_Verify Transf Addr field code correctly on Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e12a4a67-38bf-4045-bc9f-54ef1bdc2e8a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_053_Email Template_Verify Transf HS FieldCode value correct on Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7629fc8-dbff-4991-8549-47d8ac470283</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_101_Email Template_Client_Verify HP field code show correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aba79dc2-c377-4949-b298-c007cb2192d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_102_Email Template_Client_Verify HHG field code show correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b785f0b4-da0a-4818-9fcd-8aebdcae6e59</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_054_Email Template_Verify Transf HP FieldCode value correct on Received Email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>585ecaf3-ec46-4e4b-bbb6-b60c45bf8feb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_068_Email Template_Verify Transf TQ_FieldCode value correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fdb134b-893e-48ba-abe5-d90b6cc2754b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_059_Email Template_Verify Transf General field code value correct on Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43b1f976-6c58-47e4-b498-b44bd587ab5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_103_Email Template_Client_Verify TQ field code show correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c41912ff-6090-4076-9b48-217767eb3733</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_071_Email Template_Transf_Verify ClientInfo field code show correct ReceivedEmail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc39dd05-ad4e-4bb8-a057-43d8b0255c0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_098_Email Template_Client_Verify ClientInfo field code correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31ec875c-d0f5-4cca-97e9-dacdcb4cbf05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_105_Email Template_Verify ClientCustomFieldCode value correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2f0fd638-1a18-48bf-8d15-95ed86b7d3d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/Email/TC_066_Email Template_Verify Transf Mortgage FieldCode value correct Received Email File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e4e6b52-c124-4839-95e8-0bd1096c86d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_042_Email Template_Verify Transf HS FieldCode value show correct on Emailmodal File</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1182cf0c-19c6-40b1-bf1d-45f6a53547e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ARC Portal/Template/TC_007_User can search Active status successfully</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
